﻿namespace Elibre.AutoStructureDesign.Dialogs
{
    partial class SingleTrussViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_TrussType = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_SpanLengthRange = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbl_NOfMembers = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox.Location = new System.Drawing.Point(272, 54);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(347, 170);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Tai Le", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(35, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Truss Type";
            // 
            // lbl_TrussType
            // 
            this.lbl_TrussType.AutoSize = true;
            this.lbl_TrussType.Font = new System.Drawing.Font("Microsoft Tai Le", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_TrussType.Location = new System.Drawing.Point(35, 75);
            this.lbl_TrussType.Name = "lbl_TrussType";
            this.lbl_TrussType.Size = new System.Drawing.Size(49, 18);
            this.lbl_TrussType.TabIndex = 2;
            this.lbl_TrussType.Text = "label2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Tai Le", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(35, 177);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Span Length Range";
            // 
            // lbl_SpanLengthRange
            // 
            this.lbl_SpanLengthRange.AutoSize = true;
            this.lbl_SpanLengthRange.Font = new System.Drawing.Font("Microsoft Tai Le", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_SpanLengthRange.Location = new System.Drawing.Point(35, 206);
            this.lbl_SpanLengthRange.Name = "lbl_SpanLengthRange";
            this.lbl_SpanLengthRange.Size = new System.Drawing.Size(49, 18);
            this.lbl_SpanLengthRange.TabIndex = 4;
            this.lbl_SpanLengthRange.Text = "label4";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Tai Le", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(35, 120);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(136, 18);
            this.label6.TabIndex = 5;
            this.label6.Text = "Number of Members";
            // 
            // lbl_NOfMembers
            // 
            this.lbl_NOfMembers.AutoSize = true;
            this.lbl_NOfMembers.Font = new System.Drawing.Font("Microsoft Tai Le", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_NOfMembers.Location = new System.Drawing.Point(35, 138);
            this.lbl_NOfMembers.Name = "lbl_NOfMembers";
            this.lbl_NOfMembers.Size = new System.Drawing.Size(49, 18);
            this.lbl_NOfMembers.TabIndex = 6;
            this.lbl_NOfMembers.Text = "label6";
            // 
            // SingleTrussViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 267);
            this.Controls.Add(this.lbl_NOfMembers);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbl_SpanLengthRange);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbl_TrussType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SingleTrussViewer";
            this.ShowIcon = false;
            this.Text = "SingleTrussViewer";
            this.Load += new System.EventHandler(this.SingleTrussViewer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_TrussType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_SpanLengthRange;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbl_NOfMembers;
    }
}