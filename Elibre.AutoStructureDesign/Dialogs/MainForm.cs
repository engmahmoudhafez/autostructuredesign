﻿using Elibre.AutoStructureDesign.Controls;
using Elibre.AutoStructureDesign.Extensions;
using Elibre.AutoStructureDesign.Models;
using Elibre.CAD.StitcherApi.Algorithms.RoofAlgorithms;
using Elibre.CAD.StitcherApi.Models.Stitching;
using Elibre.Geometry2D;
using Elibre.Net.RoofEngine;
using Elibre.Stitcher.Models;
using Elibre.Stitcher.Models.Stitching;

using StitcherNewUIApp.Controls.Epaper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Elibre.AutoStructureDesign.Dialogs
{
    public partial class MainForm : Form
    {
        #region Properties

        private InputControl _inputControl;
        private SelectTemplateControl _selectTemplateControl;
        private TemplateControl _templateControl;
        private WallsControl _wallsControl;
        private DrawingWallsEpaper _epaperControl;
        private RoofControl _roofControl;
        private AnalysisControl _analysisControl;
        private ReportsControl _reportsControl;
        private DiagramsControl _digramsControl;
        private TakeoffsControl _takeoffsControl;
        private DrawingsControl _drawingsControl;
        private _3DControl _3dControl;

        public bool StructureBuildlingExported { get; set; }
        private UserControl _eFramerControl;
        private bool _useEfreamerControl = false;
        #endregion

        #region Constractor

        public MainForm()
        {
            InitializeComponent();
            StitcherSingleton.GetInstance.Template = GetBuiltInTemplate();
            StructuralExporter.Create(EStitcherSingleton.GetInstance.Building);
            EStitcherSingleton.GetInstance.StructureBuildling = StructuralExporter.Document.Building;
            ShowStartupControl();
        }
        #endregion

        #region Methods

        private StitcherTemplate GetBuiltInTemplate()
        {
            StitcherTemplate temp = null;
            var templateString = Elibre.Stitcher.Models.Properties.Resources.BuiltInStitcherTemplate;
            using (TextReader reader = new StringReader(templateString))
            {
                var serializer = new XmlSerializer(typeof(StitcherTemplate), new Type[] { typeof(LevelInfo), typeof(FloorLevel) });
                temp = (StitcherTemplate)serializer.Deserialize(reader);
            }
            temp.SetColors();
            return temp;
        }
        private void UpdateOpeningsShownLines()
        {
            foreach (var lev in EStitcherSingleton.GetInstance.Building.Levels)
            {
                if (lev == null)
                    continue;
                foreach (var wall in lev.FloorPlan.Walls)
                {
                    if (wall.Openings == null)
                        continue;
                    foreach (var open in wall.Openings)
                    {
                        open.ExteriorDirection = open.SingleLine2D.GetUnitVector().GetNormalVec();
                        open.UpdateShownLines();
                    }
                }
            }
        }

        private void ClearPanelControls()
        {
            mainPanel.Controls.Clear();
        }
        private void ShowCurrentControl(UserControl control)
        {
            mainPanel.Controls.Clear();
            control.Parent = mainPanel;
            control.Dock = DockStyle.Fill;
        }
        private void Show_Hide_Buttons(bool show)
        {
            panel_Highlight.Visible = show;
            radioButton_Input.Visible = show;
            radioButton_Walls.Visible = show;
            radioButton_Roof.Visible = show;
            radioButton_Digrams.Visible = show;
            radioButton_Reports.Visible = show;
            radioButton_Digrams.Visible = show;
            radioButton_Takeoffs.Visible = show;
            radioButton_Drawings.Visible = show;
            radioButton_3D.Visible = show;
        }
        private void FillRoof()
        {
            EStitcherSingleton.GetInstance.Building.RoofLines = new List<List<CAD.StitcherApi.Models.RoofSlice>>();
            EStitcherSingleton.GetInstance.Building.RoofPolygons = new List<StitcherRoofPolygon>();
            EStitcherSingleton.GetInstance.Building.UpdateWallsCurrentProfile();
            try
            {
                var slices = EStitcherSingleton.GetInstance.Building.Levels.Last(o => o.FloorPlan.Walls.Any()).FloorPlan.Walls.SelectMany(o => o.GetPerimeterCoreSlices()).CreateCopy();
                slices.Slice();
                var floorsPolysLists = slices.GetPolygons().GetOuterPolygons();
                EStitcherSingleton.GetInstance.Building.RoofLines = StitcherRoofGenerator.GenerateRoof(EStitcherSingleton.GetInstance.Building, new List<List<SlicePolygon2D>>() { floorsPolysLists });
                EStitcherSingleton.GetInstance.TrussContainer = RoofTrussDetector.GenerateRoofTruss_EhabAlgorithm(EStitcherSingleton.GetInstance.Building);

            }
            catch (Exception)
            {
                EStitcherSingleton.GetInstance.Building.RoofLines = new List<List<CAD.StitcherApi.Models.RoofSlice>>();
                EStitcherSingleton.GetInstance.Building.RoofPolygons = new List<StitcherRoofPolygon>();
                EStitcherSingleton.GetInstance.TrussContainer = null;
            }
        }

        private void OpenFromFile()
        {
            var xmlFile = @"C:\Users\Madonna\Desktop\RevitModel.xml";
            EStitcherSingleton.GetInstance.Building = StitcherBuilding.Deserialize(xmlFile);
            EStitcherSingleton.GetInstance.Building.Template = EStitcherSingleton.GetInstance.Building.Template;
            StitcherSingleton.GetInstance.Template = EStitcherSingleton.GetInstance.Building.Template;
            UpdateOpeningsShownLines();
            ShowEpaperControl();
            EStitcherSingleton.GetInstance.IsStitcherModel = true;
        }

        #endregion

        #region Show steps
        private void ShowStartupControl()
        {
            ClearPanelControls();
            Show_Hide_Buttons(false);
            if (_inputControl != null)
            {
                ShowCurrentControl(_inputControl);
                return;
            }
            _inputControl = new InputControl();
            _inputControl.Parent = mainPanel;
            _inputControl.Dock = DockStyle.Fill;
            (_inputControl as InputControl).OnOpenTemplate += (s, e) =>
                 {
                     ShowSelectTemplateControl();
                 };
        }
        private void ShowSelectTemplateControl()
        {
            ClearPanelControls();
            Show_Hide_Buttons(false);
            if (_selectTemplateControl != null)
            {
                ShowCurrentControl(_selectTemplateControl);
                return;
            }
            _selectTemplateControl = new SelectTemplateControl();
            _selectTemplateControl.Parent = mainPanel;
            _selectTemplateControl.Dock = DockStyle.Fill;
            (_selectTemplateControl as SelectTemplateControl).OnStart += (s, e) =>
            {
                ShowEpaperControl();
            };
            (_selectTemplateControl as SelectTemplateControl).OnEditTemplate += (s, e) =>
            {
                ShowTemplateControl();
            };
        }
        private void ShowTemplateControl()
        {
            ClearPanelControls();
            Show_Hide_Buttons(false);
            if (_templateControl != null)
            {
                ShowCurrentControl(_templateControl);
                _templateControl.LoadForm();
                return;
            }
            _templateControl = new TemplateControl();
            _templateControl.OnOkClicked += (s, e) =>
             {
                 ShowEpaperControl();
             };
            _templateControl.Parent = mainPanel;
            _templateControl.Dock = DockStyle.Fill;

        }
        private void ShowEpaperControl()
        {
            ClearPanelControls();
            Show_Hide_Buttons(true);
            if (_epaperControl != null && EStitcherSingleton.GetInstance.Building != null)
            {
                ShowCurrentControl(_epaperControl);
                _epaperControl.UpdateBuilding();
                return;
            }
            _epaperControl = new DrawingWallsEpaper();
            _epaperControl.Parent = mainPanel;
            _epaperControl.Dock = DockStyle.Fill;
            _epaperControl.ToStitcher += OpenStitcher;
            radioButton_Input.Checked = true;
        }
        private void ShowWallsControl()
        {
            ClearPanelControls();
            Show_Hide_Buttons(true);
          
            StructuralExporter.ExportStructureBuilding();

            if (_wallsControl != null)
            {
                ShowCurrentControl(_wallsControl);
                _wallsControl.UpdateState();
                return;
            }

            _wallsControl = new WallsControl();
            _wallsControl.Parent = mainPanel;
            _wallsControl.Dock = DockStyle.Fill;
            radioButton_Walls.Checked = true;
        }
        private void ShowRoofControl()
        {
            ClearPanelControls();
            Show_Hide_Buttons(true);
            if (_roofControl != null)
            {
                ShowCurrentControl(_roofControl);
                _roofControl.UpdateState();
                return;
            }
            _roofControl = new RoofControl();
            _roofControl.Parent = mainPanel;
            _roofControl.Dock = DockStyle.Fill;
            radioButton_Roof.Checked = true;

        }
        private void ShowAnalysisControl()
        {
            ClearPanelControls();
            Show_Hide_Buttons(true);
            if (_useEfreamerControl)
            {
                if (_eFramerControl != null)
                {
                    ShowCurrentControl(_eFramerControl);
                    return;
                }
                //Mariam
                //_eFramerControl = .......
                _eFramerControl = new UserControl();
                _eFramerControl.Parent = mainPanel;
                _eFramerControl.Dock = DockStyle.Fill;
                radioButton_Walls.Checked = true;
            }
            else
            {
                if (_analysisControl != null)
                {
                    ShowCurrentControl(_analysisControl);
                    _analysisControl.UpdateState();
                    return;
                }
                _analysisControl = new AnalysisControl();
                _analysisControl.Parent = mainPanel;
                _analysisControl.Dock = DockStyle.Fill;
                radioButton_Walls.Checked = true;
                Refresh();
            }
        }
        private void ShowReportsControl()
        {
            ClearPanelControls();
            Show_Hide_Buttons(true);
      



            if (_reportsControl != null)
            {
                ShowCurrentControl(_reportsControl);
                _reportsControl.SetButtonStates();
                return;
            }
            _reportsControl = new ReportsControl();
            _reportsControl.Parent = mainPanel;
            _reportsControl.Dock = DockStyle.Fill;
            radioButton_Reports.Checked = true;
        }
        private void ShowDigramsControl()
        {
            ClearPanelControls();
            Show_Hide_Buttons(true);
            if (_digramsControl != null)
            {
                ShowCurrentControl(_digramsControl);
                _digramsControl.ToggleButtons();
                return;
            }
            _digramsControl = new DiagramsControl();
            _digramsControl.Parent = mainPanel;
            _digramsControl.Dock = DockStyle.Fill;
            radioButton_Digrams.Checked = true;
            _digramsControl.ToggleButtons();

        }
        private void ShowTakeOffsControl()
        {
            ClearPanelControls();
            Show_Hide_Buttons(true);
            if (_takeoffsControl != null)
            {
                ShowCurrentControl(_takeoffsControl);
                return;
            }
            _takeoffsControl = new TakeoffsControl();
            _takeoffsControl.Parent = mainPanel;
            _takeoffsControl.Dock = DockStyle.Fill;

        }
        private void ShowDrawingsControl()
        {
            ClearPanelControls();
            Show_Hide_Buttons(true);
          
            if (_drawingsControl != null)
            {
                ShowCurrentControl(_drawingsControl);
                _drawingsControl.UpdateState();
                return;
            }
            _drawingsControl = new DrawingsControl();
            _drawingsControl.Parent = mainPanel;
            _drawingsControl.Dock = DockStyle.Fill;
            radioButton_Drawings.Checked = true;

        }
        private void Show3DControl()
        {
            ClearPanelControls();
            Show_Hide_Buttons(true);
            if (_3dControl != null)
            {
                ShowCurrentControl(_3dControl);
                return;
            }
            _3dControl = new _3DControl();
            _3dControl.Parent = mainPanel;
            _3dControl.Dock = DockStyle.Fill;
            radioButton_3D.Checked = true;
        }

        #endregion
        #region Event Handlers

        private void OnChangeButton(object sender, EventArgs e)
        {
            panel_Highlight.Location = new Point((sender as RadioButton).Location.X, panel_Highlight.Location.Y);
            panel_Highlight.Width = (sender as RadioButton).Width;
            if (!(sender as RadioButton).Checked && (sender as RadioButton).Name.Equals(radioButton_Input.Name))
            {
                StructuralExporter.ResetStructureBuilding();
                FillRoof();
                EStitcherSingleton.GetInstance.Building.FillWallsParts();
                StructuralExporter.ExportStructureBuilding();
            }

            if (!(sender as RadioButton).Checked)
                return;
            if ((sender as RadioButton).Name.Equals(radioButton_Input.Name))
            {
                //StructuralExporter.ResetStructureBuilding();
                ShowEpaperControl();
            }
            else if ((sender as RadioButton).Name.Equals(radioButton_Walls.Name))
            {
                StructuralExporter.ExportStructureBuilding();
                //StructuralExporter.ResetStructureBuilding();

                ShowAnalysisControl();
            }
            else if ((sender as RadioButton).Name.Equals(radioButton_Roof.Name))
            {
                ShowRoofControl();
            }
            else if ((sender as RadioButton).Name.Equals(radioButton_Digrams.Name))
            {
                StructuralExporter.ExportStructureBuilding();
                StructuralExporter.RunAnalysis();
                ShowDigramsControl();
            }
            else if ((sender as RadioButton).Name.Equals(radioButton_Reports.Name))
            {
                StructuralExporter.ExportStructureBuilding();
                StructuralExporter.RunAnalysis();
                ShowReportsControl();
            }
            else if ((sender as RadioButton).Name.Equals(radioButton_Takeoffs.Name))
            {
                ShowTakeOffsControl();
            }
            else if ((sender as RadioButton).Name.Equals(radioButton_Drawings.Name))
            {
                StructuralExporter.ExportStructureBuilding();
                ShowDrawingsControl();
            }
            else if ((sender as RadioButton).Name.Equals(radioButton_3D.Name))
            {
                Show3DControl();
            }
        }
        private void OpenStitcher(object sender,EventArgs args)
        {
            //OpenFromFile();
            //return;

            var exePath = @"E:\Projects\Elibre\GitSourceControl\StitcherApp\Output\Stitcher Protected\Stitcher.exe";
            //var exePath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Stitcher", "Stitcher.exe");

            if (File.Exists(exePath))
            {
                var process = System.Diagnostics.Process.Start(exePath, $"AutoStructureDesign");
                while (!process.HasExited)
                {
                    Application.DoEvents();
                }
                if (process.ExitCode == 143)
                {
                    var directory = Path.Combine(Path.GetTempPath(), "Elibre");
                    if (!Directory.Exists(directory))
                        directory = Directory.CreateDirectory(directory).Name;
                    var xmlFile = Path.Combine(directory, "RevitModel.xml");
                    EStitcherSingleton.GetInstance.Building = StitcherBuilding.Deserialize(xmlFile);
                    EStitcherSingleton.GetInstance.Building.Template = EStitcherSingleton.GetInstance.Building.Template;
                    StitcherSingleton.GetInstance.Template = EStitcherSingleton.GetInstance.Building.Template;
                    UpdateOpeningsShownLines();

                    ShowEpaperControl();
                    EStitcherSingleton.GetInstance.IsStitcherModel = true;
                }
            }
            else
            {
                MessageBox.Show("Cannot locate Stitcher !");
            }

        }

        #endregion
        #region Menu strips

        private void newProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EStitcherSingleton.GetInstance.IsStitcherModel = false;
            EStitcherSingleton.GetInstance.Building = null;
            ShowStartupControl();
        }
        private void existToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = false;
            base.OnClosing(e);
        }
        #endregion

    }
}
