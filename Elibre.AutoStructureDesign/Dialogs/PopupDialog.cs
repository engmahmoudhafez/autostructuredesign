﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Gma.UserActivityMonitor;
using System.Collections.Generic;

namespace Elibre.AutoStructureDesign.Dialogs
{
    public partial class PopupDialog : Form
    {
        #region Properties
        public Control ParentControl { get; set; }
        #endregion

        #region Constructors
        private PopupDialog()
        {
            InitializeComponent();
        }
        public PopupDialog(Control parentControl)
        {
            InitializeComponent();
            this.ParentControl = parentControl;
        }
        #endregion

        #region Overrides
        protected override void OnShown(EventArgs e)
        {
            HookManager.MouseDown += HookManager_MouseDown;
            btnOk.Size = new System.Drawing.Size(0, 0);
            btnOk.TabStop = false;

            base.OnShown(e);
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
                e.Handled = true;
                return;
            }

            base.OnKeyDown(e);
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            var pen = new Pen(Color.Gainsboro, 2);
            var rect = this.ClientRectangle;
            e.Graphics.DrawRectangle(pen, rect);
        }
        #endregion

        #region Methods
        public virtual bool IsValid()
        {
            return true;
        }
        protected virtual bool CommitChanges()
        {
            return true;
        }
        public virtual void DoOk()
        {
            if (!IsValid())
                return;

            if (!CommitChanges())
                return;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
        public virtual void DoCancel()
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }
        public List<Control> GetAllControls()
        {
            var returned = new List<Control>();
            foreach (var c in Controls)
            {
                List<Control> allControls = new List<Control>();
                GetContainedControls(c as Control, ref allControls);
                returned.AddRange(allControls);
            }
            return returned;
        }
        private void GetContainedControls(Control c,ref List<Control>containesControls)
        {
            if(c.Controls.Count>0)
            {
                for (int i = 0; i < c.Controls.Count; i++)
                {
                    GetContainedControls(c.Controls[i] as Control,ref containesControls);
                }
            }
            else
            {
                containesControls.Add(c as Control);
            }
        }
        #endregion

        #region Hooking
        private void HookManager_MouseDown(object sender, MouseEventArgs e)
        {
            return;
            if (Form.ActiveForm != this)
                return;

            if (this.Bounds.Contains(e.Location))
                return;

            var p = e.Location;
            var tl = this.ParentControl.PointToScreen(this.ParentControl.Location);
            var br = this.ParentControl.PointToScreen(new Point(this.ParentControl.Right, this.ParentControl.Bottom));

            if (p.X < tl.X || p.Y < tl.Y || p.X > br.X || p.Y > br.Y)
                return;

            MouseEventExtArgs args = e as MouseEventExtArgs;
            if (args != null)
                args.Handled = true;

            var timer = new Timer();
            timer.Tick += (s, a) =>
            {
                timer.Stop();
                btnOk_Click(sender, EventArgs.Empty);
            };

            timer.Interval = 100;
            timer.Start();
        }
        private void PopupDialog_FormClosed(object sender, FormClosedEventArgs e)
        {
            HookManager.MouseDown -= HookManager_MouseDown;
        }
        #endregion

        #region Events Handlers
        private void btnOk_Click(object sender, EventArgs e)
        {
            DoOk();
        }
        #endregion
    }
}
