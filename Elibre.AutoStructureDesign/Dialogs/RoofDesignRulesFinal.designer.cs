﻿using Elibre.AutoStructureDesign.Properties;
using Elibre.CAD.StitcherApi;
using Elibre.Ui;

namespace Elibre.AutoStructureDesign.Dialogs
{
    partial class RoofDesignRulesFinal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoofDesignRulesFinal));
            this.RoofLevel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BearingHeightTextBox = new Elibre.CAD.StitcherApi.SetLengthTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.HeelHeightTextBox = new Elibre.CAD.StitcherApi.SetLengthTextBox();
            this.OverHangTextBox = new Elibre.CAD.StitcherApi.SetLengthTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.RoofPitchTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.heelFromZeroLabel = new System.Windows.Forms.Label();
            this.fasciaHeightLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.LevelComboBox = new System.Windows.Forms.ComboBox();
            this.locksPanel = new System.Windows.Forms.Panel();
            this.ECheckBox = new System.Windows.Forms.CheckBox();
            this.DCheckBox = new System.Windows.Forms.CheckBox();
            this.CCheckBox = new System.Windows.Forms.CheckBox();
            this.BCheckBox = new System.Windows.Forms.CheckBox();
            this.maintainPanel = new System.Windows.Forms.Panel();
            this.maintainHHRadioButton = new System.Windows.Forms.RadioButton();
            this.maintainTopFasciaRadioButton = new System.Windows.Forms.RadioButton();
            this.ApplyBtn = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.locksPanel.SuspendLayout();
            this.maintainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnOk.Size = new System.Drawing.Size(0, 0);
            this.btnOk.TabStop = false;
            // 
            // RoofLevel
            // 
            this.RoofLevel.AutoSize = true;
            this.RoofLevel.Location = new System.Drawing.Point(10, 20);
            this.RoofLevel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.RoofLevel.Name = "RoofLevel";
            this.RoofLevel.Size = new System.Drawing.Size(89, 17);
            this.RoofLevel.TabIndex = 1;
            this.RoofLevel.Text = "A - RoofLevel";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 52);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "B - Bearing Height";
            // 
            // BearingHeightTextBox
            // 
            this.BearingHeightTextBox.Location = new System.Drawing.Point(144, 48);
            this.BearingHeightTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BearingHeightTextBox.Name = "BearingHeightTextBox";
            this.BearingHeightTextBox.ReadOnly = true;
            this.BearingHeightTextBox.Size = new System.Drawing.Size(134, 24);
            this.BearingHeightTextBox.TabIndex = 4;
            this.BearingHeightTextBox.Click += new System.EventHandler(this.TextBoxesClick);
            this.BearingHeightTextBox.TextChanged += new System.EventHandler(this.HeelHeightTextBox_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 85);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 17);
            this.label5.TabIndex = 16;
            this.label5.Text = "C - Heel Height";
            // 
            // HeelHeightTextBox
            // 
            this.HeelHeightTextBox.Location = new System.Drawing.Point(144, 80);
            this.HeelHeightTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.HeelHeightTextBox.Name = "HeelHeightTextBox";
            this.HeelHeightTextBox.ReadOnly = true;
            this.HeelHeightTextBox.Size = new System.Drawing.Size(134, 24);
            this.HeelHeightTextBox.TabIndex = 14;
            this.HeelHeightTextBox.Click += new System.EventHandler(this.TextBoxesClick);
            this.HeelHeightTextBox.TextChanged += new System.EventHandler(this.HeelHeightTextBox_TextChanged);
            // 
            // OverHangTextBox
            // 
            this.OverHangTextBox.Location = new System.Drawing.Point(144, 112);
            this.OverHangTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OverHangTextBox.Name = "OverHangTextBox";
            this.OverHangTextBox.ReadOnly = true;
            this.OverHangTextBox.Size = new System.Drawing.Size(134, 24);
            this.OverHangTextBox.TabIndex = 15;
            this.OverHangTextBox.Click += new System.EventHandler(this.TextBoxesClick);
            this.OverHangTextBox.TextChanged += new System.EventHandler(this.OverHangTextBox_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 116);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "D - OverHang";
            // 
            // RoofPitchTextBox
            // 
            this.RoofPitchTextBox.Location = new System.Drawing.Point(144, 144);
            this.RoofPitchTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.RoofPitchTextBox.Name = "RoofPitchTextBox";
            this.RoofPitchTextBox.Size = new System.Drawing.Size(134, 24);
            this.RoofPitchTextBox.TabIndex = 21;
            this.RoofPitchTextBox.Click += new System.EventHandler(this.TextBoxesClick);
            this.RoofPitchTextBox.TextChanged += new System.EventHandler(this.RoofPitchTextBox_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 148);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 17);
            this.label10.TabIndex = 18;
            this.label10.Text = "E - Roof Pitch";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage =Resources._22;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.heelFromZeroLabel);
            this.panel1.Controls.Add(this.fasciaHeightLabel);
            this.panel1.Location = new System.Drawing.Point(350, 15);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(341, 207);
            this.panel1.TabIndex = 26;
            // 
            // heelFromZeroLabel
            // 
            this.heelFromZeroLabel.AutoSize = true;
            this.heelFromZeroLabel.Location = new System.Drawing.Point(4, 35);
            this.heelFromZeroLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.heelFromZeroLabel.Name = "heelFromZeroLabel";
            this.heelFromZeroLabel.Size = new System.Drawing.Size(80, 17);
            this.heelFromZeroLabel.TabIndex = 41;
            this.heelFromZeroLabel.Text = "C from zero";
            this.heelFromZeroLabel.Visible = false;
            // 
            // fasciaHeightLabel
            // 
            this.fasciaHeightLabel.AutoSize = true;
            this.fasciaHeightLabel.Location = new System.Drawing.Point(4, 9);
            this.fasciaHeightLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.fasciaHeightLabel.Name = "fasciaHeightLabel";
            this.fasciaHeightLabel.Size = new System.Drawing.Size(78, 17);
            this.fasciaHeightLabel.TabIndex = 40;
            this.fasciaHeightLabel.Text = "F from zero";
            this.fasciaHeightLabel.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(467, 230);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 33);
            this.button1.TabIndex = 24;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OkButton1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(601, 230);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 33);
            this.button2.TabIndex = 25;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.CloseBbutton2_Click);
            // 
            // LevelComboBox
            // 
            this.LevelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.LevelComboBox.FormattingEnabled = true;
            this.LevelComboBox.Location = new System.Drawing.Point(144, 15);
            this.LevelComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LevelComboBox.Name = "LevelComboBox";
            this.LevelComboBox.Size = new System.Drawing.Size(134, 24);
            this.LevelComboBox.TabIndex = 27;
            this.LevelComboBox.SelectedIndexChanged += new System.EventHandler(this.LevelComboBox_SelectedIndexChanged);
            // 
            // locksPanel
            // 
            this.locksPanel.Controls.Add(this.ECheckBox);
            this.locksPanel.Controls.Add(this.DCheckBox);
            this.locksPanel.Controls.Add(this.CCheckBox);
            this.locksPanel.Controls.Add(this.BCheckBox);
            this.locksPanel.Location = new System.Drawing.Point(285, 41);
            this.locksPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.locksPanel.Name = "locksPanel";
            this.locksPanel.Size = new System.Drawing.Size(40, 139);
            this.locksPanel.TabIndex = 34;
            // 
            // ECheckBox
            // 
            this.ECheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.ECheckBox.AutoSize = true;
            this.ECheckBox.BackColor = System.Drawing.Color.Transparent;
            this.ECheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ECheckBox.Checked = true;
            this.ECheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ECheckBox.FlatAppearance.BorderSize = 0;
            this.ECheckBox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ECheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ECheckBox.Image =Resources.openLock;
            this.ECheckBox.Location = new System.Drawing.Point(7, 101);
            this.ECheckBox.Margin = new System.Windows.Forms.Padding(0);
            this.ECheckBox.Name = "ECheckBox";
            this.ECheckBox.Size = new System.Drawing.Size(22, 22);
            this.ECheckBox.TabIndex = 35;
            this.ECheckBox.UseVisualStyleBackColor = false;
            this.ECheckBox.Click += new System.EventHandler(this.LocksClick);
            this.ECheckBox.MouseLeave += new System.EventHandler(this.MouseLeaveLocks);
            this.ECheckBox.MouseHover += new System.EventHandler(this.MouseHoverLocks);
            // 
            // DCheckBox
            // 
            this.DCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.DCheckBox.AutoSize = true;
            this.DCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.DCheckBox.FlatAppearance.BorderSize = 0;
            this.DCheckBox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.DCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DCheckBox.Image = Resources.closeLock;
            this.DCheckBox.Location = new System.Drawing.Point(7, 69);
            this.DCheckBox.Margin = new System.Windows.Forms.Padding(0);
            this.DCheckBox.Name = "DCheckBox";
            this.DCheckBox.Size = new System.Drawing.Size(22, 22);
            this.DCheckBox.TabIndex = 34;
            this.DCheckBox.UseVisualStyleBackColor = false;
            this.DCheckBox.Click += new System.EventHandler(this.LocksClick);
            this.DCheckBox.MouseLeave += new System.EventHandler(this.MouseLeaveLocks);
            this.DCheckBox.MouseHover += new System.EventHandler(this.MouseHoverLocks);
            // 
            // CCheckBox
            // 
            this.CCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.CCheckBox.AutoSize = true;
            this.CCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.CCheckBox.FlatAppearance.BorderSize = 0;
            this.CCheckBox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.CCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CCheckBox.Image = Resources.closeLock;
            this.CCheckBox.Location = new System.Drawing.Point(7, 37);
            this.CCheckBox.Margin = new System.Windows.Forms.Padding(0);
            this.CCheckBox.Name = "CCheckBox";
            this.CCheckBox.Size = new System.Drawing.Size(22, 22);
            this.CCheckBox.TabIndex = 33;
            this.CCheckBox.UseVisualStyleBackColor = false;
            this.CCheckBox.Click += new System.EventHandler(this.LocksClick);
            this.CCheckBox.MouseLeave += new System.EventHandler(this.MouseLeaveLocks);
            this.CCheckBox.MouseHover += new System.EventHandler(this.MouseHoverLocks);
            // 
            // BCheckBox
            // 
            this.BCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.BCheckBox.AutoSize = true;
            this.BCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.BCheckBox.FlatAppearance.BorderSize = 0;
            this.BCheckBox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.BCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BCheckBox.Image = Resources.closeLock;
            this.BCheckBox.Location = new System.Drawing.Point(7, 5);
            this.BCheckBox.Margin = new System.Windows.Forms.Padding(0);
            this.BCheckBox.Name = "BCheckBox";
            this.BCheckBox.Size = new System.Drawing.Size(22, 22);
            this.BCheckBox.TabIndex = 32;
            this.BCheckBox.UseVisualStyleBackColor = false;
            this.BCheckBox.Click += new System.EventHandler(this.LocksClick);
            this.BCheckBox.MouseLeave += new System.EventHandler(this.MouseLeaveLocks);
            this.BCheckBox.MouseHover += new System.EventHandler(this.MouseHoverLocks);
            // 
            // maintainPanel
            // 
            this.maintainPanel.Controls.Add(this.maintainHHRadioButton);
            this.maintainPanel.Controls.Add(this.maintainTopFasciaRadioButton);
            this.maintainPanel.Location = new System.Drawing.Point(27, 187);
            this.maintainPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.maintainPanel.Name = "maintainPanel";
            this.maintainPanel.Size = new System.Drawing.Size(251, 57);
            this.maintainPanel.TabIndex = 35;
            // 
            // maintainHHRadioButton
            // 
            this.maintainHHRadioButton.AutoSize = true;
            this.maintainHHRadioButton.Location = new System.Drawing.Point(4, 32);
            this.maintainHHRadioButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.maintainHHRadioButton.Name = "maintainHHRadioButton";
            this.maintainHHRadioButton.Size = new System.Drawing.Size(173, 21);
            this.maintainHHRadioButton.TabIndex = 35;
            this.maintainHHRadioButton.Text = "Maintain Heal Height (C)";
            this.maintainHHRadioButton.UseVisualStyleBackColor = true;
            this.maintainHHRadioButton.CheckedChanged += new System.EventHandler(this.maintainHHRadioButton_CheckedChanged);
            // 
            // maintainTopFasciaRadioButton
            // 
            this.maintainTopFasciaRadioButton.AutoSize = true;
            this.maintainTopFasciaRadioButton.Checked = true;
            this.maintainTopFasciaRadioButton.Location = new System.Drawing.Point(4, 4);
            this.maintainTopFasciaRadioButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.maintainTopFasciaRadioButton.Name = "maintainTopFasciaRadioButton";
            this.maintainTopFasciaRadioButton.Size = new System.Drawing.Size(241, 21);
            this.maintainTopFasciaRadioButton.TabIndex = 34;
            this.maintainTopFasciaRadioButton.TabStop = true;
            this.maintainTopFasciaRadioButton.Text = "Maintain Top Of Fascia Location (F)";
            this.maintainTopFasciaRadioButton.UseVisualStyleBackColor = true;
            this.maintainTopFasciaRadioButton.CheckedChanged += new System.EventHandler(this.maintainTopFasciaRadioButton_CheckedChanged);
            // 
            // ApplyBtn
            // 
            this.ApplyBtn.Location = new System.Drawing.Point(350, 230);
            this.ApplyBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ApplyBtn.Name = "ApplyBtn";
            this.ApplyBtn.Size = new System.Drawing.Size(100, 33);
            this.ApplyBtn.TabIndex = 36;
            this.ApplyBtn.Text = "Apply to";
            this.ApplyBtn.UseVisualStyleBackColor = true;
            this.ApplyBtn.Click += new System.EventHandler(this.ApplyBtn_Click);
            // 
            // RoofDesignRulesFinal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(708, 275);
            this.Controls.Add(this.ApplyBtn);
            this.Controls.Add(this.maintainPanel);
            this.Controls.Add(this.locksPanel);
            this.Controls.Add(this.LevelComboBox);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.RoofPitchTextBox);
            this.Controls.Add(this.OverHangTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.HeelHeightTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BearingHeightTextBox);
            this.Controls.Add(this.RoofLevel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "RoofDesignRulesFinal";
            this.Padding = new System.Windows.Forms.Padding(7, 7, 2, 7);
            this.ShowIcon = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Roof Design Rules";
            this.Controls.SetChildIndex(this.RoofLevel, 0);
            this.Controls.SetChildIndex(this.BearingHeightTextBox, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.HeelHeightTextBox, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.OverHangTextBox, 0);
            this.Controls.SetChildIndex(this.RoofPitchTextBox, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.Controls.SetChildIndex(this.btnOk, 0);
            this.Controls.SetChildIndex(this.LevelComboBox, 0);
            this.Controls.SetChildIndex(this.locksPanel, 0);
            this.Controls.SetChildIndex(this.maintainPanel, 0);
            this.Controls.SetChildIndex(this.ApplyBtn, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.locksPanel.ResumeLayout(false);
            this.locksPanel.PerformLayout();
            this.maintainPanel.ResumeLayout(false);
            this.maintainPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label RoofLevel;
        private System.Windows.Forms.Label label1;
        private SetLengthTextBox BearingHeightTextBox;
        private System.Windows.Forms.Label label5;
        private SetLengthTextBox HeelHeightTextBox;
        private SetLengthTextBox OverHangTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox RoofPitchTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox LevelComboBox;
        private System.Windows.Forms.Panel locksPanel;
        private System.Windows.Forms.CheckBox ECheckBox;
        private System.Windows.Forms.CheckBox DCheckBox;
        private System.Windows.Forms.CheckBox CCheckBox;
        private System.Windows.Forms.CheckBox BCheckBox;
        private System.Windows.Forms.Panel maintainPanel;
        private System.Windows.Forms.RadioButton maintainHHRadioButton;
        private System.Windows.Forms.RadioButton maintainTopFasciaRadioButton;
        private System.Windows.Forms.Label heelFromZeroLabel;
        private System.Windows.Forms.Label fasciaHeightLabel;
        private System.Windows.Forms.Button ApplyBtn;
    }
}