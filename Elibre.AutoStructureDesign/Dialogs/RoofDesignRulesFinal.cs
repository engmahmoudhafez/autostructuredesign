﻿using Elibre.Geometry2D;
using Elibre.Ui;
using System;
using System.Linq;
using System.Windows.Forms;
using Elibre.CAD.StitcherApi.Models.Stitching;
using Elibre.CAD.StitcherApi;
using Elibre.CAD.StitcherApi.Helpers;
using Elibre.AutoStructureDesign.Properties;

namespace Elibre.AutoStructureDesign.Dialogs
{
    public partial class RoofDesignRulesFinal : PopupDialog
    {
        #region Properties
        private StitcherRoofPolygon SelectedPolygon;
        private StitcherBuilding StitcherBuildingObj;
        public int RoofLevelIndex { get; set; }
        public double OverHang { get; set; }
        public double BearingHeight { get; set; }
        public double HeelHeight { get; set; }
        public double RoofPitch { get; set; }
        public bool RoofPitchChanged { get; set; }

        public double HeelHeighFromZero { get; set; }
        public double FasciaHeightFromZero { get; set; }
        public bool ApplyToMode { get; set; }
        public int ActiveTextBox { get; set; }
        #endregion

        #region Constructor
        public RoofDesignRulesFinal(Control parentControl, StitcherRoofPolygon selectedPolygon, StitcherBuilding stitcherBuilding)
            : base(parentControl)
        {
            InitializeComponent();
            Elibre.Utilities.FontImporter.SetAllControlsFont(Controls);
            this.StitcherBuildingObj = stitcherBuilding;
            this.SelectedPolygon = selectedPolygon;

            this.FillLevelCombo();
            FillTextBoxes_Polygon();
            this.ActiveControl = RoofPitchTextBox;
            ActiveTextBox = 3;

            foreach (var c in Controls)
            {
                if (c is SetLengthTextBox)
                    ((SetLengthTextBox)c).Template = stitcherBuilding.Template;
            }
        }
        public RoofDesignRulesFinal(Control parentControl, double Angle, double overhang, double heelHeight, double bearingHeight, StitcherRoofPolygon selectedPolygon, StitcherBuilding stitcherBuilding) : base(parentControl)
        {
            InitializeComponent();

            this.StitcherBuildingObj = stitcherBuilding;
            this.SelectedPolygon = selectedPolygon;

            this.FillLevelCombo();
            FillTextBoxes_Lines(Angle, overhang, heelHeight, bearingHeight);

            this.ActiveControl = RoofPitchTextBox;
            ActiveTextBox = 3;

            foreach (var c in Controls)
            {
                if (c is SetLengthTextBox)
                    ((SetLengthTextBox)c).Template = stitcherBuilding.Template;
            }
        }

        private void FillTextBoxes_Lines(double angle, double overhang, double heelHeight, double bearingHeight)
        {
            this.BearingHeightTextBox.Text = UnitsHelper.ShowImperialLength(bearingHeight);
            this.OverHangTextBox.Text = UnitsHelper.ShowImperialLength(overhang);
            this.HeelHeightTextBox.Text = UnitsHelper.ShowImperialLength(heelHeight);

            this.RoofPitchTextBox.Text = UnitsHelper.ShowImperialAngle(angle);
        }

        #endregion

        #region Methods
        private void FillLevelCombo()
        {
            foreach (var level in StitcherBuildingObj.Levels)
            {
                LevelComboBox.Items.Add(level.LevelName);
            }
            var selectedLevel = StitcherBuildingObj.Levels[SelectedPolygon.WallsBelong[0].LevelIndex].LevelName;
            if (selectedLevel != "")
            {
                LevelComboBox.SelectedIndex = LevelComboBox.FindStringExact(selectedLevel);
            }
            else
                LevelComboBox.SelectedText = "";
        }
        private void FillTextBoxes_Polygon()
        {
            double heelHeight = 0;
            var overhang = SelectedPolygon.GetOverhang(StitcherBuildingObj, out heelHeight);
            var bearing = SelectedPolygon.GetBearingHeight(StitcherBuildingObj);

            this.BearingHeightTextBox.Text = UnitsHelper.ShowImperialLength(bearing);
            this.OverHangTextBox.Text = UnitsHelper.ShowImperialLength(overhang);
            this.HeelHeightTextBox.Text = UnitsHelper.ShowImperialLength(heelHeight);

            this.RoofPitchTextBox.Text = UnitsHelper.ShowImperialAngle(SelectedPolygon.GetSlopeDegree());

            FasciaHeightFromZero = SelectedPolygon.GetSimplePoints3D().Min(o => o.Z);
            HeelHeighFromZero = heelHeight + bearing + StitcherBuildingObj.Levels[SelectedPolygon.WallsBelong[0].LevelIndex].Elevation;


            fasciaHeightLabel.Text = "F from zero :" + UnitsHelper.ShowImperialLength(FasciaHeightFromZero);
            heelFromZeroLabel.Text = "c from zero :" + UnitsHelper.ShowImperialLength(HeelHeighFromZero);
        }

        private void MouseHoverLocks(object sender, EventArgs e)
        {
            var currentCheckBx = (sender as CheckBox);
            currentCheckBx.Cursor = Cursors.Hand;

            if (currentCheckBx.Image == Resources.Next || currentCheckBx.Image == Resources.beamicon)
                return;
            if (currentCheckBx.Checked)
                currentCheckBx.Image = Resources.unlockIco1;
            else
                currentCheckBx.Image = Resources.lockIco1;

        }
        private void MouseLeaveLocks(object sender, EventArgs e)
        {
            var currentCheckBx = (sender as CheckBox);
            if (currentCheckBx.Checked)
                currentCheckBx.Image = Resources.openLock;
            else
                currentCheckBx.Image = Resources.closeLock;
            Cursor.Current = Cursors.Default;

        }
        private void LocksClick(object sender, EventArgs e)
        {
            var checkBoxClicked = (sender as CheckBox);
            //checkBoxClicked.Checked = !checkBoxClicked.Checked;
            if (checkBoxClicked.Checked)
            {
                if (checkBoxClicked == BCheckBox || checkBoxClicked == CCheckBox)
                {
                    maintainHHRadioButton.Text = "Maintain Overhang Distance";
                }
                else
                {
                    maintainHHRadioButton.Text = "Maintain Heel Height (C)";

                }
                LockAllLocksExcept(checkBoxClicked);
                checkBoxClicked.Image = Resources.openLock;
                #region open mapping text box
                if (checkBoxClicked.Name.Equals(BCheckBox.Name))
                {
                    BearingHeightTextBox.ReadOnly = false;
                    this.ActiveControl = BearingHeightTextBox;
                }
                else
                {
                    BearingHeightTextBox.ReadOnly = true;
                }
                if (checkBoxClicked.Name.Equals(CCheckBox.Name))
                {
                    HeelHeightTextBox.ReadOnly = false;
                    this.ActiveControl = HeelHeightTextBox;
                }
                else
                {
                    HeelHeightTextBox.ReadOnly = true;
                }
                if (checkBoxClicked.Name.Equals(DCheckBox.Name))
                {
                    OverHangTextBox.ReadOnly = false;
                    this.ActiveControl = OverHangTextBox;
                }
                else
                {
                    OverHangTextBox.ReadOnly = true;
                }
                if (checkBoxClicked.Name.Equals(ECheckBox.Name))
                {
                    RoofPitchTextBox.ReadOnly = false;
                    this.ActiveControl = RoofPitchTextBox;
                }
                else
                {
                    RoofPitchTextBox.ReadOnly = true;
                }
                #endregion

            }
            else
            {
                checkBoxClicked.Image = Resources.closeLock;
                HeelHeightTextBox.ReadOnly = true;
                BearingHeightTextBox.ReadOnly = true;
                OverHangTextBox.ReadOnly = true;
                RoofPitchTextBox.ReadOnly = true;
            }
            bool found = false; ;
            foreach (var item in locksPanel.Controls)
            {
                if (item is CheckBox)
                {
                    if ((item as CheckBox).Checked == true)
                    {
                        found = true;
                        break;
                    }
                }
            }
            if (found)
            {
                maintainPanel.Visible = true;
                maintainTopFasciaRadioButton.Checked = true;
            }
            else
            {
                maintainPanel.Visible = false;
                maintainTopFasciaRadioButton.Checked = true;
            }

            //set active textbox value
            if (checkBoxClicked == BCheckBox)
                this.ActiveTextBox = 0;
            else if (checkBoxClicked == CCheckBox)
                this.ActiveTextBox = 1;
            else if (checkBoxClicked == DCheckBox)
                this.ActiveTextBox = 2;
            else if (checkBoxClicked == ECheckBox)
                this.ActiveTextBox = 3;
        }
        private void TextBoxesClick(object sender, EventArgs e)
        {
            return;
            /* Unreachable
            var currentTextBox = (sender as TextBox);
            BearingHeightTextBox.ReadOnly = true;
            HeelHeightTextBox.ReadOnly = true;
            OverHangTextBox.ReadOnly = true;
            RoofPitchTextBox.ReadOnly = true;
            currentTextBox.ReadOnly = false;
            CheckBox currentLock = null;
            if (currentTextBox.Name.Equals(BearingHeightTextBox.Name))
            {
                currentLock = BCheckBox;
            }
            else if (currentTextBox.Name.Equals(HeelHeightTextBox.Name))
            {
                currentLock = CCheckBox;
            }
            else if (currentTextBox.Name.Equals(OverHangTextBox.Name))
            {
                currentLock = DCheckBox;
            }
            else if (currentTextBox.Name.Equals(RoofPitchTextBox.Name))
            {
                currentLock = ECheckBox;
            }
            if (currentLock != null)
            {
                LockAllLocksExcept(currentLock);
                currentLock.Checked = true;
                currentLock.Image = Resources.openLock;
                maintainTopFasciaRadioButton.Checked = true;
                maintainPanel.Visible = true;
            }
            */
        }
        private void LockAllLocksExcept(CheckBox exceptLock)
        {
            foreach (var item in locksPanel.Controls)
            {
                if (item is CheckBox)
                {
                    var currentCheckBox = item as CheckBox;
                    if (currentCheckBox != exceptLock)
                    {
                        currentCheckBox.Checked = false;
                        currentCheckBox.Image = Resources.closeLock;

                    }
                }
            }
        }


        public double[,] MatMul(double[,] A, double[,] B)
        {
            try
            {
                int nRow1 = A.GetUpperBound(0) + 1;
                int nCol1 = A.GetUpperBound(1) + 1;
                int nRow2 = B.GetUpperBound(0) + 1;
                int nCol2 = B.GetUpperBound(1) + 1;

                if (nCol1 != nRow2)
                    return null;

                int m = nRow1;
                int L = nCol1;
                int n = nCol2;

                double[,] C = new double[m, n];

                for (int i = 0; i < m; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        C[i, j] = 0.0;

                        for (int k = 0; k < L; k++)
                        {
                            C[i, j] += A[i, k] * B[k, j];
                        }
                    }
                }

                return C;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle"> Rotation angle in radians</param>
        /// <param name="axis"> Axis to rotate about</param>
        /// <returns></returns>
        public double[,] GetRotationMatrix(double angle, Vector3D axis)
        {
            try
            {
                double n1 = axis.X;
                double n2 = axis.Y;
                double n3 = axis.Z;
                double cs = Math.Cos(angle);
                double sn = Math.Sin(angle);

                double[,] Rxyz = new double[4, 4];

                Rxyz[0, 0] = n1 * n1 + (1 - n1 * n1) * cs;
                Rxyz[0, 1] = n1 * n2 * (1 - cs) + n3 * sn;
                Rxyz[0, 2] = n1 * n3 * (1 - cs) - n2 * sn;
                Rxyz[0, 3] = 0.0;

                Rxyz[1, 0] = n1 * n2 * (1 - cs) - n3 * sn;
                Rxyz[1, 1] = n2 * n2 + (1 - n2 * n2) * cs;
                Rxyz[1, 2] = n2 * n3 * (1 - cs) + n1 * sn;
                Rxyz[1, 3] = 0.0;

                Rxyz[2, 0] = n1 * n3 * (1 - cs) + n2 * sn;
                Rxyz[2, 1] = n2 * n3 * (1 - cs) - n1 * sn;
                Rxyz[2, 2] = n3 * n3 + (1 - n3 * n3) * cs;
                Rxyz[2, 3] = 0.0;

                Rxyz[3, 0] = 0.0;
                Rxyz[3, 1] = 0.0;
                Rxyz[3, 2] = 0.0;
                Rxyz[3, 3] = 1.0;


                return Rxyz;

            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region overrides

        public override bool IsValid()
        {
            foreach (var c in GetAllControls())
            {
                if (c is SetLengthTextBox)
                    ((SetLengthTextBox)c).ProcessText();
            }
            this.RoofLevelIndex = LevelComboBox.SelectedIndex;
            double result;
            //if (StitcherBuildingObj.Template.IsImperial)
            if (true)
            {
                #region imperial
                string outString;
                if (UnitsHelper.IsAcceptedLength(StitcherBuildingObj.Template, this.BearingHeightTextBox.Text, out result,out outString))
                    this.BearingHeight = result;
                else
                    return false;
                if (UnitsHelper.IsAcceptedLength(StitcherBuildingObj.Template,this.HeelHeightTextBox.Text, out result,out outString))
                    this.HeelHeight = result;
                else
                {
                    MessageBox.Show("Heel height value is not valid .");
                    return false;
                }
                if (UnitsHelper.IsAcceptedLength(StitcherBuildingObj.Template,this.OverHangTextBox.Text, out result,out outString))
                    this.OverHang = result;
                else
                {
                    MessageBox.Show("Overhang lenght value is not valid .");
                    return false;
                }
                #endregion
            }
            else
            {
                #region metric

                if (double.TryParse(this.BearingHeightTextBox.Text, out result))
                    this.BearingHeight = result;
                else
                    return false;
                if (double.TryParse(this.HeelHeightTextBox.Text, out result))
                    this.HeelHeight = result;
                else
                {
                    MessageBox.Show("Heel height value is not valid .");
                    return false;
                }
                if (double.TryParse(this.OverHangTextBox.Text, out result))
                    this.OverHang = result;
                else
                {
                    MessageBox.Show("Overhang lenght value is not valid .");
                    return false;
                }
                #endregion
            }

            if (UnitsHelper.IsImperialAngle(this.RoofPitchTextBox.Text, out result))
            {
                if (!this.RoofPitch.IsApproxEqualTo(result))
                {
                    this.RoofPitch = result;
                    RoofPitchChanged = true;
                }
            }
            else
            {
                MessageBox.Show("Roof pitch value is not valid .");
                return false;
            }
            return base.IsValid();
        }

        protected override bool CommitChanges()
        {
            return base.CommitChanges();
        }
        #endregion

        #region EventHandlers

        private void CloseBbutton2_Click(object sender, EventArgs e)
        {
            DoCancel();
        }

        private void OkButton1_Click(object sender, EventArgs e)
        {
            DoOk();
        }

        private void LevelComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedLevel = LevelComboBox.SelectedItem;
            for (int i = StitcherBuildingObj.Levels.Count - 1; i >= 0; i--)
            {
                if (StitcherBuildingObj.Levels[i].LevelName == selectedLevel)
                {
                    if (RoofLevelIndex != i)
                    {
                        RoofLevelIndex = i;
                        FillTextBoxes_Polygon();
                    }
                    return;
                }
            }
        }

        #endregion

        private void RoofPitchTextBox_TextChanged(object sender, EventArgs e)
        {
            if ((Control)RoofPitchTextBox != this.ActiveControl)
                return;
            double pitch;
            double overhang;
            double bearing;
            double heelHeight;
            string outString;
            if (UnitsHelper.IsImperialAngle(RoofPitchTextBox.Text, out pitch)
                && UnitsHelper.IsAcceptedLength(StitcherBuildingObj.Template, OverHangTextBox.Text, out overhang,out outString)
                && UnitsHelper.IsAcceptedLength(StitcherBuildingObj.Template, BearingHeightTextBox.Text, out bearing, out outString)
                && UnitsHelper.IsAcceptedLength(StitcherBuildingObj.Template, HeelHeightTextBox.Text, out heelHeight, out outString)
                && LevelComboBox.SelectedIndex != -1)
            {
                var level = StitcherBuildingObj.Levels[LevelComboBox.SelectedIndex];
                var diffHeight = overhang * Math.Tan(MathUtil.ToRadian(pitch));
                if (maintainHHRadioButton.Checked)
                {
                    //change fascia height
                    FasciaHeightFromZero = HeelHeighFromZero - diffHeight;
                }
                else
                {
                    //change heel height and its text box
                    HeelHeighFromZero = FasciaHeightFromZero + diffHeight;
                    var topWallHeight = bearing + level.Elevation;
                    if (!heelHeight.IsApproxEqualTo(HeelHeighFromZero - topWallHeight))
                        HeelHeightTextBox.Text = UnitsHelper.ShowImperialLength(HeelHeighFromZero - topWallHeight);
                }
                fasciaHeightLabel.Text = "F from zero :" + UnitsHelper.ShowImperialLength(FasciaHeightFromZero);
                heelFromZeroLabel.Text = "c from zero :" + UnitsHelper.ShowImperialLength(HeelHeighFromZero);
            }
        }
        private void OverHangTextBox_TextChanged(object sender, EventArgs e)
        {
            if ((Control)OverHangTextBox != this.ActiveControl)
                return;
            double pitch;
            double overhang;
            double bearing;
            double heelHeight;
            string outString;
            if (UnitsHelper.IsImperialAngle(RoofPitchTextBox.Text, out pitch)
                && UnitsHelper.IsAcceptedLength(StitcherBuildingObj.Template, OverHangTextBox.Text, out overhang, out outString)
                && UnitsHelper.IsAcceptedLength(StitcherBuildingObj.Template, BearingHeightTextBox.Text, out bearing, out outString)
                && UnitsHelper.IsAcceptedLength(StitcherBuildingObj.Template, HeelHeightTextBox.Text, out heelHeight, out outString)
                && LevelComboBox.SelectedIndex != -1)
            {
                var diffHeight = overhang * Math.Tan(MathUtil.ToRadian(pitch));
                var level = StitcherBuildingObj.Levels[LevelComboBox.SelectedIndex];
                if (maintainHHRadioButton.Checked)
                {
                    //change pitch and its text box
                    FasciaHeightFromZero = HeelHeighFromZero - diffHeight;
                }
                else
                {
                    //change heel height and its text box
                    HeelHeighFromZero = diffHeight + FasciaHeightFromZero;
                    var topWallHeight = bearing + level.Elevation;
                    if (!heelHeight.IsApproxEqualTo(HeelHeighFromZero - topWallHeight))
                        HeelHeightTextBox.Text = UnitsHelper.ShowImperialLength(HeelHeighFromZero - topWallHeight);
                }
                fasciaHeightLabel.Text = "F from zero :" + UnitsHelper.ShowImperialLength(FasciaHeightFromZero);
                heelFromZeroLabel.Text = "c from zero :" + UnitsHelper.ShowImperialLength(HeelHeighFromZero);
            }
        }
        private void HeelHeightTextBox_TextChanged(object sender, EventArgs e)
        {
            if ((Control)HeelHeightTextBox != this.ActiveControl && (Control)BearingHeightTextBox != this.ActiveControl)
                return;
            double pitch;
            double overhang;
            double bearing;
            double heelHeight;
            string outString;
            if (UnitsHelper.IsImperialAngle(RoofPitchTextBox.Text, out pitch)
                && UnitsHelper.IsAcceptedLength(StitcherBuildingObj.Template, OverHangTextBox.Text, out overhang, out outString)
                && UnitsHelper.IsAcceptedLength(StitcherBuildingObj.Template, BearingHeightTextBox.Text, out bearing, out outString)
                && UnitsHelper.IsAcceptedLength(StitcherBuildingObj.Template, HeelHeightTextBox.Text, out heelHeight, out outString)
                && LevelComboBox.SelectedIndex != -1)
            {
                var level = StitcherBuildingObj.Levels[LevelComboBox.SelectedIndex];
                if (maintainHHRadioButton.Checked) // maintain overhang
                {
                    //change fascia height
                    var topWallHeight = bearing + level.Elevation;
                    var delta = (topWallHeight + heelHeight) - HeelHeighFromZero;

                    HeelHeighFromZero = HeelHeighFromZero + delta;
                    FasciaHeightFromZero = FasciaHeightFromZero + delta;
                }
                else
                {
                    var topWallHeight = bearing + level.Elevation;
                    var delta = (topWallHeight + heelHeight) - HeelHeighFromZero;
                    HeelHeighFromZero = HeelHeighFromZero + delta;

                    var y = HeelHeighFromZero - FasciaHeightFromZero;
                    var x = y / Math.Tan(MathUtil.ToRadian(pitch));

                    if (!x.IsApproxEqualTo(overhang) && x >= 0)
                        OverHangTextBox.Text = UnitsHelper.ShowImperialLength(x);
                    else if (x < 0)
                        OverHangTextBox.Text = UnitsHelper.ShowImperialLength(0);
                }
                fasciaHeightLabel.Text = "F from zero :" + UnitsHelper.ShowImperialLength(FasciaHeightFromZero);
                heelFromZeroLabel.Text = "c from zero :" + UnitsHelper.ShowImperialLength(HeelHeighFromZero);
            }
        }

        private void maintainTopFasciaRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            panel1.BackgroundImage = Resources.assistantFacis;

            SetActiveControlBack();
        }

        private void maintainHHRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            panel1.BackgroundImage = Resources.assistantHeel;

            SetActiveControlBack();
        }
        private void SetActiveControlBack()
        {
            if (ActiveTextBox == 0)
                this.ActiveControl = BearingHeightTextBox;
            else if (ActiveTextBox == 1)
                this.ActiveControl = HeelHeightTextBox;
            else if (ActiveTextBox == 2)
                this.ActiveControl = OverHangTextBox;
            else if (ActiveTextBox == 3)
                this.ActiveControl = RoofPitchTextBox;

        }
        private void ApplyBtn_Click(object sender, EventArgs e)
        {
            DoOk();
            ApplyToMode = true;
        }
    }

}
