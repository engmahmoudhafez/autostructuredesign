﻿using Elibre.AutoStructureDesign.Controls;
using Elibre.CAD.StitcherApi.Algorithms.RoofAlgorithms;
using StitcherRoofTrussApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign.Dialogs
{
    public partial class SingleTrussViewer : Form
    {
        #region Fields

        protected Truss _truss = null;
        //private Panel _trussPanel;
        //public TrussViewer TrussViewer { get; set; }
        //ImageList _allImagesList = new ImageList();
        #endregion

        #region Properties
        public Truss Truss
        {
            set { _truss = value; }
            get { return _truss; }
        }
        #endregion

        #region Constructor(s)
        public SingleTrussViewer(Truss truss)
        {
            _truss = truss;
            //TrussViewer = new TrussViewer(truss);

            //Controls.Add(TrussViewer);
            ////TrussViewer.Show();
            //TrussViewer.Parent = this;
            //TrussViewer.Dock = DockStyle.Right;
            InitializeComponent();
            FillData();
        }

        #endregion

        private void SingleTrussViewer_Load(object sender, EventArgs e)
        {
            CenterToScreen();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        public void FillData()
        {
            lbl_NOfMembers.Text = _truss.Members.Count.ToString();

            var count = _truss.TrussEnvelopeSlices.Count;
            var top_count = _truss.TrussEnvelopeSlices.Count(s => s.Item2 == TrussMemberType.Top);
            var post_count = _truss.TrussEnvelopeSlices.Count(s => s.Item2 == TrussMemberType.Post);

            if ((count == 3 && top_count == 2) ||
                  (((count == 5 && top_count == 2 && post_count == 2)
                  || (count == 7 && top_count == 4 && post_count == 2)) &&
                 Math.Abs(_truss.TrussEnvelopeSlices.FirstOrDefault(s => s.Item2 == TrussMemberType.Post).Item1.GetLength() - _truss.TrussEnvelopeSlices.LastOrDefault(s => s.Item2 == TrussMemberType.Post).Item1.GetLength()) < MathGeoUtility.Epislon)
                 )

            {
                var bottom = _truss.TrussEnvelopeSlices.FirstOrDefault(m => m.Item2 == TrussMemberType.Bottom).Item1;

                var length = bottom.GetLength() / 12;
                if (length < 10)
                {
                    lbl_TrussType.Text = "King Post Truss";
                    lbl_SpanLengthRange.Text = "Span Up to 16'";
                    pictureBox.Image = ResourcesImages.Truss_KingPost;
                    return;
                }
                else if (length < 22)
                {
                    lbl_TrussType.Text = "Queen Post Truss";
                    lbl_SpanLengthRange.Text = "Spans 10' to 22'";
                    pictureBox.Image = ResourcesImages.Truss_QueenPost;
                    return;
                }
                else if (length < 33)
                {
                    lbl_TrussType.Text = "Fink Truss";
                    lbl_SpanLengthRange.Text = "Spans 16' to 33'";
                    pictureBox.Image = ResourcesImages.Truss_Fink;
                    return;
                }
                else if (length < 36)
                {
                    lbl_TrussType.Text = "FanTruss";
                    lbl_SpanLengthRange.Text = "Spans 30' to 36'";
                    pictureBox.Image = ResourcesImages.Truss_ModifiedFan;
                    return;
                }
                else if (length < 44)
                {
                    lbl_TrussType.Text = "Modified Queen Truss";
                    lbl_SpanLengthRange.Text = "Spans 32' to 44'";
                    pictureBox.Image = ResourcesImages.Truss_ModifiedQueen;
                    return;
                }
                else if (length < 60)
                {
                    lbl_TrussType.Text = "Double Fink Truss";
                    lbl_SpanLengthRange.Text = "Spans 40' to 60'";
                    pictureBox.Image = ResourcesImages.Truss_DoubleFink;
                    return;
                }
                else
                {
                    lbl_TrussType.Text = "Triple Howe Truss";
                    lbl_SpanLengthRange.Text = " Spans 54' to 80'";
                    pictureBox.Image = ResourcesImages.Truss_TripleHowe;
                    return;
                }
            }
            else
            {
                lbl_TrussType.Text = "Generic Truss";
                pictureBox.Image = ResourcesImages.Truss_Generic;
                lbl_SpanLengthRange.Visible = false;
                label4.Visible = false;
            }
        }
        //protected Panel CreateTrussPanel()
        //{

        //    Panel tePan = new Panel();

        //        // 
        //        // trussEditorPane
        //        // 
        //        tePan.Anchor = ((AnchorStyles)((AnchorStyles.Bottom | AnchorStyles.Left)));
        //        tePan.Location = new Point(0, 27);
        //        tePan.Name = "trussEditorPane";
        //        tePan.Size = new Size(187, 504);
        //        tePan.TabIndex = 2;

        //        tePan.Width = 292;
        //        tePan.Dock = DockStyle.Left;
            
        //       // tePan.TypesGListBox.IntegralHeight = false;

        //        return tePan;
        //}

        //protected void CreateSideBar()
        //{
        //        _trussPanel = CreateTrussPanel();

        //        if (_trussPanel != null)
        //        {
        //            Controls.Add(_trussPanel);
        //            _trussPanel.Dock = DockStyle.Left;
        //        }
        //}
    }
}
