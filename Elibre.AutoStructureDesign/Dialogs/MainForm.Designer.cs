﻿namespace Elibre.AutoStructureDesign.Dialogs
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.buttonsPanel = new System.Windows.Forms.Panel();
            this.panel_Highlight = new System.Windows.Forms.Panel();
            this.radioButton_3D = new System.Windows.Forms.RadioButton();
            this.radioButton_Drawings = new System.Windows.Forms.RadioButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radioButton_Takeoffs = new System.Windows.Forms.RadioButton();
            this.radioButton_Digrams = new System.Windows.Forms.RadioButton();
            this.radioButton_Reports = new System.Windows.Forms.RadioButton();
            this.radioButton_Roof = new System.Windows.Forms.RadioButton();
            this.radioButton_Walls = new System.Windows.Forms.RadioButton();
            this.radioButton_Input = new System.Windows.Forms.RadioButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.existToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.buttonsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonsPanel
            // 
            this.buttonsPanel.BackColor = System.Drawing.Color.White;
            this.buttonsPanel.Controls.Add(this.panel_Highlight);
            this.buttonsPanel.Controls.Add(this.radioButton_3D);
            this.buttonsPanel.Controls.Add(this.radioButton_Drawings);
            this.buttonsPanel.Controls.Add(this.pictureBox1);
            this.buttonsPanel.Controls.Add(this.radioButton_Takeoffs);
            this.buttonsPanel.Controls.Add(this.radioButton_Digrams);
            this.buttonsPanel.Controls.Add(this.radioButton_Reports);
            this.buttonsPanel.Controls.Add(this.radioButton_Roof);
            this.buttonsPanel.Controls.Add(this.radioButton_Walls);
            this.buttonsPanel.Controls.Add(this.radioButton_Input);
            this.buttonsPanel.Controls.Add(this.menuStrip1);
            this.buttonsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonsPanel.Location = new System.Drawing.Point(0, 0);
            this.buttonsPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonsPanel.Name = "buttonsPanel";
            this.buttonsPanel.Size = new System.Drawing.Size(1513, 98);
            this.buttonsPanel.TabIndex = 2;
            // 
            // panel_Highlight
            // 
            this.panel_Highlight.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel_Highlight.Location = new System.Drawing.Point(147, 81);
            this.panel_Highlight.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel_Highlight.Name = "panel_Highlight";
            this.panel_Highlight.Size = new System.Drawing.Size(161, 7);
            this.panel_Highlight.TabIndex = 13;
            this.panel_Highlight.Visible = false;
            // 
            // radioButton_3D
            // 
            this.radioButton_3D.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton_3D.BackColor = System.Drawing.Color.White;
            this.radioButton_3D.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButton_3D.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radioButton_3D.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButton_3D.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_3D.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_3D.Location = new System.Drawing.Point(1316, 41);
            this.radioButton_3D.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButton_3D.Name = "radioButton_3D";
            this.radioButton_3D.Size = new System.Drawing.Size(161, 41);
            this.radioButton_3D.TabIndex = 12;
            this.radioButton_3D.TabStop = true;
            this.radioButton_3D.Text = "3D";
            this.radioButton_3D.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton_3D.UseVisualStyleBackColor = false;
            this.radioButton_3D.Visible = false;
            this.radioButton_3D.CheckedChanged += new System.EventHandler(this.OnChangeButton);
            // 
            // radioButton_Drawings
            // 
            this.radioButton_Drawings.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton_Drawings.BackColor = System.Drawing.Color.White;
            this.radioButton_Drawings.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButton_Drawings.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radioButton_Drawings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButton_Drawings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_Drawings.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Drawings.Location = new System.Drawing.Point(1149, 41);
            this.radioButton_Drawings.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButton_Drawings.Name = "radioButton_Drawings";
            this.radioButton_Drawings.Size = new System.Drawing.Size(161, 41);
            this.radioButton_Drawings.TabIndex = 11;
            this.radioButton_Drawings.TabStop = true;
            this.radioButton_Drawings.Text = "Drawings";
            this.radioButton_Drawings.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton_Drawings.UseVisualStyleBackColor = false;
            this.radioButton_Drawings.Visible = false;
            this.radioButton_Drawings.CheckedChanged += new System.EventHandler(this.OnChangeButton);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Elibre.AutoStructureDesign.Properties.Resources.EStitcherIcon;
            this.pictureBox1.Location = new System.Drawing.Point(12, 48);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(112, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // radioButton_Takeoffs
            // 
            this.radioButton_Takeoffs.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton_Takeoffs.BackColor = System.Drawing.Color.White;
            this.radioButton_Takeoffs.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButton_Takeoffs.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radioButton_Takeoffs.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButton_Takeoffs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_Takeoffs.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Takeoffs.Location = new System.Drawing.Point(982, 41);
            this.radioButton_Takeoffs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButton_Takeoffs.Name = "radioButton_Takeoffs";
            this.radioButton_Takeoffs.Size = new System.Drawing.Size(161, 41);
            this.radioButton_Takeoffs.TabIndex = 10;
            this.radioButton_Takeoffs.TabStop = true;
            this.radioButton_Takeoffs.Text = "Takeoffs";
            this.radioButton_Takeoffs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton_Takeoffs.UseVisualStyleBackColor = false;
            this.radioButton_Takeoffs.Visible = false;
            this.radioButton_Takeoffs.CheckedChanged += new System.EventHandler(this.OnChangeButton);
            // 
            // radioButton_Digrams
            // 
            this.radioButton_Digrams.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton_Digrams.BackColor = System.Drawing.Color.White;
            this.radioButton_Digrams.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButton_Digrams.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radioButton_Digrams.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButton_Digrams.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_Digrams.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Digrams.Location = new System.Drawing.Point(815, 41);
            this.radioButton_Digrams.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButton_Digrams.Name = "radioButton_Digrams";
            this.radioButton_Digrams.Size = new System.Drawing.Size(161, 41);
            this.radioButton_Digrams.TabIndex = 9;
            this.radioButton_Digrams.TabStop = true;
            this.radioButton_Digrams.Text = "Diagrams ";
            this.radioButton_Digrams.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton_Digrams.UseVisualStyleBackColor = false;
            this.radioButton_Digrams.Visible = false;
            this.radioButton_Digrams.CheckedChanged += new System.EventHandler(this.OnChangeButton);
            // 
            // radioButton_Reports
            // 
            this.radioButton_Reports.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton_Reports.BackColor = System.Drawing.Color.White;
            this.radioButton_Reports.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButton_Reports.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radioButton_Reports.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButton_Reports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_Reports.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Reports.Location = new System.Drawing.Point(648, 41);
            this.radioButton_Reports.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButton_Reports.Name = "radioButton_Reports";
            this.radioButton_Reports.Size = new System.Drawing.Size(161, 41);
            this.radioButton_Reports.TabIndex = 8;
            this.radioButton_Reports.TabStop = true;
            this.radioButton_Reports.Text = "Reports";
            this.radioButton_Reports.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton_Reports.UseVisualStyleBackColor = false;
            this.radioButton_Reports.Visible = false;
            this.radioButton_Reports.CheckedChanged += new System.EventHandler(this.OnChangeButton);
            // 
            // radioButton_Roof
            // 
            this.radioButton_Roof.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton_Roof.BackColor = System.Drawing.Color.White;
            this.radioButton_Roof.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButton_Roof.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radioButton_Roof.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButton_Roof.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_Roof.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Roof.Location = new System.Drawing.Point(481, 41);
            this.radioButton_Roof.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButton_Roof.Name = "radioButton_Roof";
            this.radioButton_Roof.Size = new System.Drawing.Size(161, 41);
            this.radioButton_Roof.TabIndex = 6;
            this.radioButton_Roof.TabStop = true;
            this.radioButton_Roof.Text = "Roof";
            this.radioButton_Roof.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton_Roof.UseVisualStyleBackColor = false;
            this.radioButton_Roof.Visible = false;
            this.radioButton_Roof.CheckedChanged += new System.EventHandler(this.OnChangeButton);
            // 
            // radioButton_Walls
            // 
            this.radioButton_Walls.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton_Walls.BackColor = System.Drawing.Color.White;
            this.radioButton_Walls.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButton_Walls.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radioButton_Walls.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButton_Walls.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_Walls.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Walls.Location = new System.Drawing.Point(314, 41);
            this.radioButton_Walls.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButton_Walls.Name = "radioButton_Walls";
            this.radioButton_Walls.Size = new System.Drawing.Size(161, 41);
            this.radioButton_Walls.TabIndex = 5;
            this.radioButton_Walls.TabStop = true;
            this.radioButton_Walls.Text = "Structure Model";
            this.radioButton_Walls.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton_Walls.UseVisualStyleBackColor = false;
            this.radioButton_Walls.Visible = false;
            this.radioButton_Walls.CheckedChanged += new System.EventHandler(this.OnChangeButton);
            // 
            // radioButton_Input
            // 
            this.radioButton_Input.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton_Input.BackColor = System.Drawing.Color.White;
            this.radioButton_Input.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButton_Input.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radioButton_Input.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButton_Input.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_Input.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Input.Location = new System.Drawing.Point(147, 41);
            this.radioButton_Input.Margin = new System.Windows.Forms.Padding(0);
            this.radioButton_Input.Name = "radioButton_Input";
            this.radioButton_Input.Size = new System.Drawing.Size(161, 41);
            this.radioButton_Input.TabIndex = 1;
            this.radioButton_Input.TabStop = true;
            this.radioButton_Input.Text = "Arch. Model";
            this.radioButton_Input.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton_Input.UseVisualStyleBackColor = false;
            this.radioButton_Input.Visible = false;
            this.radioButton_Input.CheckedChanged += new System.EventHandler(this.OnChangeButton);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1513, 28);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProjectToolStripMenuItem,
            this.toolStripMenuItem1,
            this.existToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newProjectToolStripMenuItem
            // 
            this.newProjectToolStripMenuItem.Name = "newProjectToolStripMenuItem";
            this.newProjectToolStripMenuItem.Size = new System.Drawing.Size(164, 26);
            this.newProjectToolStripMenuItem.Text = "New Project";
            this.newProjectToolStripMenuItem.Click += new System.EventHandler(this.newProjectToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(161, 6);
            // 
            // existToolStripMenuItem
            // 
            this.existToolStripMenuItem.Name = "existToolStripMenuItem";
            this.existToolStripMenuItem.Size = new System.Drawing.Size(164, 26);
            this.existToolStripMenuItem.Text = "Exit";
            this.existToolStripMenuItem.Click += new System.EventHandler(this.existToolStripMenuItem_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.BackColor = System.Drawing.Color.White;
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 98);
            this.mainPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(1513, 488);
            this.mainPanel.TabIndex = 3;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(61, 4);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1513, 586);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.buttonsPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MainForm";
            this.Text = "Auto Structure Design";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.buttonsPanel.ResumeLayout(false);
            this.buttonsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RadioButton radioButton_Input;
        private System.Windows.Forms.Panel buttonsPanel;
        private System.Windows.Forms.RadioButton radioButton_Roof;
        private System.Windows.Forms.RadioButton radioButton_Walls;
        private System.Windows.Forms.RadioButton radioButton_3D;
        private System.Windows.Forms.RadioButton radioButton_Drawings;
        private System.Windows.Forms.RadioButton radioButton_Takeoffs;
        private System.Windows.Forms.RadioButton radioButton_Digrams;
        private System.Windows.Forms.RadioButton radioButton_Reports;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Panel panel_Highlight;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem existToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
    }
}

