﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Controls;
using Elibre.AutoStructureDesign.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign.Dialogs
{
    public partial class FrmDiagramViewer : Form
    {
     
        public DiagramViewer DiagramViewer { get; set; }
        public FrmDiagramViewer()
        {
            DiagramViewer = new DiagramViewer();
            Controls.Add(DiagramViewer);
            DiagramViewer.Parent = this;
            DiagramViewer.Dock = DockStyle.Fill;
            InitializeComponent();
        }

        internal void UpdateDiagram(ActiveResults activeResults, DiagramType type)
        {
            if (!Controls.Contains(DiagramViewer))
            {
                Controls.Add(DiagramViewer=new DiagramViewer());
                DiagramViewer.Parent = this;
                DiagramViewer.Dock = DockStyle.Fill;
            }
    
           Text = activeResults.ToString() + "-" + type.ToString();
            switch (type)
            {
                case DiagramType.DistributedLoad:
                    Icon = Resources.Normal;
                    break;
                case DiagramType.ShearForce:
                    Icon = Resources.shear2;
                    break;
                case DiagramType.BendingMoment:
                    Icon = Resources.moment;
                    break;

            }
            DiagramViewer.UpdateDiagram(activeResults, type);
        }

        private void FrmDiagramViewer_Load(object sender, EventArgs e)
        {

        }

        private void FrmDiagramViewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            //TODO temp solution for viwer bug .
            Controls.Remove(DiagramViewer);
            
            //DiagramViewer.Dispose();
        }
    }
}
