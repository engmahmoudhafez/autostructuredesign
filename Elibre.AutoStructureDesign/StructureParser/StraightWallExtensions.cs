﻿using EFraming.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign
{
  public static  class StraightWallExtensions
    {
        public static List<Stud> GetAllStudsExceptOpeningStuds(this StraightWall wall)
        {
            var segmentList = wall.SegmentList;
                if (segmentList == null)
                    return null;

                List<Stud> studs = new List<Stud>();

                foreach (WallSegment seg in segmentList)
                {
                    if (seg == null)
                        continue;

                    if (seg.StudList == null)
                        continue;

                    foreach (Stud stud in seg.StudList)
                    {
                        if (stud == null)
                            continue;

                        studs.Add(stud);
                    }

                    //if (seg is StraightWallOpening)
                    //{
                    //    if ((seg as StraightWallOpening).TopSegment != null)
                    //    {
                    //        foreach (Stud stud in (seg as StraightWallOpening).TopSegment.StudList)
                    //        {
                    //            if (stud == null)
                    //                continue;

                    //            studs.Add(stud);
                    //        }
                    //    }

                    //    if ((seg as StraightWallOpening).BottomSegment != null)
                    //    {
                    //        foreach (Stud stud in (seg as StraightWallOpening).BottomSegment.StudList)
                    //        {
                    //            if (stud == null)
                    //                continue;

                    //            studs.Add(stud);
                    //        }
                    //    }
                    //}
                }

                return studs;
           
        }
    }
}
