﻿using EFraming;
using EFraming.Design;
using EFraming.Objects;
using Elibre.Net.Solvers;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign
{
    public static class StructureBuildingExtensions
    {

        public static List<string> SolverMessages { get; set; } = new List<string>();
        //TODO add global window to print analysis results
        public static void DisplaySolverMessage(object msg, messagetype type)
        {
            // print messages to console

            SolverMessages.Add(msg as string);

        }

        public static bool RunAnalysis(this Building building, AnalysisOptions options)
        {
            bool retVal = false;
            if (building.Solved)
            {
                building.ClearResults();
            }

            building.CalcScale();

            StructureBuildingExtensions.SolverMessages.Clear();
            if (options == null)
                options = new AnalysisOptions();

            #region Regenrate Framing Supports if Required

            if (options.RegenFramingSupports)
            {
                #region Regenerate Supports

                int n = 0;
                List<Floor> floorList = building.FloorList;

                if (floorList != null)
                    n = floorList.Count;

                for (int k = (n - 1); k > 0; k--)
                {
                    Floor floor = floorList[k];

                    if (floor == null)
                        continue;

                    foreach (FramingObject obj in floor.ObjectList)
                    {
                        if (obj == null)
                            continue;

                        ElementSupportDetector supDetector = SupportDetectorFactory.Create(obj);

                        if (obj is Beam)
                        {
                            (obj as Beam).DetectSupports();
                        }
                        else if (obj is Joist)
                        {
                            (obj as Joist).DetectSupports();
                        }
                        else if (obj is Rafter)
                        {
                            (obj as Rafter).DetectSupports();
                        }
                        else if (obj is UPost)
                        {
                            (obj as UPost).DetectSupports();
                        }
                        else if (obj is Post)
                        {
                            (obj as Post).DetectSupports();
                        }
                        else
                        {
                            if (supDetector != null)
                            {
                                supDetector.DetectSupports();
                                supDetector.Dispose();
                                supDetector = null;
                            }
                        }
                    }
                }

                #endregion
            }

            #endregion

            #region Delete Old Design Temporary Files

            FramingDesign.DeleteDesignTempFiles();
            LateralDesign.DeleteDesignTempFiles();

            #endregion

            #region Run Model Verifier if Needed

            if (options.RunVerifier)
            {
                ModelVerifier verifier = new ModelVerifier(building, VerifierCallReason.Run, options);

                foreach (var f in building.FloorList)
                {
                    f.GenerateDiaphragmUnionSupports();
                }

                verifier.Verify();

                if ((verifier.ExceptionList != null) && (verifier.ExceptionList.Count > 0))
                {
                    frmModelVerifier frm = new frmModelVerifier(verifier);

                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                    }

                    if (!frm.ProceedWithAnalysis)
                    {
                        frm.Close();
                        frm = null;
                        return false;
                    }

                    frm.Close();
                    frm = null;

                }

                #endregion


                if (building.SeismicWindAnalysisData.seismicWindInput.loadCode == SeismicWind.LoadCode.IRC2015)
                {
                    LateralDesign.RunLateralDesignPostAnalysis(building);
                    retVal = true;
                }
                else
                {
                    if (options.AnalyseLateral)
                    {
                        // CalcShearLinesWindHeight
                        LateralDesign.RunAnalysisSeismicWind(building);
                    }

                    Analysis analysis = new Analysis(building, options);
                    analysis.SetCallBackFunction(StructureBuildingExtensions.DisplaySolverMessage);

                    retVal = analysis.Run();

                    if (retVal) // Do post processing if necessary
                    {
                        try
                        {
                            if (options.AnalyseLateral && analysis.LateralSolved
                            ) // Check for solution of lateral explicit
                            {
                                // Design module gives error when diaphragm union exists
                                LateralDesign.RunLateralDesignPostAnalysis(building);
                            }

                            if (options.AnalyseFraming && analysis.FramingSolved
                            ) // Check for solution of framing explicit
                            {
                                #region Update Header Posts Reactions

                                foreach (Floor f in building.FloorList)
                                {
                                    List<StraightWall> wallList = f.GetStraightWallList();

                                    foreach (StraightWall wall in wallList)
                                    {
                                        wall.TransferHeaderReactionToHeaderPosts();
                                    }
                                }

                                #endregion

                                // Design module gives error when diaphragm union exists
                                FramingDesign.DesignFraming(building, options.DesignBeams, options.DesignPosts,
                                    options.DesignJoists, options.DesignHeaders, options.DesignHangers,
                                    options.DesignRafter, options.DesignTruss);
                                //if (options.DesignFoundation)
                                //    Design.FramingDesign.DesignFoundation(Document.Building);

                                // Update section geometry
                                building.UpdateGeometryAfterDesign();
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }

                    //_document.Solved = retVal;

                    #region Verify analysis results

                    if (retVal)
                    {
                        ModelVerifier averifier = new ModelVerifier(building, VerifierCallReason.VerifyAnalysis, options);

                        averifier.VerifyAfterSolve();
                        averifier.ExceptionList = averifier.ExceptionList.DistinctBy(args => args.FramingObject).ToList();
                        if ((averifier.ExceptionList != null) && (averifier.ExceptionList.Count > 0))
                        {
                            frmModelVerifier frm = new frmModelVerifier(averifier);
                            frm.Solved = true;
                            frm.ShowDialog();

                            //TODO :Handle the after anaysis model Verifier form

                            //if (!frm.ProceedWithAnalysis)
                            //{
                            //    frm.Close();
                            //    frm = null;
                            //    return false;
                            //}

                            frm.Close();
                            frm = null;
                        }
                    }

                    #endregion

                    if (retVal && options.VerifyAnalysisResults)
                    {
                        frmAnalysisResults resForm = new frmAnalysisResults(building, analysis.LateralForceUntV,
                            analysis.LateralForceUntH);
                        resForm.ShowDialog();
                    }

                    if (retVal && options.DisplaySolutionOrder)
                    {
                        frmSolutionOrder frmSolOrder = new frmSolutionOrder();
                        frmSolOrder.SolutionOrder = analysis.SolutionOrder;
                        frmSolOrder.ShowDialog();
                    }
                }
            }
            //set buidling solved value
            building.Solved = retVal;
            return retVal;
        }

        public static AnalysisOptions GetAnalysisOptions(this Building building)
        {
            var ops = new AnalysisOptions();
            ops.AnalyseLateral = true;
            ops.AnalyseUplift = true;
            ops.RegenFramingSupports = true;

            #region Design

            ops.DesignJoists = true;
            ops.DesignBeams = true;
            ops.DesignHeaders = true;
            ops.DesignPosts = true;
            ops.DesignHangers = true;
            ops.DesignForOverStrengthFactor = true;
            ops.DesignFoundation = true;
            ops.DesignTruss = true;
            ops.DesignRafter = true;
            #endregion
            return ops;
        }
        public static AnalysisOptions StopDesign(this AnalysisOptions ops)
        {


            ops.DesignJoists = false;
            ops.DesignBeams = false;
            ops.DesignHeaders = false;
            ops.DesignPosts = false;
            ops.DesignHangers = false;
            ops.DesignForOverStrengthFactor = false;
            ops.DesignFoundation = false;
            ops.DesignTruss = false;
            ops.DesignRafter = false;
            ops.DesignFoundation = false;
           

            return ops;
        }

        public static bool IsFloorExist(this Building building, int index)
        {
          return  building.FloorList.Count > index;
        }

    }
}
