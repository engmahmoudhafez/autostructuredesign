﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Drawings.Floor;
using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.StructureParser.GLObjects
{
    public class GLWall
    {
        private List<GLWallOpening> openings = new List<GLWallOpening>();
        private List<GLWallSegment> segments = new List<GLWallSegment>();

        public GLWall(StraightWall eframerWall)
        {

            foreach (var segment in eframerWall.SegmentList)
            {
                if (segment is StraightWallOpening)
                {
                    GLWallOpening opening = new GLWallOpening((StraightWallOpening)segment);
                    openings.Add(opening);
                }
                else if (segment is StraightWallSegment)
                {
                    GLWallSegment wallSegment = new GLWallSegment((StraightWallSegment)segment);
                    segments.Add(wallSegment);
                }
            }
        }
        public void Render(int programID)
        {
            foreach (var segment in segments)
            {
                segment.Render(programID);
            }
            foreach (var segment in openings)
            {
                segment.Render(programID);
            }
        }
        public void RenderGraphics(int programID)
        {
            foreach (var opening in openings)
            {
                opening.RenderGraphics(programID);
            }
            foreach (var wall in segments)
            {
                wall.RenderGraphics(programID);
            }
        }
        public void Fill(int programID)
        {
            foreach (var wall in segments)
            {
                wall.Fill(programID);
            }

        }
        public List<Slice2D> getSlices()
        {
            return segments.SelectMany(o => o.getSlices()).ToList().Union(openings.SelectMany(o => o.getSlices())).ToList();
        }
        public void Dispose()
        {
            foreach (var segment in segments)
            {
                segment.Dispose();
            }
            foreach (var segment in openings)
            {
                segment.Dispose();
            }
        }
    }
}
