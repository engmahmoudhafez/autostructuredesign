﻿using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.CAD.StitcherApi.Helpers;
using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.StructureParser.GLObjects
{
    class ShearPattern
    {
        private GLDrawableObject drawable;
        private GLFillableObject circle;
        private GLGraphicsObject typeText;
        private GLGraphicsObject lengthText;
        private GLGraphicsObject stripTextStart;
        private GLGraphicsObject stripTextEnd;

        private float ssliceLength = 2;

        public ShearPattern(Slice2D exteriorSlice, string _typeText, float thickness, float poiterLength=0)
        {
            var shearSlice = exteriorSlice.CreateCopy();

            typeText = new GLGraphicsObject("A", new Font("Arial", 12, FontStyle.Regular), Color.Black);
            typeText.Scale(0.025f);
            //stripTextStart = new GLGraphicsObject("MST84", new Font("Arial", 12, FontStyle.Regular), Color.Black);
            //stripTextStart.Scale(0.08f);
            //stripTextEnd = new GLGraphicsObject("MST84", new Font("Arial", 12, FontStyle.Regular), Color.Black);
            //stripTextEnd.Scale(0.08f);
            string s;
            double iv;
            UnitsHelper.IsImperialLength(shearSlice.Length.ToString(), out iv, out s);
            lengthText = new GLGraphicsObject(s, new Font("Arial", 12, FontStyle.Regular), Color.Black);
            lengthText.Scale(0.06f);

            var triangleSide = thickness * 1.8;
            var midPoint = shearSlice.GetMidPoint();
            var vec = shearSlice.End - shearSlice.Start;
            var rotAngle = vec.GetAngle();
            GLUtils.RotateSlice(shearSlice, midPoint, -rotAngle);

            var stripPointStart = new Point2D(shearSlice.Start.X + thickness * 1.5, shearSlice.Start.Y);
            var stripPointEnd = new Point2D(shearSlice.End.X - thickness * 1.5, shearSlice.End.Y);

            shearSlice.Start = new Point2D(shearSlice.Start.X, shearSlice.Start.Y - thickness * 1.5);
            shearSlice.End = new Point2D(shearSlice.End.X, shearSlice.End.Y - thickness * 1.5);

            var midPointAfter = shearSlice.GetMidPoint();
            var triSlice1 = new Slice2D(midPointAfter, new Point2D(midPointAfter.X - triangleSide, midPointAfter.Y - triangleSide));
            var triSlice2 = new Slice2D(midPointAfter, new Point2D(midPointAfter.X + triangleSide, midPointAfter.Y - triangleSide));
            var triSlice3 = new Slice2D(new Point2D(midPointAfter.X - triangleSide, midPointAfter.Y - triangleSide), new Point2D(midPointAfter.X + triangleSide, midPointAfter.Y - triangleSide));
            var typeSlice = new Slice2D(new Point2D(midPointAfter.X - triangleSide, midPointAfter.Y - 0.5 * triangleSide), new Point2D(midPointAfter.X + triangleSide, midPointAfter.Y - 0.5 * triangleSide));
            var lenSlice = new Slice2D(new Point2D(midPointAfter.X - triangleSide, midPointAfter.Y - (triangleSide + lengthText.getSize().Height / 2)), new Point2D(midPointAfter.X + triangleSide, midPointAfter.Y - (triangleSide + lengthText.getSize().Height / 2)));

            //var vSliceStart = new Slice2D(stripPointStart, new Point2D(stripPointStart.X, stripPointStart.Y - poiterLength));
            //var sSliceStart = new Slice2D(vSliceStart.End, new Point2D(vSliceStart.End.X - thickness * ssliceLength, vSliceStart.End.Y - thickness * ssliceLength));
            //var v = sSliceStart.End - sSliceStart.Start;
            //v.Normalize();
            //v *= stripTextStart.getSize().Width;
            //sSliceStart.End = sSliceStart.Start + v;

            //var vSliceEnd = new Slice2D(stripPointEnd, new Point2D(stripPointEnd.X, stripPointEnd.Y - poiterLength));
            //var sSliceEnd = new Slice2D(vSliceEnd.End, new Point2D(vSliceEnd.End.X - thickness * ssliceLength, vSliceEnd.End.Y - thickness * ssliceLength));

            //v = sSliceEnd.End - sSliceEnd.Start;
            //v.Normalize();
            //v *= stripTextEnd.getSize().Width;
            //sSliceEnd.End = sSliceEnd.Start + v;

            GLUtils.RotateSlice(shearSlice, midPoint, rotAngle);
            GLUtils.RotateSlice(triSlice1, midPoint, rotAngle);
            GLUtils.RotateSlice(triSlice2, midPoint, rotAngle);
            GLUtils.RotateSlice(triSlice3, midPoint, rotAngle);
            GLUtils.RotateSlice(typeSlice, midPoint, rotAngle);
            GLUtils.RotateSlice(lenSlice, midPoint, rotAngle);
            //GLUtils.RotateSlice(vSliceStart, midPoint, rotAngle);
            //GLUtils.RotateSlice(sSliceStart, midPoint, rotAngle);
            //GLUtils.RotateSlice(vSliceEnd, midPoint, rotAngle);
            //GLUtils.RotateSlice(sSliceEnd, midPoint, rotAngle);

            GLUtils.AlignTextToSlice(typeText, typeSlice, 0, GLUtils.GLGraphicsObjectVPosition.CENTER, GLUtils.GLGraphicsObjectHPosition.MIDDLE);
            GLUtils.AlignTextToSlice(lengthText, lenSlice, 0, GLUtils.GLGraphicsObjectVPosition.CENTER, GLUtils.GLGraphicsObjectHPosition.MIDDLE);
            //GLUtils.AlignTextToSlice(stripTextStart, sSliceStart, 0, GLUtils.GLGraphicsObjectVPosition.UP, GLUtils.GLGraphicsObjectHPosition.MIDDLE);
            //GLUtils.AlignTextToSlice(stripTextEnd, sSliceEnd, 0, GLUtils.GLGraphicsObjectVPosition.UP, GLUtils.GLGraphicsObjectHPosition.MIDDLE);

            drawable = new GLDrawableObject(new List<Slice2D>() { shearSlice, triSlice1, triSlice2, triSlice3 }, Color.Black);

            //SlicePolygon2D circleStart = new SlicePolygon2D(GLUtils.CreateCircleSlices(vSliceStart.Start, (float)thickness / 5.0f, 10));
            //SlicePolygon2D circleEnd = new SlicePolygon2D(GLUtils.CreateCircleSlices(vSliceEnd.Start, (float)thickness / 5.0f, 10));
            //circle = new GLFillableObject(new List<SlicePolygon2D> { circleStart, circleEnd });
        }

        public void Draw(int programID)
        {
            drawable.Render(programID);
        }
        public void Fill(int programID)
        {
            circle.Render(programID);
        }
        public void RenderGraphics(int programID)
        {
            typeText?.Render(programID);
            lengthText?.Render(programID);
            stripTextStart?.Render(programID);
            stripTextEnd?.Render(programID);
        }

        public void Dispose()
        {
            drawable?.Dispose();
            typeText?.Dispose();
            lengthText?.Dispose();
            stripTextEnd?.Dispose();
            stripTextStart?.Dispose();
        }
    }
}
