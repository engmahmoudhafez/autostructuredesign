﻿using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.Geometry2D;
using System.Collections.Generic;

namespace Elibre.AutoStructureDesign.StructureParser.GLObjects
{
    public class GLBaseObject
    {
        protected GLDrawableObject drawableSegment;

        protected List<Slice2D> slices;

        public virtual void Render(int programID)
        {
            drawableSegment.Render(programID);
            drawableSegment.Z_Order =(float) FloorZOrders.WALL;
        }
        public List<Slice2D> getSlices()
        {
            return slices;
        }
        public virtual void Dispose()
        {
            drawableSegment.Dispose();

        }
    }

   public enum FloorZOrders
   {
       HATCH, WALL, BEAM, POINTER, TEXT
   }
}
