﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.StructureParser.GLObjects
{
    public class GLWallOpening : GLBaseObject
    {

        Slice2D opening_centerline;
        protected GLDrawableObject Opening_centerline;

        string HDR_width;
        string HDR_depth;
        string HDR_materialstring;


        public GLWallOpening(StraightWallOpening opening)
        {
            slices = new List<Slice2D>();
            double xScale=12;
            double yScale = 12;
            opening_centerline = new Slice2D(new Geometry2D.Point2D(opening.CenterLine.P1.x * xScale, opening.CenterLine.P1.y * yScale),
                new Geometry2D.Point2D(opening.CenterLine.P2.x * xScale, opening.CenterLine.P2.y * yScale));
            var points = opening.Geom2D.VertexList;
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[0].x *xScale, points[0].y * yScale), new Geometry2D.Point2D(points[1].x *xScale, points[1].y * yScale)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[1].x *xScale, points[1].y * yScale), new Geometry2D.Point2D(points[2].x *xScale, points[2].y * yScale)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[2].x *xScale, points[2].y * yScale), new Geometry2D.Point2D(points[3].x *xScale, points[3].y * yScale)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[3].x *xScale, points[3].y * yScale), new Geometry2D.Point2D(points[0].x *xScale, points[0].y * yScale)));

            HDR_width = opening.HeaderSection.WidthString;
            HDR_depth = opening.HeaderSection.DepthString;
            HDR_materialstring = opening.HeaderSection.Species;

            drawableSegment = new GLDrawableObject(slices);

            List<Slice2D> centers = new List<Slice2D>();
            centers.Add(opening_centerline);

            Opening_centerline = new GLDrawableObject(centers);

            Opening_centerline.Thickness = 3;
            drawableSegment.Thickness = 3;

            Geometry2D.Point2D center = opening_centerline.GetMidPoint();

            float pointerLength = 3.5f * (float)slices[1].Length;
            if (opening_centerline != null)
            {
                pointerLength = opening_centerline.IsVertical() ? (float)slices[1].Length * 3.7f : (float)slices[1].Length * 5.2f;
            }
            

        }
        public override void Render(int programID)
        {
            base.Render(programID);
            GLUtils.StartDottedLinesMode(3, 0x00FF);
            Opening_centerline.Render(programID);
            GLUtils.StopDottedLinesMode();

          
        }
        public void RenderGraphics(int programID)
        {
        
        }
        public void FillArrow(int programID)
        {
            
        }
        public override void Dispose()
        {
            base.Dispose();
            Opening_centerline.Dispose();

        }
    }
}
