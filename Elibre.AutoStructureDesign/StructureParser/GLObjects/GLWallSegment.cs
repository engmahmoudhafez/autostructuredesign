﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.Geometry2D;
using System.Collections.Generic;
using System.Linq;

namespace Elibre.AutoStructureDesign.StructureParser.GLObjects
{
    public class GLWallSegment : GLBaseObject
    {

        bool IsShear;
        bool IsInterior;
        double length;

        private ShearPattern shearPattern;
        public GLWallSegment(StraightWallSegment segment)
        {
            slices = new List<Slice2D>();
            var xScale = 12;
            var yScale = 12;
            var points = segment.Geom2D.VertexList;
            Slice2D core = new Slice2D(new Geometry2D.Point2D((segment.CenterLine.P1.x * xScale), (segment.CenterLine.P1.y * yScale))
                , new Geometry2D.Point2D((segment.CenterLine.P2.x * xScale), (segment.CenterLine.P2.y * yScale)));

            slices.Add(new Slice2D(new Geometry2D.Point2D(points[0].x * xScale, points[0].y * yScale), new Geometry2D.Point2D(points[1].x * xScale, points[1].y * yScale)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[1].x * xScale, points[1].y * yScale), new Geometry2D.Point2D(points[2].x * xScale, points[2].y * yScale)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[2].x * xScale, points[2].y * yScale), new Geometry2D.Point2D(points[3].x * xScale, points[3].y * yScale)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[3].x * xScale, points[3].y * yScale), new Geometry2D.Point2D(points[0].x * xScale, points[0].y * yScale)));

            IsShear = segment.Shear;
            if (IsShear)
            {
                float pointerLength = core.IsVertical() ? (float)slices[1].Length * 9.0f : (float)slices[1].Length * 7.0f;
                shearPattern = new ShearPattern(core, "A", (float)slices[1].Length);
            }
            if (segment.ParentFramingWall.Use == WallUse.Interior)
            {
                IsInterior = true;
            }
            else
            {
                IsInterior = false;
            }
            if (IsInterior && IsShear)
            {
                List<Slice2D> ss = CrossHatches();
                for (int i = 0; i < ss.Count; i++)
                {
                    slices.Add(ss[i]);
                }
            }

            drawableSegment = new GLDrawableObject(slices);
            length = segment.Length;
            drawableSegment.Thickness = 1.5f;
        }

        public override void Render(int programID)
        {
            base.Render(programID);
            if (shearPattern != null)
            {
                shearPattern.Draw(programID);
            }
        }
        public void Fill(int programID)
        {
            if (shearPattern != null)
            {
                shearPattern.Fill(programID);
            }
        }
        public void RenderGraphics(int programID)
        {
            if (shearPattern != null)
            {
                shearPattern.RenderGraphics(programID);
            }
        }
        private List<Slice2D> CrossHatches()
        {
            List<Slice2D> copy = slices.CreateCopy();

            copy = copy.OrderByDescending(o => o.Length).ToList();

            List<Slice2D> result = new List<Slice2D>();

            Slice2D left_slice = copy[0];
            Slice2D rightslice = copy[1];
            Slice2D step_slice = copy[2];

            Vector2D unitvector = new Vector2D(left_slice.End - left_slice.Start);

            unitvector.Normalize();

            int count = (int)(left_slice.Length / step_slice.Length);

            List<Geometry2D.Point2D> left_points = new List<Geometry2D.Point2D>();
            List<Geometry2D.Point2D> right_points = new List<Geometry2D.Point2D>();


            Geometry2D.Point2D temp = left_slice.Start;
            Geometry2D.Point2D temp2 = rightslice.End;


            for (int i = 0; i < count; i++)
            {
                left_points.Add(temp + unitvector * step_slice.Length);
                right_points.Add(temp2 + unitvector * step_slice.Length);

                temp = left_points[i];
                temp2 = right_points[i];
            }
            for (int i = 0; i < left_points.Count - 1; i++)
            {

                Slice2D slice = new Slice2D(left_points[i], right_points[i + 1]);
                Slice2D slice2 = new Slice2D(left_points[i + 1], right_points[i]);

                result.Add(slice);
                result.Add(slice2);

            }

            return result;

        }
    }
}
