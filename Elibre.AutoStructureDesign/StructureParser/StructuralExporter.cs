﻿using EFraming;
using EFraming.Objects;
using Elibre.AutoStructureDesign.Models;
using Elibre.CAD.StitcherApi.Models.Stitching;
using Elibre.Geometry2D;
using Elibre.Net.Core;
using Elibre.Net.Geometry;
using Elibre.Stitcher.Models.Stitching;
using StitcherRoofTrussApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign
{
    public static class StructuralExporter
    {
        #region Fields
        private static double _structureScale = 1.0 / 12;
        private static double _arcScale = 12;
        private static Document _document;
        private static EFraming.Application.Application _app;
        private static Project _project;
        #endregion


        #region Properties
        public static EFraming.Application.Application Application
        {
            get { return _app; }
        }
        public static Document Document
        {
            get { return _document; }
            set { _document = value; }
        }
        public static double ArchitictureScale
        {
            get { return _arcScale; }
            set { _arcScale = value; }
        }
        public static double StructureScale
        {
            get { return _structureScale; }
            set { _structureScale = value; }
        }

        public static StitcherBuilding StitcherBuilding
        {
            get
            {
                return EStitcherSingleton.GetInstance.Building;
            }
        }

        public static bool StructureBuildlingExported { get; set; }

        #endregion


        #region Main Function
        public static void ResetStructureBuilding()
        {
            StructureBuildlingExported = false;
            Document.Building.Solved = false;
            Document.Solved = false;
        }

        public static void RunAnalysis()
        {
            if (!Document.Building.Solved)
            {
                Document.Building.RunAnalysis(Document.Building.GetAnalysisOptions());
            }

        }

        public static void ExportStructureBuilding()
        {
            if (!StructureBuildlingExported)
            {
                ExportBuilding();
                StructureBuildlingExported = true;
            }
        }
        private static void ExportBuilding()
        {

            ClearAllFloors();
            CreateFoundationLevel();
            ExportLevels();
            ExportWalls();
            ExportColumns();
            ExportJoistArea();
            ExportBeams();
        }

        internal static void ClearAllFloors()
        {
            _document.Building.FloorList.ForEach(f => f.ObjectList = new List<FramingObject>());
            _document.Building.FloorList = new List<Floor>();
        }

        public static void CreateFoundationLevel()
        {
            Floor foundation = new Floor();
            foundation.SubFloorLevel = 0;
            foundation.FloorLevel = 0;
            foundation.Foundation = true;
            foundation.Building = _document.Building;
            foundation.ParentList = _document.Building.FloorList;

            foundation.TopPlateLevel = -Settings.DefaultFoundationThick;
            foundation.Name = "Foundation";
            foundation.CreateFloorPlanes();
            _document.Building.FloorList.Add(foundation);
        }

        public static void Create(StitcherBuilding sbuilding)
        {
            _app = EFraming.Application.Application.Create();
            var projInfo = new ProjectInfo();
            _app.CreateNewProject(projInfo);
            _project = _app.Workspace.ActiveProject;
            _document = _project.Document;
            _document.Building.FromStitcherModel = true;
            //SetManager Variables
            ReportsManager.Document = _document;
            DiagramsManager.StructureBuilding = _document.Building;
        }

        public static bool ExportWalls(bool exportOpening = true)
        {
            for (int i = 0; i < StitcherBuilding.Levels.Count; i++)
            {
                var level = StitcherBuilding.Levels[i];
                _document.Building.SetActiveFloor(StitcherBuilding.Levels[i].LevelName);
                var walls = level.FloorPlan.Walls;
                if (walls.Count > 1)
                {
                    //TODO Need to Hanlde if there is multi diaphragms or there is no Diaphragms
                    List<SlicePolygon2D> outerPolygons = new List<SlicePolygon2D>();
                    if (StitcherBuilding.Building.Plans.Any())
                    {
                        var slices = StitcherBuilding.Building.Plans[i].FloorPolygonSlices;
                        outerPolygons = slices.GetPolygons().GetOuterPolygons();
                    }
                    else
                    {
                        var slices = walls.SelectMany(o => o.GetPerimeterCoreSlices()).CreateCopy();
                        slices.Slice();
                        outerPolygons = slices.GetPolygons().GetOuterPolygons();
                    }
                    if (outerPolygons.Any())
                    {
                        outerPolygons.ForEach(e => CreateFloorDiaphragm(e));
                    }
                }
                foreach (var wall in walls)
                {
                    //var height = 120;
                    var corePoints = wall.GetPerimeterCorePoints();
                    var p1 = Get2DPoint(corePoints[0]);
                    var p2 = Get2DPoint(corePoints[1]);
                    var p3 = Get2DPoint(corePoints[2]);
                    var p4 = Get2DPoint(corePoints[3]);
                    var sp = p1.MidPointBetween(p4);
                    var ep = p2.MidPointBetween(p3);
                    var thickness = LineUtility.PointToLineProjection(p4, p1, p2).DistanceTo(p4);
                    if (sp.DistanceTo(ep) < Settings.MinWallSegmentLength)
                        continue;
                    var w = _document.AddStraightWall(sp, ep, new[] { p1, p2, p3, p4 }, wall.Thickness, wall.WallType());

                    if (w == null)
                    {
                        continue;
                    }
                    //(w.SegmentList[0] as StraightWallSegment).SetShearStitcherModel();

                    if (!exportOpening)
                    {
                        return true;
                    }
                    if (wall.Openings != null)
                    {

                        foreach (var o in wall.Openings)
                        {


                            AddOpening(o, w);
                        }
                    }
                    w.Bearing = wall.IsBearing=true; //TODO for test purpose only

                    var isShearSegments = wall.WallParts.Select(wp => wp.IsShear).ToList();

                    if (wall.SingleWall2D.Start.DistanceTo((w.CenterPoint1 * 12).ToPoint2D()) > wall.SingleWall2D.End.DistanceTo((w.CenterPoint1 * 12).ToPoint2D()))
                    {
                        isShearSegments.Reverse();
                    }

                    if (isShearSegments.Count == w.SegmentList.Count)
                    {
                        for (int j = 0; j < w.SegmentList.Count; j++)
                        {
                            if (isShearSegments[j] && w.SegmentList[j] is StraightWallSegment)
                            {
                                (w.SegmentList[j] as StraightWallSegment).SetShearStitcherModel();
                            }
                        }
                    }
                    //var segments = w.SegmentList.Where(seg => seg.GetType() != typeof(StraightWallOpening)).ToList();
                    //segments.ForEach(seg => (seg as StraightWallSegment).SetShearStitcherModel());
                }
                _document.Building.ActiveFloor.ObjectList.OfType<Post>().ToList().ForEach(p => UpdateUpliftPostsLocation(p));
            }

            return true;


        }
        public static void UpdateUpliftPostsLocation(Post post)
        {
            var owners = post.Owners;
            var originalParentWall = post.ParentWall as StraightWall;
            var originalParentWallSegment = post.ParentWallSegment;
            var straightWalls = owners.OfType<StraightWallSegment>().ToList();
            if (straightWalls.Count == 2)
            {
                var otherwall = straightWalls.Where(w => !ReferenceEquals(w, originalParentWallSegment))
                    .FirstOrDefault().ParentFramingWall as StraightWall;
                if (otherwall is StraightWall)
                {
                    var projectP1 = LineUtility.PointToLineProjection(otherwall.CenterPoint1, originalParentWall.CenterPoint1, originalParentWall.CenterPoint2);
                    var projectP2 = LineUtility.PointToLineProjection(otherwall.CenterPoint2, originalParentWall.CenterPoint1, originalParentWall.CenterPoint2);

                    if (LineUtility.IsPointOnLineSegment(projectP1, originalParentWall.CenterPoint1, originalParentWall.CenterPoint2) && LineUtility.IsPointOnLineSegment(projectP2, originalParentWall.CenterPoint1, originalParentWall.CenterPoint2))
                        return;
                    else
                    {
                        double w = post.GetSectionWidth();
                        double d = post.GetSectionDepth();
                        Point2d u = (otherwall.CenterPoint2 - otherwall.CenterPoint1).GetUnitVector();
                        Point2d v = LineUtility.GetLeftNormal(u);

                        var position = PostLocationManager.GetPostLocation(otherwall.Geom2D.VertexList, otherwall.CenterLine, otherwall.CenterPoint1.DistanceTo(post.Position) < otherwall.CenterPoint2.DistanceTo(post.Position) ? ConnectionJointLocation.Start : ConnectionJointLocation.End, w);

                        post.Position = position;
                        Point2d p1 = position - 0.5 * w * u;
                        Point2d p2 = position + 0.5 * w * u;

                        post.Geom2D.VertexList[0] = p1 - 0.5 * d * v;
                        post.Geom2D.VertexList[1] = p2 - 0.5 * d * v;
                        post.Geom2D.VertexList[2] = p2 + 0.5 * d * v;
                        post.Geom2D.VertexList[3] = p1 + 0.5 * d * v;

                        foreach (var seg in otherwall.SegmentList)
                        {
                            if (LineUtility.IsPointOnLineSegment(post.Position, seg.CenterLine.P1, seg.CenterLine.P2))
                            {
                                post.ParentWallSegment = seg;
                                break;
                            }
                        }
                        post.CreateGeometry3D(post.Floor, false);
                        (post.ParentWallSegment as StraightWallSegment).FillWithStuds();
                        (originalParentWallSegment as StraightWallSegment).FillWithStuds();
                    }
                }
            }
        }
        public static bool ExportColumns()
        {

            for (int i = 0; i < StitcherBuilding.Levels.Count; i++)
            {
                var level = StitcherBuilding.Levels[i];
                _document.Building.SetActiveFloor(level.LevelName);
                var columns = level.FloorPlan.Columns;

                foreach (var c in columns)
                {
                    _document.AddPost(Get2DPoint(c.MainPoint));
                }
            }

            return true;
        }

        public static bool ExportJoistArea()
        {

            for (int i = 0; i < StitcherBuilding.Levels.Count; i++)
            {
                var level = StitcherBuilding.Levels[i];
                _document.Building.SetActiveFloor(level.LevelName);
                var jareas = level.FloorPlan.JoistAreas;

                foreach (var ja in jareas)
                {
                    var pts = ja.OuterPoly.GetPoints(new SlicePolygon2DOptions()).Select(e => e.Get2DPoint() * StructureScale).ToList();
                    _document.AddPolygonJoistArea(pts);
                }
            }

            return true;
        }

        public static bool ExportBeams()
        {
            for (int i = 0; i < StitcherBuilding.Levels.Count; i++)
            {
                var level = StitcherBuilding.Levels[i];
                _document.Building.SetActiveFloor(level.LevelName);
                var beams = level.FloorPlan.Beams;

                foreach (var b in beams)
                {
                    var sp = Get2DPoint(b.BeamLine2D.Start);
                    _document.AddBeam(sp, Get2DPoint(b.BeamLine2D.End), sp);
                }
            }
            return true;
        }

        public static bool CreateLevel(StitcherLevel lvl)
        {
            double t1 = Settings.DefaultFloorFramingThick;
            double t2 = Settings.DefaultFloorSheathingThickness;
            double t = t1 + t2;
            Floor f = new Floor();
            f.Building = _document.Building;
            f.ParentList = _document.Building.FloorList;
            f.Name = lvl.LevelName;
            f.Building = _document.Building;
            f.SubFloorLevel = lvl.Elevation * _structureScale + t + Settings.DefaultPlateHeight; /*lvl.Height;*/
            f.FloorLevel = f.SubFloorLevel - t2;
            f.TopPlateLevel = f.SubFloorLevel - t2 - t1;
            f.ParentList = _document.Building.FloorList;
            _document.Building.FloorList.Add(f);
            f.CreateFloorPlanes();
            return true;
        }
        public static bool ExportLevels()
        {

            double t1 = Settings.DefaultFloorFramingThick;
            double t2 = Settings.DefaultFloorSheathingThickness;
            double t = t1 + t2;

            for (var i = 0; i < StitcherBuilding.TopWallLevels.Count; i++)
            {
                if (!StitcherBuilding.Levels[i].FloorPlan.Walls.Any())
                    continue;
                var lvl = StitcherBuilding.TopWallLevels[i];
                Floor f = new Floor();
                f.Building = _document.Building;
                f.ParentList = _document.Building.FloorList;
                f.Name = StitcherBuilding.Levels[i].LevelName;
                f.Building = _document.Building;
                f.ParentList = _document.Building.FloorList;
                _document.Building.FloorList.Add(f);
                f.SubFloorLevel = (lvl.Elevation + 20) * _structureScale; /*lvl.Height;*/
                //f.PlateHeight = 120;
                f.FloorLevel = f.SubFloorLevel - t2;
                f.TopPlateLevel = f.SubFloorLevel - t2 - t1;
                f.PlateHeight = f.TopPlateLevel - f.GetPreviousFloor().SubFloorLevel;

            }


            foreach (var f in _document.Building.FloorList)
            {
                f.CreateFloorPlanes();
            }

            return true;
        }

        public static void CreateFloorDiaphragm(SlicePolygon2D polygon, double thickness = 0)
        {

            var pts = polygon.GetPoints(new SlicePolygon2DOptions()).Select(e => e.Get2DPoint() * StructureScale).ToList();
            _document.AddPolylineDiaphragm(pts, FramingAreaLocation.Floor);
        }
        #endregion

        #region Helper Functions
        private static Section MapToSection(double thickness)
        {
            var section = Sections.WallSectionList;

            var result = section[0];
            double differ = double.MaxValue;
            double crntDiff = 0;
            foreach (var sec in section)
            {
                crntDiff = Math.Abs(sec.Depth - thickness);
                if (crntDiff < differ)
                {
                    result = sec;
                    differ = crntDiff;
                }
            }

            return result;
        }

        private static void AddOpening(StitcherOpening o, StraightWall w)
        {

            var centerLine = w.CenterLine;
            var sp = Get2DPoint(o.SingleLine2D.Start);
            var ep = Get2DPoint(o.SingleLine2D.End);
            sp = LineUtility.PointToLineProjection(sp, centerLine.P1, centerLine.P2);
            ep = LineUtility.PointToLineProjection(ep, centerLine.P1, centerLine.P2);
            var type = WallOpeningType.Window;
            if (o.OpeningType == OpeningType.Door)
            {
                type = WallOpeningType.Door;
            }

            var command = new AddSWOpeningCommand(sp, ep, o.HeaderHeight * _structureScale, o.SillHeight * _structureScale, type);
            w.Building.ExecuteCommand(command);

        }
        private static Point2d Get2DPoint(Elibre.Geometry2D.Point2D point2D)
        {
            return new Point2d(point2D.X, point2D.Y) * _structureScale;
        }
        #endregion

        #region Import  Polygons from Eframer Model 

        public static bool GetWalls(Document doc, out List<List<SlicePolygon3D>> polys)
        {

            var result = new List<List<SlicePolygon3D>>();
            var studList = new List<SlicePolygon3D>();
            var topPlateList = new List<SlicePolygon3D>();
            var botPlateList = new List<SlicePolygon3D>();
            var postsList = new List<SlicePolygon3D>();
            var headersList = new List<SlicePolygon3D>();
            var sillsList = new List<SlicePolygon3D>();
            var kingstudsList = new List<SlicePolygon3D>();
            polys = new List<List<SlicePolygon3D>>() { studList, headersList, postsList, topPlateList, botPlateList, sillsList, kingstudsList };

            var walls = doc.Building.FloorList.SelectMany(o => o.ObjectList.OfType<StraightWall>());

            foreach (var w in walls)
            {
                var studGeoms = w.SegmentList.SelectMany(e => e.StudList).Select(o => o.Geom3D);
                if (studGeoms.Any())
                    studList.AddRange(studGeoms.ToSlicePolygon3D());
                var posts = w.GetHeaderPosts().Select(e => e.Geom3D);
                if (posts.Any())
                    postsList.AddRange(studGeoms.ToSlicePolygon3D());

                var ks = w.GetKingStuds()?.Select(e => e.Geom3D);
                if (ks.Any())
                    kingstudsList.AddRange(ks.ToSlicePolygon3D());
                var headers = w.GetHeaders().Select(e => e.Geom3D);
                if (headers.Any())
                    headersList.AddRange(headers.ToSlicePolygon3D());

                var topPlate = w.TopPlateList?.Where(e => e.SegmentList != null).SelectMany(e => e.SegmentList)?.Select(e => e.Geom3D);
                if (topPlate.Any())
                    topPlateList.AddRange(topPlate.ToSlicePolygon3D());

                var botPlate = w.BottomPlateList?.Where(e => e.SegmentList != null).SelectMany(e => e.SegmentList)?.Select(e => e.Geom3D);
                if (botPlate.Any())
                    botPlateList.AddRange(new List<SlicePolygon3D>(botPlate.ToSlicePolygon3D()));


                var openings = w.SegmentList.OfType<StraightWallOpening>()?
                .Where(e => e.WallOpeningType == WallOpeningType.Window);
                var sills = openings?.Select(e => e.Sill);
                sills.Concat(openings?.Select(e => e.UPSill));

                if (sills.Any())
                    sillsList.AddRange(sills.Select(e => e.Geom3D).ToSlicePolygon3D());
                var s = openings.Select(e => e.TopSegment).SelectMany(e => e.StudList).Select(e => e.Geom3D);
                if (s.Any())
                    studList.AddRange(s.ToSlicePolygon3D());
                var doors = w.SegmentList.OfType<StraightWallOpening>()?
                    .Where(e => e.WallOpeningType == WallOpeningType.Door);

                var d = doors.Where(e => e.TopSegment != null).Select(e => e.TopSegment).SelectMany(e => e.StudList)?.Select(e => e.Geom3D);
                if (d.Any())
                    studList.AddRange(d.ToSlicePolygon3D());
            }

            return true;

        }
        public static List<SlicePolygon2D> GetWalls2D(Document document)
        {
            var result = new List<SlicePolygon2D>();
            var walls = document.Building.FloorList.SelectMany(e => e.ObjectList).OfType<StraightWall>();

            foreach (var w in walls)
            {
                result.Add(w.Geom2D.ToSlicePolygon2D(_arcScale));
            }
            return result;
        }
        public static List<Slice2D> GetStudsSlices(int lvlIndex)
        {
            var result = new List<Slice2D>();
            if (!_document.Building.IsFloorExist(lvlIndex + 1))
                return result;

            var floor = _document.Building.FloorList[lvlIndex + 1];

            if (floor == null)
                return result;

            var studs = floor.ObjectList.OfType<StraightWall>()
                .SelectMany(e => e.GetAllStudsExceptOpeningStuds()).ToList();
            foreach (var w in studs)
            {
                result.AddRange(w.Geom2D.ToSlices(_arcScale).Concat(w.Geom2D.GetXLines(_arcScale)));
            }
            return result;
        }
        public static List<Slice2D> GetPostsSlices(int lvlIndex)
        {
            var result = new List<Slice2D>();
            var floor = _document.Building.FloorList[lvlIndex + 1];
            if (floor == null)
                return result;

            var walls = floor.ObjectList.OfType<StraightWall>().SelectMany(e => e.SegmentList.Where(o => o.Shear));
            var posts = new List<Post>();


            foreach (var w in walls)
            {
                if (w.StartUpliftPost != null)
                {
                    posts.Add(w.StartUpliftPost);

                }
                if (w.EndUpliftPost != null)
                {
                    posts.Add(w.EndUpliftPost);
                }

            }
            result.AddRange(posts.SelectMany(e => e.Geom2D.ToSlices(_arcScale).Concat(e.Geom2D.GetXLines(_arcScale))));
            return result;
        }

        internal static List<Slice2D> GetDiaphragms(int lvlIndex)
        {
            var result = new List<Slice2D>();
            var floor = _document.Building.FloorList[lvlIndex + 1];

            if (floor == null)
                return result;
            var diaphragms = floor.ObjectList.OfType<Diaphragm>();

            foreach (var d in diaphragms)
            {
                var s = d.Polygon.SegmentList;
                result.AddRange(s.ToSlices(_arcScale));
            }

            return result;
        }


        #endregion
    }

}
