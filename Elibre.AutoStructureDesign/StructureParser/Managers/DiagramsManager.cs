﻿
using EFraming.Objects;
using Elibre.Geometry2D;
using Elibre.Net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign
{
    public static class DiagramsManager
    {


        private static double _arcScale = 12;

        public static Building StructureBuilding { get; set; }
        public static double ArchitictureScale
        {
            get { return _arcScale; }
            set { _arcScale = value; }
        }
        #region Methods

        #region Seismic


        public static DiagramSlices GetSeismicDiagram(int lvlIndex, DiagramType type)
        {
            var diagramSlices = new DiagramSlices();

            var floor = StructureBuilding.FloorList[lvlIndex + 1];

            if (floor == null)
                return null;
            var d = floor.ObjectList.OfType<Diaphragm>().FirstOrDefault();
            if (d == null || d.AnalysisData == null)
                return null;



            Diagram hDiagram = null;
            Diagram vDiagram = null;

            switch (type)
            {
                case DiagramType.DistributedLoad:
                    hDiagram = d.AnalysisData.SeismicLoadDiagram_h;
                    vDiagram = d.AnalysisData.SeismicLoadDiagram_v;
                    break;
                case DiagramType.ShearForce:
                    hDiagram = d.AnalysisData.SeismicShearDiagram_h;
                    vDiagram = d.AnalysisData.SeismicShearDiagram_v;
                    break;
                case DiagramType.BendingMoment:
                    hDiagram = d.AnalysisData.SeismicMomentDiagram_h;
                    vDiagram = d.AnalysisData.SeismicMomentDiagram_v;
                    break;
            }

            SetDiagramTextInfo(d, diagramSlices);
            SetShearLine(d, diagramSlices, ActiveWindResults.North, ActiveResults.Seismic);
            var flip = type != DiagramType.BendingMoment ? true : false;
            SetDiagramInfo(d, hDiagram, vDiagram, diagramSlices, flip);

            return diagramSlices;


        }

        private static void SetDiagramInfo(Diaphragm d, Diagram hDiagram, Diagram vDiagram, DiagramSlices slices, bool flipYdir)
        {
            var hatchLines = new List<Slice2D>();
            var borderLines = new List<Slice2D>();

            var vForce = vDiagram.ShearForce.OrderBy(e => e.w).First();
            slices.LoadValues.Add(vForce.w.ToString() + "lb");

            var hForce = hDiagram.ShearForce.OrderBy(e => e.w).First();
            slices.LoadValues.Add(hForce.w.ToString() + "lb");

            var hPoint = d.BoundingRect.FirstCorner * ArchitictureScale;
            var vPoint = d.BoundingRect.FourthCorner * ArchitictureScale;
            var offset = -10 * 12;
            hatchLines.AddRange(hDiagram.GetHatchSlices(hPoint, offset, 0));
            hatchLines.AddRange(vDiagram.GetHatchSlices(vPoint, 0, -offset, flipYdir));

            borderLines.AddRange(hDiagram.GetBorderSlices(hPoint, offset, 0));
            borderLines.AddRange(vDiagram.GetBorderSlices(vPoint, 0, -offset, flipYdir));



            slices.HatchLines = hatchLines;
            slices.BorderLines = borderLines;

        }



        #endregion

        #region Wind
        public static DiagramSlices GetWindDiagram(int lvlIndex, ActiveWindResults activeWindResults, DiagramType type)
        {
            var diagramSlices = new DiagramSlices();


            //// Guards
            var floor = StructureBuilding.FloorList[lvlIndex + 1];
            if (floor == null)
                return null;
            var d = floor.ObjectList.OfType<Diaphragm>().FirstOrDefault();
            if (d == null || d.AnalysisData == null)
                return null;
            /////
            SetDiagramTextInfo(d, diagramSlices);
            SetShearLine(d, diagramSlices, activeWindResults, ActiveResults.Wind);

            bool flipYdir = type != DiagramType.BendingMoment ? true : false;
            Diagram targetDiagram = null;
            switch (type)
            {
                case DiagramType.Unknown:
                    break;
                case DiagramType.DistributedLoad:
                    targetDiagram = WindLoadDiagram(d, diagramSlices, activeWindResults);
                    break;
                case DiagramType.ShearForce:
                    targetDiagram = WindShearDiagram(d, diagramSlices, activeWindResults);
                    break;
                case DiagramType.BendingMoment:
                    targetDiagram = WindMomentDiagram(d, diagramSlices, activeWindResults);
                    break;
                case DiagramType.Basic:
                    break;
                default:
                    break;

            }

            SetWindDiagramInfo(d, targetDiagram, diagramSlices, activeWindResults, flipYdir);
            return diagramSlices;
        }

        private static void SetWindDiagramInfo(Diaphragm d, Diagram targetDiagram, DiagramSlices slices, ActiveWindResults activeWindResults, bool flipYdir)
        {
            var hatchLines = new List<Slice2D>();
            var borderLines = new List<Slice2D>();
            bool isHorizontal = false;

            if (activeWindResults == ActiveWindResults.East || activeWindResults == ActiveWindResults.West)
                isHorizontal = true;

            var hPoint = d.BoundingRect.FirstCorner * ArchitictureScale;
            var vPoint = d.BoundingRect.FourthCorner * ArchitictureScale;
            float offset = -10 * 12;

            switch (activeWindResults)
            {
                case ActiveWindResults.North:
                    vPoint = d.BoundingRect.FirstCorner * ArchitictureScale;
                    offset = -offset;
                    break;
                case ActiveWindResults.South:
                    break;
                case ActiveWindResults.East:
                    hPoint = d.BoundingRect.SecondCorner * ArchitictureScale;
                    offset = -1.5f * offset;

                    break;
                case ActiveWindResults.West:

                    offset = 1.5f * offset;
                    break;
                default:
                    break;
            }
            if (isHorizontal)
            {
                hatchLines.AddRange(targetDiagram.GetHatchSlices(hPoint, offset, 0));
                borderLines.AddRange(targetDiagram.GetBorderSlices(hPoint, offset, 0));
            }
            else
            {
                hatchLines.AddRange(targetDiagram.GetHatchSlices(vPoint, 0, -offset, flipYdir));
                borderLines.AddRange(targetDiagram.GetBorderSlices(vPoint, 0, -offset, flipYdir));
            }
            slices.HatchLines = hatchLines;
            slices.BorderLines = borderLines;
        }

        private static void SetShearLine(Diaphragm d, DiagramSlices diagramSlices, ActiveWindResults activeWindResults, ActiveResults activeResults)
        {

            var supports = d.HSupportList.Concat(d.VSupportList);
            var extension = ShearLineSlice.Extension;
            var rad = ShearLineSlice.CircleRadius * 12;

            foreach (var s in supports)
            {
                var sLine = new ShearLineSlice();
                Point2d u = (s.RenderPoint2 - s.RenderPoint1).GetUnitVector();

                Point2d p1 = (s.RenderPoint1 - extension * u) * ArchitictureScale;
                Point2d p2 = (s.RenderPoint2 + extension * u) * ArchitictureScale; //extend the lower extension line

                sLine.Line = new Slice2D(new Geometry2D.Point2D(p1.x, p1.y), new Geometry2D.Point2D(p2.x, p2.y));
                sLine.Name = s.Name;
                sLine.ReactionText = GetReactionText(s, activeWindResults, activeResults);
                if (s.ShearLine.Orientation == Orientation.Horizontal)
                {
                    sLine.CirclePosition = new Geometry2D.Point2D(p1.x - rad, p1.y);
                    sLine.Angle = 90;
                }
                else
                {
                    sLine.CirclePosition = new Geometry2D.Point2D(p1.x, p1.y - rad); //flip the orientation in y-dir
                }
                diagramSlices.ShearLines.Add(sLine);
            }

        }

        private static string GetReactionText(DiaphragmSupport support, ActiveWindResults activeWindResults, ActiveResults activeResults)
        {

            DiaphragmSupportAnalysisData data = (support.Common && (support.CommonSupAnalysisData != null)) ? support.CommonSupAnalysisData : support.AnalysisData;
            string text = "";
            string result_type = "";
            double reaction = 0;
            switch (activeResults)
            {
                case ActiveResults.Seismic:
                    result_type = "S";
                    reaction = data.SeismicReactionMaxCumulative.total;
                    break;
                case ActiveResults.Wind:
                    switch (activeWindResults)
                    {
                        case ActiveWindResults.North:
                            if (support.ShearLine.Orientation == Orientation.Vertical)
                            {
                                result_type = "WN";
                                reaction = data.NorthReactionComulativeWind.total;
                            }

                            break;

                        case ActiveWindResults.South:
                            if (support.ShearLine.Orientation == Orientation.Vertical)
                            {
                                result_type = "WS";
                                reaction = data.SouthReactionComulativeWind.total;
                            }


                            break;

                        case ActiveWindResults.East:
                            if (support.ShearLine.Orientation == Orientation.Horizontal)
                            {
                                result_type = "WE";
                                reaction = data.EastReactionComulativeWind.total;
                            }

                            break;

                        case ActiveWindResults.West:
                            if (support.ShearLine.Orientation == Orientation.Horizontal)
                            {
                                result_type = "WW";
                                reaction = data.EastReactionComulativeWind.total;
                            }
                            break;

                    }
                    break;
                default:
                    break;

            }
            if (string.IsNullOrEmpty(result_type))
                return "";

            return text = result_type + " " + Math.Round(reaction, MidpointRounding.AwayFromZero).ToString() + " " + "lbs";
        }

        private static void SetDiagramTextInfo(Diaphragm d, DiagramSlices ds)
        {
            ds.Text = d.GetDisplayText();
            var c = d.Polygon.Centroid;
            ds.TextPosition = new Geometry2D.Point2D(c.x * 12, c.y * 12);
        }

        private static Diagram WindShearDiagram(Diaphragm d, DiagramSlices ds, ActiveWindResults activeWindResults)
        {
            Diagram target = null;
            switch (activeWindResults)
            {
                case ActiveWindResults.North:
                    target = d.AnalysisData.WindShearForceDiagramNS.Diagram;
                    break;
                case ActiveWindResults.South:
                    target = d.AnalysisData.WindShearForceDiagramSN.Diagram;
                    break;
                case ActiveWindResults.East:
                    target = d.AnalysisData.WindShearForceDiagramEW.Diagram;
                    break;
                case ActiveWindResults.West:
                    target = d.AnalysisData.WindShearForceDiagramWE.Diagram;
                    break;
                default:
                    break;
            }

            return target;

        }
        private static Diagram WindLoadDiagram(Diaphragm d, DiagramSlices ds, ActiveWindResults activeWindResults)
        {
            Diagram target = null;
            switch (activeWindResults)
            {
                case ActiveWindResults.North:
                    target = d.AnalysisData.WindLoadDiagramNS.Diagram;
                    break;
                case ActiveWindResults.South:
                    target = d.AnalysisData.WindLoadDiagramSN.Diagram;
                    break;
                case ActiveWindResults.East:
                    target = d.AnalysisData.WindLoadDiagramEW.Diagram;
                    break;
                case ActiveWindResults.West:
                    target = d.AnalysisData.WindLoadDiagramWE.Diagram;
                    break;
                default:
                    break;
            }

            return target;

        }
        private static Diagram WindMomentDiagram(Diaphragm d, DiagramSlices ds, ActiveWindResults activeWindResults)
        {

            Diagram target = null;
            switch (activeWindResults)
            {
                case ActiveWindResults.North:
                    target = d.AnalysisData.WindBendingMomentDiagramNS.Diagram;
                    break;
                case ActiveWindResults.South:
                    target = d.AnalysisData.WindBendingMomentDiagramSN.Diagram;
                    break;
                case ActiveWindResults.East:
                    target = d.AnalysisData.WindBendingMomentDiagramEW.Diagram;
                    break;
                case ActiveWindResults.West:
                    target = d.AnalysisData.WindBendingMomentDiagramWE.Diagram;
                    break;
                default:
                    break;
            }

            return target;
        }
        #endregion
        #endregion



        #region ExtensionMethods

        private static List<Slice2D> GetHatchSlices(this Diagram ld, Point2d basePoint, double xTranslate = 0, double yTranslate = 0, bool flipDirection = false)
        {

            var result = new List<Slice2D>();
            //ld.Mirror(1);

            var xScale = ld.XScale;
            var yScale = ld.YScale;

            var hatch = new List<Line2d>();
            if (flipDirection)
            {

                var rec = ld.ParentDiaphragm.BoundingRect;
                var ml = new Line2d(rec.FourthCorner, rec.ThirdCorner);
                foreach (var l in ld.HatchLines)
                {
                    hatch.Add(MathClass.MirrorLine(l, ml));
                }

            }
            else
            {
                hatch = ld.HatchLines;
            }

            var lines = hatch.ToSlices(_arcScale);

            foreach (var l in lines)
            {
                l.Scale(xScale, yScale);
                l.Start = new Geometry2D.Point2D(l.Start.X + xTranslate + basePoint.x, l.Start.Y + yTranslate + basePoint.y);
                l.End = new Geometry2D.Point2D(l.End.X + xTranslate + basePoint.x, l.End.Y + yTranslate + basePoint.y);
                //l.Translate(40, 0);
                result.Add(l);
            }

            return result;
        }




        private static List<Slice2D> GetBorderSlices(this Diagram ld, Point2d basePoint)
        {
            var result = new List<Slice2D>();

            var xScale = ld.XScale;
            var yScale = ld.YScale;

            var verticesSlices = ld.VertexList.ToSlices(_arcScale);
            foreach (var s in verticesSlices)
            {
                s.Translate(basePoint.x * _arcScale, basePoint.y * _arcScale);
                s.Scale(xScale, yScale);
                result.Add(s);
            }

            return result;
        }


        private static List<Slice2D> GetBorderSlices(this Diagram ld, Point2d basePoint, double xTranslate = 0, double yTranslate = 0, bool flipDirection = false)
        {




            var result = new List<Slice2D>();
            //ld.Mirror(1);

            var xScale = ld.XScale;
            var yScale = ld.YScale;

            var vertices = new List<Point2d>();
            if (flipDirection)
            {

                var rec = ld.ParentDiaphragm.BoundingRect;
                var ml = new Line2d(rec.FourthCorner, rec.ThirdCorner);
                foreach (var p in ld.VertexList)
                {
                    vertices.Add(MathClass.MirrorPoint(p, ml));
                }

            }
            else
            {
                vertices = ld.VertexList;
            }

            var lines = vertices.ToSlices(_arcScale);

            foreach (var l in lines)
            {
                l.Scale(xScale, yScale);
                l.Start = new Geometry2D.Point2D(l.Start.X + xTranslate + basePoint.x, l.Start.Y + yTranslate + basePoint.y);
                l.End = new Geometry2D.Point2D(l.End.X + xTranslate + basePoint.x, l.End.Y + yTranslate + basePoint.y);
                //l.Translate(40, 0);
                result.Add(l);
            }

            return result;




        }
        #endregion
    }


    public class DiagramSlices
    {
        public List<Slice2D> HatchLines { get; set; }
        public List<Slice2D> BorderLines { get; set; }
        public List<ShearLineSlice> ShearLines { get; set; }
        public string Text { get; set; }
        public Geometry2D.Point2D TextPosition { get; set; }

        public List<string> LoadValues { get; set; }
        public List<EFraming.Objects.Point2D> LoadValuesPositions { get; set; }
        public DiagramSlices()
        {
            ShearLines = new List<ShearLineSlice>();
            LoadValues = new List<string>();
            LoadValuesPositions = new List<EFraming.Objects.Point2D>();
        }

        public List<Slice2D> GetSlices()
        {
            var result = new List<Slice2D>();
            if (HatchLines != null || HatchLines.Any())
                result.AddRange(HatchLines);


            if (ShearLines != null || ShearLines.Any())
                result.AddRange(ShearLines.Select(e => e.Line));
            return result;
        }
    }

    public class ShearLineSlice
    {
        public static float CircleRadius { get; set; } = 1.5f;
        public static float Extension { get; set; } = 3f;
        public Slice2D Line { get; set; }
        public Geometry2D.Point2D CirclePosition { get; set; }
        public float Angle { get; set; }
        public string Name { get; set; }
        public string ReactionText { get; set; }
    }
}
