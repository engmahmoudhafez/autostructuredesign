﻿using EFraming;
using EFraming.Design;
using EFraming.Design.Forms;
using EFraming.Objects;
using ShearWallDesign;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign
{
    public class ReportsManager
    {
        #region EnableDisableReports

        public static bool EnableSeismicLoadReport { get; set; }
        public static bool EnableWindLoadReport { get; set; }
        public static bool EnableHoleDownScheduleReport { get; set; }
        public static bool EnableShearWallScheduleReport { get; set; }
        public static bool EnableShearWallDesignReport { get; set; }
        public static bool EnableAtsDesignReport { get; set; }
        public static bool EnableDiaphragmDesignReport { get; set; }
        public static bool EnableUpliftCalculationsReport { get; set; }


        public static bool EnablePostDesignReport { get; set; }
        public static bool EnableBeamDesignReport { get; set; }
        public static bool EnableHeaderDesignReport { get; set; }
        public static bool EnableJoistDesignReport { get; set; }
        public static bool EnableTrussReport { get; set; }

        #endregion
        public static Document Document { get; set; }
        public static void SummaryReport()
        {
            if (Document.Building.Solved)
            {
                using (frmSummaryReport frm = new frmSummaryReport(Document,Document.Building, SummaryReportType.Both))
                {
                    DialogResult dResult = frm.ShowDialog();
                }
            }

            else
            {

            }

        }


        #region Lateral Design Report
        public static void SeismicLoadReport()
        {
            ShowReport(ReportGenerator.ReportName.SeismicLoad, ReportGenerator.ReportGenerator.RptFilePath_Lateral_SeismicLoad, -1, "");
        }
        public static void WindLoadReport()
        {
            ShowReport(ReportGenerator.ReportName.WindLoad, ReportGenerator.ReportGenerator.RptFilePath_Lateral_WindLoad, -1, "");
        }
        public static void HoleDownScheduleReport()
        {
            ProgramShearWallTypesSettingData.ShearWallDesignGroup_Report = Document.Building.ShearWallDesignGroup;
            ShowReport(ReportGenerator.ReportName.HoleDownSchedule, "", -1, "");
        }
        public static void ShearWallScheduleReport()
        {
            switch (Document.Building.SeismicWindAnalysisData.seismicWindInput.loadCode)
            {
                //crystal reports
                /*case SeismicWind.LoadCode.IBC2012:
                    ShearWallDesign.ProgramShearWallTypesSettingData.ShearWallDesignGroup_Report = _workspace.ActiveProject.Document.Building.ShearWallDesignGroup;
                    ShowReport(ReportGenerator.ReportName.ShearWallSchedule, "", -1, "");
                    break;*/


                //reportviewer
                case SeismicWind.LoadCode.IBC2012:
                    ShearWallDesign.ProgramShearWallTypesSettingData.ShearWallDesignGroup_Report = Document.Building.ShearWallDesignGroup;
                    ShowReport(ReportGenerator.ReportName.ShearWallSchedule, "", -1, "");
                    break;

                case SeismicWind.LoadCode.IRC2015:
                    ShowReport(ReportGenerator.ReportName.ShearWallSchedule_IRC, "", -1, "");
                    break;

                    /*case SeismicWind.LoadCode.IRC2015:
                        ShowReport(ReportGenerator.ReportName.ShearWallSchedule_IRC, "", -1, "");
                        break;*/
            }
        }
        public static void fTAOReport()
        {
            ShearWallDesign.ProgramShearWallTypesSettingData.ShearWallDesignGroup_Report = Document.Building.ShearWallDesignGroup;
            ShowReport(ReportGenerator.ReportName.FTAOReport, ReportGenerator.ReportGenerator.RptFilePath_FTAO, -1, "");
        }
        public static void ShearWallDesignReport()
        {
            switch (Document.Building.SeismicWindAnalysisData.seismicWindInput.loadCode)
            {
                /*case SeismicWind.LoadCode.IBC2012:
                    ShowReport(ReportGenerator.ReportName.ShearWallDesign, ReportGenerator.ReportGenerator.RptFilePath_Lateral_ShearWallDesign, -1, "");
                    break;*/

                case SeismicWind.LoadCode.IBC2012:
                    ShowReport(ReportGenerator.ReportName.ShearWallDesign, ReportGenerator.ReportGenerator.RptFilePath_Lateral_ShearWallDesign, -1, "");
                    break;
            }
        }
        public static void AtsDesignReport()
        {
            ShowReport(ReportGenerator.ReportName.AtsDesign, ReportGenerator.ReportGenerator.RptFilePath_Lateral_AtsDeign, -1, "");
        }
        public static void DiaphragmDesignReport()
        {
            ShowReport(ReportGenerator.ReportName.DiaphragmDesign, ReportGenerator.ReportGenerator.RptFilePath_Lateral_DiaphragmDesign, -1, "");
        }
        public static void UpliftCalculationsReport()
        {
            ShowReport(ReportGenerator.ReportName.UpliftCalculations, ReportGenerator.ReportGenerator.RptFilePath_Framing_UpliftCalculations, -1, "");
        }
        #endregion
        #region Design Reports

        public static void PostDesignReport()
        {
            ShowReport(ReportGenerator.ReportName.PostDesign, ReportGenerator.ReportGenerator.RptFilePath_Framing_PostDesign, -1, "");
        }
        public static void BeamDesignReport()
        {
            ShowReport(ReportGenerator.ReportName.BeamDesign, ReportGenerator.ReportGenerator.RptFilePath_Framing_BeamDesign, -1, "");
        }
        public static void HeaderDesignReport()
        {
            ShowReport(ReportGenerator.ReportName.HeaderDesign, ReportGenerator.ReportGenerator.RptFilePath_Framing_HeaderDesign, -1, "");
        }
        public static void JoistDesignReport()
        {
            ShowReport(ReportGenerator.ReportName.JoistDesign, ReportGenerator.ReportGenerator.RptFilePath_Framing_JoistDesign, -1, "");
        }
        public static void TrussReport()
        {
            ShowReport(ReportGenerator.ReportName.TrussReport, ReportGenerator.ReportGenerator.RptFilePath_Truss, -1, "");
        }
        protected static void ShowReport(ReportGenerator.ReportName report, string reportDataPath, int floorID, string objID)
        {
            FramingDesign.ShowReport(null, Document, report, reportDataPath, floorID, objID);
        }

        public static void DisableLateralDesignReports()
        {
            EnableSeismicLoadReport = false;
            EnableWindLoadReport = false;
            EnableHoleDownScheduleReport = false;
            EnableShearWallScheduleReport = false;
            EnableShearWallDesignReport = false;
            EnableAtsDesignReport = false;
            EnableDiaphragmDesignReport = false;
            EnableUpliftCalculationsReport = false;
        }
        public static void EnableLateralDesignReports()
        {
            EnableSeismicLoadReport = true;
            EnableWindLoadReport = true;
            EnableHoleDownScheduleReport = true;
            EnableShearWallScheduleReport = true;
            EnableShearWallDesignReport = true;
            EnableAtsDesignReport = true;
            EnableDiaphragmDesignReport = true;
            EnableUpliftCalculationsReport = true;
        }
        public static void EnableFramingDesignReports()
        {
            EnablePostDesignReport = true;
            EnableBeamDesignReport = true;
            EnableHeaderDesignReport = true;
            EnableJoistDesignReport = true;
            EnableTrussReport = true;
        }
        public static void DisableFramingDesignReports()
        {
            EnablePostDesignReport = false;
            EnableBeamDesignReport = false;
            EnableHeaderDesignReport = false;
            EnableJoistDesignReport = false;
            EnableTrussReport = false;
        }
        public static void DisableAllReports()
        {
            DisableFramingDesignReports();
            DisableLateralDesignReports();
        }
        #endregion
    }
}
