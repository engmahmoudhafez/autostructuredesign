﻿using EFraming;
using EFraming.Objects;
using Elibre.Net.Core;
using Elibre.Net.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign
{
    public static class DocumentExtensions
    {
        public static StraightWall AddStraightWall(this Document doc, Point2d p1, Point2d p2, Point2d[] Geom2D, double thickness, WallUse use = WallUse.Exterior, TrimMode tm = TrimMode.Manual)
        {
            var building = doc.Building;
            if ((p1 - p2).GetLength() < Settings.MinWallSegmentLength)
                return null;

            if (building == null)
                building = new Building();

            Alignment align = Alignment.Center;
            var studSection = new Section()
            {
                Width = 0.5 * thickness,
                Depth = thickness
            };
            studSection.Area = studSection.Width * studSection.Depth;
            studSection.Ix = studSection.Width * Math.Pow(studSection.Depth, 3) / 12.0;
            studSection.Iy = studSection.Depth * Math.Pow(studSection.Width, 3) / 12.0;
            studSection.Sxx = studSection.Ix * 2 * studSection.Depth;
            studSection.Syy = studSection.Iy * 2 * studSection.Width;

            if (!MathClass.IsLowerPoint(p1, p2))
            {

                Geom2D = Geom2D.Reverse().ToArray();
                var rep = p1;
                p1 = p2;
                p2 = rep;
            }


            var geom2d = new Geom2D(Geom2D);
            StraightWall wall = new StraightWall(p1, p2, building.ActiveFloor, align)
            {
                Geom2D = geom2d,
                IncludeOwnWeight = Settings.WallTemplate.IncludeOwnWeight,
                Section = studSection,
                Use = use
            }
            ;
            wall.SetSection(studSection);
            wall.Geom2D = geom2d;
            geom2d.Parent = wall;

            wall.CreateGeometryForStitcher(p1, p2, align);
            wall.CreateGeometry3D();


            AddFramingObjectCommand command = new AddFramingObjectCommand(wall);
            doc.ExecuteCommand(command);

            building.RefreshWallSegmentsAndOpeningsNames();

            var postSection = new Section()
            {
                Width = thickness,
                Depth = thickness
            };
            postSection.Area = postSection.Width * postSection.Depth;
            postSection.Ix = postSection.Width * Math.Pow(postSection.Depth, 3) / 12.0;
            postSection.Iy = postSection.Depth * Math.Pow(postSection.Width, 3) / 12.0;
            postSection.Sxx = postSection.Ix * 2 * postSection.Depth;
            postSection.Syy = postSection.Iy * 2 * postSection.Width;

            building.ActiveFloor.ObjectList.OfType<Post>().ToList().ForEach(p => p.SetSection(postSection));


            if (command.Executed)
            {
                return wall;
            }
            else
            {
                wall.Dispose();
                return null;
            }
        }

        public static bool AddPolylineDiaphragm(this Document doc, List<Point2d> v, FramingAreaLocation loc)
        {

            var building = doc.Building;

            Diaphragm diaphragm = new Diaphragm(v.ToArray(), building.ActiveFloor);
            diaphragm.Location = FramingAreaLocation.Floor;

            AddFramingObjectCommand command = new AddFramingObjectCommand(diaphragm);

            bool error;
            bool cancel = false;
            foreach (var d in building.ActiveFloor.ObjectList.OfType<Diaphragm>().Cast<Diaphragm>())
            {
                error = false;
                error = PolygonUtility.ArePolygonsOverlapped(d.VertexList.ToArray(), v.ToArray());
                if (error)
                {
                    var result = MessageBox.Show("The newely added diaphragm is overlapped with another diaphragm. Do you want to continue", "Overlapped Diaphragm", MessageBoxButtons.OKCancel);
                    if (result == DialogResult.Cancel)
                    {
                        cancel = true;
                    }
                    break;
                }
            }

            if (!cancel)
            {
                doc.ExecuteCommand(command);
            }

            return command.Executed;

        }
    }
}
