﻿using EFraming;
using EFraming.Objects;
using Elibre.Geometry2D;
using Elibre.Net.Core;
using Elibre.Net.Geometry;
using Elibre.Stitcher.Models.Stitching;
using StitcherRoofTrussApp.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign
{
    public static class EframerExtensions
    {

        public static List<SlicePolygon3D> ToSlicePolygon3D(this List<EFraming.Objects.Geom3D> geo3d)
        {
            var polygons = new List<SlicePolygon3D>();
            foreach (var g in geo3d)
            {
                if (g == null)
                {
                    continue;
                }
                if (g.TrianglesList != null)
                {
                    foreach (var tr in g.TrianglesList)
                    {
                        var p = new List<Elibre.Geometry2D.Point3D>() {
                           g.VertexList[tr.ID1].ToPoint3D(),
                            g.VertexList[tr.ID2].ToPoint3D(),
                            g.VertexList[tr.ID3].ToPoint3D() };
                        polygons.Add(new SlicePolygon3D(p));

                    }
                }

                if (g.QuadList != null)
                {
                    foreach (var q in g.QuadList)
                    {
                        var p = new List<Elibre.Geometry2D.Point3D>() {
                           g.VertexList[q.ID1].ToPoint3D(),
                           g.VertexList[q.ID2].ToPoint3D(),
                           g.VertexList[q.ID3].ToPoint3D(),
                           g.VertexList[q.ID4].ToPoint3D() };
                        polygons.Add(new SlicePolygon3D(p));

                    }
                }
            }
            return polygons;
        }

        public static IEnumerable<SlicePolygon3D> ToSlicePolygon3D(this IEnumerable<EFraming.Objects.Geom3D> geo3d)
        {
            var polygons = new List<SlicePolygon3D>();
            foreach (var g in geo3d)
            {
                if (g == null)
                {
                    continue;
                }
                if (g.TrianglesList != null)
                {
                    foreach (var tr in g.TrianglesList)
                    {
                        var p = new List<Elibre.Geometry2D.Point3D>() {
                            g.VertexList[tr.ID1].ToPoint3D(),
                            g.VertexList[tr.ID2].ToPoint3D(),
                            g.VertexList[tr.ID3].ToPoint3D() };
                        polygons.Add(new SlicePolygon3D(p));

                    }
                }

                if (g.QuadList != null)
                {
                    foreach (var q in g.QuadList)
                    {
                        var p = new List<Elibre.Geometry2D.Point3D>() {
                            g.VertexList[q.ID1].ToPoint3D(),
                            g.VertexList[q.ID2].ToPoint3D(),
                            g.VertexList[q.ID3].ToPoint3D(),
                            g.VertexList[q.ID4].ToPoint3D() };
                        polygons.Add(new SlicePolygon3D(p));

                    }
                }
            }
            return polygons;
        }
        public static Vector2D ToVector2D(this Elibre.Geometry2D.Point2D p)
        {
            return new Vector2D(p.X, p.Y);
        }
        public static Vector3D ToVector3D(this Elibre.Geometry2D.Point2D p)
        {
            return new Vector3D(p.X, p.Y, 0);
        }
        public static Vector3D ToVector3D(this Elibre.Geometry2D.Point3D p)
        {
            return new Vector3D(p.X, p.Y, p.Z);
        }
        public static Vector2D ToVector2D(this Elibre.Geometry2D.Point3D p)
        {
            return new Vector2D(p.X, p.Y);
        }
        public static Elibre.Geometry2D.Point3D ToPoint3D(this Vector3D p)
        {
            return new Elibre.Geometry2D.Point3D(p.X, p.Y, p.Z);
        }
        public static Elibre.Geometry2D.Point2D ToPoint2D(this Vector3D p)
        {
            return new Elibre.Geometry2D.Point2D(p.X, p.Y);
        }
        public static Elibre.Geometry2D.Point2D ToPoint2D(this Vector2D p)
        {
            return new Elibre.Geometry2D.Point2D(p.X, p.Y);
        }
        public static Elibre.Geometry2D.Point3D ToPoint3D(this Vector2D p)
        {
            return new Elibre.Geometry2D.Point3D(p.X, p.Y, 0);
        }
        public static Elibre.Geometry2D.Point3D ToPoint3D(this Point3d p)
        {
            return new Elibre.Geometry2D.Point3D(p.x, p.y, p.z);
        }
        public static Elibre.Geometry2D.Point2D ToPoint2D(this Point2d p)
        {
            return new Elibre.Geometry2D.Point2D(p.x, p.y);
        }
        public static Point3d ToPoint3d(this Geometry2D.Point3D p)
        {
            return new Point3d(p.X, p.Y,p.Z);
        }
        public static void AppendText(this RichTextBox box, string text, Color color, bool AddNewLine = true)
        {
            if (AddNewLine)
            {
                text += Environment.NewLine;
            }

            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }

        public static WallUse WallType(this StitcherWall wall)
        {
            if (wall.IsInteriorWall)
                return WallUse.Interior;
            else
                return WallUse.Exterior;
        }


        public static SlicePolygon2D ToSlicePolygon2D(this Geom2D geom)
        {
            var slices = new List<Slice2D>();
            var n = geom.VertexList.Length;
            var v = geom.VertexList;
            var k = 0;
            for (int i = 0; i < n; i++)
            {
                k = (i + 1) % n;
                slices.Add(new Slice2D(Get2DPoint(v[i]), Get2DPoint(v[k])));
            }

            return new SlicePolygon2D(slices);
        }

        public static SlicePolygon2D ToSlicePolygon2D(this Geom2D geom, double scale)
        {
            return new SlicePolygon2D(ToSlices(geom, scale));
        }
        public static List<Slice2D> ToSlices(this Geom2D geom, double scale)
        {
            var slices = new List<Slice2D>();
            var n = geom.VertexList.Length;
            var v = geom.VertexList;
            var k = 0;
            for (int i = 0; i < n; i++)
            {
                k = (i + 1) % n;
                slices.Add(new Slice2D(Get2DPoint(v[i] * scale), Get2DPoint(v[k] * scale)));
            }

            return slices;
        }

        public static List<Slice2D> ToSlices(this List<Point2d> v, double scale)
        {
            var slices = new List<Slice2D>();
            var n = v.Count;

            var k = 0;
            for (int i = 0; i < n; i++)
            {
                k = (i + 1) % n;
                slices.Add(new Slice2D(Get2DPoint(v[i] * scale), Get2DPoint(v[k] * scale)));
            }

            return slices;
        }
        public static List<Slice2D> ToSlices(this List<Segment> segments, double scale)
        {
            var slices = new List<Slice2D>();

            foreach (var s in segments)
            {
                slices.Add(new Slice2D(Get2DPoint(s.StartPoint * scale), Get2DPoint(s.EndPoint * scale)));
            }



            return slices;
        }
        public static List<Slice2D> ToSlices(this List<Line2d> lines, double scale)
        {
            var slices = new List<Slice2D>();

            foreach (var l in lines)
            {
                slices.Add(new Slice2D(Get2DPoint(l.P1 * scale), Get2DPoint(l.P2 * scale)));
            }
            return slices;
        }
        public static Slice2D Scale(this Slice2D slice, double xScale, double yScale)
        {
            slice.Start = new Geometry2D.Point2D(slice.Start.X * xScale, slice.Start.Y * yScale);
            slice.End = new Geometry2D.Point2D(slice.End.X * xScale, slice.End.Y * yScale);
            return slice;
        }
        public static List<Slice2D> GetXLines(this Geom2D geom, double scale)
        {
            var slices = new List<Slice2D>();

            if (geom.VertexList.Length != 4)
                return slices;
            slices.Add(new Slice2D(Get2DPoint(geom.VertexList[0] * scale), Get2DPoint(geom.VertexList[2] * scale)));
            slices.Add(new Slice2D(Get2DPoint(geom.VertexList[1] * scale), Get2DPoint(geom.VertexList[3] * scale)));
            return slices;
        }

        private static Elibre.Geometry2D.Point2D Get2DPoint(Elibre.Net.Core.Point2d p)
        {
            return new Elibre.Geometry2D.Point2D(p.x, p.y);
        }
        public static Point2d Get2DPoint(this Elibre.Geometry2D.Point2D p)
        {
            return new Point2d(p.X, p.Y);
        }
    }
}
