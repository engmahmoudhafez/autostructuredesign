﻿namespace Elibre.AutoStructureDesign.Controls
{
    partial class InputControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_ProjectInfo = new System.Windows.Forms.Button();
            this.button_StructureTemplate = new System.Windows.Forms.Button();
            this.button_ArchTemplate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_ProjectInfo
            // 
            this.button_ProjectInfo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_ProjectInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_ProjectInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button_ProjectInfo.Image = global::Elibre.AutoStructureDesign.Properties.Resources.information_512;
            this.button_ProjectInfo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_ProjectInfo.Location = new System.Drawing.Point(195, 188);
            this.button_ProjectInfo.Name = "button_ProjectInfo";
            this.button_ProjectInfo.Size = new System.Drawing.Size(709, 57);
            this.button_ProjectInfo.TabIndex = 6;
            this.button_ProjectInfo.Text = "Project Information";
            this.button_ProjectInfo.UseVisualStyleBackColor = true;
            this.button_ProjectInfo.Click += new System.EventHandler(this.button_ProjectInfo_Click);
            // 
            // button_StructureTemplate
            // 
            this.button_StructureTemplate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_StructureTemplate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_StructureTemplate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button_StructureTemplate.Image = global::Elibre.AutoStructureDesign.Properties.Resources.templateIcon;
            this.button_StructureTemplate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_StructureTemplate.Location = new System.Drawing.Point(195, 251);
            this.button_StructureTemplate.Name = "button_StructureTemplate";
            this.button_StructureTemplate.Size = new System.Drawing.Size(351, 117);
            this.button_StructureTemplate.TabIndex = 7;
            this.button_StructureTemplate.Text = "                    Structural Template";
            this.button_StructureTemplate.UseVisualStyleBackColor = true;
            this.button_StructureTemplate.Click += new System.EventHandler(this.button_StructureTemplate_Click);
            // 
            // button_ArchTemplate
            // 
            this.button_ArchTemplate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_ArchTemplate.Enabled = false;
            this.button_ArchTemplate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_ArchTemplate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button_ArchTemplate.Image = global::Elibre.AutoStructureDesign.Properties.Resources.DrawIcon;
            this.button_ArchTemplate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_ArchTemplate.Location = new System.Drawing.Point(553, 251);
            this.button_ArchTemplate.Name = "button_ArchTemplate";
            this.button_ArchTemplate.Size = new System.Drawing.Size(351, 117);
            this.button_ArchTemplate.TabIndex = 8;
            this.button_ArchTemplate.Text = "                         Architectural Template";
            this.button_ArchTemplate.UseVisualStyleBackColor = true;
            // 
            // InputControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::Elibre.AutoStructureDesign.Properties.Resources.Cover;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.button_ArchTemplate);
            this.Controls.Add(this.button_StructureTemplate);
            this.Controls.Add(this.button_ProjectInfo);
            this.DoubleBuffered = true;
            this.Name = "InputControl";
            this.Size = new System.Drawing.Size(1065, 645);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button button_ProjectInfo;
        private System.Windows.Forms.Button button_StructureTemplate;
        private System.Windows.Forms.Button button_ArchTemplate;
    }
}
