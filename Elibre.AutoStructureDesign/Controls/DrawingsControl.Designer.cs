﻿namespace Elibre.AutoStructureDesign.Controls
{
    partial class DrawingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnShowFloor = new System.Windows.Forms.Button();
            this.btnShowRoof = new System.Windows.Forms.Button();
            this.cb_levels = new System.Windows.Forms.ComboBox();
            this.buttonDetails = new System.Windows.Forms.Button();
            this.buttonNotes = new System.Windows.Forms.Button();
            this.panel_Highlight = new System.Windows.Forms.Panel();
            this.btn_autocad = new System.Windows.Forms.Button();
            this.btn_pdf = new System.Windows.Forms.Button();
            this.btn_text = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnShowFloor
            // 
            this.btnShowFloor.BackColor = System.Drawing.Color.Transparent;
            this.btnShowFloor.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnShowFloor.FlatAppearance.BorderSize = 0;
            this.btnShowFloor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShowFloor.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold);
            this.btnShowFloor.ForeColor = System.Drawing.Color.Black;
            this.btnShowFloor.Location = new System.Drawing.Point(971, 8);
            this.btnShowFloor.Margin = new System.Windows.Forms.Padding(2);
            this.btnShowFloor.Name = "btnShowFloor";
            this.btnShowFloor.Size = new System.Drawing.Size(120, 38);
            this.btnShowFloor.TabIndex = 0;
            this.btnShowFloor.Text = "Floor";
            this.btnShowFloor.UseVisualStyleBackColor = false;
            this.btnShowFloor.Click += new System.EventHandler(this.btnShowFloor_Click);
            // 
            // btnShowRoof
            // 
            this.btnShowRoof.BackColor = System.Drawing.Color.Transparent;
            this.btnShowRoof.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnShowRoof.FlatAppearance.BorderSize = 0;
            this.btnShowRoof.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShowRoof.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold);
            this.btnShowRoof.ForeColor = System.Drawing.Color.Black;
            this.btnShowRoof.Location = new System.Drawing.Point(1097, 8);
            this.btnShowRoof.Margin = new System.Windows.Forms.Padding(2);
            this.btnShowRoof.Name = "btnShowRoof";
            this.btnShowRoof.Size = new System.Drawing.Size(120, 38);
            this.btnShowRoof.TabIndex = 0;
            this.btnShowRoof.Text = "Roof";
            this.btnShowRoof.UseVisualStyleBackColor = false;
            this.btnShowRoof.Click += new System.EventHandler(this.btnShowRoof_Click);
            // 
            // cb_levels
            // 
            this.cb_levels.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb_levels.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.cb_levels.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb_levels.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_levels.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_levels.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_levels.FormattingEnabled = true;
            this.cb_levels.Location = new System.Drawing.Point(1504, 15);
            this.cb_levels.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.cb_levels.Name = "cb_levels";
            this.cb_levels.Size = new System.Drawing.Size(139, 27);
            this.cb_levels.TabIndex = 1;
            this.cb_levels.SelectedIndexChanged += new System.EventHandler(this.cb_levels_SelectedIndexChanged);
            // 
            // buttonDetails
            // 
            this.buttonDetails.BackColor = System.Drawing.Color.Transparent;
            this.buttonDetails.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.buttonDetails.FlatAppearance.BorderSize = 0;
            this.buttonDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDetails.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDetails.ForeColor = System.Drawing.Color.Black;
            this.buttonDetails.Location = new System.Drawing.Point(1223, 8);
            this.buttonDetails.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.Size = new System.Drawing.Size(120, 38);
            this.buttonDetails.TabIndex = 2;
            this.buttonDetails.Text = "Details";
            this.buttonDetails.UseVisualStyleBackColor = false;
            this.buttonDetails.Click += new System.EventHandler(this.buttonDetails_Click);
            // 
            // buttonNotes
            // 
            this.buttonNotes.BackColor = System.Drawing.Color.Transparent;
            this.buttonNotes.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.buttonNotes.FlatAppearance.BorderSize = 0;
            this.buttonNotes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNotes.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNotes.ForeColor = System.Drawing.Color.Black;
            this.buttonNotes.Location = new System.Drawing.Point(1349, 8);
            this.buttonNotes.Margin = new System.Windows.Forms.Padding(2);
            this.buttonNotes.Name = "buttonNotes";
            this.buttonNotes.Size = new System.Drawing.Size(120, 38);
            this.buttonNotes.TabIndex = 3;
            this.buttonNotes.Text = "Notes";
            this.buttonNotes.UseVisualStyleBackColor = false;
            this.buttonNotes.Click += new System.EventHandler(this.buttonNotes_Click);
            // 
            // panel_Highlight
            // 
            this.panel_Highlight.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel_Highlight.Location = new System.Drawing.Point(971, 45);
            this.panel_Highlight.Margin = new System.Windows.Forms.Padding(2);
            this.panel_Highlight.Name = "panel_Highlight";
            this.panel_Highlight.Size = new System.Drawing.Size(120, 7);
            this.panel_Highlight.TabIndex = 15;
            // 
            // btn_autocad
            // 
            this.btn_autocad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_autocad.AutoSize = true;
            this.btn_autocad.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_autocad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_autocad.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btn_autocad.Image = global::Elibre.AutoStructureDesign.Properties.Resources._DWG;
            this.btn_autocad.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_autocad.Location = new System.Drawing.Point(14, 622);
            this.btn_autocad.Margin = new System.Windows.Forms.Padding(2);
            this.btn_autocad.Name = "btn_autocad";
            this.btn_autocad.Size = new System.Drawing.Size(56, 56);
            this.btn_autocad.TabIndex = 16;
            this.btn_autocad.UseVisualStyleBackColor = true;
            this.btn_autocad.Click += new System.EventHandler(this.btn_autocad_Click);
            // 
            // btn_pdf
            // 
            this.btn_pdf.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_pdf.AutoSize = true;
            this.btn_pdf.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_pdf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pdf.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btn_pdf.Image = global::Elibre.AutoStructureDesign.Properties.Resources._PDF;
            this.btn_pdf.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_pdf.Location = new System.Drawing.Point(93, 622);
            this.btn_pdf.Margin = new System.Windows.Forms.Padding(2);
            this.btn_pdf.Name = "btn_pdf";
            this.btn_pdf.Size = new System.Drawing.Size(56, 56);
            this.btn_pdf.TabIndex = 16;
            this.btn_pdf.UseVisualStyleBackColor = true;
            // 
            // btn_text
            // 
            this.btn_text.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_text.AutoSize = true;
            this.btn_text.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_text.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_text.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btn_text.Image = global::Elibre.AutoStructureDesign.Properties.Resources._Text;
            this.btn_text.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_text.Location = new System.Drawing.Point(175, 622);
            this.btn_text.Margin = new System.Windows.Forms.Padding(2);
            this.btn_text.Name = "btn_text";
            this.btn_text.Size = new System.Drawing.Size(56, 56);
            this.btn_text.TabIndex = 16;
            this.btn_text.UseVisualStyleBackColor = true;
            // 
            // DrawingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btn_text);
            this.Controls.Add(this.btn_pdf);
            this.Controls.Add(this.btn_autocad);
            this.Controls.Add(this.panel_Highlight);
            this.Controls.Add(this.buttonNotes);
            this.Controls.Add(this.buttonDetails);
            this.Controls.Add(this.cb_levels);
            this.Controls.Add(this.btnShowRoof);
            this.Controls.Add(this.btnShowFloor);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DrawingsControl";
            this.Size = new System.Drawing.Size(1658, 693);
            this.Load += new System.EventHandler(this.DrawingsControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnShowFloor;
        private System.Windows.Forms.Button btnShowRoof;
        private System.Windows.Forms.ComboBox cb_levels;
        private System.Windows.Forms.Button buttonDetails;
        private System.Windows.Forms.Button buttonNotes;
        private System.Windows.Forms.Panel panel_Highlight;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_StructureTemplate;
        private System.Windows.Forms.Button btn_autocad;
        private System.Windows.Forms.Button btn_pdf;
        private System.Windows.Forms.Button btn_text;
    }
}
