﻿namespace Elibre.AutoStructureDesign.Controls
{
    partial class DiagramsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.windMoment = new System.Windows.Forms.Button();
            this.windShear = new System.Windows.Forms.Button();
            this.WindNormal = new System.Windows.Forms.Button();
            this.seismicMomentbtn = new System.Windows.Forms.Button();
            this.seismicShearBtn = new System.Windows.Forms.Button();
            this.SeismicNormalBtn = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel2.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label7);
            this.panel2.Location = new System.Drawing.Point(134, 102);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(797, 38);
            this.panel2.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 19);
            this.label7.TabIndex = 0;
            this.label7.Text = "Seismic";
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.seismicMomentbtn);
            this.panel1.Controls.Add(this.seismicShearBtn);
            this.panel1.Controls.Add(this.SeismicNormalBtn);
            this.panel1.Location = new System.Drawing.Point(134, 146);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(797, 150);
            this.panel1.TabIndex = 20;
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel3.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(134, 333);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(797, 38);
            this.panel3.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Wind";
            // 
            // panel4
            // 
            this.panel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel4.Controls.Add(this.windMoment);
            this.panel4.Controls.Add(this.windShear);
            this.panel4.Controls.Add(this.WindNormal);
            this.panel4.Location = new System.Drawing.Point(134, 377);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(797, 150);
            this.panel4.TabIndex = 22;
            // 
            // windMoment
            // 
            this.windMoment.BackColor = System.Drawing.Color.White;
            this.windMoment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.windMoment.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.windMoment.ForeColor = System.Drawing.Color.Black;
            this.windMoment.Image = global::Elibre.AutoStructureDesign.Properties.Resources.momentDiagram160x80;
            this.windMoment.Location = new System.Drawing.Point(534, 0);
            this.windMoment.Name = "windMoment";
            this.windMoment.Size = new System.Drawing.Size(261, 147);
            this.windMoment.TabIndex = 2;
            this.windMoment.Text = "Bending Moment";
            this.windMoment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.windMoment.UseCompatibleTextRendering = true;
            this.windMoment.UseVisualStyleBackColor = false;
            this.windMoment.Click += new System.EventHandler(this.windMomentBtn_Click);
            // 
            // windShear
            // 
            this.windShear.BackColor = System.Drawing.Color.White;
            this.windShear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.windShear.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.windShear.ForeColor = System.Drawing.Color.Black;
            this.windShear.Image = global::Elibre.AutoStructureDesign.Properties.Resources.ShearDiagram130x80;
            this.windShear.Location = new System.Drawing.Point(267, 0);
            this.windShear.Name = "windShear";
            this.windShear.Size = new System.Drawing.Size(261, 147);
            this.windShear.TabIndex = 1;
            this.windShear.Text = "Shear Force";
            this.windShear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.windShear.UseCompatibleTextRendering = true;
            this.windShear.UseVisualStyleBackColor = false;
            this.windShear.Click += new System.EventHandler(this.windShearBtn_Click);
            // 
            // WindNormal
            // 
            this.WindNormal.BackColor = System.Drawing.Color.White;
            this.WindNormal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WindNormal.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WindNormal.ForeColor = System.Drawing.Color.Black;
            this.WindNormal.Image = global::Elibre.AutoStructureDesign.Properties.Resources.LoadDiagram;
            this.WindNormal.Location = new System.Drawing.Point(0, 0);
            this.WindNormal.Name = "WindNormal";
            this.WindNormal.Size = new System.Drawing.Size(261, 147);
            this.WindNormal.TabIndex = 0;
            this.WindNormal.Text = "Normal Force";
            this.WindNormal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.WindNormal.UseCompatibleTextRendering = true;
            this.WindNormal.UseVisualStyleBackColor = false;
            this.WindNormal.Click += new System.EventHandler(this.windNormalBtn_Click);
            // 
            // seismicMomentbtn
            // 
            this.seismicMomentbtn.BackColor = System.Drawing.Color.White;
            this.seismicMomentbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.seismicMomentbtn.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seismicMomentbtn.ForeColor = System.Drawing.Color.Black;
            this.seismicMomentbtn.Image = global::Elibre.AutoStructureDesign.Properties.Resources.momentDiagram160x80;
            this.seismicMomentbtn.Location = new System.Drawing.Point(534, 0);
            this.seismicMomentbtn.Name = "seismicMomentbtn";
            this.seismicMomentbtn.Size = new System.Drawing.Size(261, 147);
            this.seismicMomentbtn.TabIndex = 2;
            this.seismicMomentbtn.Text = "Bending Moment";
            this.seismicMomentbtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.seismicMomentbtn.UseCompatibleTextRendering = true;
            this.seismicMomentbtn.UseVisualStyleBackColor = false;
            this.seismicMomentbtn.Click += new System.EventHandler(this.seismicMomentbtn_Click);
            // 
            // seismicShearBtn
            // 
            this.seismicShearBtn.BackColor = System.Drawing.Color.White;
            this.seismicShearBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.seismicShearBtn.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seismicShearBtn.ForeColor = System.Drawing.Color.Black;
            this.seismicShearBtn.Image = global::Elibre.AutoStructureDesign.Properties.Resources.ShearDiagram130x80;
            this.seismicShearBtn.Location = new System.Drawing.Point(267, 0);
            this.seismicShearBtn.Name = "seismicShearBtn";
            this.seismicShearBtn.Size = new System.Drawing.Size(261, 147);
            this.seismicShearBtn.TabIndex = 1;
            this.seismicShearBtn.Text = "Shear Force";
            this.seismicShearBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.seismicShearBtn.UseCompatibleTextRendering = true;
            this.seismicShearBtn.UseVisualStyleBackColor = false;
            this.seismicShearBtn.Click += new System.EventHandler(this.seismicShearBtn_Click);
            // 
            // SeismicNormalBtn
            // 
            this.SeismicNormalBtn.BackColor = System.Drawing.Color.White;
            this.SeismicNormalBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SeismicNormalBtn.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SeismicNormalBtn.ForeColor = System.Drawing.Color.Black;
            this.SeismicNormalBtn.Image = global::Elibre.AutoStructureDesign.Properties.Resources.LoadDiagram;
            this.SeismicNormalBtn.Location = new System.Drawing.Point(0, 0);
            this.SeismicNormalBtn.Name = "SeismicNormalBtn";
            this.SeismicNormalBtn.Size = new System.Drawing.Size(261, 147);
            this.SeismicNormalBtn.TabIndex = 0;
            this.SeismicNormalBtn.Text = "Normal Force";
            this.SeismicNormalBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.SeismicNormalBtn.UseCompatibleTextRendering = true;
            this.SeismicNormalBtn.UseVisualStyleBackColor = false;
            this.SeismicNormalBtn.Click += new System.EventHandler(this.SeismicNormalBtn_Click);
            // 
            // DiagramsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "DiagramsControl";
            this.Size = new System.Drawing.Size(1058, 671);
            this.Load += new System.EventHandler(this.DigramsControl_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button seismicMomentbtn;
        private System.Windows.Forms.Button seismicShearBtn;
        private System.Windows.Forms.Button SeismicNormalBtn;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button windMoment;
        private System.Windows.Forms.Button windShear;
        private System.Windows.Forms.Button WindNormal;
    }
}
