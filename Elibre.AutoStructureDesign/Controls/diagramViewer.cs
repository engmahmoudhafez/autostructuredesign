﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EFraming.Objects;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.States;
using Elibre.AutoStructureDesign.Models;

namespace Elibre.AutoStructureDesign.Controls
{
    public partial class DiagramViewer : UserControl
    {

        GLStateRenderer glControl;
        DiagramsState _diagramsState;
        private int _lastSelectedIndex = -1;

        public ActiveResults ActiveResults { get; set; }
        public DiagramType DiagramType { get; set; }
        public ActiveWindResults ActiveWindResults { get; set; }
        public DiagramViewer()
        {
            InitializeComponent();

            glControl = EStitcherSingleton.GetInstance.GlobalGLRenderer;
            glControl.Parent = this;
            glControl.Dock = DockStyle.Fill;
            directionCb.DataSource = Enum.GetNames(typeof(ActiveWindResults));


        }

        public void UpdateDiagram(ActiveResults activeResults, DiagramType type)
        {
            ActiveResults = activeResults;
            DiagramType = type;
        }


        private void FillLevelsCB()
        {
            //_lastSelectedIndex = cb_levels.SelectedIndex;
            //cb_levels.Items.Clear();
            //var names = EStitcherSingleton.GetInstance.Building.Levels.Select(e => e.LevelName);
            //cb_levels.Items.AddRange(names.ToArray());
            //if (_lastSelectedIndex < 0)
            //    cb_levels.SelectedIndex = 0;
            //else
            //    cb_levels.SelectedIndex = _lastSelectedIndex;

            //if (cb_levels.Items.Count == 1)
            //{
            //    cb_levels.Visible = false;
            //}
            //else
            //{
            //    cb_levels.Visible = true;
            //}
        }

        public void UpdateState()
        {


            glControl = EStitcherSingleton.GetInstance.GlobalGLRenderer;
            glControl.Parent = this;
            glControl.Dock = DockStyle.Fill;
            Controls.Add(glControl);
            FillLevelsCB();
            _diagramsState.Update();

            EStitcherSingleton.GetInstance.StatesManager.activeState = _diagramsState;
            _diagramsState.UpdateShownLevel(0);
        }



        private void DiagramViewer_Load(object sender, EventArgs e)
        {
            _diagramsState = new DiagramsState();
            _diagramsState.ActiveResults = ActiveResults;
            _diagramsState.DiagramType = DiagramType;
            EStitcherSingleton.GetInstance.StatesManager.activeState = _diagramsState;
            Controls.Add(glControl);
            // FillLevelsCB();

            ToggleDirectionCBVisiblity();
            _diagramsState.UpdateShownLevel(0);
            directionCb.SelectedIndex = 0;
        }

        private void ToggleDirectionCBVisiblity()
        {
            if (ActiveResults == ActiveResults.Seismic)
            {
                directionCb.Visible = false;
                //directionLbl .Visible= false;
            }
            else
            {
                directionCb.Visible = true;
                // directionLbl.Visible = true;
            }
        }

        private void directionCb_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_diagramsState == null)
                return;
            var cb = sender as ComboBox;
            var i = cb.SelectedIndex;
            ActiveWindResults = _diagramsState.ActiveWindResults = (ActiveWindResults)i;
            _diagramsState.UpdateShownLevel(0);

        }



    }
}
