﻿namespace Elibre.AutoStructureDesign.Controls
{
    partial class DiagramViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.directionCb = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // directionCb
            // 
            this.directionCb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.directionCb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.directionCb.Cursor = System.Windows.Forms.Cursors.Hand;
            this.directionCb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.directionCb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.directionCb.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.directionCb.FormattingEnabled = true;
            this.directionCb.Location = new System.Drawing.Point(776, 22);
            this.directionCb.Name = "directionCb";
            this.directionCb.Size = new System.Drawing.Size(121, 21);
            this.directionCb.TabIndex = 3;
            this.directionCb.SelectedIndexChanged += new System.EventHandler(this.directionCb_SelectedIndexChanged);
            // 
            // DiagramViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.directionCb);
            this.Name = "DiagramViewer";
            this.Size = new System.Drawing.Size(918, 399);
            this.Load += new System.EventHandler(this.DiagramViewer_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox directionCb;
    }
}
