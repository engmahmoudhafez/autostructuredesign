﻿namespace StitcherNewUIApp.Controls.Epaper
{
    partial class DrawingWallsEpaper
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_Viewer = new System.Windows.Forms.Panel();
            this.panel_Highlight = new System.Windows.Forms.Panel();
            this.radioButton_Stitcher = new System.Windows.Forms.RadioButton();
            this.radioButton_Draw = new System.Windows.Forms.RadioButton();
            this.levelsComboBox = new System.Windows.Forms.ComboBox();
            this.panel_Viewer.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_Viewer
            // 
            this.panel_Viewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Viewer.Controls.Add(this.panel_Highlight);
            this.panel_Viewer.Controls.Add(this.radioButton_Stitcher);
            this.panel_Viewer.Controls.Add(this.radioButton_Draw);
            this.panel_Viewer.Controls.Add(this.levelsComboBox);
            this.panel_Viewer.Location = new System.Drawing.Point(3, 3);
            this.panel_Viewer.Name = "panel_Viewer";
            this.panel_Viewer.Size = new System.Drawing.Size(754, 621);
            this.panel_Viewer.TabIndex = 0;
            // 
            // panel_Highlight
            // 
            this.panel_Highlight.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel_Highlight.Location = new System.Drawing.Point(76, 53);
            this.panel_Highlight.Name = "panel_Highlight";
            this.panel_Highlight.Size = new System.Drawing.Size(116, 7);
            this.panel_Highlight.TabIndex = 16;
            // 
            // radioButton_Stitcher
            // 
            this.radioButton_Stitcher.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton_Stitcher.BackColor = System.Drawing.Color.Transparent;
            this.radioButton_Stitcher.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButton_Stitcher.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radioButton_Stitcher.FlatAppearance.BorderSize = 0;
            this.radioButton_Stitcher.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButton_Stitcher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_Stitcher.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Stitcher.Location = new System.Drawing.Point(198, 13);
            this.radioButton_Stitcher.Name = "radioButton_Stitcher";
            this.radioButton_Stitcher.Size = new System.Drawing.Size(116, 41);
            this.radioButton_Stitcher.TabIndex = 15;
            this.radioButton_Stitcher.TabStop = true;
            this.radioButton_Stitcher.Text = "Stitcher";
            this.radioButton_Stitcher.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton_Stitcher.UseVisualStyleBackColor = false;
            this.radioButton_Stitcher.Click += new System.EventHandler(this.radioButton_Stitcher_Click);
            // 
            // radioButton_Draw
            // 
            this.radioButton_Draw.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton_Draw.BackColor = System.Drawing.Color.Transparent;
            this.radioButton_Draw.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButton_Draw.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radioButton_Draw.FlatAppearance.BorderSize = 0;
            this.radioButton_Draw.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButton_Draw.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_Draw.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Draw.Location = new System.Drawing.Point(76, 13);
            this.radioButton_Draw.Margin = new System.Windows.Forms.Padding(0);
            this.radioButton_Draw.Name = "radioButton_Draw";
            this.radioButton_Draw.Size = new System.Drawing.Size(116, 41);
            this.radioButton_Draw.TabIndex = 14;
            this.radioButton_Draw.TabStop = true;
            this.radioButton_Draw.Text = "Draw";
            this.radioButton_Draw.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton_Draw.UseVisualStyleBackColor = false;
            // 
            // levelsComboBox
            // 
            this.levelsComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.levelsComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.levelsComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.levelsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.levelsComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.levelsComboBox.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.levelsComboBox.FormattingEnabled = true;
            this.levelsComboBox.Items.AddRange(new object[] {
            "Level 1",
            "---------------",
            "+ Add"});
            this.levelsComboBox.Location = new System.Drawing.Point(586, 13);
            this.levelsComboBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.levelsComboBox.Name = "levelsComboBox";
            this.levelsComboBox.Size = new System.Drawing.Size(152, 27);
            this.levelsComboBox.TabIndex = 1;
            this.levelsComboBox.SelectedIndexChanged += new System.EventHandler(this.levelsComboBox_SelectedIndexChanged);
            // 
            // DrawingWallsEpaper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel_Viewer);
            this.Name = "DrawingWallsEpaper";
            this.Size = new System.Drawing.Size(760, 624);
            this.panel_Viewer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_Viewer;
        private System.Windows.Forms.ComboBox levelsComboBox;
        private System.Windows.Forms.Panel panel_Highlight;
        private System.Windows.Forms.RadioButton radioButton_Stitcher;
        private System.Windows.Forms.RadioButton radioButton_Draw;
    }
}
