﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StitcherNewUIApp.Controls.Epaper
{
    public partial class StateControl : UserControl
    {
        public StateControl(Image image,string text)
        {
            InitializeComponent();
            this.pictureBox1.Image = image;
            this.label1.Text = text;
        }
    }
}
