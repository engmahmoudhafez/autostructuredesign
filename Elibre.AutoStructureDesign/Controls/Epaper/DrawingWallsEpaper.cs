﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Elibre.CAD.StitcherApi.NewUIStructure;
using Elibre.CAD.StitcherApi.Models.Stitching;
using Elibre.CAD.StitcherApi.Algorithms.RoofAlgorithms;
using StitcherNewUIApp.Viewers.Epaper;
using Elibre.CAD.StitcherApi.Models;
using Elibre.Geometry2D;
using Elibre.Stitcher.Models.Stitching;
using Elibre.Stitcher.Models;
using Elibre.Net.RoofEngine;
using Elibre.AutoStructureDesign.Models;
using Elibre.AutoStructureDesign;
using Elibre.AutoStructureDesign.Extensions;

namespace StitcherNewUIApp.Controls.Epaper
{
    public partial class DrawingWallsEpaper : UserControl
    {
        private EpaperWallsViewer _epaperWallsViewer { get; set; }
        private StitcherInfo _stitcherInfo { get; set; }
        public StitcherBuilding StitcherBuilding { get; set; }
        public EventHandler ToStitcher { get; set; }
        private int _lastSelectedIndex = 0;
        private const string _addFloorString = "+ Add";
        private const string _spliterName = "---------------";
        public DrawingWallsEpaper()
        {
            InitializeComponent();
            _stitcherInfo = new StitcherInfo();
            levelsComboBox.Items.Clear();
            if (EStitcherSingleton.GetInstance.Building == null)
            {
                EStitcherSingleton.GetInstance.Building = new StitcherBuilding();
                EStitcherSingleton.GetInstance.Building.Template = StitcherSingleton.GetInstance.Template;
                StitcherBuilding = EStitcherSingleton.GetInstance.Building;
                StitcherBuilding.DeclareVariables();
                levelsComboBox.Items.Add("Level 1");
            }
            else
            {
                foreach (var lev in EStitcherSingleton.GetInstance.Building.Levels)
                {
                    levelsComboBox.Items.Add(lev.LevelName);
                }
            }
            levelsComboBox.Items.Add(_spliterName);
            levelsComboBox.Items.Add(_addFloorString);
            StitcherBuilding = EStitcherSingleton.GetInstance.Building;
            _stitcherInfo.Building = StitcherBuilding;

            _epaperWallsViewer = new EpaperWallsViewer(StitcherBuilding);
            _epaperWallsViewer.OnFloorChanged += On_WallsChanged;
            _epaperWallsViewer.Parent = panel_Viewer;
            _epaperWallsViewer.Dock = DockStyle.Fill;
            levelsComboBox.BringToFront();
            levelsComboBox.SelectedIndex = 0;
        }
        private void On_WallsChanged(object sender, EventArgs e)
        {
            StructuralExporter.ResetStructureBuilding();
            _stitcherInfo.Building = StitcherBuilding;
            _stitcherInfo.Building.FillWallsParts();
        }

        private void levelsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (levelsComboBox.Text.Equals(_addFloorString))
            {
                var levelName = "Level " + (levelsComboBox.Items.Count - 1).ToString();
                levelsComboBox.Items.Insert(levelsComboBox.Items.Count - 2, levelName);
                levelsComboBox.SelectedIndex = levelsComboBox.FindStringExact(levelName);
                _epaperWallsViewer._apaperMode = EpaperWallsViewer.EpaperMode.Draw;
                _epaperWallsViewer._drawTypeMode = EpaperWallsViewer.DrawTypeMode.Rectangle;
                RefreshPlanInViewer();
            }
            else if (levelsComboBox.SelectedIndex == levelsComboBox.Items.Count - 2)
            {
                levelsComboBox.SelectedIndex = _lastSelectedIndex;
            }
            else
            {
                _epaperWallsViewer._apaperMode = EpaperWallsViewer.EpaperMode.Draw;
                _epaperWallsViewer._drawTypeMode = EpaperWallsViewer.DrawTypeMode.Rectangle;
                RefreshPlanInViewer();
            }
            _lastSelectedIndex = levelsComboBox.SelectedIndex;
        }

        private void RefreshPlanInViewer()
        {
            var levelIndex = levelsComboBox.SelectedIndex;
            if (StitcherBuilding.Levels.Count - 1 < levelIndex) //add the new level
            {
                var elevation = StitcherBuilding.Levels[levelIndex - 1].Elevation + 120;
                StitcherBuilding.Levels.Add(new StitcherLevel(levelsComboBox.Text, elevation, true));
                StitcherBuilding.TopWallLevels.Add(new StitcherLevel("T.O.W." + (levelIndex + 1).ToString(), StitcherBuilding.Levels[levelIndex].Elevation + 100, false));
                StitcherBuilding.HeaderHeightLevals.Add(new StitcherLevel("Header Height " + (levelIndex + 1).ToString(), StitcherBuilding.Levels[levelIndex].Elevation + 80, false));
            }

            _epaperWallsViewer.ApaperLevel = StitcherBuilding.Levels[levelIndex];
            _epaperWallsViewer.Invalidate();
        }

        internal void SetTemplate(StitcherTemplate template)
        {
            StitcherBuilding.Template = template;
        }

        internal void UpdateBuilding()
        {
            StitcherBuilding = EStitcherSingleton.GetInstance.Building;
            _epaperWallsViewer.UpdateBuilding();
            _epaperWallsViewer.CenterDrawings();
        }

        private void radioButton_Stitcher_Click(object sender, EventArgs e)
        {
            ToStitcher?.Invoke(sender, e);

        }
    }
}
