﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Elibre.AutoStructureDesign.Models;
using Elibre.AutoStructureDesign.States;
using Elibre.AutoStructureDesign.Libraries.GLUtils;

namespace Elibre.AutoStructureDesign.Controls
{
    public partial class _3DControl : UserControl
    {
        _3DState _3d_state;
        GLStateRenderer glControl;

        public _3DControl()
        {
            InitializeComponent();
            glControl = EStitcherSingleton.GetInstance.GlobalGLRenderer;
            glControl.Parent = this;
            glControl.Dock = DockStyle.Fill;
        }

        private void _3DControl_Load(object sender, EventArgs e)
        {
            _3d_state = new _3DState();
            EStitcherSingleton.GetInstance.StatesManager.activeState = _3d_state;
            Controls.Add(glControl);
        }
    }
}
