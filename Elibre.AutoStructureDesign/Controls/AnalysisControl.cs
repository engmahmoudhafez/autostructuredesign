﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.States;
using Elibre.AutoStructureDesign.Models;

namespace Elibre.AutoStructureDesign.Controls
{
    public partial class AnalysisControl : UserControl
    {
        GLStateRenderer glControl;
        AnalysisState analysisState;
        public AnalysisControl()
        {
            InitializeComponent();
            glControl = EStitcherSingleton.GetInstance.GlobalGLRenderer;
            glControl.Parent = panel_Drawing;
            glControl.Dock = DockStyle.Fill;
        }

        private void AnalysisControl_Load(object sender, EventArgs e)
        {
            analysisState = new AnalysisState();
            EStitcherSingleton.GetInstance.StatesManager.activeState = analysisState;
            panel_Drawing.Controls.Remove(glControl);
            panel_Drawing.Controls.Add(glControl);
            analysisState.UpdateShownDrawing(0);
            UpdateComboBoxVisability();
        }
        public void UpdateState()
        {
            glControl = EStitcherSingleton.GetInstance.GlobalGLRenderer;
            glControl.Parent = panel_Drawing;
            glControl.Dock = DockStyle.Fill;
            panel_Drawing.Controls.Remove(glControl);
            panel_Drawing.Controls.Add(glControl);
            analysisState.Update();
            EStitcherSingleton.GetInstance.StatesManager.activeState = analysisState;
            analysisState.UpdateBuilding();
            analysisState.UpdateShownDrawing(0);
            analysisState.CenterDrawings();
            UpdateComboBoxVisability();
        }
        void UpdateComboBoxVisability()
        {
            levelsComboBox.Items.Clear();
            foreach (var lev in EStitcherSingleton.GetInstance.Building.Levels)
            {
                //if (!lev.FloorPlan.Walls.Any())
                //    continue;
                levelsComboBox.Items.Add(lev.LevelName);
            }
            levelsComboBox.SelectedIndex = 0;
            

        }
        private void radioButtonMode_CheckedChanged(object sender, EventArgs e)
        {
         
            if (!(sender as RadioButton).Checked)
                return;
            if ((sender as RadioButton).Name.Equals(radioButton_Beam.Name))
            {
                analysisState.CurrentMode = AnalysisState.Mode.Beam;
            }
            else if ((sender as RadioButton).Name.Equals(radioButton_Joist.Name))
            {
                analysisState.CurrentMode = AnalysisState.Mode.JoistArea;
            }
            else if ((sender as RadioButton).Name.Equals(radioButton_Post.Name))
            {
                analysisState.CurrentMode = AnalysisState.Mode.Post;
            }
            else if ((sender as RadioButton).Name.Equals(radioButton_ShearWall.Name))
            {
                analysisState.CurrentMode = AnalysisState.Mode.ShearWall;
            }

        }

        private void levelsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (analysisState == null)
                return;
            analysisState.UpdateShownDrawing(levelsComboBox.SelectedIndex);
        }

       
    }
}
