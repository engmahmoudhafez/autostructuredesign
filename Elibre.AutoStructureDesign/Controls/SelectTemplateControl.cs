﻿using System;
using System.Windows.Forms;
using EFraming.Forms;
using Elibre.AutoStructureDesign.Models;
using System.IO;
using System.Reflection;

namespace Elibre.AutoStructureDesign.Controls
{
    public partial class SelectTemplateControl : UserControl
    {
        public EventHandler OnStart { get; set; }
        public EventHandler OnEditTemplate { get; set; }
        public SelectTemplateControl()
        {
            InitializeComponent();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            OnStart?.Invoke(sender, e);
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            frmSaveConfigurations frm = new frmSaveConfigurations(EStitcherSingleton.GetInstance.StructureBuildling);
            frm.Show();
        }

        private void btn_Load_Click(object sender, EventArgs e)
        {
            frmLoadConfigurations frm = new frmLoadConfigurations();
            frm.Show();
        }

        private void SelectTemplateControl_Load(object sender, EventArgs e)
        {
            string root = Assembly.GetExecutingAssembly().Location;
            string newPath = Path.GetFullPath(Path.Combine(root, @"..\"));

            string subdir = newPath + @"\Templates";
            if (!Directory.Exists(subdir))
            {
                Directory.CreateDirectory(subdir);
            }

            var path_default_xml = subdir + @"\Default.xml";
            if (!File.Exists(path_default_xml))
            {
                var assembly = Assembly.GetExecutingAssembly();
                var resourceName = "Elibre.AutoStructureDesign.ConfigurationTemplatesXML.Default.xml";

                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                using (StreamReader reader = new StreamReader(stream))
                {
                    string result = reader.ReadToEnd();
                    TextWriter txt = new StreamWriter(path_default_xml);
                    txt.Write(result);
                    txt.Close();
                }
            }

            foreach (var path in Directory.GetFiles(subdir, "*.xml"))
            {
                var name = Path.GetFileNameWithoutExtension(path);
                lstBox_Templates.Items.Add(name);
            }
            if (lstBox_Templates.Items.Count > 0)
                lstBox_Templates.SelectedIndex = 0;
        }

        private void lstBox_Templates_SelectedIndexChanged(object sender, EventArgs e)
        {
         
        }

        private void lstBox_Templates_DoubleClick(object sender, EventArgs e)
        {
            OnEditTemplate?.Invoke(sender, e);
        }
    }
}
