﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Elibre.AutoStructureDesign.Models;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.Drawings;

namespace Elibre.AutoStructureDesign.Controls
{
    public partial class WallsControl : UserControl
    {
        GLStateRenderer glControl;
        WallState wallState;
        private int _lastSelectedIndex=-1;
        public WallsControl()
        {
            InitializeComponent();
            glControl = EStitcherSingleton.GetInstance.GlobalGLRenderer;
            glControl.Parent = this;
            glControl.Dock = DockStyle.Fill;
        }

        private void WallsControl_Load(object sender, EventArgs e)
        {
            wallState = new WallState();
            EStitcherSingleton.GetInstance.StatesManager.activeState = wallState;
            Controls.Add(glControl);
            wallState.CenterDrawings();
            FillLevelsCB();
        }

        private void FillLevelsCB()
        {
            _lastSelectedIndex = cb_levels.SelectedIndex;
            cb_levels.Items.Clear();
            var names = EStitcherSingleton.GetInstance.Building.Levels.Select(e => e.LevelName);
            cb_levels.Items.AddRange(names.ToArray());
            if (_lastSelectedIndex<0)
                cb_levels.SelectedIndex = 0;
            else
                cb_levels.SelectedIndex = _lastSelectedIndex;
        }

        public void UpdateState()
        {


            glControl = EStitcherSingleton.GetInstance.GlobalGLRenderer;
            glControl.Parent = this;
            glControl.Dock = DockStyle.Fill;
            Controls.Add(glControl);
            FillLevelsCB();
            wallState.Update();
            EStitcherSingleton.GetInstance.StatesManager.activeState = wallState;
            // wallState.UpdateShownLevel(cb_levels.SelectedItem.ToString());
            wallState.CenterDrawings();
        }



        private void cb_levels_SelectedIndexChanged(object sender, EventArgs e)
        {
            wallState.UpdateShownLevel(cb_levels.SelectedIndex);
        }
    }
}
