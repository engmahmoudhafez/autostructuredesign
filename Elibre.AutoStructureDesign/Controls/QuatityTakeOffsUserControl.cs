﻿using Elibre.CAD.StitcherApi.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign.Controls
{
    public partial class QuantityTakeOffsDialog : UserControl
    {
        public List<QuatityTakeOff> CurrentData { get; set; }

        public QuantityTakeOffsDialog()
        {
            InitializeComponent();
            this.CurrentData = new List<QuatityTakeOff>();
        }
        public void ShowData(List<QuatityTakeOff> list)
        {
            this.CurrentData = list;

            QuantityTakeOffGrid.AutoGenerateColumns = false;
            QuantityTakeOffGrid.DataSource = list;
            QuantityTakeOffGrid.ClearSelection();

            foreach (DataGridViewRow row in QuantityTakeOffGrid.Rows)
            {
                var stringValue = row.Cells[0].Value.ToString();
                if (stringValue.Contains("Doors"))
                    row.DefaultCellStyle.BackColor = Color.WhiteSmoke;
                else if (stringValue.Contains("Windows"))
                    row.DefaultCellStyle.BackColor = Color.LightGray;
                else if (stringValue.Contains("Walls"))
                    row.DefaultCellStyle.BackColor = Color.WhiteSmoke;
                else if (stringValue.Contains("Floor"))
                    row.DefaultCellStyle.BackColor = Color.LightGray;
                else if (stringValue.Contains("Roof"))
                    row.DefaultCellStyle.BackColor = Color.WhiteSmoke;
                else if (stringValue.Contains("Eaves") || stringValue.Contains("Rakes") || stringValue.Contains("Valleys") || stringValue.Contains("Hips") || stringValue.Contains("Ridges"))
                    row.DefaultCellStyle.BackColor = Color.LightGray;
            }
        }
        
    }
}
