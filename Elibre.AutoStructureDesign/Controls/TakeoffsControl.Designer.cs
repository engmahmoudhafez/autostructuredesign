﻿namespace Elibre.AutoStructureDesign.Controls
{
    partial class TakeoffsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStruc = new System.Windows.Forms.Button();
            this.btnArch = new System.Windows.Forms.Button();
            this.panelContainer = new System.Windows.Forms.Panel();
            this.panel_Highlight = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // btnStruc
            // 
            this.btnStruc.Enabled = false;
            this.btnStruc.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnStruc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStruc.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold);
            this.btnStruc.ForeColor = System.Drawing.Color.Black;
            this.btnStruc.Location = new System.Drawing.Point(139, 8);
            this.btnStruc.Name = "btnStruc";
            this.btnStruc.Size = new System.Drawing.Size(120, 38);
            this.btnStruc.TabIndex = 1;
            this.btnStruc.Text = "Structure";
            this.btnStruc.UseVisualStyleBackColor = true;
            this.btnStruc.Click += new System.EventHandler(this.btnStruc_Click);
            // 
            // btnArch
            // 
            this.btnArch.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnArch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnArch.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold);
            this.btnArch.ForeColor = System.Drawing.Color.Black;
            this.btnArch.Location = new System.Drawing.Point(13, 8);
            this.btnArch.Name = "btnArch";
            this.btnArch.Size = new System.Drawing.Size(120, 38);
            this.btnArch.TabIndex = 2;
            this.btnArch.Text = "Architectural";
            this.btnArch.UseVisualStyleBackColor = true;
            this.btnArch.Click += new System.EventHandler(this.btnArch_Click);
            // 
            // panelContainer
            // 
            this.panelContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelContainer.Location = new System.Drawing.Point(3, 65);
            this.panelContainer.Name = "panelContainer";
            this.panelContainer.Size = new System.Drawing.Size(724, 506);
            this.panelContainer.TabIndex = 3;
            // 
            // panel_Highlight
            // 
            this.panel_Highlight.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel_Highlight.Location = new System.Drawing.Point(13, 45);
            this.panel_Highlight.Name = "panel_Highlight";
            this.panel_Highlight.Size = new System.Drawing.Size(120, 7);
            this.panel_Highlight.TabIndex = 14;
            // 
            // TakeoffsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel_Highlight);
            this.Controls.Add(this.panelContainer);
            this.Controls.Add(this.btnStruc);
            this.Controls.Add(this.btnArch);
            this.Name = "TakeoffsControl";
            this.Size = new System.Drawing.Size(730, 574);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStruc;
        private System.Windows.Forms.Button btnArch;
        private System.Windows.Forms.Panel panelContainer;
        private System.Windows.Forms.Panel panel_Highlight;
    }
}
