﻿namespace Elibre.AutoStructureDesign.Controls
{
    partial class AnalysisControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_Drawing = new System.Windows.Forms.Panel();
            this.levelsComboBox = new System.Windows.Forms.ComboBox();
            this.radioButtonBearing = new System.Windows.Forms.RadioButton();
            this.radioButtonStud = new System.Windows.Forms.RadioButton();
            this.radioButton_Openings = new System.Windows.Forms.RadioButton();
            this.radioButton_ShearWall = new System.Windows.Forms.RadioButton();
            this.radioButton_Post = new System.Windows.Forms.RadioButton();
            this.radioButton_Beam = new System.Windows.Forms.RadioButton();
            this.radioButton_Joist = new System.Windows.Forms.RadioButton();
            this.panel_Drawing.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_Drawing
            // 
            this.panel_Drawing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Drawing.Controls.Add(this.levelsComboBox);
            this.panel_Drawing.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Drawing.Location = new System.Drawing.Point(0, 0);
            this.panel_Drawing.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel_Drawing.Name = "panel_Drawing";
            this.panel_Drawing.Size = new System.Drawing.Size(641, 469);
            this.panel_Drawing.TabIndex = 0;
            // 
            // levelsComboBox
            // 
            this.levelsComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.levelsComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.levelsComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.levelsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.levelsComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.levelsComboBox.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.levelsComboBox.FormattingEnabled = true;
            this.levelsComboBox.Items.AddRange(new object[] {
            "Level 1",
            "---------------",
            "+ Add"});
            this.levelsComboBox.Location = new System.Drawing.Point(496, 11);
            this.levelsComboBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.levelsComboBox.Name = "levelsComboBox";
            this.levelsComboBox.Size = new System.Drawing.Size(131, 21);
            this.levelsComboBox.TabIndex = 4;
            this.levelsComboBox.SelectedIndexChanged += new System.EventHandler(this.levelsComboBox_SelectedIndexChanged);
            // 
            // radioButtonBearing
            // 
            this.radioButtonBearing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButtonBearing.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButtonBearing.AutoSize = true;
            this.radioButtonBearing.Enabled = false;
            this.radioButtonBearing.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButtonBearing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonBearing.Image = global::Elibre.AutoStructureDesign.Properties.Resources.bearingWall;
            this.radioButtonBearing.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButtonBearing.Location = new System.Drawing.Point(518, 427);
            this.radioButtonBearing.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButtonBearing.Name = "radioButtonBearing";
            this.radioButtonBearing.Size = new System.Drawing.Size(100, 28);
            this.radioButtonBearing.TabIndex = 7;
            this.radioButtonBearing.TabStop = true;
            this.radioButtonBearing.Text = "       Bearing Wall";
            this.radioButtonBearing.UseVisualStyleBackColor = true;
            // 
            // radioButtonStud
            // 
            this.radioButtonStud.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButtonStud.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButtonStud.AutoSize = true;
            this.radioButtonStud.Enabled = false;
            this.radioButtonStud.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButtonStud.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonStud.Image = global::Elibre.AutoStructureDesign.Properties.Resources.Stud;
            this.radioButtonStud.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButtonStud.Location = new System.Drawing.Point(450, 427);
            this.radioButtonStud.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButtonStud.Name = "radioButtonStud";
            this.radioButtonStud.Size = new System.Drawing.Size(59, 28);
            this.radioButtonStud.TabIndex = 6;
            this.radioButtonStud.TabStop = true;
            this.radioButtonStud.Text = "      Stud";
            this.radioButtonStud.UseVisualStyleBackColor = true;
            // 
            // radioButton_Openings
            // 
            this.radioButton_Openings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButton_Openings.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton_Openings.AutoSize = true;
            this.radioButton_Openings.Enabled = false;
            this.radioButton_Openings.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButton_Openings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_Openings.Image = global::Elibre.AutoStructureDesign.Properties.Resources.Opening;
            this.radioButton_Openings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButton_Openings.Location = new System.Drawing.Point(338, 427);
            this.radioButton_Openings.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButton_Openings.Name = "radioButton_Openings";
            this.radioButton_Openings.Size = new System.Drawing.Size(100, 28);
            this.radioButton_Openings.TabIndex = 5;
            this.radioButton_Openings.TabStop = true;
            this.radioButton_Openings.Text = "     Floor Opening";
            this.radioButton_Openings.UseVisualStyleBackColor = true;
            // 
            // radioButton_ShearWall
            // 
            this.radioButton_ShearWall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButton_ShearWall.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton_ShearWall.AutoSize = true;
            this.radioButton_ShearWall.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButton_ShearWall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_ShearWall.Image = global::Elibre.AutoStructureDesign.Properties.Resources.shearWallIco20;
            this.radioButton_ShearWall.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButton_ShearWall.Location = new System.Drawing.Point(240, 427);
            this.radioButton_ShearWall.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButton_ShearWall.Name = "radioButton_ShearWall";
            this.radioButton_ShearWall.Size = new System.Drawing.Size(89, 28);
            this.radioButton_ShearWall.TabIndex = 4;
            this.radioButton_ShearWall.TabStop = true;
            this.radioButton_ShearWall.Text = "      Shear Wall";
            this.radioButton_ShearWall.UseVisualStyleBackColor = true;
            this.radioButton_ShearWall.CheckedChanged += new System.EventHandler(this.radioButtonMode_CheckedChanged);
            // 
            // radioButton_Post
            // 
            this.radioButton_Post.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButton_Post.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton_Post.AutoSize = true;
            this.radioButton_Post.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButton_Post.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_Post.Image = global::Elibre.AutoStructureDesign.Properties.Resources.ColumnsIcon;
            this.radioButton_Post.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButton_Post.Location = new System.Drawing.Point(174, 427);
            this.radioButton_Post.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButton_Post.Name = "radioButton_Post";
            this.radioButton_Post.Size = new System.Drawing.Size(58, 28);
            this.radioButton_Post.TabIndex = 3;
            this.radioButton_Post.TabStop = true;
            this.radioButton_Post.Text = "      Post";
            this.radioButton_Post.UseVisualStyleBackColor = true;
            this.radioButton_Post.CheckedChanged += new System.EventHandler(this.radioButtonMode_CheckedChanged);
            // 
            // radioButton_Beam
            // 
            this.radioButton_Beam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButton_Beam.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton_Beam.AutoSize = true;
            this.radioButton_Beam.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButton_Beam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_Beam.Image = global::Elibre.AutoStructureDesign.Properties.Resources.beamicon;
            this.radioButton_Beam.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButton_Beam.Location = new System.Drawing.Point(102, 427);
            this.radioButton_Beam.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButton_Beam.Name = "radioButton_Beam";
            this.radioButton_Beam.Size = new System.Drawing.Size(64, 28);
            this.radioButton_Beam.TabIndex = 2;
            this.radioButton_Beam.TabStop = true;
            this.radioButton_Beam.Text = "      Beam";
            this.radioButton_Beam.UseVisualStyleBackColor = true;
            this.radioButton_Beam.CheckedChanged += new System.EventHandler(this.radioButtonMode_CheckedChanged);
            // 
            // radioButton_Joist
            // 
            this.radioButton_Joist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButton_Joist.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton_Joist.AutoSize = true;
            this.radioButton_Joist.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.radioButton_Joist.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_Joist.Image = global::Elibre.AutoStructureDesign.Properties.Resources.joist_Area_2;
            this.radioButton_Joist.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButton_Joist.Location = new System.Drawing.Point(13, 427);
            this.radioButton_Joist.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButton_Joist.Name = "radioButton_Joist";
            this.radioButton_Joist.Size = new System.Drawing.Size(80, 28);
            this.radioButton_Joist.TabIndex = 1;
            this.radioButton_Joist.TabStop = true;
            this.radioButton_Joist.Text = "     Joist Area";
            this.radioButton_Joist.UseVisualStyleBackColor = true;
            this.radioButton_Joist.CheckedChanged += new System.EventHandler(this.radioButtonMode_CheckedChanged);
            // 
            // AnalysisControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radioButtonBearing);
            this.Controls.Add(this.radioButtonStud);
            this.Controls.Add(this.radioButton_Openings);
            this.Controls.Add(this.radioButton_ShearWall);
            this.Controls.Add(this.radioButton_Post);
            this.Controls.Add(this.radioButton_Beam);
            this.Controls.Add(this.radioButton_Joist);
            this.Controls.Add(this.panel_Drawing);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "AnalysisControl";
            this.Size = new System.Drawing.Size(641, 469);
            this.Load += new System.EventHandler(this.AnalysisControl_Load);
            this.panel_Drawing.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel_Drawing;
        private System.Windows.Forms.RadioButton radioButton_Joist;
        private System.Windows.Forms.RadioButton radioButton_Beam;
        private System.Windows.Forms.RadioButton radioButton_Post;
        private System.Windows.Forms.RadioButton radioButton_ShearWall;
        private System.Windows.Forms.RadioButton radioButton_Openings;
        private System.Windows.Forms.RadioButton radioButtonStud;
        private System.Windows.Forms.RadioButton radioButtonBearing;
        private System.Windows.Forms.ComboBox levelsComboBox;
    }
}
