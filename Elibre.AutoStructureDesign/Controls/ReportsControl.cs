﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Elibre.AutoStructureDesign.Models;
using EFraming.Objects;
using System.IO;

namespace Elibre.AutoStructureDesign.Controls
{
    public partial class ReportsControl : UserControl
    {
        public Building StructureBuildling { get; set; }

        public ReportsControl()
        {
            InitializeComponent();

            StructureBuildling = EStitcherSingleton.GetInstance.StructureBuildling;
            SetButtonStates();

        }

        public void SetButtonStates()
        {
            ReportsManager.DisableAllReports();
            if (!StructureBuildling.Solved)
                summaryReportnBtn.Enabled = false;
            else
                summaryReportnBtn.Enabled = true;


            InitializeFramingRV();
            InitializeLateralRV();

            #region Set ButtonState


            postBtn.Enabled = ReportsManager.EnablePostDesignReport;
            SeismicLoadBtn.Enabled = ReportsManager.EnableSeismicLoadReport;
            WindLoadBtn.Enabled = ReportsManager.EnableWindLoadReport;
            HoldDownBtn.Enabled = ReportsManager.EnableHoleDownScheduleReport;
            ShearWallBtn.Enabled = ReportsManager.EnableShearWallScheduleReport;
            ShearWallDesignBtn.Enabled = ReportsManager.EnableShearWallDesignReport;
            AtsBtn.Enabled = ReportsManager.EnableAtsDesignReport;
            DiaphragmBtn.Enabled = ReportsManager.EnableDiaphragmDesignReport;
            UpliftBtn.Enabled = ReportsManager.EnableUpliftCalculationsReport;
            beamBtn.Enabled = ReportsManager.EnableBeamDesignReport;
            headerDesighnBtn.Enabled = ReportsManager.EnableHeaderDesignReport;
            button4.Enabled = ReportsManager.EnableJoistDesignReport;
           // TrussBtn.Enabled = ReportsManager.EnableTrussReport;


            ChangeBackColor(postBtn);
            ChangeBackColor(beamBtn);
            ChangeBackColor(postBtn);
            ChangeBackColor(headerDesighnBtn);
            ChangeBackColor(button4);
            //ChangeBackColor(TrussBtn);

            ChangeBackColor(SeismicLoadBtn);
            ChangeBackColor(WindLoadBtn);
            ChangeBackColor(HoldDownBtn);
            ChangeBackColor(ShearWallBtn);
            ChangeBackColor(ShearWallDesignBtn);
            ChangeBackColor(AtsBtn);
            ChangeBackColor(DiaphragmBtn);
            ChangeBackColor(UpliftBtn);
            ChangeBackColor(summaryReportnBtn);

            #endregion
        }

        private void ChangeBackColor(Button btn)
        {
            if (btn.Enabled)
            {
                btn.BackColor = Color.White;
            }
            else
            {

                btn.BackColor = System.Drawing.SystemColors.ControlLight;
            }
        }

        private void InitializeFramingRV()
        {
            var obj = ReportsManager.Document.Building.FloorList.SelectMany(e => e.ObjectList);
          
            if (File.Exists(ReportGenerator.ReportGenerator.RptFilePath_Framing_BeamDesign))
            {
                var hasBeams = obj.OfType<Beam>().Any();
                ReportsManager.EnableBeamDesignReport = hasBeams;

            }

            if (File.Exists(ReportGenerator.ReportGenerator.RptFilePath_Framing_HeaderDesign))
            {
                ReportsManager.EnableHeaderDesignReport = true;
            }

            if (File.Exists(ReportGenerator.ReportGenerator.RptFilePath_Framing_PostDesign))
            {
                ReportsManager.EnablePostDesignReport = true;
            }

            if (File.Exists(ReportGenerator.ReportGenerator.RptFilePath_Framing_JoistDesign))
            {
                ReportsManager.EnableJoistDesignReport = true;
            }

        }
        private void InitializeLateralRV()
        {

            int diaphragmCount = 0;
            foreach (Floor floor in StructureBuildling.FloorList)
                diaphragmCount += floor.GetDiaphragmList().Count;

            //if (diaphragmCount > 0 && File.Exists(ReportGenerator.ReportGenerator.RptFilePath_Lateral_SeismicLoad))
            //{
            //    ReportsManager.EnableSeismicLoadReport = true;
            //}

            if (File.Exists(ReportGenerator.ReportGenerator.RptFilePath_Lateral_SeismicLoad))
            {
                ReportsManager.EnableSeismicLoadReport = true;
            }


            if (File.Exists(ReportGenerator.ReportGenerator.RptFilePath_Lateral_WindLoad))
            {
                ReportsManager.EnableWindLoadReport = true;
            }

            if (diaphragmCount > 0 && File.Exists(ReportGenerator.ReportGenerator.RptFilePath_Lateral_SeismicLoad))
            {
                ReportsManager.EnableShearWallScheduleReport = true;
            }

            if (File.Exists(ReportGenerator.ReportGenerator.RptFilePath_Lateral_ShearWallDesign))
            {
                ReportsManager.EnableShearWallDesignReport = true;
            }

            if (diaphragmCount > 0 && File.Exists(ReportGenerator.ReportGenerator.RptFilePath_Lateral_SeismicLoad))
            {
                ReportsManager.EnableHoleDownScheduleReport = true;
            }

            if (File.Exists(ReportGenerator.ReportGenerator.RptFilePath_Framing_UpliftCalculations))
            {
                ReportsManager.EnableUpliftCalculationsReport = true;
            }

            if (File.Exists(ReportGenerator.ReportGenerator.RptFilePath_Lateral_DiaphragmDesign))
            {
                ReportsManager.EnableDiaphragmDesignReport = true;
            }

        }

        #region Framing design Buttons
        private void postBtn_Click(object sender, EventArgs e)
        {
            ReportsManager.PostDesignReport();
        }
        private void beamBtn_Click(object sender, EventArgs e)
        {
            ReportsManager.BeamDesignReport();
        }
        private void headerBtn_Click(object sender, EventArgs e)
        {
            ReportsManager.HeaderDesignReport();
        }
        private void JoistBtn_Click(object sender, EventArgs e)
        {
            ReportsManager.JoistDesignReport();
        }
        private void TrussBtn_Click(object sender, EventArgs e)
        {
            ReportsManager.TrussReport();
        }
        #endregion

        #region Lateral Design Buttons


        private void SeismicLoadBtn_Click(object sender, EventArgs e)
        {
            ReportsManager.SeismicLoadReport();
        }
        private void WindLoadBtn_Click(object sender, EventArgs e)
        {
            ReportsManager.SeismicLoadReport();
        }
        private void ShearWallScheduleBtn_Click(object sender, EventArgs e)
        {
            ReportsManager.ShearWallScheduleReport();
        }
        private void HoldDownBtn_Click(object sender, EventArgs e)
        {
            ReportsManager.HoleDownScheduleReport();
        }
        private void DiaphragmBtn_Click(object sender, EventArgs e)
        {
            ReportsManager.DiaphragmDesignReport();
        }
        private void AtsBtn_Click(object sender, EventArgs e)
        {
            ReportsManager.AtsDesignReport();
        }
        private void UpliftBtn_Click(object sender, EventArgs e)
        {
            ReportsManager.UpliftCalculationsReport();
        }
        private void ShearWallDesignBtn_Click(object sender, EventArgs e)
        {
            ReportsManager.ShearWallDesignReport();
        }



        #endregion

        private void summaryReportnBtn_Click_1(object sender, EventArgs e)
        {
            ReportsManager.SummaryReport();
        }
    }
}
