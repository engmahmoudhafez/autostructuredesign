﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.Models;
using Elibre.AutoStructureDesign.States;
using Elibre.CAD.StitcherApi.Algorithms.RoofAlgorithms;

namespace Elibre.AutoStructureDesign.Controls
{
    public partial class TrussViewer : UserControl
    {

        GLStateRenderer glControl;
        GLStatesManager glStatesManager;
        TrussState _trussState;
        Truss _truss;
        public TrussViewer(Truss truss)
        {
            _truss = truss;
            glControl = new GLStateRenderer();
            glStatesManager = new GLStatesManager();

            _trussState = new TrussState(_truss);
            glStatesManager.activeState = _trussState;

            glControl.setStatesManager(glStatesManager);
            //EStitcherSingleton.GetInstance.GlobalGLRenderer;
              
            glControl.Parent = this;
            glControl.Dock = DockStyle.Fill;
        }

        
        private void TrussViewer_Load(object sender, EventArgs e)
        {
            _trussState = new TrussState(_truss);
            // EStitcherSingleton.GetInstance.StatesManager
            glStatesManager.activeState = _trussState;
            Controls.Add(glControl);
           
            _trussState.CenterDrawings();
        }
    }
}
