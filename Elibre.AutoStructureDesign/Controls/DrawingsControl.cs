﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Elibre.AutoStructureDesign.Models;
using OpenTK.Graphics.OpenGL4;
using OpenTK;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.Drawings;
using Elibre.AutoStructureDesign.States;

namespace Elibre.AutoStructureDesign.Controls
{
    public partial class DrawingsControl : UserControl
    {
        GLStateRenderer glControl;
        RoofState roofState;
        FloorState floorState;
        DetailsState detailsState;
        NotesState notesState;

        GLState currentState;
        public DrawingsControl()
        {
            InitializeComponent();
            glControl = EStitcherSingleton.GetInstance.GlobalGLRenderer;
            glControl.Parent = this;
            glControl.Dock = DockStyle.Fill;
        }

        private void DrawingsControl_Load(object sender, EventArgs e)
        {
            roofState = new RoofState();
            floorState = new FloorState();
            detailsState = new DetailsState();
            notesState = new NotesState();           
            floorState.setActiveLevel(1);
            EStitcherSingleton.GetInstance.StatesManager.activeState = floorState;
            currentState = floorState;
            Controls.Add(glControl);
            btn_text.Hide();
            UpdateState();
        }

        public void UpdateState()
        {
            EStitcherSingleton.GetInstance.StatesManager.activeState = currentState;
            glControl = EStitcherSingleton.GetInstance.GlobalGLRenderer;
            glControl.Parent = this;
            glControl.Dock = DockStyle.Fill;
            Controls.Add(glControl);
            currentState.UpdateStateItems();
            currentState.Update();
            cb_levels.Items.Clear();
            int i = 0;
            foreach (var item in EStitcherSingleton.GetInstance.Building.Levels)
            {
                cb_levels.Items.Add("Level " + i);
                ++i;
            }
            cb_levels.SelectedIndex = 0;
            currentState.CenterDrawings();
        }

        private void btnShowFloor_Click(object sender, EventArgs e)
        {
            EStitcherSingleton.GetInstance.StatesManager.activeState = floorState;
            currentState = floorState;
            currentState.UpdateStateItems();
            floorState.CenterDrawings();
            cb_levels.Visible = true;
            panel_Highlight.Location = new Point(btnShowFloor.Location.X, panel_Highlight.Location.Y);
            btn_text.Hide();
            
        }

        private void btnShowRoof_Click(object sender, EventArgs e)
        {
            EStitcherSingleton.GetInstance.StatesManager.activeState = roofState;
            currentState = roofState;
            currentState.UpdateStateItems();
            roofState.CenterDrawings();
            cb_levels.Visible = false;
            panel_Highlight.Location = new Point(btnShowRoof.Location.X, panel_Highlight.Location.Y);
            btn_text.Hide();

        }

        private void cb_levels_SelectedIndexChanged(object sender, EventArgs e)
        {
            floorState.setActiveLevel(cb_levels.SelectedIndex + 1);
            floorState.CenterDrawings();
        }

        private void buttonDetails_Click(object sender, EventArgs e)
        {
            panel_Highlight.Location = new Point(buttonDetails.Location.X, panel_Highlight.Location.Y);
            EStitcherSingleton.GetInstance.StatesManager.activeState = detailsState;
            currentState = detailsState;
            detailsState.CenterDrawings();
            cb_levels.Visible = false;
            btn_text.Hide();

        }

        private void buttonNotes_Click(object sender, EventArgs e)
        {
            panel_Highlight.Location = new Point(buttonNotes.Location.X, panel_Highlight.Location.Y);
            EStitcherSingleton.GetInstance.StatesManager.activeState = notesState;
            currentState = notesState;
            notesState.CenterDrawings();
            cb_levels.Visible = false;
            btn_text.Show();
        }

        private void btn_autocad_Click(object sender, EventArgs e)
        {

        }
    }
}
