﻿namespace Elibre.AutoStructureDesign.Controls
{
    partial class SelectTemplateControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOk = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Load = new System.Windows.Forms.Button();
            this.lstBox_Templates = new System.Windows.Forms.ListBox();
            this.groupBoxTemplates = new System.Windows.Forms.GroupBox();
            this.groupBoxTemplates.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOk.Location = new System.Drawing.Point(447, 458);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(110, 42);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = "Start";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Save.Location = new System.Drawing.Point(33, 458);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(110, 42);
            this.btn_Save.TabIndex = 3;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Load
            // 
            this.btn_Load.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Load.Location = new System.Drawing.Point(150, 458);
            this.btn_Load.Name = "btn_Load";
            this.btn_Load.Size = new System.Drawing.Size(110, 42);
            this.btn_Load.TabIndex = 4;
            this.btn_Load.Text = "Load";
            this.btn_Load.UseVisualStyleBackColor = true;
            this.btn_Load.Click += new System.EventHandler(this.btn_Load_Click);
            // 
            // lstBox_Templates
            // 
            this.lstBox_Templates.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBox_Templates.FormattingEnabled = true;
            this.lstBox_Templates.ItemHeight = 25;
            this.lstBox_Templates.Location = new System.Drawing.Point(33, 50);
            this.lstBox_Templates.Name = "lstBox_Templates";
            this.lstBox_Templates.Size = new System.Drawing.Size(523, 379);
            this.lstBox_Templates.TabIndex = 5;
            this.lstBox_Templates.SelectedIndexChanged += new System.EventHandler(this.lstBox_Templates_SelectedIndexChanged);
            this.lstBox_Templates.DoubleClick += new System.EventHandler(this.lstBox_Templates_DoubleClick);
            // 
            // groupBoxTemplates
            // 
            this.groupBoxTemplates.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBoxTemplates.Controls.Add(this.lstBox_Templates);
            this.groupBoxTemplates.Controls.Add(this.buttonOk);
            this.groupBoxTemplates.Controls.Add(this.btn_Load);
            this.groupBoxTemplates.Controls.Add(this.btn_Save);
            this.groupBoxTemplates.Location = new System.Drawing.Point(257, 99);
            this.groupBoxTemplates.Name = "groupBoxTemplates";
            this.groupBoxTemplates.Size = new System.Drawing.Size(593, 532);
            this.groupBoxTemplates.TabIndex = 6;
            this.groupBoxTemplates.TabStop = false;
            this.groupBoxTemplates.Text = "Templates";
            // 
            // SelectTemplateControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.groupBoxTemplates);
            this.Name = "SelectTemplateControl";
            this.Size = new System.Drawing.Size(1057, 707);
            this.Load += new System.EventHandler(this.SelectTemplateControl_Load);
            this.groupBoxTemplates.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Load;
        private System.Windows.Forms.ListBox lstBox_Templates;
        private System.Windows.Forms.GroupBox groupBoxTemplates;
    }
}
