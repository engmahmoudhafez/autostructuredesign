﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Elibre.AutoStructureDesign.Models;
using EFraming.Design.Forms;
using EFraming.Objects;
using System.IO;
using System.Xml.Linq;
using EFraming.Design;
using FRDesign.Forms;
using EFraming;

namespace Elibre.AutoStructureDesign.Controls
{
    public partial class TemplateControl : UserControl
    {
        List<FRDesign.LoadCombination.Code.AnalysisLoadCombinationList> lst;
        public EventHandler OnOkClicked { get; set; }
        public TemplateControl()
        {
            InitializeComponent();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            OnOkClicked?.Invoke(sender, e);
        }

        private void ShearWallTypesDesignGroups()
        {
            var building = EStitcherSingleton.GetInstance.StructureBuildling;
            frmSelectedSWTGroup frm = new frmSelectedSWTGroup(building);
            frm.TopLevel = false;
            frm.Parent = panel_ShearWallTypesDesignGroups;
            FitFormInsideTab(frm);
        }

        private void DefineSectionGroups()
        {
            var building = EStitcherSingleton.GetInstance.StructureBuildling;

            #region Getting all framing design groups
            var masterDesignConfigsFolderFullPath = MasterDesignConfigurations.MasterDesignConfigsFolder();
            var path_FDG = Path.Combine(masterDesignConfigsFolderFullPath,
                MasterDesignConfigurations.FolderNameFramingDesignGroups);

            var filePath = path_FDG + "\\" + "Default" + ".xml";

            if (!XNode.DeepEquals(XDocument.Load("XMLFiles\\Default Design Configurations\\Framing Design Groups\\" + "Default" + ".xml"),
                XDocument.Load(filePath)))
            {
                File.Delete(filePath);
                File.Copy("XMLFiles\\Default Design Configurations\\Framing Design Groups\\" + "Default" + ".xml"
                , filePath);
            }

            #endregion
            FRDesign.Forms.frmSectionGroups frm = new FRDesign.Forms.frmSectionGroups(building.FramingDesign_Group_Set, path_FDG);
            frm.TopLevel = false;
            frm.Parent = panel_DefineSectionGroups;
            frm.IsFromStitcherModel = true;
            FitFormInsideTab(frm);

        }

        private void StructuralHardwareDesignGroup()
        {
            Building building = EStitcherSingleton.GetInstance.StructureBuildling;

            frmStrHardwareDesignGrps frm = new frmStrHardwareDesignGrps(MasterDesignConfigurations.GetSHDG(building), building.Solved);
            frm.TopLevel = false;
            frm.Parent = panel_StructuralHardwareDesignGroup;
            frm.IsFromStitcherModel = true;
            FitFormInsideTab(frm);
        }

        private void FramingAreaSettings()
        {
            Building building = EStitcherSingleton.GetInstance.StructureBuildling;

            var path_FASG = Path.Combine(MasterDesignConfigurations.MasterDesignConfigsFolder(),
                   MasterDesignConfigurations.FolderNameFramingAreaSettings);
            var activeFAS = MasterDesignConfigurations.GetFasg(building);
            UIDesignDialogs.Forms.DesignConfigurationManager_Forms.frmFramingArea frm =
                 new UIDesignDialogs.Forms.DesignConfigurationManager_Forms.frmFramingArea(path_FASG, activeFAS);
            frm.TopLevel = false;
            frm.Parent = panel_FramingAreaSettings;
            frm.IsFromStitcherModel = true;
            FitFormInsideTab(frm);            

        }

        private void BuildingCodes()
        {
            Building building = EStitcherSingleton.GetInstance.StructureBuildling;

            frmCodeLoads frm = new frmCodeLoads();
            frm.TopLevel = false;
            frm.Parent = panel_BuildingCode;
            frm.IsFromStitcherModel = true;

            LateralDesign.PrepareSeismicWindData(building);

            frm.IsSolved = building.Solved;
            frm.analysisData = building.SeismicWindAnalysisData;

            LateralDesign.building = building;
            frm.IRC2015DefaultCheck = new CanUseIRC2015(LateralDesign.CanUseIRC2015Code);
            FitFormInsideTab(frm);
        }

        private void LoadCombinations()
        {
            Building building = EStitcherSingleton.GetInstance.StructureBuildling;

            lst = new List<FRDesign.LoadCombination.Code.AnalysisLoadCombinationList>();
            lst.AddRange(building.LoadCombinationsList);

            frmEditLoadCombinations frm = new frmEditLoadCombinations(lst, building.Solved, building.SelectedLoadCombinations.Name);
            frm.TopLevel = false;
            frm.Parent = panel_LoadCombinations;
            frm.IsFromStitcherModel = true;
            FitFormInsideTab(frm);
        }

        private void TemplateTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeSelection();
        }
        private void ChangeSelection()
        {
            switch (TemplateTab.SelectedIndex)
            {
                case 0:
                    ShearWallTypesDesignGroups();
                    return;
                case 1:
                    DefineSectionGroups();
                    return;
                case 2:
                    StructuralHardwareDesignGroup();
                    return;
                case 3:
                    FramingAreaSettings();
                    return;
                case 4:
                    BuildingCodes();
                    return;
                case 5:
                    LoadCombinations();
                    return;
            }

        }
        private void tab_DefineSectionGroups_Leave(object sender, EventArgs e)
        {
            var building = EStitcherSingleton.GetInstance.StructureBuildling;
            var frm = TemplateTab.SelectedTab.Tag as frmSectionGroups;
            building.FramingDesign_Group_Set = frm.group_Set;
            building.OnFramingDesign_Group_SetChanged();
            frm.Close();
            frm.Dispose();
        }

        private void tab_StructuralHardwareDesignGroup_Leave(object sender, EventArgs e)
        {
            var building = EStitcherSingleton.GetInstance.StructureBuildling;
            var frm = TemplateTab.SelectedTab.Tag as frmStrHardwareDesignGrps;
            MasterDesignConfigurations.ApplySHDG(frm.SDHG, building);
            frm.Close();
            frm.Dispose();
        }
        private void FitFormInsideTab(Form frm)
        {
            TemplateTab.SelectedTab.Tag = frm;
            frm.Dock = DockStyle.Left;
            frm.FormBorderStyle = FormBorderStyle.None;
            frm.Show();
            frm.AutoScroll = true;
            frm.BringToFront();
        }

        private void tab_FramingAreaSettings_Leave(object sender, EventArgs e)
        {
            var building = EStitcherSingleton.GetInstance.StructureBuildling;
            var frm = TemplateTab.SelectedTab.Tag as UIDesignDialogs.Forms.DesignConfigurationManager_Forms.frmFramingArea;
            var selFAS = frm.SelFramingAreasettings;
            building.DesignFasgConfig = selFAS.Name;
            MasterDesignConfigurations.ApplyFASG(selFAS, building);
            frm.Close();
            frm.Dispose();
        }

        private void tab_BuildingCode_Leave(object sender, EventArgs e)
        {
            var building = EStitcherSingleton.GetInstance.StructureBuildling;
            var frm = TemplateTab.SelectedTab.Tag as frmCodeLoads;
           // frm.save();
            building.SeismicWindAnalysisData = frm.analysisData;
            frm.Close();
            frm.Dispose();
        }

        private void tab_LoadCombinations_Leave(object sender, EventArgs e)
        {
            var building = EStitcherSingleton.GetInstance.StructureBuildling;
            var frm = TemplateTab.SelectedTab.Tag as frmEditLoadCombinations;
         
            List<FRDesign.LoadCombination.Code.AnalysisLoadCombinationList> loadCombs = frm.GetLoadCombinations();
            building.LoadCombinationsList = loadCombs;

            if (frm.SelectedLoadCombinations == null)
            {
                if (building.LoadCombinationsList == null || building.LoadCombinationsList.Count == 0)
                    loadCombs = FramingDesign.GetDefaultLoadCombinationList();

                foreach (FRDesign.LoadCombination.Code.AnalysisLoadCombinationList item in loadCombs)
                {
                    if (item.Selected)
                        building.SelectedLoadCombinations = new FRDesign.LoadCombination.Code.AnalysisLoadCombinationList(item);
                }

            }
            else
                foreach (FRDesign.LoadCombination.Code.AnalysisLoadCombinationList item in loadCombs)
                {
                    if (item.Name == frm.SelectedLoadCombinations)
                    {
                        building.SelectedLoadCombinations = new FRDesign.LoadCombination.Code.AnalysisLoadCombinationList(item);
                    }
                }

            FramingDesign.DisposeList(lst);
            GC.SuppressFinalize(lst);
            frm.Close();
            frm.Dispose();
        }

        private void TemplateControl_Load(object sender, EventArgs e)
        {
            LoadForm();
        }
        public void LoadForm()
        {
            TemplateTab.SelectedIndex = 0;
            ChangeSelection();

        }
        private void tab_ShearwallTypesDesignGroups_Leave(object sender, EventArgs e)
        {
            var building = EStitcherSingleton.GetInstance.StructureBuildling;
            var frm = TemplateTab.SelectedTab.Tag as frmSelectedSWTGroup;
            frm.Close();
            frm.Dispose();
        }

        private void TemplateControl_Enter(object sender, EventArgs e)
        {
            //TemplateTab.SelectedIndex = 0;
            //TemplateTab_SelectedIndexChanged(sender, e);
        }
    }
}
