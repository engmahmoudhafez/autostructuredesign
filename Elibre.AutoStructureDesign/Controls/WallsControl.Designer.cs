﻿namespace Elibre.AutoStructureDesign.Controls
{
    partial class WallsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cb_levels = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cb_levels
            // 
            this.cb_levels.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb_levels.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.cb_levels.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb_levels.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_levels.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_levels.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_levels.FormattingEnabled = true;
            this.cb_levels.Location = new System.Drawing.Point(428, 16);
            this.cb_levels.Name = "cb_levels";
            this.cb_levels.Size = new System.Drawing.Size(121, 21);
            this.cb_levels.TabIndex = 0;
            this.cb_levels.SelectedIndexChanged += new System.EventHandler(this.cb_levels_SelectedIndexChanged);
            // 
            // WallsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cb_levels);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "WallsControl";
            this.Size = new System.Drawing.Size(566, 462);
            this.Load += new System.EventHandler(this.WallsControl_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cb_levels;
    }
}
