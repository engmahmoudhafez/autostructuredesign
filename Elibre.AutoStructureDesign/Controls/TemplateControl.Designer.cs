﻿namespace Elibre.AutoStructureDesign.Controls
{
    partial class TemplateControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOk = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.TemplateTab = new System.Windows.Forms.TabControl();
            this.tab_ShearwallTypesDesignGroups = new System.Windows.Forms.TabPage();
            this.panel_ShearWallTypesDesignGroups = new System.Windows.Forms.Panel();
            this.tab_DefineSectionGroups = new System.Windows.Forms.TabPage();
            this.panel_DefineSectionGroups = new System.Windows.Forms.Panel();
            this.tab_StructuralHardwareDesignGroup = new System.Windows.Forms.TabPage();
            this.panel_StructuralHardwareDesignGroup = new System.Windows.Forms.Panel();
            this.tab_FramingAreaSettings = new System.Windows.Forms.TabPage();
            this.panel_FramingAreaSettings = new System.Windows.Forms.Panel();
            this.tab_BuildingCode = new System.Windows.Forms.TabPage();
            this.panel_BuildingCode = new System.Windows.Forms.Panel();
            this.tab_LoadCombinations = new System.Windows.Forms.TabPage();
            this.panel_LoadCombinations = new System.Windows.Forms.Panel();
            this.TemplateTab.SuspendLayout();
            this.tab_ShearwallTypesDesignGroups.SuspendLayout();
            this.tab_DefineSectionGroups.SuspendLayout();
            this.tab_StructuralHardwareDesignGroup.SuspendLayout();
            this.tab_FramingAreaSettings.SuspendLayout();
            this.tab_BuildingCode.SuspendLayout();
            this.tab_LoadCombinations.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(773, 626);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(136, 35);
            this.buttonOk.TabIndex = 0;
            this.buttonOk.Text = "Start";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // TemplateTab
            // 
            this.TemplateTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TemplateTab.Controls.Add(this.tab_ShearwallTypesDesignGroups);
            this.TemplateTab.Controls.Add(this.tab_DefineSectionGroups);
            this.TemplateTab.Controls.Add(this.tab_StructuralHardwareDesignGroup);
            this.TemplateTab.Controls.Add(this.tab_FramingAreaSettings);
            this.TemplateTab.Controls.Add(this.tab_BuildingCode);
            this.TemplateTab.Controls.Add(this.tab_LoadCombinations);
            this.TemplateTab.ItemSize = new System.Drawing.Size(250, 25);
            this.TemplateTab.Location = new System.Drawing.Point(14, 6);
            this.TemplateTab.Name = "TemplateTab";
            this.TemplateTab.Padding = new System.Drawing.Point(20, 3);
            this.TemplateTab.SelectedIndex = 0;
            this.TemplateTab.Size = new System.Drawing.Size(895, 614);
            this.TemplateTab.TabIndex = 3;
            this.TemplateTab.SelectedIndexChanged += new System.EventHandler(this.TemplateTab_SelectedIndexChanged);
            // 
            // tab_ShearwallTypesDesignGroups
            // 
            this.tab_ShearwallTypesDesignGroups.Controls.Add(this.panel_ShearWallTypesDesignGroups);
            this.tab_ShearwallTypesDesignGroups.Location = new System.Drawing.Point(4, 29);
            this.tab_ShearwallTypesDesignGroups.Name = "tab_ShearwallTypesDesignGroups";
            this.tab_ShearwallTypesDesignGroups.Padding = new System.Windows.Forms.Padding(3);
            this.tab_ShearwallTypesDesignGroups.Size = new System.Drawing.Size(887, 581);
            this.tab_ShearwallTypesDesignGroups.TabIndex = 0;
            this.tab_ShearwallTypesDesignGroups.Text = "Shear Wall Types Design Groups";
            this.tab_ShearwallTypesDesignGroups.UseVisualStyleBackColor = true;
            this.tab_ShearwallTypesDesignGroups.Leave += new System.EventHandler(this.tab_ShearwallTypesDesignGroups_Leave);
            // 
            // panel_ShearWallTypesDesignGroups
            // 
            this.panel_ShearWallTypesDesignGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_ShearWallTypesDesignGroups.Location = new System.Drawing.Point(3, 3);
            this.panel_ShearWallTypesDesignGroups.Name = "panel_ShearWallTypesDesignGroups";
            this.panel_ShearWallTypesDesignGroups.Size = new System.Drawing.Size(881, 575);
            this.panel_ShearWallTypesDesignGroups.TabIndex = 4;
            // 
            // tab_DefineSectionGroups
            // 
            this.tab_DefineSectionGroups.Controls.Add(this.panel_DefineSectionGroups);
            this.tab_DefineSectionGroups.Location = new System.Drawing.Point(4, 29);
            this.tab_DefineSectionGroups.Name = "tab_DefineSectionGroups";
            this.tab_DefineSectionGroups.Padding = new System.Windows.Forms.Padding(3);
            this.tab_DefineSectionGroups.Size = new System.Drawing.Size(887, 581);
            this.tab_DefineSectionGroups.TabIndex = 1;
            this.tab_DefineSectionGroups.Text = "Define Section Groups";
            this.tab_DefineSectionGroups.UseVisualStyleBackColor = true;
            this.tab_DefineSectionGroups.Leave += new System.EventHandler(this.tab_DefineSectionGroups_Leave);
            // 
            // panel_DefineSectionGroups
            // 
            this.panel_DefineSectionGroups.AutoSize = true;
            this.panel_DefineSectionGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_DefineSectionGroups.Location = new System.Drawing.Point(3, 3);
            this.panel_DefineSectionGroups.Name = "panel_DefineSectionGroups";
            this.panel_DefineSectionGroups.Size = new System.Drawing.Size(881, 575);
            this.panel_DefineSectionGroups.TabIndex = 0;
            // 
            // tab_StructuralHardwareDesignGroup
            // 
            this.tab_StructuralHardwareDesignGroup.Controls.Add(this.panel_StructuralHardwareDesignGroup);
            this.tab_StructuralHardwareDesignGroup.Location = new System.Drawing.Point(4, 29);
            this.tab_StructuralHardwareDesignGroup.Name = "tab_StructuralHardwareDesignGroup";
            this.tab_StructuralHardwareDesignGroup.Size = new System.Drawing.Size(887, 581);
            this.tab_StructuralHardwareDesignGroup.TabIndex = 2;
            this.tab_StructuralHardwareDesignGroup.Text = "Structural Hardware Design Group";
            this.tab_StructuralHardwareDesignGroup.UseVisualStyleBackColor = true;
            this.tab_StructuralHardwareDesignGroup.Leave += new System.EventHandler(this.tab_StructuralHardwareDesignGroup_Leave);
            // 
            // panel_StructuralHardwareDesignGroup
            // 
            this.panel_StructuralHardwareDesignGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_StructuralHardwareDesignGroup.Location = new System.Drawing.Point(0, 0);
            this.panel_StructuralHardwareDesignGroup.Name = "panel_StructuralHardwareDesignGroup";
            this.panel_StructuralHardwareDesignGroup.Size = new System.Drawing.Size(887, 581);
            this.panel_StructuralHardwareDesignGroup.TabIndex = 0;
            // 
            // tab_FramingAreaSettings
            // 
            this.tab_FramingAreaSettings.Controls.Add(this.panel_FramingAreaSettings);
            this.tab_FramingAreaSettings.Location = new System.Drawing.Point(4, 29);
            this.tab_FramingAreaSettings.Name = "tab_FramingAreaSettings";
            this.tab_FramingAreaSettings.Size = new System.Drawing.Size(887, 581);
            this.tab_FramingAreaSettings.TabIndex = 3;
            this.tab_FramingAreaSettings.Text = "Framing Area Settings";
            this.tab_FramingAreaSettings.UseVisualStyleBackColor = true;
            this.tab_FramingAreaSettings.Leave += new System.EventHandler(this.tab_FramingAreaSettings_Leave);
            // 
            // panel_FramingAreaSettings
            // 
            this.panel_FramingAreaSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_FramingAreaSettings.Location = new System.Drawing.Point(0, 0);
            this.panel_FramingAreaSettings.Name = "panel_FramingAreaSettings";
            this.panel_FramingAreaSettings.Size = new System.Drawing.Size(887, 581);
            this.panel_FramingAreaSettings.TabIndex = 0;
            // 
            // tab_BuildingCode
            // 
            this.tab_BuildingCode.Controls.Add(this.panel_BuildingCode);
            this.tab_BuildingCode.Location = new System.Drawing.Point(4, 29);
            this.tab_BuildingCode.Name = "tab_BuildingCode";
            this.tab_BuildingCode.Size = new System.Drawing.Size(887, 581);
            this.tab_BuildingCode.TabIndex = 4;
            this.tab_BuildingCode.Text = "Building Code";
            this.tab_BuildingCode.UseVisualStyleBackColor = true;
            this.tab_BuildingCode.Leave += new System.EventHandler(this.tab_BuildingCode_Leave);
            // 
            // panel_BuildingCode
            // 
            this.panel_BuildingCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_BuildingCode.Location = new System.Drawing.Point(0, 0);
            this.panel_BuildingCode.Name = "panel_BuildingCode";
            this.panel_BuildingCode.Size = new System.Drawing.Size(887, 581);
            this.panel_BuildingCode.TabIndex = 0;
            // 
            // tab_LoadCombinations
            // 
            this.tab_LoadCombinations.Controls.Add(this.panel_LoadCombinations);
            this.tab_LoadCombinations.Location = new System.Drawing.Point(4, 29);
            this.tab_LoadCombinations.Name = "tab_LoadCombinations";
            this.tab_LoadCombinations.Size = new System.Drawing.Size(887, 581);
            this.tab_LoadCombinations.TabIndex = 5;
            this.tab_LoadCombinations.Text = "Load Combinations";
            this.tab_LoadCombinations.UseVisualStyleBackColor = true;
            this.tab_LoadCombinations.Leave += new System.EventHandler(this.tab_LoadCombinations_Leave);
            // 
            // panel_LoadCombinations
            // 
            this.panel_LoadCombinations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_LoadCombinations.Location = new System.Drawing.Point(0, 0);
            this.panel_LoadCombinations.Name = "panel_LoadCombinations";
            this.panel_LoadCombinations.Size = new System.Drawing.Size(887, 581);
            this.panel_LoadCombinations.TabIndex = 0;
            // 
            // TemplateControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TemplateTab);
            this.Controls.Add(this.buttonOk);
            this.Name = "TemplateControl";
            this.Size = new System.Drawing.Size(926, 674);
            this.Load += new System.EventHandler(this.TemplateControl_Load);
            this.TemplateTab.ResumeLayout(false);
            this.tab_ShearwallTypesDesignGroups.ResumeLayout(false);
            this.tab_DefineSectionGroups.ResumeLayout(false);
            this.tab_DefineSectionGroups.PerformLayout();
            this.tab_StructuralHardwareDesignGroup.ResumeLayout(false);
            this.tab_FramingAreaSettings.ResumeLayout(false);
            this.tab_BuildingCode.ResumeLayout(false);
            this.tab_LoadCombinations.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOk;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TabControl TemplateTab;
        private System.Windows.Forms.TabPage tab_ShearwallTypesDesignGroups;
        private System.Windows.Forms.TabPage tab_DefineSectionGroups;
        private System.Windows.Forms.TabPage tab_StructuralHardwareDesignGroup;
        private System.Windows.Forms.TabPage tab_FramingAreaSettings;
        private System.Windows.Forms.TabPage tab_BuildingCode;
        private System.Windows.Forms.TabPage tab_LoadCombinations;
        private System.Windows.Forms.Panel panel_ShearWallTypesDesignGroups;
        private System.Windows.Forms.Panel panel_DefineSectionGroups;
        private System.Windows.Forms.Panel panel_StructuralHardwareDesignGroup;
        private System.Windows.Forms.Panel panel_FramingAreaSettings;
        private System.Windows.Forms.Panel panel_BuildingCode;
        private System.Windows.Forms.Panel panel_LoadCombinations;
    }
}
