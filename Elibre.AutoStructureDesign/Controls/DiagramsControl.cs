﻿using System;
using System.Windows.Forms;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.Models;
using Elibre.AutoStructureDesign.States;
using EFraming.Objects;
using Elibre.AutoStructureDesign.Dialogs;

namespace Elibre.AutoStructureDesign.Controls
{
    public partial class DiagramsControl : UserControl
    {

        private FrmDiagramViewer _frmDiagramViewer;
        public DiagramsControl()
        {
            InitializeComponent();
        }

        private void DigramsControl_Load(object sender, EventArgs e)
        {
            _frmDiagramViewer = new FrmDiagramViewer();
            _frmDiagramViewer.Invalidate();
        }
        #region Buttons Method;

        #endregion
        private void SeismicNormalBtn_Click(object sender, EventArgs e)
        {
            ShowDiagramForm(ActiveResults.Seismic, DiagramType.DistributedLoad);
        }

        private void ShowDiagramForm(ActiveResults activeResults, DiagramType type)
        {

            _frmDiagramViewer.UpdateDiagram(activeResults, type);

            _frmDiagramViewer.ShowDialog();

        }

        private void seismicShearBtn_Click(object sender, EventArgs e)
        {
            ShowDiagramForm(ActiveResults.Seismic, DiagramType.ShearForce);
        }

        private void seismicMomentbtn_Click(object sender, EventArgs e)
        {
            ShowDiagramForm(ActiveResults.Seismic, DiagramType.BendingMoment);
        }

        private void windNormalBtn_Click(object sender, EventArgs e)
        {
            ShowDiagramForm(ActiveResults.Wind, DiagramType.DistributedLoad);
        }

        private void windShearBtn_Click(object sender, EventArgs e)
        {
            ShowDiagramForm(ActiveResults.Wind, DiagramType.ShearForce);
        }

        private void windMomentBtn_Click(object sender, EventArgs e)
        {
            ShowDiagramForm(ActiveResults.Wind, DiagramType.BendingMoment);
        }

        internal void ToggleButtons()
        {
            if (StructuralExporter.Document.Building.Solved)
            {
                seismicMomentbtn.Enabled = true;
                seismicShearBtn.Enabled = true;
                SeismicNormalBtn.Enabled = true;
                WindNormal.Enabled = true;
                windMoment.Enabled = true;
                windShear.Enabled = true;
            }
            else
            {

                seismicMomentbtn.Enabled = false;
                seismicShearBtn.Enabled = false;
                SeismicNormalBtn.Enabled = false;
                WindNormal.Enabled = false;
                windMoment.Enabled = false;
                windShear.Enabled = false;
            }
        }
    }
}
