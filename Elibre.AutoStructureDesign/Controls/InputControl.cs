﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EFraming;

namespace Elibre.AutoStructureDesign.Controls
{
    public partial class InputControl : UserControl
    {
        public EventHandler OnOpenTemplate { get; set; }
        public InputControl()
        {
            InitializeComponent();
        }

        private void button_StructureTemplate_Click(object sender, EventArgs e)
        {
            OnOpenTemplate?.Invoke(sender, e);
        }

        private void button_ProjectInfo_Click(object sender, EventArgs e)
        {
            ProjectInformation();
        }
        private void ProjectInformation()
        {
            Project project = StructuralExporter.Document.Project;

            if (project == null)
                return;

            ProjectInfo info = project.ProjectInfo;

            if (info == null)
            {
                info = new ProjectInfo();
                project.ProjectInfo = info;
            }


            frmProjectInfo frmInfo = new frmProjectInfo();

            frmInfo.projectID = info.ID;
            frmInfo.projectTitle = info.Title;
            frmInfo.subject = info.Subject;
            frmInfo.designedBy = info.DesignedBy;
            frmInfo.checkedBy = info.CheckedBy;
            frmInfo.date = info.Date;
            frmInfo.jobNumber = info.JobNumber;
            frmInfo.MasterDesignConfig = info.MasterDesignConfig;

            if( DialogResult.OK == frmInfo.ShowDialog())
            {
                frmInfo.ReadData();
                info.ID = frmInfo.projectID;
                info.Title = frmInfo.projectTitle;
                info.Subject = frmInfo.subject;
                info.DesignedBy = frmInfo.designedBy;
                info.CheckedBy = frmInfo.checkedBy;
                info.Date = frmInfo.date;
                info.JobNumber = frmInfo.jobNumber;
                info.MasterDesignConfig = frmInfo.MasterDesignConfig;
            }


        }
    }
}
