﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Elibre.AutoStructureDesign.Models;
using Elibre.AutoStructureDesign.Drawings;
using Elibre.AutoStructureDesign.Libraries.GLUtils;

namespace Elibre.AutoStructureDesign.Controls
{
    public partial class RoofControl : UserControl
    {
        GLStateRenderer glControl;
        RoofTrussViewer roofState;
        public RoofControl()
        {
            InitializeComponent();
            glControl = EStitcherSingleton.GetInstance.GlobalGLRenderer;
            glControl.Parent = panelContainer;
            glControl.Dock = DockStyle.Fill;
        }

        private void RoofControl_Load(object sender, EventArgs e)
        {
            roofState = new RoofTrussViewer();
            EStitcherSingleton.GetInstance.StatesManager.activeState = roofState;
            panelContainer.Controls.Clear();
            panelContainer.Controls.Add(glControl);
            roofState.CenterDrawings();
        }
        public void UpdateState()
        {
            glControl = EStitcherSingleton.GetInstance.GlobalGLRenderer;
            glControl.Parent = panelContainer;
            panelContainer.Controls.Clear();
            panelContainer.Controls.Add(glControl);
            glControl.Dock = DockStyle.Fill;
            roofState.Update();
            EStitcherSingleton.GetInstance.StatesManager.activeState = roofState;
            roofState.UpdateShownRoof();
            roofState.CenterDrawings();
        }
    }
}
