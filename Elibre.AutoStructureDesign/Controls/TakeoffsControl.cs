﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Elibre.CAD.StitcherApi.Models;
using Elibre.Geometry2D;
using Elibre.CAD.StitcherApi.Helpers;
using Elibre.Stitcher.Models.Stitching;
using Elibre.CAD.StitcherApi.Models.Stitching;
using MoreLinq;
using Elibre.CAD.Analysis;
using Elibre.AutoStructureDesign.Models;

namespace Elibre.AutoStructureDesign.Controls
{
    public partial class TakeoffsControl : UserControl
    {
        public StitcherBuilding StitcherBuildingObj { get; set; }
        public Dictionary<Slice3D, StitcherHelper.RoofLineType> RoofLinesTypesDict2 { get; set; }
        public Dictionary<string, string> DoorLetterPerTypeDict { get; set; }
        public Dictionary<string, string> WindowLetterPerTypeDict { get; set; }
        public List<int> SelectedFloorsIndexes { get; set; }
        private double perc = 0.3; // 0.001 is the old 
        public TakeoffsControl()
        {
            InitializeComponent();

            ShowArch();
        }

        #region Methods
        private void ShowArch()
        {
            StitcherBuildingObj = EStitcherSingleton.GetInstance.Building;
            var index = 0;
            SelectedFloorsIndexes = new List<int>();
            foreach (var floor in StitcherBuildingObj.Levels)
            {
                SelectedFloorsIndexes.Add(index);
                index++;
            }

            RoofLinesTypesDict2 = StitcherHelper.GetRoofLinesTypesDict(StitcherBuildingObj.RoofPolygons, perc);
            var takeOffs = FindQuantityTakeOffs();
            var control = new QuantityTakeOffsDialog();
            control.Parent = panelContainer;
            control.Dock = DockStyle.Fill;
            control.ShowData(takeOffs);
            control.Invalidate();
            control.Refresh();
        }
        private List<QuatityTakeOff> FindQuantityTakeOffs()
        {
            //get all quantities
            List<QuatityTakeOff> doorsTakeoffs = new List<QuatityTakeOff>();
            List<QuatityTakeOff> windowsTakeoffs = new List<QuatityTakeOff>();
            GetWindowsDoorsTakeoffs(out windowsTakeoffs, out doorsTakeoffs);
            var wallsTakeoffs = GetWallsTakeoffs();
            var roofsTakeoffs = GetRoofTakeoffs();
            var rooflinesTakeoffs = new List<QuatityTakeOff>();
            if (RoofLinesTypesDict2 != null && RoofLinesTypesDict2.Count != 0)
                rooflinesTakeoffs = GetRoofLinesTakeoffs(RoofLinesTypesDict2);
            var floorsTakeoffs = GetFloorsTakeoffs();

            //add takeoffs to total list
            List<QuatityTakeOff> totalQuantityTakeOffs = new List<QuatityTakeOff>();
            totalQuantityTakeOffs.AddRange(doorsTakeoffs);
            totalQuantityTakeOffs.AddRange(windowsTakeoffs);
            totalQuantityTakeOffs.AddRange(wallsTakeoffs);
            totalQuantityTakeOffs.AddRange(floorsTakeoffs);
            totalQuantityTakeOffs.AddRange(roofsTakeoffs);
            totalQuantityTakeOffs.AddRange(rooflinesTakeoffs);

            //get the strings
            ChangeTakeOffsToStrings(totalQuantityTakeOffs);
            return totalQuantityTakeOffs;
        }

        private List<QuatityTakeOff> GetRoofLinesTakeoffs(Dictionary<Slice3D, StitcherHelper.RoofLineType> RoofLinesTypesDict2)
        {
            List<QuatityTakeOff> roofsLinesTakeoffs = new List<QuatityTakeOff>();
            var linesDict = new Dictionary<string, List<Slice3D>>();
            foreach (var row in RoofLinesTypesDict2)
            {
                if (!linesDict.ContainsKey(row.Value.ToString()))
                    linesDict[row.Value.ToString()] = new List<Slice3D>() { row.Key };
                else
                    linesDict[row.Value.ToString()].Add(row.Key);
            }
            var stringList = new List<string>() { "Eaves", "Rakes", "Hips", "Valleys", "Ridges" };
            for (int i = 0; i < stringList.Count; i++)
            {
                var row = new KeyValuePair<string, List<Slice3D>>();
                if (linesDict.ContainsKey(stringList[i]))
                    row = linesDict.Where(o => o.Key == stringList[i]).ElementAt(0);
                else
                    continue;

                double totLength = row.Value.Select(o => Helper3D.GetDistance3D(o.Start, o.End)).Sum() / 12;
                string symbol = "";
                string Type = "";
                #region get symbol
                if (row.Key == "Eaves")
                {
                    symbol = "EAVE-1";
                    Type = "Eave Type 1";
                }
                else if (row.Key == "Rakes")
                {
                    symbol = "RAKE-1";
                    Type = "Rake Type 1";
                }
                else if (row.Key == "Valleys")
                {
                    symbol = "VALY-1";
                    Type = "Valley Type 1";
                }
                else if (row.Key == "Hips")
                {
                    symbol = "HIP-1";
                    Type = "Hip Ridge Type 1";
                }
                else if (row.Key == "Ridges")
                {
                    symbol = "RIDG-1";
                    Type = "Ridge Type 1";
                }

                #endregion
                roofsLinesTakeoffs.Add(new QuatityTakeOff()
                {
                    ElementName = row.Key.ToString(),
                    Type = Type,
                    Symbol = symbol,
                    Material = "-",
                    Quantity = row.Value.Count,
                    QuantityString = row.Value.Count.ToString(),
                    HeightString = "-",
                    WidthString = "-",
                    Area = 0,
                    AreaString = "-",
                    Length = totLength.ToString("0.##"),
                    Volume = "-"
                });
            }


            return roofsLinesTakeoffs;
        }
        private void GetWindowsDoorsTakeoffs(out List<QuatityTakeOff> windowsTakeoffs, out List<QuatityTakeOff> doorsTakeoffs)
        {
            DoorLetterPerTypeDict = new Dictionary<string, string>();
            WindowLetterPerTypeDict = new Dictionary<string, string>();
            //get windows & doors dictionary of type and it's symbols
            var doorDict = new Dictionary<string, List<StitcherOpening>>();
            var windowDict = new Dictionary<string, List<StitcherOpening>>();
            #region get the dictionaries
            //get windows & doors from normal walls
            for (int i = 0; i < StitcherBuildingObj.Levels.Count; i++)
            {
                var level = StitcherBuildingObj.Levels[i];
                if (!SelectedFloorsIndexes.Contains(i))
                    continue;
                foreach (var wall in level.FloorPlan.Walls)
                {
                    if (!SelectedFloorsIndexes.Contains(wall.LevelIndex))
                        continue;
                    foreach (var opening in wall.Openings)
                    {
                        if (opening.OpeningType == OpeningType.Window)
                        {
                            if (!windowDict.ContainsKey(opening.RevitType))
                                windowDict[opening.RevitType] = new List<StitcherOpening>() { opening };
                            else
                                windowDict[opening.RevitType].Add(opening);
                        }
                        else if (opening.OpeningType == OpeningType.Door)
                        {
                            if (!doorDict.ContainsKey(opening.RevitType))
                                doorDict[opening.RevitType] = new List<StitcherOpening>() { opening };
                            else
                                doorDict[opening.RevitType].Add(opening);
                        }
                    }
                }
            }

            #endregion
            //get doors takeoffs
            var doorIndex = 1;
            windowsTakeoffs = new List<QuatityTakeOff>();
            doorsTakeoffs = new List<QuatityTakeOff>();
            foreach (var row in doorDict)
            {
                var typeName = row.Value.ElementAt(0).RevitType;
                var width = row.Value.ElementAt(0).Width / 12;// revitInfo.DoorSymbols.Where(o => o.SymbolName == typeName).ElementAt(0).Width / 12;
                var height = row.Value.ElementAt(0).Height / 12;// revitInfo.DoorSymbols.Where(o => o.SymbolName == typeName).ElementAt(0).Height / 12;
                var area = width * height;
                var material = "Wood";
                if (typeName.Contains("Garage"))
                    material = "Steel";

                //get the dict which draw circles with letter beside doors slices
                var doorSymbol = "D" + doorIndex.ToString();
                DoorLetterPerTypeDict[row.Key] = doorSymbol;

                doorsTakeoffs.Add(new QuatityTakeOff()
                {
                    ElementName = "Doors",
                    Symbol = doorSymbol,
                    Type = row.Key,
                    Material = material,
                    Quantity = row.Value.Count,
                    QuantityString = row.Value.Count.ToString(),
                    Height = height,
                    HeightString = height.ToString(),
                    Width = width,
                    WidthString = width.ToString(),
                    Area = area,
                    AreaString = area.ToString(),
                    Length = "-",
                    Volume = "-"
                });

                doorIndex++;
            }

            //get windows takeoffs
            var windowIndex = 1;
            foreach (var row in windowDict)
            {
                var typeName = row.Value.ElementAt(0).RevitType;
                var width = row.Value.ElementAt(0).Width / 12; //revitInfo.WindowSymbols.Where(o => o.SymbolName == typeName).ElementAt(0).Width / 12;
                var height = row.Value.ElementAt(0).Height / 12;//revitInfo.WindowSymbols.Where(o => o.SymbolName == typeName).ElementAt(0).Height / 12;
                var area = width * height;

                //get the dict which draw circles with letter beside doors slices
                var windowSymbol = "W" + windowIndex.ToString();
                WindowLetterPerTypeDict[row.Key] = windowSymbol;

                windowsTakeoffs.Add(new QuatityTakeOff()
                {
                    ElementName = "Windows",
                    Symbol = windowSymbol,
                    Type = row.Key,
                    Material = "Glass",
                    Quantity = row.Value.Count,
                    QuantityString = row.Value.Count.ToString(),
                    Height = height,
                    HeightString = height.ToString(),
                    Width = width,
                    WidthString = width.ToString(),
                    Area = area,
                    AreaString = area.ToString(),
                    Length = "-",
                    Volume = "-"
                });

                windowIndex++;
            }

        }
        private List<QuatityTakeOff> GetWallsTakeoffs()
        {
            List<QuatityTakeOff> wallsTakeoffs = new List<QuatityTakeOff>();
            var wallDict = new Dictionary<string, List<StitcherWall>>();
            #region get dict
            var walls = StitcherBuildingObj.Levels.SelectMany(o => o.FloorPlan.Walls).Where(o => o.LevelIndex != -1).ToList();
            foreach (var wall in walls)
            {
                if (!wallDict.ContainsKey(wall.RevitType))
                    wallDict[wall.RevitType] = new List<StitcherWall>() { wall };
                else
                    wallDict[wall.RevitType].Add(wall);
            }
            #endregion
            var wallIndex = 1;
            foreach (var row in wallDict)
            {
                //get walls area
                var wallsArea = row.Value.Select(o => Math.Abs(new Polygon3D(o.CurrentProfile3D).GetArea())).Sum();

                //TODO: to be reviewed by abdo
                if (wallsArea.IsApproxZero())
                    continue;

                //get walls volume
                var wallType = StitcherBuildingObj.Template.WallTypes.Where(o => o.TypeName == row.Key).ToList().ElementAt(0);
                var wallThickness = wallType.GetThickness();
                var wallsVol = wallsArea * wallThickness;

                //get 2d length of walls
                var wallsLength = row.Value.Select(o => o.SingleWall2D.Length).Sum();

                //get walls takeoffs
                wallsTakeoffs.Add(new QuatityTakeOff()
                {
                    ElementName = "Walls",
                    Symbol = "L" + wallIndex.ToString(),
                    Type = row.Key,
                    Material = "Wood",
                    Quantity = row.Value.Count,
                    QuantityString = row.Value.Count.ToString(),
                    HeightString = "-",
                    WidthString = "-",
                    Area = wallsArea,
                    AreaString = wallsArea.ToString(),
                    Volume = wallsVol.ToString("0.##"),
                    Length = wallsLength.ToString("0.##")
                });

                wallIndex++;
            }
            return wallsTakeoffs;
        }
        private List<QuatityTakeOff> GetFloorsTakeoffs()
        {
            List<QuatityTakeOff> floorsTakeoffs = new List<QuatityTakeOff>();
            var floorIndex = 1;
            foreach (var type in StitcherBuildingObj.Template.FloorTypes)
            {
                var floorPolys = StitcherBuildingObj.Levels.SelectMany(o => o.FloorPlan.FloorPolygons).Where(o => o.RevitTypeName.Equals(type.TypeName)).ToList();
                if (floorPolys.Count == 0)
                    continue;

                if (floorPolys.Count > 0)
                    floorsTakeoffs.Add(GetFloorTakeOff(floorPolys, floorIndex));

                floorIndex++;
            }

            return floorsTakeoffs;
        }
        private QuatityTakeOff GetFloorTakeOff(List<StitcherFloorPolygon> floorPolys, int floorIndex)
        {
            double floorsArea = 0;
            foreach (var poly in floorPolys)
            {
                floorsArea += poly.FloorPolygon.ToPolygon2D().PolygonArea();
            }
            //get floors volume
            var floorType = StitcherBuildingObj.Template.FloorTypes.Where(o => o.TypeName == floorPolys[0].RevitTypeName).ToList().ElementAt(0);
            var roofThickness = floorType.GetThickness();
            var floorsVol = floorsArea * roofThickness;
            return new QuatityTakeOff()
            {
                ElementName = "Floors",
                Symbol = "F" + floorIndex.ToString(),
                Type = floorPolys[0].RevitTypeName,
                Material = "Concrete",
                Area = floorsArea,
                AreaString = floorsArea.ToString(),
                HeightString = "-",
                WidthString = "-",
                Quantity = floorPolys.Count,
                Length = "-",
                Volume = floorsVol.ToString("0.##")
            };
        }
        private List<QuatityTakeOff> GetRoofTakeoffs()
        {
            List<QuatityTakeOff> roofsTakeoffs = new List<QuatityTakeOff>();
            var roofIndex = 1;
            var simplePolys = StitcherBuildingObj.RoofPolygons.Where(o => o.HasDetected && o.Shape == StitcherRoofPolygon.RoofShape.Simple).ToList();
            foreach (var type in StitcherBuildingObj.Template.RoofTypes)
            {
                var roofPolys = simplePolys.Where(o => o.RevitTypeName.Equals(type.TypeName)).ToList();

                if (roofPolys.Count > 0)
                    roofsTakeoffs.Add(GetRoofTakeOff(roofPolys, roofIndex));
                roofIndex++;
            }
            return roofsTakeoffs;
        }
        private QuatityTakeOff GetRoofTakeOff(List<StitcherRoofPolygon> simplePolys, int roofIndex)
        {
            double rowArea = 0;
            double thickness = 0;
            foreach (var poly in simplePolys)
            {
                var triangles = poly.GetVrPoly().Triangles;
                foreach (var tra in triangles)
                {
                    var list = new List<Point3D>() { tra.P1, tra.P2, tra.P3 };
                    rowArea += new Polygon3D(list).GetArea();
                }
                var roofType = StitcherBuildingObj.Template.RoofTypes.FirstOrDefault(o => o.TypeName.Equals(poly.RevitTypeName));
                var roofThickness = roofType.GetThickness();
                thickness = roofThickness;
            }
            var roofsVol = rowArea * thickness;

            var tekeOff = new QuatityTakeOff()
            {
                ElementName = "Roofs - " + UnitsHelper.ShowImperialAngle(simplePolys[0].GetSlopeDegree()),
                Type = simplePolys[0].RevitTypeName,
                Symbol = "R" + roofIndex.ToString(),
                Material = "Spanish tile",
                QuantityString = "-",
                HeightString = "-",
                WidthString = "-",
                Area = rowArea,
                AreaString = rowArea.ToString(),
                Length = "-",
                Volume = roofsVol.ToString("0.##")
            };
            return tekeOff;
        }

        private void ChangeTakeOffsToStrings(List<QuatityTakeOff> totallQuantityTakeOffs)
        {
            for (int i = 0; i < totallQuantityTakeOffs.Count; i++)
            {
                if (totallQuantityTakeOffs[i].Quantity != 0)
                {
                    totallQuantityTakeOffs[i].QuantityString = totallQuantityTakeOffs[i].Quantity.ToString();

                    if (totallQuantityTakeOffs[i].HeightString != "-" && totallQuantityTakeOffs[i].WidthString != "-")
                    {
                        int intValue = (int)totallQuantityTakeOffs[i].Height;
                        int decimalValue = (int)((totallQuantityTakeOffs[i].Height - intValue) * 12);
                        totallQuantityTakeOffs[i].HeightString = intValue.ToString() + "' " + decimalValue.ToString() + "\"";

                        intValue = (int)totallQuantityTakeOffs[i].Width;
                        decimalValue = (int)((totallQuantityTakeOffs[i].Width - intValue) * 12);
                        totallQuantityTakeOffs[i].WidthString = intValue.ToString() + "' " + decimalValue.ToString() + "\"";
                    }

                    //intValue = (int)totallQuantityTakeOffs[i].Area;
                    //decimalValue = (int)((totallQuantityTakeOffs[i].Area - intValue) * 12);
                    //totallQuantityTakeOffs[i].AreaString = intValue.ToString() + "' " + decimalValue.ToString() + "\"";
                    if (totallQuantityTakeOffs[i].Area == 0)
                        totallQuantityTakeOffs[i].AreaString = "-";
                    else
                        totallQuantityTakeOffs[i].AreaString = totallQuantityTakeOffs[i].Area.ToString("0.##");
                }
                else
                {
                    totallQuantityTakeOffs[i].QuantityString = "-";
                    totallQuantityTakeOffs[i].HeightString = "-";
                    totallQuantityTakeOffs[i].WidthString = "-";

                    //totallQuantityTakeOffs[i].Symbol = "-";

                    //var intValue = (int)totallQuantityTakeOffs[i].Area;
                    //var decimalValue = (int)((totallQuantityTakeOffs[i].Area - intValue) * 12);
                    //totallQuantityTakeOffs[i].AreaString = intValue.ToString() + "' " + decimalValue.ToString() + "\"";

                    if (totallQuantityTakeOffs[i].Area == 0)
                        totallQuantityTakeOffs[i].AreaString = "-";
                    else
                        totallQuantityTakeOffs[i].AreaString = totallQuantityTakeOffs[i].Area.ToString("0.##");
                }
            }
        }

        #endregion

        private void btnArch_Click(object sender, EventArgs e)
        {
            panelContainer.Controls.Clear();
            ShowArch();
            panel_Highlight.Location= new Point(btnArch.Location.X, panel_Highlight.Location.Y);
        }

        private void btnStruc_Click(object sender, EventArgs e)
        {
            panelContainer.Controls.Clear();
            panel_Highlight.Location = new Point(btnStruc.Location.X, panel_Highlight.Location.Y);

        }
    }
}
