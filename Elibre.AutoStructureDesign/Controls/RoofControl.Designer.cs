﻿namespace Elibre.AutoStructureDesign.Controls
{
    partial class RoofControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelContainer = new System.Windows.Forms.Panel();
            this.radioButtonArea = new System.Windows.Forms.RadioButton();
            this.radioButtonPost = new System.Windows.Forms.RadioButton();
            this.radioButtonOpening = new System.Windows.Forms.RadioButton();
            this.radioButtonStrap = new System.Windows.Forms.RadioButton();
            this.radioButtonBeam = new System.Windows.Forms.RadioButton();
            this.radioButtonGirder = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // panelContainer
            // 
            this.panelContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelContainer.Location = new System.Drawing.Point(3, 3);
            this.panelContainer.Name = "panelContainer";
            this.panelContainer.Size = new System.Drawing.Size(707, 510);
            this.panelContainer.TabIndex = 0;
            // 
            // radioButtonArea
            // 
            this.radioButtonArea.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButtonArea.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButtonArea.AutoSize = true;
            this.radioButtonArea.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonArea.Image = global::Elibre.AutoStructureDesign.Properties.Resources.truss_Area;
            this.radioButtonArea.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButtonArea.Location = new System.Drawing.Point(275, 520);
            this.radioButtonArea.Name = "radioButtonArea";
            this.radioButtonArea.Size = new System.Drawing.Size(71, 29);
            this.radioButtonArea.TabIndex = 3;
            this.radioButtonArea.TabStop = true;
            this.radioButtonArea.Text = "      Area";
            this.radioButtonArea.UseVisualStyleBackColor = true;
            // 
            // radioButtonPost
            // 
            this.radioButtonPost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButtonPost.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButtonPost.AutoSize = true;
            this.radioButtonPost.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonPost.Image = global::Elibre.AutoStructureDesign.Properties.Resources.ColumnsIcon;
            this.radioButtonPost.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButtonPost.Location = new System.Drawing.Point(178, 520);
            this.radioButtonPost.Name = "radioButtonPost";
            this.radioButtonPost.Size = new System.Drawing.Size(91, 29);
            this.radioButtonPost.TabIndex = 6;
            this.radioButtonPost.TabStop = true;
            this.radioButtonPost.Text = "      Column";
            this.radioButtonPost.UseVisualStyleBackColor = true;
            // 
            // radioButtonOpening
            // 
            this.radioButtonOpening.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButtonOpening.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButtonOpening.AutoSize = true;
            this.radioButtonOpening.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonOpening.Image = global::Elibre.AutoStructureDesign.Properties.Resources.Opening;
            this.radioButtonOpening.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButtonOpening.Location = new System.Drawing.Point(352, 520);
            this.radioButtonOpening.Name = "radioButtonOpening";
            this.radioButtonOpening.Size = new System.Drawing.Size(91, 29);
            this.radioButtonOpening.TabIndex = 5;
            this.radioButtonOpening.TabStop = true;
            this.radioButtonOpening.Text = "     Opening";
            this.radioButtonOpening.UseVisualStyleBackColor = true;
            // 
            // radioButtonStrap
            // 
            this.radioButtonStrap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButtonStrap.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButtonStrap.AutoSize = true;
            this.radioButtonStrap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonStrap.Image = global::Elibre.AutoStructureDesign.Properties.Resources.strap;
            this.radioButtonStrap.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButtonStrap.Location = new System.Drawing.Point(449, 520);
            this.radioButtonStrap.Name = "radioButtonStrap";
            this.radioButtonStrap.Size = new System.Drawing.Size(77, 29);
            this.radioButtonStrap.TabIndex = 4;
            this.radioButtonStrap.TabStop = true;
            this.radioButtonStrap.Text = "      Strap";
            this.radioButtonStrap.UseVisualStyleBackColor = true;
            // 
            // radioButtonBeam
            // 
            this.radioButtonBeam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButtonBeam.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButtonBeam.AutoSize = true;
            this.radioButtonBeam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonBeam.Image = global::Elibre.AutoStructureDesign.Properties.Resources.beamicon;
            this.radioButtonBeam.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButtonBeam.Location = new System.Drawing.Point(98, 519);
            this.radioButtonBeam.Name = "radioButtonBeam";
            this.radioButtonBeam.Size = new System.Drawing.Size(74, 29);
            this.radioButtonBeam.TabIndex = 2;
            this.radioButtonBeam.TabStop = true;
            this.radioButtonBeam.Text = "     Beam";
            this.radioButtonBeam.UseVisualStyleBackColor = true;
            // 
            // radioButtonGirder
            // 
            this.radioButtonGirder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButtonGirder.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButtonGirder.AutoSize = true;
            this.radioButtonGirder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonGirder.Image = global::Elibre.AutoStructureDesign.Properties.Resources.beam_Icon;
            this.radioButtonGirder.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButtonGirder.Location = new System.Drawing.Point(12, 519);
            this.radioButtonGirder.Name = "radioButtonGirder";
            this.radioButtonGirder.Size = new System.Drawing.Size(80, 29);
            this.radioButtonGirder.TabIndex = 1;
            this.radioButtonGirder.TabStop = true;
            this.radioButtonGirder.Text = "      Girder";
            this.radioButtonGirder.UseVisualStyleBackColor = true;
            // 
            // RoofControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radioButtonPost);
            this.Controls.Add(this.radioButtonOpening);
            this.Controls.Add(this.radioButtonStrap);
            this.Controls.Add(this.radioButtonArea);
            this.Controls.Add(this.radioButtonBeam);
            this.Controls.Add(this.radioButtonGirder);
            this.Controls.Add(this.panelContainer);
            this.Name = "RoofControl";
            this.Size = new System.Drawing.Size(713, 563);
            this.Load += new System.EventHandler(this.RoofControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelContainer;
        private System.Windows.Forms.RadioButton radioButtonGirder;
        private System.Windows.Forms.RadioButton radioButtonBeam;
        private System.Windows.Forms.RadioButton radioButtonArea;
        private System.Windows.Forms.RadioButton radioButtonStrap;
        private System.Windows.Forms.RadioButton radioButtonOpening;
        private System.Windows.Forms.RadioButton radioButtonPost;
    }
}
