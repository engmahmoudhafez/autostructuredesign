﻿namespace Elibre.AutoStructureDesign.Controls
{
    partial class ReportsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.summaryReportnBtn = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ShearWallDesignBtn = new System.Windows.Forms.Button();
            this.UpliftBtn = new System.Windows.Forms.Button();
            this.AtsBtn = new System.Windows.Forms.Button();
            this.DiaphragmBtn = new System.Windows.Forms.Button();
            this.HoldDownBtn = new System.Windows.Forms.Button();
            this.ShearWallBtn = new System.Windows.Forms.Button();
            this.WindLoadBtn = new System.Windows.Forms.Button();
            this.SeismicLoadBtn = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.headerDesighnBtn = new System.Windows.Forms.Button();
            this.beamBtn = new System.Windows.Forms.Button();
            this.postBtn = new System.Windows.Forms.Button();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // summaryReportnBtn
            // 
            this.summaryReportnBtn.Location = new System.Drawing.Point(704, 705);
            this.summaryReportnBtn.Name = "summaryReportnBtn";
            this.summaryReportnBtn.Size = new System.Drawing.Size(234, 34);
            this.summaryReportnBtn.TabIndex = 17;
            this.summaryReportnBtn.Text = "Summary Report";
            this.summaryReportnBtn.UseVisualStyleBackColor = true;
            this.summaryReportnBtn.Visible = false;
            // 
            // panel4
            // 
            this.panel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel4.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label2);
            this.panel4.Location = new System.Drawing.Point(144, 306);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1071, 38);
            this.panel4.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Lateral Design";
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel3.Controls.Add(this.ShearWallDesignBtn);
            this.panel3.Controls.Add(this.UpliftBtn);
            this.panel3.Controls.Add(this.AtsBtn);
            this.panel3.Controls.Add(this.DiaphragmBtn);
            this.panel3.Controls.Add(this.HoldDownBtn);
            this.panel3.Controls.Add(this.ShearWallBtn);
            this.panel3.Controls.Add(this.WindLoadBtn);
            this.panel3.Controls.Add(this.SeismicLoadBtn);
            this.panel3.Location = new System.Drawing.Point(143, 350);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1071, 297);
            this.panel3.TabIndex = 21;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel2.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(140, 74);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1071, 38);
            this.panel2.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Framing Design";
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.headerDesighnBtn);
            this.panel1.Controls.Add(this.beamBtn);
            this.panel1.Controls.Add(this.postBtn);
            this.panel1.Location = new System.Drawing.Point(140, 118);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1071, 150);
            this.panel1.TabIndex = 18;
            // 
            // ShearWallDesignBtn
            // 
            this.ShearWallDesignBtn.BackColor = System.Drawing.Color.White;
            this.ShearWallDesignBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ShearWallDesignBtn.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShearWallDesignBtn.ForeColor = System.Drawing.Color.Black;
            this.ShearWallDesignBtn.Image = global::Elibre.AutoStructureDesign.Properties.Resources.shearWallIco;
            this.ShearWallDesignBtn.Location = new System.Drawing.Point(801, 153);
            this.ShearWallDesignBtn.Name = "ShearWallDesignBtn";
            this.ShearWallDesignBtn.Size = new System.Drawing.Size(267, 141);
            this.ShearWallDesignBtn.TabIndex = 7;
            this.ShearWallDesignBtn.Text = "Shear Wall Design";
            this.ShearWallDesignBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ShearWallDesignBtn.UseCompatibleTextRendering = true;
            this.ShearWallDesignBtn.UseVisualStyleBackColor = false;
            this.ShearWallDesignBtn.Click += new System.EventHandler(this.ShearWallDesignBtn_Click);
            // 
            // UpliftBtn
            // 
            this.UpliftBtn.BackColor = System.Drawing.Color.White;
            this.UpliftBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UpliftBtn.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpliftBtn.ForeColor = System.Drawing.Color.Black;
            this.UpliftBtn.Image = global::Elibre.AutoStructureDesign.Properties.Resources.UpliftIcon;
            this.UpliftBtn.Location = new System.Drawing.Point(534, 153);
            this.UpliftBtn.Name = "UpliftBtn";
            this.UpliftBtn.Size = new System.Drawing.Size(263, 141);
            this.UpliftBtn.TabIndex = 6;
            this.UpliftBtn.Text = "Uplift Calculations";
            this.UpliftBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.UpliftBtn.UseVisualStyleBackColor = false;
            this.UpliftBtn.Click += new System.EventHandler(this.UpliftBtn_Click);
            // 
            // AtsBtn
            // 
            this.AtsBtn.BackColor = System.Drawing.Color.White;
            this.AtsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AtsBtn.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AtsBtn.ForeColor = System.Drawing.Color.Black;
            this.AtsBtn.Image = global::Elibre.AutoStructureDesign.Properties.Resources.atsIcon;
            this.AtsBtn.Location = new System.Drawing.Point(267, 153);
            this.AtsBtn.Name = "AtsBtn";
            this.AtsBtn.Size = new System.Drawing.Size(261, 141);
            this.AtsBtn.TabIndex = 5;
            this.AtsBtn.Text = "Ats Design";
            this.AtsBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AtsBtn.UseCompatibleTextRendering = true;
            this.AtsBtn.UseVisualStyleBackColor = false;
            this.AtsBtn.Click += new System.EventHandler(this.AtsBtn_Click);
            // 
            // DiaphragmBtn
            // 
            this.DiaphragmBtn.BackColor = System.Drawing.Color.White;
            this.DiaphragmBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DiaphragmBtn.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DiaphragmBtn.ForeColor = System.Drawing.Color.Black;
            this.DiaphragmBtn.Image = global::Elibre.AutoStructureDesign.Properties.Resources.windLoadIcon;
            this.DiaphragmBtn.Location = new System.Drawing.Point(0, 153);
            this.DiaphragmBtn.Name = "DiaphragmBtn";
            this.DiaphragmBtn.Size = new System.Drawing.Size(261, 141);
            this.DiaphragmBtn.TabIndex = 4;
            this.DiaphragmBtn.Text = "Diaphragm Design";
            this.DiaphragmBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.DiaphragmBtn.UseCompatibleTextRendering = true;
            this.DiaphragmBtn.UseVisualStyleBackColor = false;
            this.DiaphragmBtn.Click += new System.EventHandler(this.DiaphragmBtn_Click);
            // 
            // HoldDownBtn
            // 
            this.HoldDownBtn.BackColor = System.Drawing.Color.White;
            this.HoldDownBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HoldDownBtn.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HoldDownBtn.ForeColor = System.Drawing.Color.Black;
            this.HoldDownBtn.Image = global::Elibre.AutoStructureDesign.Properties.Resources.atsIcon;
            this.HoldDownBtn.Location = new System.Drawing.Point(801, 0);
            this.HoldDownBtn.Name = "HoldDownBtn";
            this.HoldDownBtn.Size = new System.Drawing.Size(267, 147);
            this.HoldDownBtn.TabIndex = 3;
            this.HoldDownBtn.Text = "Hold-Down Schedule";
            this.HoldDownBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.HoldDownBtn.UseCompatibleTextRendering = true;
            this.HoldDownBtn.UseVisualStyleBackColor = false;
            this.HoldDownBtn.Click += new System.EventHandler(this.HoldDownBtn_Click);
            // 
            // ShearWallBtn
            // 
            this.ShearWallBtn.BackColor = System.Drawing.Color.White;
            this.ShearWallBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ShearWallBtn.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShearWallBtn.ForeColor = System.Drawing.Color.Black;
            this.ShearWallBtn.Image = global::Elibre.AutoStructureDesign.Properties.Resources.shearWallIco;
            this.ShearWallBtn.Location = new System.Drawing.Point(534, 0);
            this.ShearWallBtn.Name = "ShearWallBtn";
            this.ShearWallBtn.Size = new System.Drawing.Size(263, 147);
            this.ShearWallBtn.TabIndex = 2;
            this.ShearWallBtn.Text = "Shear Wall Schedule";
            this.ShearWallBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ShearWallBtn.UseCompatibleTextRendering = true;
            this.ShearWallBtn.UseVisualStyleBackColor = false;
            this.ShearWallBtn.Click += new System.EventHandler(this.ShearWallScheduleBtn_Click);
            // 
            // WindLoadBtn
            // 
            this.WindLoadBtn.BackColor = System.Drawing.Color.White;
            this.WindLoadBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WindLoadBtn.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WindLoadBtn.ForeColor = System.Drawing.Color.Black;
            this.WindLoadBtn.Image = global::Elibre.AutoStructureDesign.Properties.Resources.windLoadIcon;
            this.WindLoadBtn.Location = new System.Drawing.Point(267, 0);
            this.WindLoadBtn.Name = "WindLoadBtn";
            this.WindLoadBtn.Size = new System.Drawing.Size(261, 147);
            this.WindLoadBtn.TabIndex = 1;
            this.WindLoadBtn.Text = "Wind Load";
            this.WindLoadBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.WindLoadBtn.UseCompatibleTextRendering = true;
            this.WindLoadBtn.UseVisualStyleBackColor = false;
            this.WindLoadBtn.Click += new System.EventHandler(this.WindLoadBtn_Click);
            // 
            // SeismicLoadBtn
            // 
            this.SeismicLoadBtn.BackColor = System.Drawing.Color.White;
            this.SeismicLoadBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SeismicLoadBtn.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SeismicLoadBtn.ForeColor = System.Drawing.Color.Black;
            this.SeismicLoadBtn.Image = global::Elibre.AutoStructureDesign.Properties.Resources.SeismicLoadIcon;
            this.SeismicLoadBtn.Location = new System.Drawing.Point(0, 0);
            this.SeismicLoadBtn.Name = "SeismicLoadBtn";
            this.SeismicLoadBtn.Size = new System.Drawing.Size(261, 147);
            this.SeismicLoadBtn.TabIndex = 0;
            this.SeismicLoadBtn.Text = "Seismic Load";
            this.SeismicLoadBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.SeismicLoadBtn.UseCompatibleTextRendering = true;
            this.SeismicLoadBtn.UseVisualStyleBackColor = false;
            this.SeismicLoadBtn.Click += new System.EventHandler(this.SeismicLoadBtn_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.White;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Image = global::Elibre.AutoStructureDesign.Properties.Resources.JoistArea;
            this.button4.Location = new System.Drawing.Point(801, 0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(267, 147);
            this.button4.TabIndex = 3;
            this.button4.Text = "Joist Design";
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseCompatibleTextRendering = true;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.JoistBtn_Click);
            // 
            // headerDesighnBtn
            // 
            this.headerDesighnBtn.BackColor = System.Drawing.Color.White;
            this.headerDesighnBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.headerDesighnBtn.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headerDesighnBtn.ForeColor = System.Drawing.Color.Black;
            this.headerDesighnBtn.Image = global::Elibre.AutoStructureDesign.Properties.Resources.header_icon;
            this.headerDesighnBtn.Location = new System.Drawing.Point(534, 0);
            this.headerDesighnBtn.Name = "headerDesighnBtn";
            this.headerDesighnBtn.Size = new System.Drawing.Size(261, 147);
            this.headerDesighnBtn.TabIndex = 2;
            this.headerDesighnBtn.Text = "Header Design";
            this.headerDesighnBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.headerDesighnBtn.UseCompatibleTextRendering = true;
            this.headerDesighnBtn.UseVisualStyleBackColor = false;
            this.headerDesighnBtn.Click += new System.EventHandler(this.headerBtn_Click);
            // 
            // beamBtn
            // 
            this.beamBtn.BackColor = System.Drawing.Color.White;
            this.beamBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.beamBtn.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.beamBtn.ForeColor = System.Drawing.Color.Black;
            this.beamBtn.Image = global::Elibre.AutoStructureDesign.Properties.Resources.beamIco;
            this.beamBtn.Location = new System.Drawing.Point(267, 0);
            this.beamBtn.Name = "beamBtn";
            this.beamBtn.Size = new System.Drawing.Size(261, 147);
            this.beamBtn.TabIndex = 1;
            this.beamBtn.Text = "Beam Design";
            this.beamBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.beamBtn.UseCompatibleTextRendering = true;
            this.beamBtn.UseVisualStyleBackColor = false;
            this.beamBtn.Click += new System.EventHandler(this.beamBtn_Click);
            // 
            // postBtn
            // 
            this.postBtn.BackColor = System.Drawing.Color.White;
            this.postBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.postBtn.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.postBtn.ForeColor = System.Drawing.Color.Black;
            this.postBtn.Image = global::Elibre.AutoStructureDesign.Properties.Resources.PostIcon;
            this.postBtn.Location = new System.Drawing.Point(0, 0);
            this.postBtn.Name = "postBtn";
            this.postBtn.Size = new System.Drawing.Size(261, 147);
            this.postBtn.TabIndex = 0;
            this.postBtn.Text = "Post Design";
            this.postBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.postBtn.UseCompatibleTextRendering = true;
            this.postBtn.UseVisualStyleBackColor = false;
            this.postBtn.Click += new System.EventHandler(this.postBtn_Click);
            // 
            // ReportsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.summaryReportnBtn);
            this.Margin = new System.Windows.Forms.Padding(35, 2, 35, 2);
            this.Name = "ReportsControl";
            this.Size = new System.Drawing.Size(1360, 767);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button summaryReportnBtn;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button ShearWallDesignBtn;
        private System.Windows.Forms.Button UpliftBtn;
        private System.Windows.Forms.Button AtsBtn;
        private System.Windows.Forms.Button DiaphragmBtn;
        private System.Windows.Forms.Button HoldDownBtn;
        private System.Windows.Forms.Button ShearWallBtn;
        private System.Windows.Forms.Button WindLoadBtn;
        private System.Windows.Forms.Button SeismicLoadBtn;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button headerDesighnBtn;
        private System.Windows.Forms.Button beamBtn;
        private System.Windows.Forms.Button postBtn;
    }
}
