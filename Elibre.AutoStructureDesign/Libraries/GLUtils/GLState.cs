﻿using Elibre.AutoStructureDesign.Models;
using Elibre.Geometry2D;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign.Libraries.GLUtils
{
    public abstract class GLState:IViewPortTransformer
    {
        protected Viewer2DDragHandler Dragging { get; set; }
        protected List<double> ZoomList { get; set; }
        public double InitialZoom { set; get; }
        public Graphics Graphics { set; get; }
        public bool PauseViewer { get; set; }
        public bool HideSmallLines { get; set; }

        private double _zoomFactor = 1;
        [DefaultValue(1.0)]
        public virtual double ZoomFactor
        {
            get { return _zoomFactor; }
            set { _zoomFactor = value; }
        }
        private ShaderProgram drawProgram;
        private ShaderProgram fillProgram;
        private ShaderProgram textProgram;
        private ShaderProgram _3DProgram;

        protected Camera camera;
        protected Camera3D camera3D;

        protected int drawProgramId, fillProgramId, textProgramId,_3DProgramId;
        private bool initialized = false;

        protected Vector2 lastMousePos { get; set; }
        protected Vector2 curMousePos { get; set; }
        protected GLStateRenderer glControl;
        public GLState()
        {
            Dragging = new Viewer2DDragHandler();
            camera = new Camera(new Vector3(0, 0, 1));

            drawProgram = new DrawShaderProgram();
            fillProgram = new FillShaderProgram();
            textProgram = new TextShaderProgram();
            _3DProgram = new _3DShaderProgram();

            drawProgramId = drawProgram.GetProgramID();
            fillProgramId = fillProgram.GetProgramID();
            textProgramId = textProgram.GetProgramID();
            _3DProgramId = _3DProgram.GetProgramID();

            glControl = EStitcherSingleton.GetInstance.GlobalGLRenderer;
            initialized = true;
        }
        public abstract void Render();

        Point panningStart;
        Point2D panningStartOffset;
        protected virtual bool IsPanningPattern(MouseEventArgs e)
        {
            return e.Button == MouseButtons.Middle || (e.Button == MouseButtons.Left && Control.ModifierKeys.HasFlag(Keys.Alt));
        }

        public abstract void Dispose();

        public abstract void OnMouseDown(MouseEventArgs e);
        public abstract void OnMouseUp(MouseEventArgs e);
        public abstract void OnMouseMove(MouseEventArgs e);

        public virtual void OnMouseWheel(MouseEventArgs e)
        {
            ZoomFactor *= (1.0f + (20.0f / (float)e.Delta));
            Zoom(glControl.ClientRectangle.X + e.X, glControl.ClientRectangle.Y + e.Y, e.Delta);
        }

        public void OnResize(EventArgs e)
        {
            if (initialized)
            {
                GL.Viewport(0, 0, glControl.Width, glControl.Height);
                CenterBounds(new Rectangle2D(0, 0, glControl.Width, glControl.Height));
            }
            CenterDrawings();
        }

        public void OnMouseDoubleClick(MouseEventArgs e)
        {
            if (IsPanningPattern(e))
                CenterDrawings();
        }
        public abstract void CenterDrawings();

        public virtual void CenterBounds(Rectangle2D rect)
        {
            if (camera != null)
            {
                var regionMidPoint = GetMidPoint(rect);
                var target = (new Vector3((float)regionMidPoint.X, -(float)regionMidPoint.Y, 0)).Xy;
                var initialPoint = camera.Target.Xy;
                var transformLineVector = target - initialPoint;
                var finalZoom = GetZoomFromCenterDrawing(rect);
                camera.translate(transformLineVector);
                camera.SetZoom((float)finalZoom, glControl.Width, glControl.Height);
                camera.update(glControl.Width, glControl.Height);
                ZoomFactor = GetZoomFromCenterDrawing(rect);
            }
        }
        public double GetZoomFromCenterDrawing(Rectangle2D bound)
        {
            var zoomFactor = 0.0;
            var drawingBounds = bound;
            int spacingPercent = 10;
            var spacingValue = -Math.Min(30, Math.Min(glControl.Width * spacingPercent / 100.0, glControl.Height * spacingPercent / 100.0));
            double Width = glControl.Width;
            double Height = glControl.Height;
            Width = Width + spacingValue;
            Height = Height + spacingValue;
            if (drawingBounds.Width > 0 && drawingBounds.Height > 0)
            {
                var ratio = Math.Min(Width / drawingBounds.Width, Height / drawingBounds.Height);
                zoomFactor = (float)ratio;
                zoomFactor *= .9f;
            }
            return zoomFactor;
        }
        private Point2D GetMidPoint(Rectangle2D rect)
        {
            return new Point2D(rect.Left + rect.Width / 2,
                     rect.Top + rect.Height / 2);
        }
        protected virtual bool HandlePanningMouseDown(MouseEventArgs e)
        {
            if (PauseViewer)
                return true;
            if (IsPanningPattern(e))
            {
                curMousePos = new Vector2(glControl.ClientRectangle.X + e.X, glControl.ClientRectangle.Y + e.Y);
                lastMousePos = curMousePos;
                panningStart = glControl.PointToScreen(e.Location);
                //panningStartOffset = this.Offset;
                this.Dragging.BeginPanning(e.Location);
                //this.Cursor = this.Theme.MovingCursor;
                return true;
            }

            return false;
        }
        protected virtual bool HandlePanningMouseMove(MouseEventArgs e)
        {
            if (!this.Dragging.IsPanning)
                return false;

            curMousePos = new Vector2(glControl.ClientRectangle.X + e.X, glControl.ClientRectangle.Y + e.Y);
            return true;
        }
        protected virtual bool HandlePanningMouseUp(MouseEventArgs e)
        {
            if (!this.Dragging.IsPanning)
                return false;

            Dragging.Stop();
            return true;
        }
        protected virtual Cursor GetMouseCursor(Point location)
        {
            return null;
        }

        public void Zoom(int X, int Y, int Delta)
        {
            Vector3 v3 = new Vector3(X, Y, 0);
            Vector2 mBefore = UnProject(v3).Xy;
            camera.SetZoomFactor((1.0f + (20.0f / (float)Delta)), glControl.Width, glControl.Height);
            Vector2 mAfter = UnProject(v3).Xy;
            camera.translate(mBefore - mAfter);
            //camera.update(Width, Height);
        }

        public void Rotate(float angle)
        {
            camera.Rotate(angle);
            camera.update(glControl.Width, glControl.Height);
        }
        public void Pan(Vector2 v)
        {
            camera.translate(v);
            camera.update(glControl.Width, glControl.Height);
        }
        public Camera GetCamera()
        {
            return camera;
        }
        public virtual void Update()
        {
            if (camera.getView() != Matrix4.Identity)
            {
                var _curMousePos = UnProject(new Vector3(curMousePos.X, curMousePos.Y, 0)).Xy;
                var _lastMousePos = UnProject(new Vector3(lastMousePos.X, lastMousePos.Y, 0)).Xy;

                Vector2 v = Dragging.IsPanning ? (_curMousePos - _lastMousePos) : new Vector2();
                v.X = -v.X; v.Y = -v.Y;
                if (v != Vector2.Zero)
                    camera.translate(v);
            }
            camera.update(glControl.Width, glControl.Height);
            lastMousePos = curMousePos;

        }
        public virtual void UpdateStateItems() { }

        protected int Start3D()
        {
            _3DProgram.Use();
            camera3D.render(_3DProgramId);
            return _3DProgramId;
        }

        protected int StartDraw()
        {
            drawProgram.Use();
            camera.render(drawProgramId);
            return drawProgramId;
        }
        protected int StartFill()
        {
            fillProgram.Use();
            camera.render(fillProgramId);
            return fillProgramId;
        }
        protected int StartGraphics()
        {
            textProgram.Use();
            camera.render(textProgramId);
            return textProgramId;
        }
        public Vector2 ToWorld(Vector2 v)
        {
            var p = UnProject(new Vector3((float)v.X, (float)v.Y, 0));
            p.Y = 1 - p.Y;
            return p.Xy;
        }
        public Vector2 ToViewport(Vector3 v)
        {
            return ConvertToViewport(v, camera.getView(), camera.getProjection(), (int)glControl.Width, (int)glControl.Height);
        }
        public Vector2 ToViewPort(Vector2 v)
        {
            return ConvertToViewport(new Vector3(v.X, v.Y, 0), camera.getView(), camera.getProjection(), (int)glControl.Width, (int)glControl.Height);
        }
        public Point2D ToViewPort(Point2D v)
        {
            return ConvertToPoint2D(new Vector3((float)v.X, (float)v.Y, 0), camera.getView(), camera.getProjection(), (int)glControl.Width, (int)glControl.Height);
        }
        protected Vector2 ConvertToViewport(Vector3 pos, Matrix4 viewMatrix, Matrix4 projectionMatrix, int screenWidth, int screenHeight)
        {
            pos = Vector3.Transform(pos, viewMatrix);
            pos = Vector3.Transform(pos, projectionMatrix);
            int winX = (int)Math.Round(((pos.X + 1) / 2.0) * screenWidth);
            int winY = (int)Math.Round(((1 - pos.Y) / 2.0) * screenHeight);
            return new Vector2(winX, winY);
        }
        protected Point2D ConvertToPoint2D(Vector3 pos, Matrix4 viewMatrix, Matrix4 projectionMatrix, int screenWidth, int screenHeight)
        {
            var npos = new Vector4(pos.X, -pos.Y, pos.Z, 1);
            npos = Vector4.Transform(npos, viewMatrix);
            npos = Vector4.Transform(npos, projectionMatrix);
            npos /= npos.W;
            npos.X = npos.X * 0.5f + 0.5f;
            npos.Y = -npos.Y * 0.5f + 0.5f;
            npos.X = npos.X * screenWidth;
            npos.Y = npos.Y * screenHeight;
            return new Point2D((int)npos.X, (int)npos.Y);
        }
        protected PointF ConvertToPointF(Vector3 pos, Matrix4 viewMatrix, Matrix4 projectionMatrix, int screenWidth, int screenHeight)
        {
            pos = Vector3.Transform(pos, viewMatrix);
            pos = Vector3.Transform(pos, projectionMatrix);
            int winX = (int)Math.Round(((pos.X + 1) / 2.0) * screenWidth);
            int winY = (int)Math.Round(((1 - pos.Y) / 2.0) * screenHeight);
            return new PointF(winX, winY);
        }
        protected Vector3 UnProject(Vector3 mouse)
        {
            Vector4 vec;

            vec.X = 2.0f * mouse.X / (float)glControl.Width - 1;
            vec.Y = -(2.0f * mouse.Y / (float)glControl.Height - 1);
            vec.Z = mouse.Z;
            vec.W = 1.0f;

            Matrix4 viewInv = Matrix4.Invert(camera.getView());
            Matrix4 projInv = Matrix4.Invert(camera.getProjection());

            Vector4.Transform(ref vec, ref projInv, out vec);
            Vector4.Transform(ref vec, ref viewInv, out vec);

            if (vec.W > float.Epsilon || vec.W < -float.Epsilon)
            {
                vec.X /= vec.W;
                vec.Y /= vec.W;
                vec.Z /= vec.W;
            }
            return new Vector3(vec.X, vec.Y, vec.Z);
        }
        public virtual float ZoomToViewPort(double value)
        {
            return Convert.ToSingle(value * this.ZoomFactor);
        }
        public virtual SizeF ZoomToViewPort(double width, double height)
        {
            return new SizeF(ZoomToViewPort(width), ZoomToViewPort(height));
        }
        public virtual SizeF ZoomToViewPort(Size2D size)
        {
            return ZoomToViewPort(size.Width, size.Height);
        }

        public PointF ToViewPort(double x, double y)
        {
            return ConvertToPointF(new Vector3((float)x, (float)y, 0), camera.getView(), camera.getProjection(), glControl.Width, glControl.Height);
        }
        public RectangleF ToViewPort(double x, double y, double width, double height)
        {
            throw new NotImplementedException();
        }

        public double ZoomToWorld(double value)
        {
            return value / this.ZoomFactor;
        }
        
        public Size2D ZoomToWorld(Size2D size)
        {
            return new Size2D(ZoomToWorld(glControl.Width), ZoomToWorld(glControl.Height));
        }

        public Point2D ToWorld(double x, double y)
        {
            Vector2 v = ToWorld(new Vector2((float)x, (float)y));
            return new Point2D(v.X, v.Y);
        }

        public Point2D ToWorld(Point2D p)
        {
            Vector2 v = ToWorld(new Vector2((float)p.X, (float)p.Y));
            return new Point2D(v.X, v.Y);
        }

        public RectangleF ToViewPort(Rectangle2D rect)
        {
            throw new NotImplementedException();
        }

        public Size2D ZoomToWorld(double width, double height)
        {
            throw new NotImplementedException();
        }

        public Rectangle2D ToWorld(double x, double y, double width, double height)
        {
            throw new NotImplementedException();
        }

        public Rectangle2D ToWorld(Rectangle2D rect)
        {
            throw new NotImplementedException();
        }

        PointF IViewPortTransformer.ToViewPort(Point2D p)
        {
            throw new NotImplementedException();
        }
    }
}
