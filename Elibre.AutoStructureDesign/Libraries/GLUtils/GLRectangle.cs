﻿using Elibre.Geometry2D;
using OpenTK;
using System.Collections.Generic;
using System.Drawing;

namespace Elibre.AutoStructureDesign.Libraries.GLUtils
{
    public class GLRectangle
    {
        public Vector2 Start, End;
        private int Thickness;
        private float BorderAlpha, FillAlpha;
        private Color BorderColor, FillColor;
        private GLDrawableObject borderObject;
        private GLFillableObject fillObject;
        private List<Slice2D> slices;
        private  Color fcolor= Color.White;

        public float Z_Order {
            get
            {
                return zOrder;
            }
            set {
                zOrder = value;
                borderObject.Z_Order = zOrder;
                fillObject.Z_Order = zOrder;
            }
        }

        private float zOrder=0;
        public GLRectangle(Vector2 start, Vector2 end, Color borderColor, Color fillColor, int thickness = 1, float borderAlpha = 1, float fillAlpha = 1)
        {
            Start = start;
            End = end;
            BorderColor = borderColor;
            FillColor = fillColor;
            Thickness = thickness;
            BorderAlpha = borderAlpha;
            FillAlpha = fillAlpha;
            Update();
        }
        public GLRectangle(Rectangle2D rect)
        {
            Start = new Vector2((float)rect.X, (float)rect.Y);
            End = new Vector2((float)(rect.X + rect.Width), (float)(rect.Y + rect.Height));
            BorderColor = Color.Black;
            FillColor = Color.White;
            Thickness = 1;
            BorderAlpha = 1;
            FillAlpha = 0;
            Update();
        }
        public GLRectangle(Rectangle2D rect, Color borderColor, Color fillColor, int thickness = 1, float borderAlpha = 1, float fillAlpha = 1)
        {
            Start = new Vector2((float)rect.X,(float)rect.Y);
            End = new Vector2((float)(rect.X+rect.Width), (float)(rect.Y+rect.Height));
            BorderColor = borderColor;
            FillColor = fillColor;
            Thickness = thickness;
            BorderAlpha = borderAlpha;
            FillAlpha = fillAlpha;
            Update();
        }
        public GLRectangle(Slice2D slice, Color borderColor, Color fillColor, int thickness = 1, float borderAlpha = 1, float fillAlpha = 1) :this(slice.GetBounds(),borderColor,fillColor,thickness,borderAlpha,fillAlpha)
        {
        }
        public void Update()
        {
            Point2D _Start = new Point2D(Start.X, Start.Y);
            Point2D _End = new Point2D(End.X, End.Y);
            Slice2D top = new Slice2D(_Start, new Point2D(End.X, _Start.Y));
            Slice2D right = new Slice2D(new Point2D(End.X, Start.Y), _End);
            Slice2D bottom = new Slice2D(_End, new Point2D(Start.X, End.Y));
            Slice2D left = new Slice2D(new Point2D(Start.X, End.Y), _Start);
            slices = new List<Slice2D>() { top, right, bottom, left };
            var poly = new SlicePolygon2D(slices);
            borderObject = new GLDrawableObject(slices, BorderColor);
            borderObject.Thickness = Thickness;
            borderObject.Alpha = BorderAlpha;
            fillObject = new GLFillableObject(poly, FillColor);
            fillObject.Alpha = FillAlpha;
        }
        public void Draw(int programID)
        {
            borderObject.Render(programID);
        }
        public void Fill(int programID)
        {
            fillObject.Render(programID);
        }
        public void Dispose()
        {
            borderObject?.Dispose();
            fillObject?.Dispose();
        }
    }
}