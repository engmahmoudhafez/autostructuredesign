﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL4;

namespace Elibre.AutoStructureDesign.Libraries.GLUtils
{
    public abstract class ShaderProgram
    {
        protected string vertexSource;
        protected string fragmentSource;

        protected int programID = -1;
        protected ShaderProgram()
        {
 
        }
        protected void init()
        {
            programID = GL.CreateProgram();
            int vertexShader = GL.CreateShader(ShaderType.VertexShader);
            int fragmentShader = GL.CreateShader(ShaderType.FragmentShader);
            string infoLog;
            //Compile vertex shader
            GL.ShaderSource(vertexShader, vertexSource);
            GL.CompileShader(vertexShader);
            infoLog = GL.GetShaderInfoLog(vertexShader);
            Console.WriteLine(infoLog);
            if (vertexShader < 0)
            {
                Console.WriteLine(infoLog);
                return;
            }
            //Compile fragment shader
            GL.ShaderSource(fragmentShader, fragmentSource);
            GL.CompileShader(fragmentShader);
            infoLog = GL.GetShaderInfoLog(fragmentShader);
            Console.WriteLine(infoLog);
            if (vertexShader < 0)
            {
                Console.WriteLine(infoLog);
                return;
            }
            GL.AttachShader(programID, vertexShader);
            GL.AttachShader(programID, fragmentShader);
            GL.LinkProgram(programID);
            GL.DeleteShader(vertexShader);
            GL.DeleteShader(fragmentShader);
            infoLog = GL.GetProgramInfoLog(programID);
            Console.WriteLine(infoLog);
        }
        public void Use()
        {
            GL.UseProgram(programID);
        }
        public int GetProgramID()
        {
            return programID;
        }
    }
    public class DrawShaderProgram : ShaderProgram
    {

        public DrawShaderProgram()
        {
            vertexSource = "#version 330 core\n" +
     "layout(location = 0) in vec2 position;" +
     "layout(location = 1) in vec3 color;" +
     "uniform mat4 model;" +
     "uniform mat4 view;" +
     "uniform mat4 projection;" +
     "uniform float zorder;" +
     "out vec3 vertexColor;" +
     "void main()" +
     "{" +
     "    vertexColor = color;" +
     "    vec4 Position = vec4(position.x, 1.0f - position.y, zorder, 1.0f);" +
     "    gl_Position = projection * view * model * Position;" +
     "}";
            fragmentSource = "#version 330 core\n" +
                "in vec3 vertexColor;" +
                "uniform int hasDrawColor;" +
                "uniform vec3 drawColor;" +
                "uniform float alpha;" +
                "out vec4 OutColor;" +
                "void main()" +
                "{" +
                "   OutColor = hasDrawColor==1 ? vec4(drawColor,alpha) : vec4(vertexColor, alpha);" +
                "}";
        init();
        }
    }
    public class FillShaderProgram : ShaderProgram
    {

        public FillShaderProgram()
        {
            vertexSource = "#version 330 core\n" +
    "layout(location = 0) in vec2 position;" +
    "uniform mat4 model;" +
    "uniform mat4 view;" +
    "uniform float zorder;" +
    "uniform mat4 projection;" +
    "void main()" +
    "{" +
    "    vec4 Position = vec4(position.x, 1.0f - position.y, zorder, 1.0f);" +
    "    gl_Position = projection * view * model * Position;" +
    "}";
            fragmentSource = "#version 330 core\n" +
                "uniform vec3 fillColor;" +
                "uniform float alpha;" +
                "out vec4 OutColor;" +
                "void main()" +
                "{" +
                "   OutColor = vec4(fillColor, alpha);" +
                "}";
            init();
        }
    }

    public class _3DShaderProgram : ShaderProgram
    {

        public _3DShaderProgram() 
        {
            vertexSource = "#version 330 core\n" +
    "layout(location = 0) in vec3 position;" +
    "uniform mat4 model;" +
    "uniform mat4 view;" +
    "uniform mat4 projection;" +
    "void main()" +
    "{" +
    "    vec4 Position = vec4(position.x, position.y, position.z, 1.0f);" +
    "    gl_Position = projection * view * model * Position;" +
    "}";
            fragmentSource = "#version 330 core\n" +
                "uniform vec3 fillColor;" +
                "uniform float alpha;" +
                "out vec4 OutColor;" +
                "void main()" +
                "{" +
                "   OutColor = vec4(fillColor, alpha);" +
                "}";
            init();
        }
    }

    public class TextShaderProgram : ShaderProgram
    {

        public TextShaderProgram()
        {
            vertexSource = "#version 330 core\n" +
            "precision highp float;" +
            "layout(location = 0) in vec2 position;" +
            "layout(location = 1) in vec2 uv;" +
            "        uniform mat4 model;" +
            "uniform mat4 view;" +
            "uniform mat4 projection;" +
                "uniform float zorder;" +

            "out vec2 UV;" +

            "        void main()" +
            "        {" +
            "            UV = uv;" +
            "            vec4 Position = vec4(position.x, position.y, zorder, 1.0f);" +
            "            gl_Position = projection * view * model * Position;" +
            "        }";
            fragmentSource = "#version 330 core\n" +

            "in vec2 UV;" +
             "       uniform sampler2D tex;" +

            "out vec4 OutColor;" +

                "    void main()" +
                 "   {" +
                   "     OutColor = texture(tex, UV);" +
                  "  }";
            init();
        }
    }
}
