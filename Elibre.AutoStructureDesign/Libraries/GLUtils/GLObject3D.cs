﻿using Elibre.Geometry2D;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.Libraries.GLUtils
{
    public class GLObject3D : IDisposable
    {
        protected int VAO = -1, VBO = -1, VAO_Border = -1;

        List<Vector3> _positions;
        List<SlicePolygon3D> _polygons;
        public Color FillColor { get; set; } = Color.Black;
        public bool Visible { get; set; } = true;
        protected Matrix4 modelMatrix = Matrix4.Identity;
        public Vector3 Center { get; private set; }
        public Vector3 Max
        {
            get
            {
                return max;
            }
        }
        public Vector3 Min
        {
            get
            {
                return min;
            }
        }
        private Vector3 min, max;
        public GLObject3D(List<SlicePolygon3D> polygons)
        {
            _polygons = polygons;
            _positions = new List<Vector3>();
            constructArrays();
            init();
        }
        public GLObject3D(List<Vector3> points, List<int> indices)
        {
            _positions = new List<Vector3>();
            for (int i = 0; i < indices.Count; i++)
            {
                _positions.Add(points[indices[i]]);
            }
            min = max = _positions[0];
            foreach (var item in _positions)
            {
                min.X = min.X <= item.X ? min.X : item.X;
                min.Y = min.Y <= item.Y ? min.Y : item.Y;
                min.Z = min.Z <= item.Z ? min.Z : item.Z;

                max.X = max.X >= item.X ? max.X : item.X;
                max.Y = max.Y >= item.Y ? max.Y : item.Y;
                max.Z = max.Z >= item.Z ? max.Z : item.Z;
            }
            Center = new Vector3(min.X + (max.X - min.X) / 2, min.Y + (max.Y - min.Y) / 2, min.Z + (max.Z - min.Z) / 2);

            init();

        }
        public GLObject3D(List<SlicePolygon3D> polygons, Color fill)
        {
            FillColor = fill;
            _polygons = polygons;
            _positions = new List<Vector3>();
            constructArrays();
            init();
        }

        private void constructArrays()
        {
            foreach (var poly in _polygons)
            {
                if (poly.Slices.Count == 4)
                {
                    var points = poly.GetPoints();
                    _positions.Add(new Vector3((float)points[0].X, (float)points[0].Y, (float)points[0].Z));
                    _positions.Add(new Vector3((float)points[1].X, (float)points[1].Y, (float)points[1].Z));
                    _positions.Add(new Vector3((float)points[2].X, (float)points[2].Y, (float)points[2].Z));
                    _positions.Add(new Vector3((float)points[2].X, (float)points[2].Y, (float)points[2].Z));
                    _positions.Add(new Vector3((float)points[3].X, (float)points[3].Y, (float)points[3].Z));
                    _positions.Add(new Vector3((float)points[0].X, (float)points[0].Y, (float)points[0].Z));
                }
                else if (poly.Slices.Count == 3)
                {
                    var points = poly.GetPoints();
                    _positions.Add(new Vector3((float)points[0].X, (float)points[0].Y, (float)points[0].Z));
                    _positions.Add(new Vector3((float)points[1].X, (float)points[1].Y, (float)points[1].Z));
                    _positions.Add(new Vector3((float)points[2].X, (float)points[2].Y, (float)points[2].Z));
                }
            }
            min = max = _positions[0];
            foreach (var item in _positions)
            {
                min.X = min.X <= item.X ? min.X : item.X;
                min.Y = min.Y <= item.Y ? min.Y : item.Y;
                min.Z = min.Z <= item.Z ? min.Z : item.Z;

                max.X = max.X >= item.X ? max.X : item.X;
                max.Y = max.Y >= item.Y ? max.Y : item.Y;
                max.Z = max.Z >= item.Z ? max.Z : item.Z;
            }
            Center = new Vector3(min.X + (max.X - min.X) / 2, min.Y + (max.Y - min.Y) / 2, min.Z + (max.Z - min.Z) / 2);
        }
        private void init()
        {
            VAO = GL.GenVertexArray();
            GL.BindVertexArray(VAO);
            VBO = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            var positionsArray = _positions.ToArray();
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(sizeof(float) * 3 * _positions.Count), positionsArray, BufferUsageHint.StaticDraw);
            Array.Clear(positionsArray, 0, positionsArray.Length);
            positionsArray = null;
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);
            GL.BindVertexArray(0);
        }
        public void Render(int programID)
        {
            if (Visible)
            {
                int modelLoc = GL.GetUniformLocation(programID, "model");
                GL.UniformMatrix4(modelLoc, false, ref modelMatrix);

                int alphaLoc = GL.GetUniformLocation(programID, "alpha");
                GL.Uniform1(alphaLoc, 1);

                Vector4 color;
                int colorLoc = GL.GetUniformLocation(programID, "fillColor");

                color = new Vector4((float)FillColor.R / 255, (float)FillColor.G / 255, (float)FillColor.B / 255, (float)FillColor.A / 255);
                GL.Uniform3(colorLoc, color.X, color.Y, color.Z);

                GL.BindVertexArray(VAO);
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.DrawArrays(PrimitiveType.Triangles, 0, _positions.Count);
                GL.BindVertexArray(0);
            }
        }
        public void Translate(Vector3 t)
        {
            modelMatrix *= Matrix4.CreateTranslation(t);
        }

        public void Dispose()
        {
            if (VBO > 0)
                GL.DeleteBuffer(VBO);
            if (VAO > 0)
                GL.DeleteVertexArray(VAO);
            if (VAO_Border > 0)
                GL.DeleteVertexArray(VAO_Border);
            VAO = -1;
            VBO = -1;
            VAO_Border = -1;
        }
    }
}
