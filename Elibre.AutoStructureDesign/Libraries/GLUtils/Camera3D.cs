﻿using OpenTK;
using OpenTK.Graphics.ES20;
using System;

namespace Elibre.AutoStructureDesign.Libraries.GLUtils
{
    public class Camera3D
    {
        private Vector3 eye, target, up;
        private Matrix4 view = Matrix4.Identity, projection = Matrix4.Identity;
        public float Yaw = 0.0f, Pitch = 0.0f, Roll = 0.0f;
        float near, far,fov;
        public float Radius
        {
            get
            {
                return _radius;
            }
            set
            {
                _radius = value;
                if (_radius <= near)
                    _radius = near-5;
                else if (_radius >= far)
                    _radius = far-5;
                UpdateView();
            }
        }
        private float _radius;
        public Vector3 Target
        {
            get { return target; }
        }
        public Camera3D(Vector3 _eye, Vector3 _target, float width, float height, float _near = 0.5f, float _far = 1000.0f,float _fov=MathHelper.PiOver4)
        {
            this.eye = _eye;
            this.target = _target;
            this.up = new Vector3(0,1,0);
            near = _near;
            far = _far;
            fov = _fov;

            var d = eye - target;
            Yaw = (float)Math.Atan2(d.Z, d.X);
            float a = d.Y / d.Length;
            if (a <= -1.0) a = -1.0f; else if (a > 1.0) a = 1.0f;
            Pitch = (float)Math.Asin(a);

            if (Pitch >= MathHelper.DegreesToRadians(89))
                Pitch = (float)MathHelper.DegreesToRadians(89);
            else if (Pitch <= MathHelper.DegreesToRadians(-89))
                Pitch = (float)MathHelper.DegreesToRadians(-89);

            _radius = (eye - target).Length;
            view = Matrix4.LookAt(eye, target, up);
        }
        public void UpdateView()
        {
            eye.X = Target.X + (float)(Math.Cos((Yaw)) * Math.Cos((Pitch))) * _radius;
            eye.Y = Target.Y + (float)(Math.Sin((Pitch))) * _radius;
            eye.Z = Target.Z + (float)(Math.Sin((Yaw)) * Math.Cos((Pitch))) * _radius;

            view = Matrix4.LookAt(eye, target, up);
        }
        public void UpdateProjection(float width, float height)
        {
            projection = Matrix4.CreatePerspectiveFieldOfView(fov, (float)width / (float)height, near, far);
        }
        public void render(int programID)
        {
            int projectionLoc = GL.GetUniformLocation(programID, "projection");
            GL.UniformMatrix4(projectionLoc, false, ref projection);
            int viewLoc = GL.GetUniformLocation(programID, "view");
            GL.UniformMatrix4(viewLoc, false, ref view);
        }
        public void Rotate(float _yaw, float _pitch)
        {
            Yaw += _yaw;
            Pitch += _pitch;

            if (Pitch >= MathHelper.DegreesToRadians(89))
                Pitch = (float)MathHelper.DegreesToRadians(89);
            else if (Pitch <= MathHelper.DegreesToRadians(-89))
                Pitch = (float)MathHelper.DegreesToRadians(-89);

            UpdateView();
        }
        public void Pan(Vector2 t)
        {
            Vector3 right = -view.Column0.Xyz;
            Vector3 up = view.Column1.Xyz;
            right *= t.X * _radius/30;
            up *= t.Y * _radius/30;
            eye += right;
            eye += up;
            target += right;
            target += up;
            UpdateView();
        }
        public void translate(Vector3 v)
        {
            view *= Matrix4.CreateTranslation(v);
        }
        public Matrix4 getView() { return view; }
        public Matrix4 getProjection() { return projection; }
    }
}
