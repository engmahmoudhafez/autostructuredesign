﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign.Libraries.GLUtils
{
    public class GLStatesManager
    {
        public GLState activeState { get; set; }
        public void Render()
        {
            activeState.Render();
        }
        public void OnMouseDown(MouseEventArgs e)
        {
            activeState.OnMouseDown(e);
        }

        public void OnMouseUp(MouseEventArgs e)
        {
            activeState.OnMouseUp(e);
        }

        public void OnMouseMove(MouseEventArgs e)
        {
            activeState.OnMouseMove(e);
        }

        public void OnMouseWheel(MouseEventArgs e)
        {
            activeState.OnMouseWheel(e);
        }
        public void OnResize(EventArgs e)
        {
            activeState?.OnResize(e);
        }

        public void OnMouseDoubleClick(MouseEventArgs e)
        {
            activeState.OnMouseDoubleClick(e);
        }
        public void Update()
        {
            activeState?.Update();
        }
    }
}
