﻿using Elibre.Geometry2D;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Elibre.AutoStructureDesign.Libraries.GLUtils
{
    public class GLUtils
    {
        public enum GLGraphicsObjectVPosition
        {
            UP,CENTER, DOWN
        }
        public enum GLGraphicsObjectHPosition
        {
            LEFT,MIDDLE,RIGHT
        }
        public static void RotateSlice(Slice2D slice, Point2D around, double angle)
        {
            var start = new Point2D(slice.Start.X - around.X, slice.Start.Y - around.Y);
            var startX = (Math.Cos(angle) * start.X - Math.Sin(angle) * start.Y) + around.X;
            var startY = (Math.Sin(angle) * start.X + Math.Cos(angle) * start.Y) + around.Y;
            slice.Start = new Point2D(startX, startY);

            var end = new Point2D(slice.End.X - around.X, slice.End.Y - around.Y);
            var endX = (Math.Cos(angle) * end.X - Math.Sin(angle) * end.Y) + around.X;
            var endY = (Math.Sin(angle) * end.X + Math.Cos(angle) * end.Y) + around.Y;
            slice.End = new Point2D(endX, endY);
        }
        public static void StartDottedLinesMode(int factor,short pattern)
        {
            OpenTK.Graphics.OpenGL.GL.LineStipple(factor, pattern);
            OpenTK.Graphics.OpenGL.GL.Enable(OpenTK.Graphics.OpenGL.EnableCap.LineStipple);
        }
        public static void StopDottedLinesMode()
        {
            OpenTK.Graphics.OpenGL.GL.Disable(OpenTK.Graphics.OpenGL.EnableCap.LineStipple);
        }

        public static List<Slice2D> CreateCircleSlices(Point2D center,float radius,int slicesCount)
        {
            List<Point2D> circlePoints = new List<Point2D>();
            List<Slice2D> circleSlices = new List<Slice2D>();
            for (int i = 0; i < slicesCount; i++)
            {
                var angle = 2 * Math.PI * i / slicesCount;
                circlePoints.Add(new Point2D(center.X + Math.Cos(angle) * radius, center.Y + Math.Sin(angle) * radius));
            }
            for (int i = 1; i < circlePoints.Count; i++)
            {
                circleSlices.Add(new Slice2D(circlePoints[i - 1], circlePoints[i]));
            }
            circleSlices.Add(new Slice2D(circlePoints[circlePoints.Count-1], circlePoints[0]));
            return circleSlices;
        }

        public static void AlignTextToSlice(GLGraphicsObject text, Slice2D slice,float margin, GLGraphicsObjectVPosition vpos,GLGraphicsObjectHPosition hpos)
        {
            SizeF textSize = text.getSize();
            double rotAngle;
            Vector2 v=new Vector2();
            Point2D point = slice.GetMidPoint();
            if (slice.IsVertical())
            {
                rotAngle = -Math.PI / 2;
                float cd = 0.0f;
                if (vpos == GLGraphicsObjectVPosition.UP)
                {
                    cd = textSize.Height * (1 + margin);
                }
                else if (vpos == GLGraphicsObjectVPosition.DOWN)
                {
                    cd = 0;
                }
                else if (vpos == GLGraphicsObjectVPosition.CENTER)
                {
                    cd=textSize.Height / 2;
                }
                v = new Vector2(cd, textSize.Width / 2);
                if (slice.Start.Y > slice.End.Y)
                {
                    if (hpos == GLGraphicsObjectHPosition.LEFT)
                    {
                        point = slice.End;
                    }
                    else if (hpos == GLGraphicsObjectHPosition.RIGHT)
                    {
                        point = slice.Start;
                    }
                }
                else
                {
                    if (hpos == GLGraphicsObjectHPosition.LEFT)
                        point = slice.Start;
                    else if (hpos == GLGraphicsObjectHPosition.RIGHT)
                        point = slice.End;
                }
                if (hpos == GLGraphicsObjectHPosition.LEFT)
                {
                    cd = 0.0f;
                    if (vpos == GLGraphicsObjectVPosition.UP)
                    {
                        cd = textSize.Height * (1 + margin);
                    }
                    else if (vpos == GLGraphicsObjectVPosition.DOWN)
                    {
                        cd = 0;
                    }
                    else if (vpos == GLGraphicsObjectVPosition.CENTER)
                    {
                        cd = textSize.Height / 2;
                    }
                    v = new Vector2(cd, 0);
                }
                else if (hpos == GLGraphicsObjectHPosition.RIGHT)
                {
                    cd = 0.0f;
                    if (vpos == GLGraphicsObjectVPosition.UP)
                    {
                        cd = textSize.Height * (1 + margin);
                    }
                    else if (vpos == GLGraphicsObjectVPosition.DOWN)
                    {
                        cd = 0;
                    }
                    else if (vpos == GLGraphicsObjectVPosition.CENTER)
                    {
                        cd = textSize.Height / 2;
                    }
                    v = new Vector2(cd, textSize.Width);
                }
            }
            else if (slice.IsHorizontal())
            {
                rotAngle = 0.0;
                float cd = 0.0f;
                if (vpos == GLGraphicsObjectVPosition.UP)
                {
                    cd = textSize.Height * (1 + margin);
                }
                else if (vpos == GLGraphicsObjectVPosition.DOWN)
                {
                    cd = 0;
                }
                else if (vpos == GLGraphicsObjectVPosition.CENTER)
                {
                    cd = textSize.Height / 2;
                }
                v = new Vector2(-textSize.Width / 2, cd);
                if (slice.Start.X > slice.End.X)
                {
                    if (hpos == GLGraphicsObjectHPosition.LEFT)
                        point = slice.End;
                    else if (hpos == GLGraphicsObjectHPosition.RIGHT)
                        point = slice.Start;
                }
                else
                {
                    if (hpos == GLGraphicsObjectHPosition.LEFT)
                        point = slice.Start;
                    else if (hpos == GLGraphicsObjectHPosition.RIGHT)
                        point = slice.End;
                }
                if (hpos == GLGraphicsObjectHPosition.LEFT)
                {
                    cd = 0.0f;
                    if (vpos == GLGraphicsObjectVPosition.UP)
                    {
                        cd = textSize.Height * (1 + margin);
                    }
                    else if (vpos == GLGraphicsObjectVPosition.DOWN)
                    {
                        cd = 0;
                    }
                    else if (vpos == GLGraphicsObjectVPosition.CENTER)
                    {
                        cd = textSize.Height / 2;
                    }
                    v = new Vector2(0, cd);
                }
                else if (hpos == GLGraphicsObjectHPosition.RIGHT)
                {
                    cd = 0.0f;
                    if (vpos == GLGraphicsObjectVPosition.UP)
                    {
                        cd = textSize.Height * (1 + margin);
                    }
                    else if (vpos == GLGraphicsObjectVPosition.DOWN)
                    {
                        cd = 0;
                    }
                    else if (vpos == GLGraphicsObjectVPosition.CENTER)
                    {
                        cd = textSize.Height / 2;
                    }
                    v = new Vector2(-textSize.Width / 2, cd);
                }
            }
            else
            {
                Vector2D vec = new Vector2D();
                if (slice.Start.X > slice.End.X)
                {
                    vec = slice.Start - slice.End;
                    if (hpos == GLGraphicsObjectHPosition.LEFT)
                        point = slice.End;
                    else if (hpos == GLGraphicsObjectHPosition.RIGHT)
                        point = slice.Start;
                }
                else
                {
                    vec = slice.End - slice.Start;
                    if (hpos == GLGraphicsObjectHPosition.LEFT)
                        point = slice.Start;
                    else if (hpos == GLGraphicsObjectHPosition.RIGHT)
                        point = slice.End;
                }
                rotAngle = -vec.GetAngle();
                Vector3 rotv = new Vector3();
                if (hpos == GLGraphicsObjectHPosition.MIDDLE)
                {
                    float cd = 0.0f;
                    if (vpos == GLGraphicsObjectVPosition.UP)
                    {
                        cd = textSize.Height * (1 + margin);
                    }
                    else if (vpos == GLGraphicsObjectVPosition.DOWN)
                    {
                        cd = 0;
                    }
                    else if (vpos == GLGraphicsObjectVPosition.CENTER)
                    {
                        cd = textSize.Height / 2;
                    }
                    v = new Vector2(-textSize.Width / 2, cd);
                }
                else if (hpos == GLGraphicsObjectHPosition.LEFT)
                {
                    float cd = 0.0f;
                    if (vpos == GLGraphicsObjectVPosition.UP)
                    {
                        cd = textSize.Height * (1 + margin);
                    }
                    else if (vpos == GLGraphicsObjectVPosition.DOWN)
                    {
                        cd = 0;
                    }
                    else if (vpos == GLGraphicsObjectVPosition.CENTER)
                    {
                        cd = textSize.Height / 2;
                    }
                    v = new Vector2(0, cd);
                }
                else if (hpos == GLGraphicsObjectHPosition.RIGHT)
                {
                    float cd = 0.0f;
                    if (vpos == GLGraphicsObjectVPosition.UP)
                    {
                        cd = textSize.Height * (1 + margin);
                    }
                    else if (vpos == GLGraphicsObjectVPosition.DOWN)
                    {
                        cd = 0;
                    }
                    else if (vpos == GLGraphicsObjectVPosition.CENTER)
                    {
                        cd = textSize.Height / 2;
                    }
                    v = new Vector2(-textSize.Width, cd);
                }
                Matrix4 m = Matrix4.CreateRotationZ((float)rotAngle);
                rotv = new Vector3((float)v.X, (float)v.Y, 0);
                rotv = Vector3.Transform(rotv, m);
                v.X = rotv.X;
                v.Y = rotv.Y;
            }
            text.Rotate((float)(rotAngle));
            var trnsVector = new Vector3((float)point.X, -(float)point.Y, 0);

            text.Translate(trnsVector);
            text.Translate(v);
        }
    }
}
