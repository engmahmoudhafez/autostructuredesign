﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using Elibre.Geometry2D;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Elibre.AutoStructureDesign.Libraries.GLUtils
{
    public class GLGraphicsObject : IDisposable
    {
        private Texture texture;
        private List<Vector2> UVs;
        private int uvsVBO = -1;


        private List<Vector2> positions;

        public List<Vector2> Positions
        {
            get { return positions; }
        }

      
        private Matrix4 modelMatrix = Matrix4.Identity;

        public Matrix4 ModelMatrix
        {
            get { return modelMatrix; }
           
        }

        protected int VAO = 0;
        protected int VBO = 0;
        public float Z_Order { get; set; } = 0;

        public object Tag { get; set; }

        GLRectangle border;
        SizeF _size;
        private int dimension;
        public bool visible
        {
            get; set;
        } = true;

        public GLGraphicsObject(Bitmap _bitmap)
        {
            dimension = _bitmap.Width;

            _size = _bitmap.Size;
            texture = new Texture(_bitmap);

            var uvY0 = (float)(_size.Height / _bitmap.Height);
            var uvX1 = (float)(_size.Width / _bitmap.Width);

            positions = new List<Vector2>();
            UVs = new List<Vector2>();

            positions.Add(new Vector2(0, 0));
            UVs.Add(new Vector2(0, 0));

            positions.Add(new Vector2(0, -_size.Height));
            UVs.Add(new Vector2(0, uvY0));

            positions.Add(new Vector2(_size.Width, 0));
            UVs.Add(new Vector2(uvX1, 0));

            positions.Add(new Vector2(_size.Width, -_size.Height));
            UVs.Add(new Vector2(uvX1, uvY0));
            
            init();
        }
        public GLGraphicsObject(string str, Font font, Color _color)
        {
            dimension = 512;
            Bitmap _bitmap;

            float aspect = 1;

            _size = new SizeF();
            _bitmap = createBitmap(str, font, _color, aspect, ref _size);
            texture = new Texture(_bitmap);

            var uvY0 = (float)(_size.Height / _bitmap.Height);
            var uvX1 = (float)(_size.Width / _bitmap.Width);

            positions = new List<Vector2>();
            UVs = new List<Vector2>();

            positions.Add(new Vector2(0, 0));
            UVs.Add(new Vector2(0, 0));

            positions.Add(new Vector2(0, -_size.Height));
            UVs.Add(new Vector2(0, uvY0));

            positions.Add(new Vector2(_size.Width, 0));
            UVs.Add(new Vector2(uvX1, 0));

            positions.Add(new Vector2(_size.Width, -_size.Height));
            UVs.Add(new Vector2(uvX1, uvY0));


            init();
            _bitmap.Dispose();
        }
        public GLGraphicsObject(string str, Font font, Color _color, Color backColor)
        {
            dimension = 512;
            Bitmap _bitmap;

            float aspect = 1;

            _size = new SizeF();
            _bitmap = createBitmap(str, font, _color, backColor, aspect, ref _size);
            texture = new Texture(_bitmap);

            var uvY0 = (float)(_size.Height / _bitmap.Height);
            var uvX1 = (float)(_size.Width / _bitmap.Width);

            positions = new List<Vector2>();
            UVs = new List<Vector2>();

            positions.Add(new Vector2(0, 0));
            UVs.Add(new Vector2(0, 0));

            positions.Add(new Vector2(0, -_size.Height));
            UVs.Add(new Vector2(0, uvY0));

            positions.Add(new Vector2(_size.Width, 0));
            UVs.Add(new Vector2(uvX1, 0));

            positions.Add(new Vector2(_size.Width, -_size.Height));
            UVs.Add(new Vector2(uvX1, uvY0));


            init();
            _bitmap.Dispose();
        }
        public GLGraphicsObject(string str, Font font, Color _color, Color borderColor, float thickness)
        {
            dimension = 512;
            Bitmap _bitmap;

            float aspect = 1;

            _size = new SizeF();
            _bitmap = createBitmap(str, font, _color, borderColor, thickness, aspect, ref _size);
            texture = new Texture(_bitmap);

            var uvY0 = (float)(_size.Height / _bitmap.Height);
            var uvX1 = (float)(_size.Width / _bitmap.Width);

            positions = new List<Vector2>();
            UVs = new List<Vector2>();

            positions.Add(new Vector2(0, 0));
            UVs.Add(new Vector2(0, 0));

            positions.Add(new Vector2(0, -_size.Height));
            UVs.Add(new Vector2(0, uvY0));

            positions.Add(new Vector2(_size.Width, 0));
            UVs.Add(new Vector2(uvX1, 0));

            positions.Add(new Vector2(_size.Width, -_size.Height));
            UVs.Add(new Vector2(uvX1, uvY0));


            init();
            _bitmap.Dispose();
        }
        public GLGraphicsObject(string str, Font font, Color _color, Color borderColor, float thickness, Color backColor)
        {
            dimension = 512;
            Bitmap _bitmap;

            float aspect = 1;

            _size = new SizeF();
            _bitmap = createBitmap(str, font, _color, borderColor, thickness, backColor, aspect, ref _size);
            texture = new Texture(_bitmap);

            var uvY0 = (float)(_size.Height / _bitmap.Height);
            var uvX1 = (float)(_size.Width / _bitmap.Width);

            positions = new List<Vector2>();
            UVs = new List<Vector2>();

            positions.Add(new Vector2(0, 0));
            UVs.Add(new Vector2(0, 0));

            positions.Add(new Vector2(0, -_size.Height));
            UVs.Add(new Vector2(0, uvY0));

            positions.Add(new Vector2(_size.Width, 0));
            UVs.Add(new Vector2(uvX1, 0));

            positions.Add(new Vector2(_size.Width, -_size.Height));
            UVs.Add(new Vector2(uvX1, uvY0));


            init();
            _bitmap.Dispose();
        }
        public void SetDimension(int _dim)
        {
            dimension = _dim;
        }
        private Bitmap createBitmap(string text, Font font, Color _color, float aspect, ref SizeF bmpSize)
        {
            var img = new Bitmap(dimension, (int)(dimension / aspect));
            Graphics g = Graphics.FromImage(img);
            g.SmoothingMode = SmoothingMode.HighQuality;
            SolidBrush drawBrush = new SolidBrush(_color);
            bmpSize = g.MeasureString(text, font);
            var fontSize = (float)dimension / bmpSize.Width;
            font = new Font(font.FontFamily, (float)fontSize * font.Size, font.Style, GraphicsUnit.Pixel);
            bmpSize = g.MeasureString(text, font);
            img.Dispose();
            img = new Bitmap(dimension, dimension);
            g = Graphics.FromImage(img);
            g.DrawString(text, font, drawBrush, new PointF(0, 0));
            return img;
        }
        private Bitmap createBitmap(string text, Font font, Color _color, Color backColor, float aspect, ref SizeF bmpSize)
        {
            var img = new Bitmap(dimension, (int)(dimension / aspect));
            Graphics g = Graphics.FromImage(img);
            g.SmoothingMode = SmoothingMode.HighQuality;
            SolidBrush drawBrush = new SolidBrush(_color);
            bmpSize = g.MeasureString(text, font);
            var fontSize = (float)dimension / bmpSize.Width;
            font = new Font(font.FontFamily, (float)fontSize * font.Size, font.Style, GraphicsUnit.Pixel);
            bmpSize = g.MeasureString(text, font);
            img.Dispose();
            img = new Bitmap(dimension, dimension);
            g = Graphics.FromImage(img);
            g.FillRectangle(new SolidBrush(backColor), new Rectangle(0, 0, (int)bmpSize.Width, (int)bmpSize.Height));
            g.DrawString(text, font, drawBrush, new PointF(0, 0));
            return img;
        }
        private Bitmap createBitmap(string text, Font font, Color _color, Color borderColor, float thickness, Color backColor, float aspect, ref SizeF bmpSize)
        {
            var img = new Bitmap(dimension, (int)(dimension / aspect));
            Graphics g = Graphics.FromImage(img);
            g.SmoothingMode = SmoothingMode.HighQuality;
            SolidBrush drawBrush = new SolidBrush(_color);
            bmpSize = g.MeasureString(text, font);
            var fontSize = (float)dimension / bmpSize.Width;
            font = new Font(font.FontFamily, (float)fontSize * font.Size, font.Style, GraphicsUnit.Pixel);
            bmpSize = g.MeasureString(text, font);
            img.Dispose();
            img = new Bitmap(dimension, dimension);
            g = Graphics.FromImage(img);
            g.FillRectangle(new SolidBrush(backColor), new Rectangle(0, 0, (int)bmpSize.Width, (int)bmpSize.Height));
            g.DrawRectangle(new Pen(borderColor, thickness), new Rectangle(0, 0, (int)bmpSize.Width, (int)bmpSize.Height));
            g.DrawString(text, font, drawBrush, new PointF(0, 0));
            return img;
        }
        private Bitmap createBitmap(string text, Font font, Color _color, Color borderColor, float thickness, float aspect, ref SizeF bmpSize)
        {
            var img = new Bitmap(dimension, (int)(dimension / aspect));
            Graphics g = Graphics.FromImage(img);
            g.SmoothingMode = SmoothingMode.HighQuality;
            SolidBrush drawBrush = new SolidBrush(_color);
            bmpSize = g.MeasureString(text, font);
            var fontSize = (float)dimension / bmpSize.Width;
            font = new Font(font.FontFamily, (float)fontSize * font.Size, font.Style, GraphicsUnit.Pixel);
            bmpSize = g.MeasureString(text, font);
            img.Dispose();
            img = new Bitmap(dimension, dimension);
            g = Graphics.FromImage(img);
            g.DrawRectangle(new Pen(borderColor, thickness), new Rectangle(0, 0, (int)bmpSize.Width, (int)bmpSize.Height));
            g.DrawString(text, font, drawBrush, new PointF(0, 0));
            return img;
        }
        public void Scale(float scaleValue)
        {
            modelMatrix *= Matrix4.CreateScale(scaleValue);
            _size.Width *= scaleValue;
            _size.Height *= scaleValue;
        }
        public SizeF getSize()
        {
            return _size;
        }
        public void Translate(Vector3 t)
        {
            modelMatrix *= Matrix4.CreateTranslation(t);
        }
        public void Translate(Vector2 t)
        {
            modelMatrix *= Matrix4.CreateTranslation(new Vector3((float)t.X, (float)t.Y, 0));
        }
        public void Translate(Vector2D t)
        {
            modelMatrix *= Matrix4.CreateTranslation(new Vector3((float)t.X, (float)t.Y, 0));
        }
        public void Translate(Point2D t)
        {
            modelMatrix *= Matrix4.CreateTranslation(new Vector3((float)t.X, (float)t.Y, 0));
        }
        public void Rotate(float angle)
        {
            modelMatrix *= Matrix4.CreateRotationZ(angle);
        }

        public bool IsIntersects(GLGraphicsObject other)
        {
            List<Vector2> other_copy = new List<Vector2>(other.Positions);
            List<Vector2> my_copy = new List<Vector2>(positions);

            for (int i=0;i<my_copy.Count;i++)
            {
                var v = my_copy[i];
                Vector3 v3 = new Vector3(v.X, v.Y, 0);
                v3 = Vector3.Transform(v3, modelMatrix);
                my_copy[i] = new Vector2(v3.X, v3.Y);
            }
            for (int i = 0; i < other_copy.Count; i++)
            {
                var v = other_copy[i];
                Vector3 v3 = new Vector3(v.X, v.Y, 0);
                v3 = Vector3.Transform(v3, other.modelMatrix);
                other_copy[i] = new Vector2(v3.X, v3.Y);
            }
            List<Slice2D> other_slices = new List<Slice2D>();
            List<Slice2D> my_slices = new List<Slice2D>();
            for (int i = 0; i < other_copy.Count-1; i++)
            {
                other_slices.Add(new Slice2D (new Point2D(other_copy[i].X,other_copy[i].Y),
                    new Point2D (other_copy[i+1].X, other_copy[i+1].Y)));
                my_slices.Add(new Slice2D(new Point2D(my_copy[i].X, my_copy[i].Y), 
                    new Point2D(my_copy[i+1].X, my_copy[i + 1].Y)));
            }
            if (other_slices.GetBounds().IntersectsWith(my_slices.GetBounds()))
            {
                return true;
            }
            return false;
        }
        public bool IsIntersects(List<GLGraphicsObject> others)
        {
            foreach (var item in others)
            {
                if (this.IsIntersects(item))
                    return true;
            }
            return false;
        }

        protected void init()
        {
            VAO = GL.GenVertexArray();
            GL.BindVertexArray(VAO);
            VBO = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(sizeof(float) * 2 * positions.Count), positions.ToArray(), BufferUsageHint.StaticDraw);
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);

            if (texture != null)
            {
                uvsVBO = GL.GenBuffer();
                GL.BindBuffer(BufferTarget.ArrayBuffer, uvsVBO);
                GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(sizeof(float) * 2 * UVs.Count), UVs.ToArray(), BufferUsageHint.StaticDraw);
                GL.EnableVertexAttribArray(1);
                GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);
            }
            GL.BindVertexArray(0);
        }
        public void Update()
        {
            GL.BindVertexArray(VAO);
            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(sizeof(float) * 2 * positions.Count), positions.ToArray(), BufferUsageHint.StaticDraw);
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);

            if (texture != null)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, uvsVBO);
                GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(sizeof(float) * 2 * UVs.Count), UVs.ToArray(), BufferUsageHint.StaticDraw);
                GL.EnableVertexAttribArray(1);
                GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);
            }
            GL.BindVertexArray(0);
        }
        public void Render(int programID)
        {
            if (visible)
            {
                GL.Disable(EnableCap.DepthTest);
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

                GL.ActiveTexture(TextureUnit.Texture0);
                int texLoc = GL.GetUniformLocation(programID, "tex");
                GL.Uniform1(texLoc, 0);
                texture.bind();

                int modelLoc = GL.GetUniformLocation(programID, "model");
                GL.UniformMatrix4(modelLoc, false, ref modelMatrix);

                int zorderLoc = GL.GetUniformLocation(programID, "zorder");
                GL.Uniform1(zorderLoc, Z_Order);

                GL.BindVertexArray(VAO);
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.DrawArrays(PrimitiveType.TriangleStrip, 0, positions.Count);
                GL.BindVertexArray(0);
                if (texture != null) texture.unbind();

                GL.Disable(EnableCap.Blend);
                GL.Enable(EnableCap.DepthTest);
            }
        }
        public void Dispose()
        {
            GL.DeleteBuffer(uvsVBO);
            GL.DeleteBuffer(VBO);
            GL.DeleteTexture(texture.GetTextureID());
            GL.DeleteVertexArray(VAO);
            texture.Dispose();
        }
    }
}
