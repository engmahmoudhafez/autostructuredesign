﻿using Elibre.AutoCad;
using Elibre.Geometry2D;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Elibre.AutoStructureDesign.Libraries.GLUtils
{
    public abstract class GLObject : IDisposable
    {

        protected int VAO = -1, VBO = -1, CBO = -1;
        protected List<Slice2D> slices;

        protected EventHandler ConstructArraysHandler = null;
        protected EventHandler InitializeVAOHandler = null;

        public Vector2 Min { get; protected set; }
        public Vector2 Max { get; protected set; }
        public float Alpha { get; set; } = 1f;
        public float Z_Order { get; set; } = 0;
        public bool Visible
        {
            get; set;
        } = true;
        protected Matrix4 modelMatrix = Matrix4.Identity, model = Matrix4.Identity;
        public GLObject()
        {
            slices = new List<Slice2D>();
        }
        public GLObject(List<Slice2D> _slices)
        {
            slices = _slices;
        }
        public abstract void Render(int programID);
        public void Add(Slice2D slice)
        {
            slices.Add(slice);
            ConstructArraysHandler?.Invoke(this, EventArgs.Empty);
            InitializeVAOHandler?.Invoke(this, EventArgs.Empty);
        }
        public void Clear()
        {
            slices.Clear();
            ConstructArraysHandler?.Invoke(this, EventArgs.Empty);
            InitializeVAOHandler?.Invoke(this, EventArgs.Empty);
        }
        public void Translate(Vector2 t)
        {
            modelMatrix *= Matrix4.CreateTranslation(new Vector3((float)t.X, (float)t.Y, 0));
        }
        public void AddRange(List<Slice2D> _slices)
        {
            if (_slices == null)
                return;
            slices.AddRange(_slices);
            ConstructArraysHandler?.Invoke(this, EventArgs.Empty);
            InitializeVAOHandler?.Invoke(this, EventArgs.Empty);
        }
        public void Remove(Slice2D slice)
        {
            slices.Remove(slice);
            ConstructArraysHandler?.Invoke(this, EventArgs.Empty);
            InitializeVAOHandler?.Invoke(this, EventArgs.Empty);
        }
        public void RemoveAt(int index)
        {
            slices.RemoveAt(index);
            ConstructArraysHandler?.Invoke(this, EventArgs.Empty);
            InitializeVAOHandler?.Invoke(this, EventArgs.Empty);
        }
        public void RemoveRange(int from, int to)
        {
            slices.RemoveRange(from, to);
            ConstructArraysHandler?.Invoke(this, EventArgs.Empty);
            InitializeVAOHandler?.Invoke(this, EventArgs.Empty);
        }
        public void RemoveRange(List<Slice2D> _slices)
        {
            if (_slices == null)
                return;
            int count = slices.Count;
            for (int i = count - 1; i >= 0; i--)
            {
                Slice2D s = slices[i];
                if (_slices.Contains(s))
                    slices.RemoveAt(i);
            }
            ConstructArraysHandler?.Invoke(this, EventArgs.Empty);
            InitializeVAOHandler?.Invoke(this, EventArgs.Empty);
        }

        public void Dispose()
        {
            if (VBO > 0)
                GL.DeleteBuffer(VBO);
            if (CBO > 0)
                GL.DeleteBuffer(CBO);
            if (VAO > 0)
                GL.DeleteVertexArray(VAO);
            VAO = -1;
            VBO = -1;
            CBO = -1;
        }
    }
    public class GLDrawableObject : GLObject
    {
        public enum LineTypes
        {
            SOLID = 0,
            DASHED = 0x00FF,
            DOTTED = 0x0101,
            DASH_DOTTED = 0x1C47
        }
        protected List<Vector2> positions { get; set; }
        protected List<Vector3> colors { get; set; }
        public LineTypes LineType = LineTypes.SOLID;
        public int LineTypeFactor = 2;
        public float Thickness
        {
            get; set;
        } = 1f;
        public void SetDrawColor(Color color)
        {
            hasDrawColor = true;
            drawColor = color;
        }
        private bool hasDrawColor = false;
        private Color drawColor = Color.Black;
        public GLDrawableObject() : base()
        {
            ConstructArraysHandler = new EventHandler(ConstructArrays);
            InitializeVAOHandler = new EventHandler(InitializeVAO);
        }
        public GLDrawableObject(List<Slice2D> _list) : base(_list)
        {
            ConstructArraysHandler = new EventHandler(ConstructArrays);
            InitializeVAOHandler = new EventHandler(InitializeVAO);
            ConstructArraysHandler?.Invoke(this, EventArgs.Empty);
            InitializeVAOHandler?.Invoke(this, EventArgs.Empty);
        }
        public GLDrawableObject(List<Slice2D> _list, Color color) : base(_list)
        {
            hasDrawColor = true;
            drawColor = color;
            ConstructArraysHandler = new EventHandler(ConstructArrays);
            InitializeVAOHandler = new EventHandler(InitializeVAO);
            ConstructArraysHandler?.Invoke(this, EventArgs.Empty);
            InitializeVAOHandler?.Invoke(this, EventArgs.Empty);
        }
        public GLDrawableObject(List<Slice2D> _list, Color color, float thickness) : base(_list)
        {
            hasDrawColor = true;
            drawColor = color;
            ConstructArraysHandler = new EventHandler(ConstructArrays);
            InitializeVAOHandler = new EventHandler(InitializeVAO);
            ConstructArraysHandler?.Invoke(this, EventArgs.Empty);
            InitializeVAOHandler?.Invoke(this, EventArgs.Empty);
            Thickness = thickness;
        }
        public void Update(List<Slice2D> updatedSlice)
        {
            ConstructArrays(this, null);

            GL.BindVertexArray(VAO);
            VBO = GL.GenBuffer();
            var positionsArray = positions.ToArray();
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(sizeof(float) * 2 * positions.Count), positionsArray, BufferUsageHint.StaticDraw);
            Array.Clear(positionsArray, 0, positionsArray.Length);
            positionsArray = null;
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);
            if (hasDrawColor == false)
            {
                var colorsArray = colors.ToArray();
                GL.BindBuffer(BufferTarget.ArrayBuffer, CBO);
                GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(sizeof(float) * 3 * colors.Count), colorsArray, BufferUsageHint.StaticDraw);
                Array.Clear(colorsArray, 0, colorsArray.Length);
                colorsArray = null;
                GL.EnableVertexAttribArray(1);
                GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);
            }
            GL.BindVertexArray(0);
        }
        protected void ConstructArrays(Object sender, EventArgs args)
        {
            positions = new List<Vector2>();
            colors = new List<Vector3>();
            if (slices == null || slices.Count == 0 || slices[0] == null) return;
            Max = Min = new Vector2((float)slices[0].Start.X, (float)slices[0].Start.Y);
            foreach (var slice in slices)
            {
                if (slice.IsSegment)
                {
                    Vector2 start = new Vector2((float)slice.Start.X, (float)slice.Start.Y);
                    positions.Add(start);
                    Vector2 end = new Vector2((float)slice.End.X, (float)slice.End.Y);
                    positions.Add(end);
                    Max = Vector2.Max(Max, start);
                    Max = Vector2.Max(Max, end);

                    Min = Vector2.Min(Min, start);
                    Min = Vector2.Min(Min, end);

                    if (hasDrawColor == false)
                    {
                        Color c = Color.Black;
                        if (slice is AutoCadSlice)
                            c = ((AutoCadSlice)slice).Color;
                        colors.Add(new Vector3(c.R / 255.0f, c.G / 255.0f, c.B / 255.0f));
                        colors.Add(new Vector3(c.R / 255.0f, c.G / 255.0f, c.B / 255.0f));
                    }
                }
                else if (slice.IsCircleArc)
                {
                    if (slice.IsClosed)
                    {
                        Circle2D circle = (Circle2D)slice.GetShape();
                        int num_segments = 30;
                        float stepAngle = (float)(Math.PI * 2 / num_segments);
                        for (int i = 0; i < num_segments; i++)
                        {
                            var point1 = circle.GetPointAtAngle((i * stepAngle));
                            var point2 = circle.GetPointAtAngle(((i + 1) * stepAngle));
                            Vector2 p1 = new Vector2((float)point1.X, (float)point1.Y);
                            Vector2 p2 = new Vector2((float)point2.X, (float)point2.Y);
                            positions.Add(p1);
                            positions.Add(p2);
                            Max = Vector2.Max(Max, p1);
                            Max = Vector2.Max(Max, p2);
                            Min = Vector2.Min(Min, p1);
                            Min = Vector2.Min(Min, p2);

                            if (hasDrawColor == false)
                            {
                                Color c = Color.Black;
                                if (slice is AutoCadSlice)
                                    c = ((AutoCadSlice)slice).Color;
                                colors.Add(new Vector3(c.R / 255.0f, c.G / 255.0f, c.B / 255.0f));
                                colors.Add(new Vector3(c.R / 255.0f, c.G / 255.0f, c.B / 255.0f));
                            }
                        }
                    }
                    else
                    {
                        Arc2D arc = (Arc2D)slice.GetShape();
                        var startAngle = (float)Math.Min(arc.StartAngle, arc.EndAngle);
                        var endAngle = (float)Math.Max(arc.StartAngle, arc.EndAngle);
                        int num_segments = 15;
                        float stepAngle = (endAngle - startAngle) / num_segments;
                        for (int i = 0; i < num_segments; i++)
                        {
                            var point1 = arc.GetPointAtAngle(startAngle + (i * stepAngle));
                            var point2 = arc.GetPointAtAngle(startAngle + ((i + 1) * stepAngle));
                            Vector2 p1 = new Vector2((float)point1.X, (float)point1.Y);
                            Vector2 p2 = new Vector2((float)point2.X, (float)point2.Y);
                            positions.Add(p1);
                            positions.Add(p2);
                            Max = Vector2.Max(Max, p1);
                            Max = Vector2.Max(Max, p2);
                            Min = Vector2.Min(Min, p1);
                            Min = Vector2.Min(Min, p2);

                            if (hasDrawColor == false)
                            {
                                Color c = Color.Black;
                                if (slice is AutoCadSlice)
                                    c = ((AutoCadSlice)slice).Color;
                                colors.Add(new Vector3(c.R / 255.0f, c.G / 255.0f, c.B / 255.0f));
                                colors.Add(new Vector3(c.R / 255.0f, c.G / 255.0f, c.B / 255.0f));
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Unsupported segment");
                }
            }
        }
        protected void InitializeVAO(Object sender, EventArgs args)
        {
            if (VAO > 0)
            {
                GL.DeleteVertexArray(VAO);
                GL.DeleteBuffer(VBO);
                if (hasDrawColor == false) GL.DeleteBuffer(CBO);
            }
            VAO = GL.GenVertexArray();
            GL.BindVertexArray(VAO);
            VBO = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            var positionsArray = positions.ToArray();
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(sizeof(float) * 2 * positions.Count), positionsArray, BufferUsageHint.StaticDraw);
            Array.Clear(positionsArray, 0, positionsArray.Length);
            positionsArray = null;
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);
            if (hasDrawColor == false)
            {
                var colorsArray = colors.ToArray();
                CBO = GL.GenBuffer();
                GL.BindBuffer(BufferTarget.ArrayBuffer, CBO);
                GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(sizeof(float) * 3 * colors.Count), colorsArray, BufferUsageHint.StaticDraw);
                Array.Clear(colorsArray, 0, colorsArray.Length);
                colorsArray = null;
                GL.EnableVertexAttribArray(1);
                GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);
            }

            GL.BindVertexArray(0);
        }
        public override void Render(int programID)
        {
            if (Visible)
            {
                Vector3 drawColorVector = new Vector3((float)drawColor.R / 255, (float)drawColor.G / 255, (float)drawColor.B / 255);
                int hasColorLoc = GL.GetUniformLocation(programID, "hasDrawColor");
                GL.Uniform1(hasColorLoc, hasDrawColor ? 1 : 0);
                int zorderLoc = GL.GetUniformLocation(programID, "zorder");
                GL.Uniform1(zorderLoc, Z_Order);
                int colorLoc = GL.GetUniformLocation(programID, "drawColor");
                GL.Uniform3(colorLoc, drawColorVector.X, drawColorVector.Y, drawColorVector.Z);
                int alphaLoc = GL.GetUniformLocation(programID, "alpha");
                GL.Uniform1(alphaLoc, Alpha);
                int modelLoc = GL.GetUniformLocation(programID, "model");
                GL.UniformMatrix4(modelLoc, false, ref modelMatrix);
                GL.BindVertexArray(VAO);

                if (Alpha < 1)
                {
                    GL.Disable(EnableCap.DepthTest);
                    GL.Enable(EnableCap.Blend);
                    GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                }
                if (LineType != LineTypes.SOLID)
                {
                    OpenTK.Graphics.OpenGL.GL.LineStipple(LineTypeFactor, (short)LineType);
                    OpenTK.Graphics.OpenGL.GL.Enable(OpenTK.Graphics.OpenGL.EnableCap.LineStipple);
                }

                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
                GL.LineWidth(Thickness);
                GL.DrawArrays(PrimitiveType.Lines, 0, positions.Count);
                GL.BindVertexArray(0);

                if (LineType != LineTypes.SOLID)
                    OpenTK.Graphics.OpenGL.GL.Disable(OpenTK.Graphics.OpenGL.EnableCap.LineStipple);

                if (Alpha < 1)
                {
                    GL.Disable(EnableCap.Blend);
                    GL.Enable(EnableCap.DepthTest);
                }
            }
        }
    }

    public class GLFillableObject : GLObject
    {
        protected List<Vector2> _positions { get; set; }
        protected List<Vector3> _colors { get; set; }
        private List<Point2D> _points;
        public Vector3 FillColor = new Vector3(0);
        public GLFillableObject() : base()
        {
            ConstructArraysHandler = new EventHandler(ConstructArrays);
            InitializeVAOHandler = new EventHandler(InitializeVAO);
        }
        public GLFillableObject(SlicePolygon2D poly) : base(null)
        {
            _points = poly.Triangles.SelectMany(o => o.Points).ToList();
            Init();
        }
        private void Init()
        {
            ConstructArraysHandler = new EventHandler(ConstructArrays);
            InitializeVAOHandler = new EventHandler(InitializeVAO);
            ConstructArraysHandler?.Invoke(this, EventArgs.Empty);
            InitializeVAOHandler?.Invoke(this, EventArgs.Empty);
        }
        public GLFillableObject(Polygon2D poly) : base(poly.GetSlices())
        {
            _points = poly.Triangulate2().SelectMany(o => o.Points).ToList();
            Init();
        }
        public GLFillableObject(List<SlicePolygon2D> polys) : base(null)
        {
            foreach (var p in polys)
            {
                if (p.Triangles == null)
                    p.UpdateTriangles();
            }
            _points = polys.SelectMany(o => o.Triangles.SelectMany(m => m.Points)).ToList();
            Init();
        }
        public GLFillableObject(List<SlicePolygon2D> polys, Color color) : base(null)
        {
            foreach (var p in polys)
            {
                if (p.Triangles == null)
                    p.UpdateTriangles();
            }
            FillColor = new Vector3((float)color.R / 255, (float)color.G / 255, (float)color.B / 255);
            _points = polys.SelectMany(o => o.Triangles.SelectMany(m => m.Points)).ToList();
            Init();
        }
        public GLFillableObject(SlicePolygon2D poly, Color color) : base(null)
        {
            if (poly.Triangles == null)
                poly.UpdateTriangles();
            FillColor = new Vector3((float)color.R / 255, (float)color.G / 255, (float)color.B / 255);
            _points = poly.Triangles.SelectMany(o => o.Points).ToList();
            Init();
        }
        public void Update(List<SlicePolygon2D> updatedPolygons)
        {
            ConstructArrays(this, null);

            GL.BindVertexArray(VAO);
            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            var positionsArray = _positions.ToArray();
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(sizeof(float) * 2 * _positions.Count), positionsArray, BufferUsageHint.StaticDraw);
            Array.Clear(positionsArray, 0, positionsArray.Length);
            positionsArray = null;
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);

            GL.BindVertexArray(0);
        }

        protected void ConstructArrays(Object sender, EventArgs args)
        {
            _positions = new List<Vector2>();
            if (_points.Count > 0)
            {
                var bounds = _points.GetBounds();
                foreach (var p in _points)
                {
                    _positions.Add(new Vector2((float)p.X, (float)p.Y));
                }
                Min = new Vector2((float)bounds.Left, (float)bounds.Top);
                Max = new Vector2((float)bounds.Right, (float)bounds.Bottom);
            }
        }
        protected void InitializeVAO(Object sender, EventArgs args)
        {
            if (VAO > -1)
            {
                GL.DeleteVertexArray(VAO);
                GL.DeleteBuffer(VBO);
            }
            VAO = GL.GenVertexArray();
            GL.BindVertexArray(VAO);
            VBO = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            var positionsArray = _positions.ToArray();
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(sizeof(float) * 2 * _positions.Count), positionsArray, BufferUsageHint.StaticDraw);
            Array.Clear(positionsArray, 0, positionsArray.Length);
            positionsArray = null;
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);

            GL.BindVertexArray(0);
        }
        public override void Render(int programID)
        {
            if (Visible && _positions != null)
            {
                int zorderLoc = GL.GetUniformLocation(programID, "zorder");
                GL.Uniform1(zorderLoc, Z_Order);
                int modelLoc = GL.GetUniformLocation(programID, "model");
                GL.UniformMatrix4(modelLoc, false, ref modelMatrix);
                int alphaLoc = GL.GetUniformLocation(programID, "alpha");
                GL.Uniform1(alphaLoc, Alpha);
                int fillColorLoc = GL.GetUniformLocation(programID, "fillColor");
                GL.Uniform3(fillColorLoc, ref FillColor);

                if (Alpha < 1)
                {
                    GL.Disable(EnableCap.DepthTest);
                    GL.Enable(EnableCap.Blend);
                    GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                }

                GL.BindVertexArray(VAO);
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.DrawArrays(PrimitiveType.Triangles, 0, _positions.Count);
                GL.BindVertexArray(0);

                if (Alpha < 1)
                {
                    GL.Disable(EnableCap.Blend);
                    GL.Enable(EnableCap.DepthTest);
                }
            }
        }
    }
}
