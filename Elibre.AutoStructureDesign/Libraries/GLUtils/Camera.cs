﻿using OpenTK;
using OpenTK.Graphics.ES20;

namespace Elibre.AutoStructureDesign.Libraries.GLUtils
{
    public class Camera
    {
        private Vector3 eye, target, up;
        private Matrix4 view = Matrix4.Identity, projection = Matrix4.Identity;
        public float rotationAngle = 0.0f;
        float zoomFactor=1;
        public Vector3 Target
        {
            get { return target; }
        }

        public Camera(Vector3 eye)
        {
            this.eye = eye;
            this.target = new Vector3(eye.X, eye.Y, 0.0f);
            this.up = new Vector3(0, 1, 0);
        }
        public void SetZoomFactor(float factor, float width, float height)
        {
            zoomFactor *= factor;
            projection = Matrix4.CreateOrthographicOffCenter((-width / 2) / zoomFactor, (width / 2) / zoomFactor, -(height / 2) / zoomFactor, (height / 2) / zoomFactor, -1, 10);
        }
        public void SetZoom(float factor, float width, float height)
        {
            zoomFactor = factor;
            projection = Matrix4.CreateOrthographicOffCenter((-width / 2) / zoomFactor, (width / 2) / zoomFactor, -(height / 2) / zoomFactor, (height / 2) / zoomFactor, -1, 10);
        }
        public void update(float width, float height)
        {
            Matrix4 rotation = Matrix4.CreateRotationZ(MathHelper.DegreesToRadians(rotationAngle));
            view = Matrix4.LookAt(eye, target, up);
            view = view * rotation;
            projection = Matrix4.CreateOrthographicOffCenter((-width / 2) / zoomFactor, (width / 2) / zoomFactor, -(height / 2) / zoomFactor, (height / 2) / zoomFactor, -1, 10);
        }
        

        public void render(int programID)
        {
            int projectionLoc = GL.GetUniformLocation(programID, "projection");
            GL.UniformMatrix4(projectionLoc, false, ref projection);
            int viewLoc = GL.GetUniformLocation(programID, "view");
            GL.UniformMatrix4(viewLoc, false, ref view);
        }
        public void Rotate(float angle)
        {
            rotationAngle += angle;
        }
        public void setEye(Vector2 v)
        {
            eye.X = target.X = v.X;
            eye.Y = target.Y = v.Y;
        }
        public void translate(Vector2 v)
        {
            eye.X = target.X = eye.X + v.X;
            eye.Y = target.Y = eye.Y + v.Y;
        }
        public Matrix4 getView() { return view; }
        public Matrix4 getProjection() { return projection; }

    }
}
