﻿using Elibre.AutoStructureDesign.Models;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign.Libraries.GLUtils
{
    public class GLStateRenderer : GLControl
    {
        private GLStatesManager statesManager;
        public GLStateRenderer() : base(OpenTK.Graphics.GraphicsMode.Default, 4, 0, OpenTK.Graphics.GraphicsContextFlags.ForwardCompatible)
        {

        }
        public void setStatesManager(GLStatesManager mgr)
        {
            statesManager = mgr;
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            GL.ClearColor(Color.White);
            GL.Enable(EnableCap.DepthTest);
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            Render();
            SwapBuffers();
            Invalidate();
        }
        public void Render()
        {
            statesManager.Render();
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            statesManager.OnMouseDown(e);
        }
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            statesManager.OnMouseMove(e);
        }
        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            statesManager.OnMouseUp(e);
        }
        protected override void OnMouseWheel(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            statesManager.OnMouseWheel(e);
            Invalidate();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            statesManager?.OnResize(e);
            Invalidate();
        }
        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            base.OnDoubleClick(e);
            statesManager.OnMouseDoubleClick(e);
            Invalidate();
        }
        protected override void OnInvalidated(InvalidateEventArgs e)
        {
            base.OnInvalidated(e);
            statesManager.Update();
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
