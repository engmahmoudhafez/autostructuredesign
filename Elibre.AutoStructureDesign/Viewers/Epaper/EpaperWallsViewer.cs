﻿using Elibre.Geometry2D;
using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using Elibre.CAD.StitcherApi.Models.Stitching;
using Elibre.Ui;
using System.Diagnostics;
using StitcherNewUIApp.Controls.Epaper;
using Elibre.CAD.StitcherApi.Models;
using Elibre.CAD.StitcherApi.Algorithms.RoofAlgorithms;
using Elibre.CAD.StitcherApi.Algorithms;
using Elibre.CAD.StitcherApi.Algorithms.WallDetectors;
using Elibre.CAD.Analysis;
using Elibre.CAD.StitcherApi.Dialogs.DimensionFixing;
using Ag01.Logic;
using Elibre.Stitcher.Models.Stitching;
using Elibre.Stitcher.ServerLogic;
using Elibre.Stitcher.Revit;
using Elibre.AutoStructureDesign.Properties;
using Elibre.GDI;
using Elibre.AutoStructureDesign.Dialogs;
using Elibre.AutoStructureDesign.Models;
using Elibre.AutoStructureDesign;

namespace StitcherNewUIApp.Viewers.Epaper
{
    public class EpaperWallsViewer : Viewer2D
    {
        #region Properties
        //consts
        private const double GridUnitLength = 10;
        private const double _distanceToDimension = 30;
        private const double _defaultWallThickness = 6;

        //public
        public StitcherLevel ApaperLevel { get; set; }
        public EpaperMode _apaperMode { get; set; }
        public DrawTypeMode _drawTypeMode { get; set; }
        public DrawTypeMode _approvedLine_RectMode { get; set; }
        public EventHandler OnFloorChanged { get; set; }
        public List<List<RoofSlice>> RoofSegments { get; set; }

        //private
        private Slice2D _drawSlice;
        private Dictionary<DrawTypeMode, Cursor> _cursorDict;
        private Slice2D _deSelectionSlice;
        private Point2DSnapper _snapper;
        private Point2DSnappingInfo _snapperInfo;
        private StitcherBuilding _building;
        private Panel _statePanel;
        private Stopwatch _statePanelWatch;
        private StitcherOpening _currentSwappingOpening;
        private RoofSlice _roofSliceUnderMouse;
        private bool _applyToRoof;
        private RoofDesignRulesOptions _appliedRoofDesginRules;
        private Cursor _roofCursor;

        //dimensionWork
        private List<Slice2D> _splittedWalls;
        private List<Dimension> _dimensions;
        private List<Dimension> _detectedDimensionsOnWalls;
        private Dimension _dimensionUnderMouse;

        //buttons
        private Button _buttonGableRoof;
        private Button _buttonSwapDoorHorizontally;
        private Button _buttonSwapDoorVertically;
        private Button _buttonSwapDoorWindow;

        #endregion

        #region Constractor

        public EpaperWallsViewer(StitcherBuilding building)
        {
            _building = building;
            ApaperLevel = building.Levels[0];
            _apaperMode = EpaperMode.Rectangle;
            _cursorDict = GetCursorDict();
            var options = new SnappingOptions(Point2DSnappingType.Nearest);
            _snapper = new Point2DSnapper(this, options);
            _roofCursor = new Cursor(Resources.epaperRoofIcon.GetHicon());
            _dimensions = new List<Dimension>();
            _detectedDimensionsOnWalls = new List<Dimension>();
            _splittedWalls = new List<Slice2D>();

            #region Initialize buttons

            _buttonSwapDoorHorizontally = new Button();
            _buttonSwapDoorHorizontally.Image = Resources.swapDoorRightLeftIcon;
            _buttonSwapDoorHorizontally.Parent = this;
            _buttonSwapDoorHorizontally.Visible = false;
            _buttonSwapDoorHorizontally.Size = new Size(20, 20);
            _buttonSwapDoorHorizontally.BackColor = Color.Transparent;
            _buttonSwapDoorHorizontally.FlatStyle = FlatStyle.Flat;
            _buttonSwapDoorHorizontally.Click += button_SwapHorizontal_Click;
            _buttonSwapDoorHorizontally.FlatAppearance.BorderColor = Color.FromArgb(0, 0, 0, 0);

            _buttonSwapDoorVertically = new Button();
            _buttonSwapDoorVertically.Image = Resources.swapDoorUpDownIcon;
            _buttonSwapDoorVertically.Parent = this;
            _buttonSwapDoorVertically.Visible = false;
            _buttonSwapDoorVertically.Size = new Size(20, 20);
            _buttonSwapDoorVertically.BackColor = Color.Transparent;
            _buttonSwapDoorVertically.FlatStyle = FlatStyle.Flat;
            _buttonSwapDoorVertically.Click += button_SwapVertical_Click;
            _buttonSwapDoorVertically.FlatAppearance.BorderColor = Color.FromArgb(0, 0, 0, 0);

            _buttonGableRoof = new Button();
            _buttonGableRoof.Image = Resources.epaperGableRoof;
            _buttonGableRoof.Parent = this;
            _buttonGableRoof.Visible = false;
            _buttonGableRoof.Size = new Size(20, 20);
            _buttonGableRoof.BackColor = Color.Transparent;
            _buttonGableRoof.FlatStyle = FlatStyle.Flat;
            _buttonGableRoof.Click += button_GableRoof_Click;
            _buttonGableRoof.FlatAppearance.BorderColor = Color.FromArgb(0, 0, 0, 0);

            _buttonSwapDoorWindow = new Button();
            _buttonSwapDoorWindow.Image = Resources.EpaperSwapDoorWindow;
            _buttonSwapDoorWindow.Parent = this;
            _buttonSwapDoorWindow.Visible = false;
            _buttonSwapDoorWindow.Size = new Size(20, 20);
            _buttonSwapDoorWindow.BackColor = Color.Transparent;
            _buttonSwapDoorWindow.FlatStyle = FlatStyle.Flat;
            _buttonSwapDoorWindow.Click += button_SwapDoorsWindows_Click;
            _buttonSwapDoorWindow.FlatAppearance.BorderColor = Color.FromArgb(0, 0, 0, 0);

            #endregion
        }

        #endregion

        #region Methods

        private Dictionary<DrawTypeMode, Cursor> GetCursorDict()
        {
            var dict = new Dictionary<DrawTypeMode, Cursor>();
            foreach (DrawTypeMode val in Enum.GetValues(typeof(DrawTypeMode)))
            {
                IntPtr handle = new IntPtr();
                if (val == DrawTypeMode.Rectangle)
                    handle = Resources.apaperRectangleIcon.GetHicon();
                else if (val == DrawTypeMode.Line)
                    handle = Resources.apaperLineIcon.GetHicon();
                else if (val == DrawTypeMode.Window)
                    handle = Resources.apapeWindowrIcon.GetHicon();
                else if (val == DrawTypeMode.Door)
                    handle = Resources.apaperDoorIcon.GetHicon();
                else if (val == DrawTypeMode.Dimension)
                    handle = Resources.epaperDimensionIcon.GetHicon();

                var cursor = new Cursor(handle);
                dict.Add(val, cursor);
            }
            return dict;
        }
        private EpaperMode GetNextMode(MouseEventArgs e)
        {
            var number = (int)_apaperMode;
            if (e.Delta > 1)
                number--;
            else
                number++;
            if (number > Enum.GetNames(typeof(EpaperMode)).Count() - 1)
                number = 0;
            else if (number < 0)
                number = Enum.GetNames(typeof(EpaperMode)).Count() - 1;
            return (EpaperMode)number;
        }
        private void HandleOpeningClick(Point2D currentPt, OpeningType type)
        {
            if (_drawSlice == null)
            {
                if (_snapperInfo.SnappingType != Point2DSnappingType.None)
                {
                    var wallUnderCursor = ApaperLevel.FloorPlan.Walls.FirstOrDefault(o => o.SingleWall2D.Contains(_snapperInfo.TargetPoint));
                    if (wallUnderCursor.Openings != null && wallUnderCursor.Openings.Any(o => o.SingleLine2D.Contains(_snapperInfo.TargetPoint)))
                        return;
                    if (wallUnderCursor == null)
                        return;
                    _drawSlice = new Slice2D(_snapperInfo.TargetPoint, _snapperInfo.TargetPoint);
                }
            }
            else if (_drawSlice.Length.IsApproxGreaterThan(0))
            {
                var wallUnderCursor = ApaperLevel.FloorPlan.Walls.FirstOrDefault(o => o.SingleWall2D.Contains(_drawSlice.GetStartPoint()));
                if (wallUnderCursor.Openings == null)
                    wallUnderCursor.Openings = new List<StitcherOpening>();
                if (wallUnderCursor.Openings.Any(o => o.SingleLine2D.IsIntersects(_drawSlice)))
                    return;
                if (!wallUnderCursor.SingleWall2D.Contains(_drawSlice))
                    return;
                AddOpening(wallUnderCursor, _drawSlice, type);
                OnFloorChanged?.Invoke(null, EventArgs.Empty);
                _drawSlice = null;
            }
        }

        private void HandleOpeningHover(Point2D currentPt)
        {
            var wallUnderCursor = ApaperLevel.FloorPlan.Walls.FirstOrDefault(o => o.SingleWall2D.Contains(_drawSlice.GetStartPoint(), ZoomToWorld(7)));
            var proj = currentPt.GetProjection(wallUnderCursor.SingleWall2D);
            if (wallUnderCursor.SingleWall2D.IsSegment)
            {
                _drawSlice.End = proj;
            }
            else if (wallUnderCursor.SingleWall2D.IsCircleArc)
            {
                var ptBetween = proj + (_drawSlice.GetStartPoint() - proj) * 0.5;
                var projBetween = ptBetween.GetProjection(wallUnderCursor.SingleWall2D);
                var arc = Arc2D.From3Points(_drawSlice.GetStartPoint(), projBetween, proj);
                if (arc.Radius != 0)
                {
                    if (_drawSlice.IsSegment)
                        _drawSlice = new Slice2D(arc.Center, arc.Radius, arc.StartAngle, arc.EndAngle);
                    else
                    {
                        _drawSlice.CircleRadius = arc.Radius;
                        _drawSlice.Center = arc.Center;
                        _drawSlice.StartAngle = arc.StartAngle;
                        _drawSlice.EndAngle = arc.EndAngle;
                    }
                }
            }
        }
        private bool RemoveWallsByDeselectedSlice()
        {
            var anythingRemoved = false;
            var bounds = _deSelectionSlice.GetBounds();
            var boundsSlices = bounds.ToSlices();

            var allOpenings = ApaperLevel.FloorPlan.Walls.Where(o => o.Openings != null).SelectMany(o => o.Openings).ToList();
            foreach (var wall in ApaperLevel.FloorPlan.Walls)
            {
                var toBeRempvedOpenings = new List<StitcherOpening>();
                if (wall.Openings == null) continue;
                foreach (var open in wall.Openings)
                {
                    if (bounds.Contains(open.SingleLine2D.GetStartPoint()) || bounds.Contains(open.SingleLine2D.GetEndPoint()) || boundsSlices.Any(ee => ee.IsIntersects(open.SingleLine2D)))
                    {
                        toBeRempvedOpenings.Add(open);
                        anythingRemoved = true;
                    }
                }
                toBeRempvedOpenings.ForEach(o => wall.Openings.Remove(o));
            }

            if (!anythingRemoved)//remove walls if no intersected openings
            {
                var toBeRempvedWalls = new List<StitcherWall>();
                foreach (var wall in ApaperLevel.FloorPlan.Walls)
                {
                    if (bounds.Contains(wall.SingleWall2D.GetStartPoint()) || bounds.Contains(wall.SingleWall2D.GetEndPoint()) || boundsSlices.Any(ee => ee.IsIntersects(wall.SingleWall2D)))
                    {
                        toBeRempvedWalls.Add(wall);
                        anythingRemoved = true;
                    }
                }
                toBeRempvedWalls.ForEach(o => ApaperLevel.FloorPlan.Walls.Remove(o));
            }
            if (anythingRemoved)
                AddWalls_UpdateWalls(ApaperLevel.FloorPlan, new List<Slice2D>());
            return anythingRemoved;
        }
        private void ShowDoorSwappingButtons(Slice2D singleLine2D)
        {
            var vec = singleLine2D.GetUnitVector() * ZoomToWorld(_buttonSwapDoorVertically.Width + 10);
            var pt1 = ToViewPort(singleLine2D.GetMidPoint() - vec);
            var pt2 = ToViewPort(singleLine2D.GetMidPoint());
            var pt3 = ToViewPort(singleLine2D.GetMidPoint() + vec);
            var points = new List<PointF>() { pt1, pt2, pt3 };
            points = points.OrderBy(o => o.X).ThenBy(o => o.Y).ToList();

            _buttonSwapDoorHorizontally.Location = new Point((int)points[0].X, (int)points[0].Y);
            _buttonSwapDoorHorizontally.Visible = true;
            _buttonSwapDoorVertically.Location = new Point((int)points[1].X, (int)points[1].Y);
            _buttonSwapDoorVertically.Visible = true;
            _buttonSwapDoorWindow.Location = new Point((int)points[2].X, (int)points[2].Y);
            _buttonSwapDoorWindow.Visible = true;
            _buttonSwapDoorWindow.Image = Resources.EpaperSwapWindowDoor;
        }
        private void ShowWindowSwappingButtons(Slice2D singleLine2D)
        {
            var vec = singleLine2D.GetUnitVector() * ZoomToWorld(_buttonSwapDoorWindow.Width + 10);
            var pt1 = ToViewPort(singleLine2D.GetMidPoint() - 0.5 * vec);

            _buttonSwapDoorWindow.Visible = true;
            _buttonSwapDoorWindow.Location = new Point((int)pt1.X, (int)pt1.Y);
            _buttonSwapDoorWindow.Image = Resources.EpaperSwapDoorWindow;
        }
        private void HideDoorSwappingButtons()
        {
            _buttonSwapDoorHorizontally.Visible = false;
            _buttonSwapDoorVertically.Visible = false;
            _buttonSwapDoorWindow.Visible = false;
        }
        private void AddOpening(StitcherWall wallUnderCursor, Slice2D slice, OpeningType type)
        {
            var sliceType = type == OpeningType.Door ? SliceType.Door : SliceType.Window;
            var opening = new StitcherOpening(new AnalysisSlice(slice, sliceType) { WallThickness = wallUnderCursor.Thickness }, type)
            {
                RevitType = type == OpeningType.Window ? _building.Template.WindowSymbols.OrderBy(o => Math.Abs(o.Width - slice.Length)).ElementAt(0).SymbolName : _building.Template.DoorSymbols.OrderBy(o => Math.Abs(o.Width - slice.Length)).ElementAt(0).SymbolName,
            };
            opening.UpdateHeightByTemplate(_building.Template);
            if (type == OpeningType.Window)
                opening.SillHeight = 20;
            if (wallUnderCursor.Openings == null)
                wallUnderCursor.Openings = new List<StitcherOpening>();
            wallUnderCursor.Openings.Add(opening);
        }
        private void AddWalls_UpdateWalls(StitcherFloorPlan apaperPlan, List<Slice2D> slices)
        {
            if (EStitcherSingleton.GetInstance.IsStitcherModel)
            {
                var newSlices = slices.CreateCopy();
                newSlices.Slice();
                var doubleLines = FillStitcherLevelsAlgorithms.GetDoubleLines(newSlices, _defaultWallThickness);
                var slicedDoubles = doubleLines.CreateCopy();
                slicedDoubles.Slice();
                var floorsPolys = slicedDoubles.GetPolygons().GetOuterPolygons();
                if (floorsPolys != null)
                {
                    floorsPolys.AddRange(ApaperLevel.FloorPlan.FloorPolygons.Select(o => o.FloorPolygon));
                    floorsPolys = floorsPolys.Distinct().Where(o => !floorsPolys.Any(e => e != o && e.Contains(o))).ToList();
                }
                var singleDoors = new List<Slice2D>();
                var singleWindows = new List<Slice2D>();
                _building.Template.SnapToCurrentWallTypes = false;

                //var detectedWalls = new List<StitcherWall>();
                var detectedWalls = FloorCreator.CreateFloor(doubleLines
                                                             , new List<Slice2D>()
                                                             , new List<Slice2D>()
                                                             , _defaultWallThickness
                                                             , _building.Template
                                                             , floorsPolys
                                                             , null
                                                             , out singleDoors
                                                             , out singleWindows);
                apaperPlan.Walls.AddRange(detectedWalls);
            }
            else
            {

                var oldWallsCount = apaperPlan.Walls.Count;
                var arcs = slices.Where(o => o.IsCircleArc).ToList();
                slices = slices.Where(o => o.IsSegment).ToList();
                var keepOldWalls = true;
                var singleLines = ApaperLevel.FloorPlan.Walls.Select(o => o.SingleWall2D).ToList();
                if (slices.Count == 0 || singleLines.Any(o => slices.Any(e => o.IsIntersects(e))))
                    keepOldWalls = false;
                //save current openings
                var existingOpenings = ApaperLevel.FloorPlan.Walls.Where(o => o.Openings != null).SelectMany(o => o.Openings).ToList();
                var newSlices = slices.CreateCopy();
                if (!keepOldWalls)
                    newSlices.AddRange(singleLines);
                newSlices.Slice();
                //set splitted slices
                _splittedWalls = newSlices.CreateCopy();

                var doubleLines = FillStitcherLevelsAlgorithms.GetDoubleLines(newSlices, _defaultWallThickness);
                var slicedDoubles = doubleLines.CreateCopy();
                slicedDoubles.Slice();
                var floorsPolys = slicedDoubles.GetPolygons().GetOuterPolygons();
                if (floorsPolys != null)
                {
                    floorsPolys.AddRange(ApaperLevel.FloorPlan.FloorPolygons.Select(o => o.FloorPolygon));
                    floorsPolys = floorsPolys.Distinct().Where(o => !floorsPolys.Any(e => e != o && e.Contains(o))).ToList();
                }
                var singleDoors = new List<Slice2D>();
                var singleWindows = new List<Slice2D>();
                _building.Template.SnapToCurrentWallTypes = false;

                //var detectedWalls = new List<StitcherWall>();
                var detectedWalls = FloorCreator.CreateFloor(doubleLines
                                                             , new List<Slice2D>()
                                                             , new List<Slice2D>()
                                                             , _defaultWallThickness
                                                             , _building.Template
                                                             , floorsPolys
                                                             , null
                                                             , out singleDoors
                                                             , out singleWindows);
                foreach (var open in existingOpenings)
                {
                    var midPt = open.SingleLine2D.GetMidPoint();
                    var containingWall = detectedWalls.FirstOrDefault(o => o.SingleWall2D.Contains(midPt));
                    if (containingWall != null)
                    {
                        if (containingWall.Openings == null)
                            containingWall.Openings = new List<StitcherOpening>();
                        containingWall.Openings.Add(open);
                    }
                }
                if (keepOldWalls)
                {
                    ApaperLevel.FloorPlan.UpdateStitcherWallsData(detectedWalls, _building.Template);
                    FillStitcherLevelsAlgorithms.Center_ConnectSingleLines(detectedWalls);
                    detectedWalls.ForEach(o => o.SetIsBearing(_building.Template));
                    ApaperLevel.FloorPlan.Walls.AddRange(detectedWalls);
                }
                else
                {
                    ApaperLevel.FloorPlan.Walls = detectedWalls;
                    ApaperLevel.FloorPlan.UpdateStitcherWallsData(_building.Template);
                    FillStitcherLevelsAlgorithms.Center_ConnectSingleLines(ApaperLevel.FloorPlan.Walls);
                    //set IsBearing 
                    ApaperLevel.FloorPlan.Walls.ForEach(o => o.SetIsBearing(_building.Template));
                }
                var levelIndex = _building.Levels.FindIndex(o => o == ApaperLevel);
                var topLevelId = _building.TopWallLevels[levelIndex].Id;
                ApaperLevel.FloorPlan.Walls.ForEach(o => o.TopLevelId = topLevelId);
                if (arcs.Count > 0)
                {
                    var arcedWalls = arcs.Select(o => GetArcedWall(o)).ToList();
                    ApaperLevel.FloorPlan.Walls.AddRange(arcedWalls);
                }
                var newWallsCount = apaperPlan.Walls.Count;
                if (newWallsCount != oldWallsCount)
                {
                    OnFloorChanged?.Invoke(null, EventArgs.Empty);
                    UpdateDimensions();
                }
            }

        }

        private StitcherWall GetArcedWall(Slice2D arc)
        {
            var wall = new StitcherWall(new AnalysisSlice(arc) { WallThickness = _defaultWallThickness });
            var levelIndex = _building.Levels.FindIndex(o => o == ApaperLevel);
            var topLevelId = _building.TopWallLevels[levelIndex].Id;
            var type = _building.Template.WallTypes.FirstOrDefault(o => o.TypeName.Equals("Elibre Arc Wall Type"));
            if (type == null)
            {
                type = new RevitNewType() { TypeName = "Arc wall Type", TypeLayers = new List<RevitTypeLayer>() { new RevitTypeLayer() { Thickness = 6, Function = 0,LayerType= RevitTypeLayer.LayerTypeEnum.Main } } };
                _building.Template.WallTypes.Add(type);
            }
            wall.RevitType = type.TypeName;
            wall.TopLevelId = topLevelId;
            wall.LevelIndex = levelIndex;
            wall.UpdateRectangle2D();
            wall.SetWallLayers(_building.Template);

            return wall;
        }

        private void UpdateRoofButtonImage()
        {
            if (_roofSliceUnderMouse.IsGable)
                _buttonGableRoof.Image = Resources.epaperHeepRoof;
            else
                _buttonGableRoof.Image = Resources.epaperGableRoof;
        }
        private void UpdateDimensions()
        {
            var solver = new LocationSolver(_dimensions, _splittedWalls, (int)GridUnitLength);
            solver.Predict(allowNegative: true);
            _detectedDimensionsOnWalls = solver.GetDetectedDimensions();
        }

        internal void UpdateBuilding()
        {
            _building = EStitcherSingleton.GetInstance.Building;
            ApaperLevel = EStitcherSingleton.GetInstance.Building.Levels[0];
        }

        #region State panel
        private void RemoveStatePanel()
        {
            _statePanelWatch = null;
            _statePanel.Dispose();
            this.Controls.Remove(_statePanel);
            _statePanel = null;
        }
        private void UpdateStateViewer()
        {
            if (_statePanel != null)
                RemoveStatePanel();
            #region state panel

            var text = "";
            var image = GetStateIcon(out text);

            _statePanel = new Panel();
            _statePanel.Parent = this;
            _statePanel.BackColor = Color.Gray;
            _statePanel.Size = new Size(316, 97);
            _statePanel.Location = new Point(0, 0);
            var control = new StateControl(image, text);
            control.Parent = _statePanel;
            control.Location = new Point(0, 0);
            control.Dock = DockStyle.Fill;
            #endregion
            _statePanelWatch = new Stopwatch();
            _statePanelWatch.Start();
        }
        private Image GetStateIcon(out string stateText)
        {
            if (_apaperMode == EpaperMode.Line)
            {
                stateText = "Line";
                return Resources.apaperLineIcon;
            }
            else if (_apaperMode == EpaperMode.Rectangle)
            {
                stateText = "Rectangle";
                return Resources.apaperRectangleIcon;
            }
            else if (_apaperMode == EpaperMode.Arc)
            {
                stateText = "Arc";
                return Resources.apaperArcIcon;
            }
            else if (_apaperMode == EpaperMode.Window)
            {
                stateText = "Window";
                return Resources.apapeWindowrIcon;
            }
            else if (_apaperMode == EpaperMode.Roof)
            {
                stateText = "Roof";
                return Resources.epaperRoofIcon;
            }
            else
            {
                stateText = "Door";
                return Resources.apaperDoorIcon;
            }
        }

        #endregion

        #endregion

        #region enums

        public enum EpaperMode
        {
            Line,
            Rectangle,
            Arc,
            Window,
            Door,
            Roof,
            Draw
        }
        public enum DrawTypeMode
        {
            Line,
            Rectangle,
            Window,
            Door,
            Dimension
        }
        #endregion

        #region Paint
        protected override void Draw(Graphics g)
        {
            g.SmoothingMode = SmoothingMode.AntiAlias;
            //DrawRoof(g);
            DrawBelowFloor(g);
            var alpha = 255;
            //if (_ChangingMode)
            //    alpha = 150;
            DrawGridLines(g, alpha);
            var mainColor = Color.FromArgb(alpha, Color.Black);

            if (ApaperLevel.FloorPlan.Walls != null && _apaperMode != EpaperMode.Roof)
            {
                //TestSlices?.DrawTo(g, Color.Blue, this, 2);
                ApaperLevel.FloorPlan.DrawPlan(g, this, 70, _building.Template);
                ApaperLevel.FloorPlan.Walls.Select(o => o.SingleWall2D).DrawTo(g, Color.Gray, this, 1);

                var windows = ApaperLevel.FloorPlan.Walls.Where(o => o.Openings != null).SelectMany(o => o.Openings.Where(e => e.OpeningType == OpeningType.Window)).ToList();
                foreach (var w in windows)
                {
                    var whitePen = new Pen(Color.White, ZoomToViewPort((w.SingleLine2D as AnalysisSlice).WallThickness));
                    w.SingleLine2D.DrawTo(g, whitePen, this, new DrawingOptions() { ForcePenColor = true });
                    w.ShownLines.DrawTo(g, Color.Blue, this, 2);
                }
                var doors = ApaperLevel.FloorPlan.Walls.Where(o => o.Openings != null).SelectMany(o => o.Openings.Where(e => e.OpeningType == OpeningType.Door)).ToList();
                foreach (var d in doors)
                {
                    var whitePen = new Pen(Color.White, ZoomToViewPort((d.SingleLine2D as AnalysisSlice).WallThickness));
                    d.SingleLine2D.DrawTo(g, whitePen, this, new DrawingOptions() { ForcePenColor = true });
                    d.ShownLines.DrawTo(g, Color.Orange, this, 2);
                }
            }
            if (_deSelectionSlice != null)
            {
                var deSelectedRect = _deSelectionSlice.GetBounds().ToPolygon2D().ToSlicePolygon();
                deSelectedRect.FillTo(g, new SolidBrush(Color.FromArgb(100, Color.Gray)), this);
                deSelectedRect.DrawPathTo(g, Pens.Gray, this);
            }
            #region dimensions
            //draw all dimensions
            foreach (var dim in _dimensions)
            {
                var shownText = Elibre.CAD.StitcherApi.Helpers.UnitsHelper.ShowLengthText(_building.Template, dim.Length);
                DrawDimension(g, dim.Slice, new Pen(Color.Gray, 2), shownText);
            }
            //draw detected Dimensions
            foreach (var dim in _detectedDimensionsOnWalls)
            {
                var shownText = Elibre.CAD.StitcherApi.Helpers.UnitsHelper.ShowLengthText(_building.Template, dim.Length);
                dim.Slice.DrawTo(g, new Pen(Color.Blue, (float)_defaultWallThickness), this, new DrawingOptions() { ForcePenColor = true });
                DrawTextOnLine(g, dim.Slice, shownText);
            }
            //draw dimension under mouse
            if (_dimensionUnderMouse != null)
            {
                var shownText = Elibre.CAD.StitcherApi.Helpers.UnitsHelper.ShowLengthText(_building.Template, _dimensionUnderMouse.Length);
                DrawDimension(g, _dimensionUnderMouse.Slice, new Pen(Color.Orange, 2), shownText);
            }
            #endregion
            switch (_apaperMode)
            {
                case EpaperMode.Draw:
                    {
                        switch (_drawTypeMode)
                        {
                            case DrawTypeMode.Line:
                                #region handle line
                                if (_drawSlice != null)
                                {
                                    var pen = new Pen(Color.Gray, 6);
                                    _drawSlice.DrawTo(g, pen, this, new DrawingOptions() { ForcePenColor = true });
                                    DrawDimensionForLine(g, _drawSlice);
                                }
                                #endregion
                                break;
                            case DrawTypeMode.Rectangle:
                                #region handle Rectangle
                                if (_drawSlice != null)
                                {
                                    var pen = new Pen(Color.Gray, 6);
                                    var bounds = _drawSlice.GetBounds();
                                    bounds.Draw(g, pen, this);
                                    DrawRectangleDimensions(g, bounds);
                                }
                                #endregion
                                break;
                            case DrawTypeMode.Dimension:
                                #region handle Dimension
                                if (_drawSlice != null)
                                {
                                    DrawDimension(g, _drawSlice, new Pen(Color.Gray, 2));
                                }
                                #endregion
                                break;
                            case DrawTypeMode.Window:
                                #region handle window
                                if (_drawSlice != null)
                                {
                                    var bluePen = new Pen(Color.Blue, 6);
                                    _drawSlice.DrawTo(g, bluePen, this, new DrawingOptions() { ForcePenColor = true });
                                    DrawDimensionForLine(g, _drawSlice);
                                }
                                #endregion
                                break;
                            case DrawTypeMode.Door:
                                #region handle door
                                if (_drawSlice != null)
                                {
                                    var orangePen = new Pen(Color.Orange, 6);
                                    _drawSlice.DrawTo(g, orangePen, this, new DrawingOptions() { ForcePenColor = true });
                                    DrawDimensionForLine(g, _drawSlice);
                                }
                                #endregion
                                break;
                        }
                        break;
                    }
                case EpaperMode.Line:
                    {
                        #region handle line
                        if (_drawSlice != null)
                        {
                            var pen = new Pen(Color.Gray, 6);
                            _drawSlice.DrawTo(g, pen, this, new DrawingOptions() { ForcePenColor = true });
                            DrawDimensionForLine(g, _drawSlice);
                        }

                        #endregion
                        break;
                    }
                case EpaperMode.Rectangle:
                    {
                        #region handle line
                        if (_drawSlice != null)
                        {
                            var pen = new Pen(Color.Gray, 6);
                            var bounds = _drawSlice.GetBounds();
                            bounds.Draw(g, pen, this);
                            DrawRectangleDimensions(g, bounds);
                        }
                        #endregion
                        break;
                    }
                case EpaperMode.Arc:
                    {
                        #region handle arc
                        if (_drawSlice != null)
                        {
                            var pen = new Pen(Color.Gray, 6);
                            _drawSlice.DrawTo(g, pen, this, new DrawingOptions() { ForcePenColor = true });
                            if (_drawSlice.IsCircleArc)
                                DrawDimensionForLine(g, _drawSlice);
                        }

                        #endregion
                        break;
                    }
                case EpaperMode.Window:
                    {
                        #region handle window
                        if (_drawSlice != null)
                        {
                            var bluePen = new Pen(Color.Blue, 6);
                            _drawSlice.DrawTo(g, bluePen, this, new DrawingOptions() { ForcePenColor = true });
                            DrawDimensionForLine(g, _drawSlice);
                        }

                        #endregion
                        break;
                    }
                case EpaperMode.Door:
                    {
                        #region handle door
                        if (_drawSlice != null)
                        {
                            var orangePen = new Pen(Color.Orange, 6);
                            _drawSlice.DrawTo(g, orangePen, this, new DrawingOptions() { ForcePenColor = true });
                            DrawDimensionForLine(g, _drawSlice);
                        }

                        #endregion
                        break;
                    }
                case EpaperMode.Roof:
                    {
                        #region handle roof
                        if (RoofSegments != null)
                        {
                            RoofSegments.SelectMany(o => o).Where(o => o.IsGable).Select(o => o.Segment).DrawTo(g, Color.Green, this, 2);
                            RoofSegments.SelectMany(o => o).Where(o => !o.IsGable).Select(o => o.Segment).DrawTo(g, Color.Black, this, 2);
                        }
                        if (_roofSliceUnderMouse != null)
                            _roofSliceUnderMouse.Segment.DrawTo(g, new Pen(Color.Orange, 3), this, new DrawingOptions() { ForcePenColor = true });
                        #endregion
                        break;
                    }

            }
            if (_snapperInfo != null && _deSelectionSlice == null /*&& !_ChangingMode */&& _snapperInfo.SnappingType != Point2DSnappingType.None && _apaperMode != EpaperMode.Roof)
            {
                //draw snap point
                var size = new Size(0, 0);
                var rect = new Rectangle(new Point((int)(ToViewPort(_snapperInfo.TargetPoint)).X, (int)ToViewPort(_snapperInfo.TargetPoint).Y), size);
                rect.Inflate(5, 5);
                g.DrawRectangle(Pens.Green, rect);

                var allWallsPoints = ApaperLevel.FloorPlan.Walls.SelectMany(o => new List<Point2D>() { o.SingleWall2D.Start, o.SingleWall2D.End }).Distinct().ToList();
                var hozPtIndex = allWallsPoints.FindIndex(o => o.Y.IsApproxEqualTo(_snapperInfo.TargetPoint.Y));
                var pen = new Pen(Color.Green, 2);
                pen.DashStyle = DashStyle.DashDot;
                if (hozPtIndex != -1)
                    g.DrawLine(pen, ToViewPort(_snapperInfo.TargetPoint), ToViewPort(allWallsPoints[hozPtIndex]));
                var vertPtIndex = allWallsPoints.FindIndex(o => o.X.IsApproxEqualTo(_snapperInfo.TargetPoint.X));
                if (vertPtIndex != -1)
                    g.DrawLine(pen, ToViewPort(_snapperInfo.TargetPoint), ToViewPort(allWallsPoints[vertPtIndex]));
            }
        }

        private void DrawRoof(Graphics g)
        {
            if (_building.RoofPolygons == null)
                return;
            //var levelIndex = _building.Levels.FindIndex(o => o == ApaperLevel);
            //if (levelIndex != 0)
            //    return;
            foreach (var poly in _building.RoofPolygons)
            {
                poly.SimplePolygon2D.Slices.DrawTo(g, Color.Gray, this, 2);
            }
        }

        private void DrawBelowFloor(Graphics g)
        {
            var levelIndex = _building.Levels.FindIndex(o => o == ApaperLevel);
            if (levelIndex == 0)
                return;
            var belowLevel = _building.Levels[levelIndex - 1];
            var pen = new Pen(Color.LightGray, 6);
            belowLevel.FloorPlan.Walls.Select(o => o.SingleWall2D).DrawTo(g, pen, this, new DrawingOptions() { ForcePenColor = true });
        }

        private void DrawRectangleDimensions(Graphics g, Rectangle2D rect)
        {
            var slice1 = new Slice2D(new Point2D(rect.Left, rect.Top - _distanceToDimension), new Point2D(rect.Right, rect.Top - _distanceToDimension));
            var slice2 = new Slice2D(new Point2D(rect.Left - _distanceToDimension, rect.Top), new Point2D(rect.Left - _distanceToDimension, rect.Bottom));
            DrawDimension(g, slice1, new Pen(Color.Gray, 1));
            DrawDimension(g, slice2, new Pen(Color.Gray, 1));
        }
        private void DrawDimensionForLine(Graphics g, Slice2D slice)
        {
            var dimensionLine = GetDimensionLine(slice);
            if (dimensionLine != null)
                DrawDimension(g, dimensionLine, new Pen(Color.Gray, 1));
        }
        private void DrawDimension(Graphics g, Slice2D dimensionLine, Pen arrowPen, string shownText = null)
        {
            arrowPen.StartCap = arrowPen.EndCap = LineCap.ArrowAnchor;
            arrowPen.CustomEndCap = new AdjustableArrowCap(4, 7, false);
            arrowPen.CustomStartCap = new AdjustableArrowCap(4, 7, false);

            dimensionLine.DrawTo(g, arrowPen, this, new DrawingOptions() { ForcePenColor = true });
            DrawTextOnLine(g, dimensionLine, shownText);

            //draw the 2 normal small lines
            var vec = (dimensionLine.End - dimensionLine.Start).GetUnit();
            var normVec = new Vector2D(-vec.Y, vec.X);
            var s_Start = new Slice2D(dimensionLine.Start + (normVec * 5), dimensionLine.Start - (normVec * 5));
            var s_End = new Slice2D(dimensionLine.End + (normVec * 5), dimensionLine.End - (normVec * 5));
            s_Start.DrawTo(g, Pens.LightGray, this);
            s_End.DrawTo(g, Pens.LightGray, this);

        }
        private void DrawTextOnLine(Graphics g, Slice2D line, string shownText = null)
        {
            var rect = ToViewPort(line.GetMidPoint()).GetRectangle(80, 120);
            rect.Y -= 10;
            if (shownText != null)
                g.CenterText(shownText, Font, Brushes.Black, rect, true);
            else
                g.CenterText(Elibre.CAD.StitcherApi.Helpers.UnitsHelper.ShowLengthText(_building.Template, line.Length), Font, Brushes.Black, rect, true);
        }
        private Slice2D GetDimensionLine(Slice2D slice)
        {
            if (slice.IsSegment)
            {
                var vec = slice.GetUnitVector().GetNormalVec() * _distanceToDimension;
                var s = slice.CreateCopy();
                s.Translate(vec);
                return s;
            }
            else if (slice.IsCircleArc)
            {
                return new Slice2D(slice.Center, slice.GetMidPoint());
            }
            return null;
        }
        private void DrawGridLines(Graphics g, int alpha)
        {
            var rect = ToWorld(ClientRectangle);
            var gridUnitToView = ZoomToViewPort(GridUnitLength);
            var hozLinesCount = (int)(rect.Height / gridUnitToView);
            var vertLinesCount = (int)(rect.Width / gridUnitToView);
            if (hozLinesCount > 3000 || vertLinesCount > 3000)
                return;
            var basePt = ToViewPort(0, 0);
            double startY = basePt.Y;
            double startX = basePt.X;
            if (startY > 0)
            {
                while (startY - (5 * gridUnitToView) > 0)
                    startY = startY - (5 * gridUnitToView);
            }
            else
            {
                while (startY + (5 * gridUnitToView) < 0)
                    startY = startY + (5 * gridUnitToView);
            }
            if (startX > 0)
            {
                while (startX - (5 * gridUnitToView) > 0)
                    startX = startX - (5 * gridUnitToView);
            }
            else
            {
                while (startX + (5 * gridUnitToView) < 0)
                    startX = startX + (5 * gridUnitToView);
            }

            var pen = new Pen(Color.FromArgb(alpha - 100, Color.LightSkyBlue), 1);
            var darkPen = new Pen(Color.FromArgb(alpha - 30, Color.DeepSkyBlue), 1);

            for (int i = 0; i < hozLinesCount + 50; i++)
            {
                var y = startY + (i * gridUnitToView);
                if ((i + 1) % 5 == 0)
                    g.DrawLine(darkPen, new Point(0, (int)y), new Point(ClientRectangle.Width, (int)y));
                else
                    g.DrawLine(pen, new Point(0, (int)y), new Point(ClientRectangle.Width, (int)y));
            }
            for (int i = 0; i < vertLinesCount + 50; i++)
            {
                var x = startX + (i * gridUnitToView);
                if ((i + 1) % 5 == 0)
                    g.DrawLine(darkPen, new Point((int)x, 0), new Point((int)x, ClientRectangle.Height));
                else
                    g.DrawLine(pen, new Point((int)x, 0), new Point((int)x, ClientRectangle.Height));
            }
        }
        #endregion

        #region Overrides
        protected override void UpdateCursor(Point location)
        {
            Cursor = GetMouseCursor(location);
        }
        public override void CenterDrawings()
        {
            if (ApaperLevel.FloorPlan.Walls.Count > 0)
                CenterBounds(ApaperLevel.FloorPlan.Walls.Select(o => o.SingleWall2D).GetBounds());
        }
        protected override Cursor GetMouseCursor(Point location)
        {
            if (_apaperMode == EpaperMode.Draw)
                return _cursorDict[_drawTypeMode];
            else
                return _roofCursor;
        }
        protected override bool HandleMouseMove(MouseEventArgs e)
        {
            var currentPt = ToWorld(e.Location.X, e.Location.Y);
            //if (_statePanel != null && _statePanelWatch != null && _statePanelWatch.ElapsedMilliseconds > 2000)
            //    RemoveStatePanel();
            //if (_ChangingMode)
            //    return true;

            //delete elements
            if (_deSelectionSlice != null)
            {
                _deSelectionSlice.End = currentPt;
                Invalidate();
                return true;
            }
            _dimensionUnderMouse = _dimensions.FirstOrDefault(o => o.Slice.Contains(currentPt, ZoomToWorld(7)));
            //change door direction
            if (_apaperMode != EpaperMode.Roof)
            {
                var allDoors = ApaperLevel.FloorPlan.Walls?.Where(o => o.Openings != null).SelectMany(o => o.Openings).ToList();
                var doorUnderMouse = allDoors?.FirstOrDefault(o => o.SingleLine2D.Contains(currentPt.GetProjection(o.SingleLine2D)) && o.SingleLine2D.Contains(currentPt, ZoomToViewPort(10)));
                if (doorUnderMouse != null)
                {
                    _currentSwappingOpening = doorUnderMouse;
                    if (_currentSwappingOpening.OpeningType == OpeningType.Door)
                        ShowDoorSwappingButtons(doorUnderMouse.SingleLine2D);
                    else if (_currentSwappingOpening.OpeningType == OpeningType.Window)
                        ShowWindowSwappingButtons(doorUnderMouse.SingleLine2D);
                    return true;
                }
                else
                {
                    HideDoorSwappingButtons();
                }
            }


            switch (_apaperMode)
            {
                case EpaperMode.Draw:
                    {
                        _snapperInfo = _snapper.Snap_LinesOnGrid_Lines_Grids(GridUnitLength, ApaperLevel.FloorPlan.Walls.Select(o => o.SingleWall2D), currentPt);
                        if (_drawSlice != null)
                        {
                            _drawSlice.End = _snapperInfo.TargetPoint;
                            var allWalls = ApaperLevel.FloorPlan.Walls;
                            var wallContainsLine = allWalls.FirstOrDefault(o => o.SingleWall2D.Contains(_drawSlice));
                            if (wallContainsLine != null)
                            {
                                if (wallContainsLine.IsInteriorWall)
                                    _drawTypeMode = DrawTypeMode.Door;
                                else
                                    _drawTypeMode = DrawTypeMode.Window;
                            }
                            else
                            {
                                if (_approvedLine_RectMode == DrawTypeMode.Rectangle && (_drawSlice.IsHorizontal() || _drawSlice.IsVertical()))
                                    _drawTypeMode = DrawTypeMode.Dimension;
                                //var rect = _drawSlice.GetBounds();
                                //if ((rect.Width.IsApproxGreaterThan(0) || rect.Height.IsApproxGreaterThan(0)) && rect.GetArea().IsApproxEqualTo(0))
                                //    _drawTypeMode = DrawTypeMode.Dimension;
                                else
                                    _drawTypeMode = _approvedLine_RectMode;
                            }
                        }
                        UpdateCursor(new Point());
                        break;
                    }
                case EpaperMode.Line:
                    {
                        #region handle line
                        _snapperInfo = _snapper.SnapToGrid(GridUnitLength, currentPt);
                        if (_drawSlice != null)
                            _drawSlice.End = _snapperInfo.TargetPoint;
                        #endregion
                        break;
                    }
                case EpaperMode.Rectangle:
                    {
                        #region handle line
                        _snapperInfo = _snapper.SnapToGrid(GridUnitLength, currentPt);
                        if (_drawSlice != null)
                            _drawSlice.End = _snapperInfo.TargetPoint;
                        #endregion
                        break;
                    }
                case EpaperMode.Arc:
                    {
                        #region handle arc
                        _snapperInfo = _snapper.SnapToGrid(GridUnitLength, currentPt);
                        if (_drawSlice != null)
                        {
                            if (!_drawSlice.IsCircleArc)
                                _drawSlice.End = _snapperInfo.TargetPoint;
                            else
                            {
                                if (!_snapperInfo.TargetPoint.IsApproxEqualTo(_drawSlice.GetStartPoint()) && !_snapperInfo.TargetPoint.IsApproxEqualTo(_drawSlice.GetEndPoint()))
                                {
                                    var arc = Arc2D.From3Points(_drawSlice.GetStartPoint(), _snapperInfo.TargetPoint, _drawSlice.GetEndPoint());
                                    if (arc.Radius != 0)
                                    {
                                        _drawSlice.CircleRadius = arc.Radius;
                                        _drawSlice.Center = arc.Center;
                                        _drawSlice.StartAngle = arc.StartAngle;
                                        _drawSlice.EndAngle = arc.EndAngle;
                                    }
                                }
                            }
                        }
                        #endregion
                        break;
                    }
                case EpaperMode.Window:
                    {
                        #region handle window
                        _snapperInfo = _snapper.Snap(ApaperLevel.FloorPlan.Walls.Select(o => o.SingleWall2D), e.Location);
                        if (_drawSlice != null)
                            HandleOpeningHover(currentPt);
                        #endregion
                        break;
                    }
                case EpaperMode.Door:
                    {
                        #region handle Door
                        //var allDoors = ApaperPlan.Walls.Where(o => o.Openings != null).SelectMany(o => o.Openings.Where(ee => ee.OpeningType == OpeningType.Door)).ToList();
                        //var doorUnderMouse = allDoors.FirstOrDefault(o => o.SingleLine2D.Contains(currentPt.GetProjection(o.SingleLine2D)) && o.SingleLine2D.Contains(currentPt, ZoomToViewPort(10)));
                        //if (doorUnderMouse != null)
                        //{
                        //    _currentSwappingDoor = doorUnderMouse;
                        //    ShowDoorSwappingButtons(doorUnderMouse.SingleLine2D);
                        //}
                        //else
                        {
                            //HideDoorSwappingButtons();
                            _snapperInfo = _snapper.Snap(ApaperLevel.FloorPlan.Walls.Select(o => o.SingleWall2D), e.Location);
                            if (_drawSlice != null)
                                HandleOpeningHover(currentPt);
                        }
                        #endregion
                        break;
                    }
                case EpaperMode.Roof:
                    {
                        #region handle roof
                        if (RoofSegments != null)
                        {
                            var allSlices = RoofSegments.SelectMany(o => o).ToList();
                            _roofSliceUnderMouse = allSlices.FirstOrDefault(o => o.Segment.Contains(currentPt, ZoomToWorld(10)));
                            if (_roofSliceUnderMouse == null)
                                _buttonGableRoof.Visible = false;
                            else
                            {
                                var location = ToViewPort(_roofSliceUnderMouse.Segment.GetMidPoint());
                                _buttonGableRoof.Visible = true;
                                _buttonGableRoof.Location = new Point((int)location.X, (int)location.Y);
                                UpdateRoofButtonImage();
                            }
                        }
                        #endregion
                        break;
                    }
            }
            Invalidate();
            return base.HandleMouseMove(e);
        }

        protected override bool HandleMouseDown(MouseEventArgs e)
        {
            if (HandlePanningMouseDown(e))
                return true;
            var currentPt = ToWorld(e.X, e.Y);
            if (e.Button == MouseButtons.Left)
            {
                if (_apaperMode == EpaperMode.Draw)
                {
                    if (_drawSlice == null)
                    {
                        _snapperInfo = _snapper.Snap_LinesOnGrid_Lines_Grids(GridUnitLength, ApaperLevel.FloorPlan.Walls.Select(o => o.SingleWall2D), currentPt);
                        _drawSlice = new Slice2D(_snapperInfo.TargetPoint, _snapperInfo.TargetPoint);
                        _drawTypeMode = DrawTypeMode.Rectangle;
                        _approvedLine_RectMode = DrawTypeMode.Rectangle;
                        UpdateCursor(new Point());
                    }
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                if (_deSelectionSlice == null)
                    _deSelectionSlice = new Slice2D(currentPt, currentPt);
            }

            Dragging.Begin(e.Location);
            return base.HandleMouseDown(e);
        }
        protected override bool HandleMouseUp(MouseEventArgs e)
        {
            var currentPt = ToWorld(e.Location.X, e.Location.Y);
            if (e.Button == MouseButtons.Left)
            {
                //if (_ChangingMode)
                //{
                //    _ChangingMode = false;
                //    PauseViewer = false;
                //    return true;
                //}
                switch (_apaperMode)
                {
                    case EpaperMode.Draw:
                        {
                            _snapperInfo = _snapper.Snap_LinesOnGrid_Lines_Grids(GridUnitLength, ApaperLevel.FloorPlan.Walls.Select(o => o.SingleWall2D), currentPt);
                            if (_drawSlice != null)
                            {
                                if (Dragging.IsClick())
                                {
                                    #region Handle fast click
                                    if (_dimensionUnderMouse != null)
                                    {
                                        var dlg = new Elibre.CAD.StitcherApi.Dialogs.DimensionFixing.SetLengthDialog(this, _dimensionUnderMouse.Length, _building.Template, true);
                                        if (dlg.ShowDialog() == DialogResult.OK)
                                        {
                                            _dimensionUnderMouse.Length = dlg.NewLength;
                                            UpdateDimensions();
                                        }
                                        _drawSlice = null;
                                        _drawTypeMode = DrawTypeMode.Rectangle;
                                    }
                                    else if (_drawTypeMode == DrawTypeMode.Rectangle && _approvedLine_RectMode != DrawTypeMode.Line)
                                    {
                                        _drawTypeMode = DrawTypeMode.Line;
                                        _approvedLine_RectMode = DrawTypeMode.Line;
                                        UpdateCursor(new Point());
                                    }
                                    else
                                    {
                                        if (_drawTypeMode == DrawTypeMode.Line)
                                        {
                                            //add line to walls
                                            AddWalls_UpdateWalls(ApaperLevel.FloorPlan, new List<Slice2D>() { _drawSlice });
                                            _drawSlice.Start = _snapperInfo.TargetPoint;
                                            _drawSlice.End = _snapperInfo.TargetPoint;
                                            OnFloorChanged?.Invoke(null, e);
                                        }
                                        else if (_drawTypeMode == DrawTypeMode.Door)
                                        {
                                            HandleOpeningClick(currentPt, OpeningType.Door);
                                            _drawTypeMode = DrawTypeMode.Rectangle;
                                        }
                                        else if (_drawTypeMode == DrawTypeMode.Window)
                                        {
                                            HandleOpeningClick(currentPt, OpeningType.Window);
                                            _drawTypeMode = DrawTypeMode.Rectangle;
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region When click after dragging

                                    if (_drawTypeMode == DrawTypeMode.Rectangle)
                                    {
                                        var rect = _drawSlice.GetBounds();
                                        var newSlices = rect.ToSlices();
                                        AddWalls_UpdateWalls(ApaperLevel.FloorPlan, newSlices);
                                        OnFloorChanged?.Invoke(null, e);
                                        _drawSlice = null;
                                    }
                                    else if (_drawTypeMode == DrawTypeMode.Dimension)
                                    {
                                        var dlg = new Elibre.CAD.StitcherApi.Dialogs.DimensionFixing.SetLengthDialog(this, _drawSlice.Length, _building.Template, true);
                                        if (dlg.ShowDialog() == DialogResult.OK)
                                        {
                                            _dimensions.Add(new Dimension(_drawSlice.CreateCopy(), dlg.NewLength));
                                            UpdateDimensions();
                                        }
                                        _drawSlice = null;
                                        _drawTypeMode = DrawTypeMode.Rectangle;
                                    }
                                    else if (_drawTypeMode == DrawTypeMode.Door)
                                    {
                                        HandleOpeningClick(currentPt, OpeningType.Door);
                                        _drawTypeMode = DrawTypeMode.Rectangle;
                                    }
                                    else if (_drawTypeMode == DrawTypeMode.Window)
                                    {
                                        HandleOpeningClick(currentPt, OpeningType.Window);
                                        _drawTypeMode = DrawTypeMode.Rectangle;
                                    }
                                    #endregion
                                }

                            }
                            break;
                        }
                    case EpaperMode.Line:
                        {
                            #region handle line
                            _snapperInfo = _snapper.SnapToGrid(GridUnitLength, currentPt);
                            if (_drawSlice == null)
                                _drawSlice = new Slice2D(_snapperInfo.TargetPoint, _snapperInfo.TargetPoint);
                            else
                            {
                                AddWalls_UpdateWalls(ApaperLevel.FloorPlan, new List<Slice2D>() { _drawSlice });
                                _drawSlice.Start = _snapperInfo.TargetPoint;
                                _drawSlice.End = _snapperInfo.TargetPoint;
                                OnFloorChanged?.Invoke(null, e);
                            }
                            #endregion
                            break;
                        }
                    case EpaperMode.Rectangle:
                        {
                            #region handle rectangle
                            _snapperInfo = _snapper.SnapToGrid(GridUnitLength, currentPt);
                            if (_drawSlice == null)
                                _drawSlice = new Slice2D(_snapperInfo.TargetPoint, _snapperInfo.TargetPoint);
                            else
                            {
                                var newSlices = _drawSlice.GetBounds().ToSlices();
                                AddWalls_UpdateWalls(ApaperLevel.FloorPlan, newSlices);
                                OnFloorChanged?.Invoke(null, e);
                                _drawSlice = null;
                            }
                            #endregion
                            break;
                        }
                    case EpaperMode.Arc:
                        {
                            #region handle arc
                            _snapperInfo = _snapper.SnapToGrid(GridUnitLength, currentPt);
                            if (_drawSlice == null)
                                _drawSlice = new Slice2D(_snapperInfo.TargetPoint, _snapperInfo.TargetPoint);
                            else
                            {
                                if (!_drawSlice.IsCircleArc)
                                {
                                    var arc = Arc2D.From3Points(_drawSlice.GetStartPoint(), _drawSlice.GetMidPoint() + _drawSlice.GetUnitVector().GetNormalVec(), _drawSlice.GetEndPoint());
                                    _drawSlice = new Slice2D(arc.Center, arc.Radius, arc.StartAngle, arc.EndAngle);
                                }
                                else
                                {
                                    AddWalls_UpdateWalls(ApaperLevel.FloorPlan, new List<Slice2D>() { _drawSlice });
                                    OnFloorChanged?.Invoke(null, e);
                                    _drawSlice = null;
                                }
                            }
                            #endregion
                            break;
                        }
                    case EpaperMode.Window:
                        {
                            #region handle window
                            _snapperInfo = _snapper.Snap(ApaperLevel.FloorPlan.Walls.Select(o => o.SingleWall2D), e.Location);
                            HandleOpeningClick(currentPt, OpeningType.Window);

                            #endregion
                            break;
                        }
                    case EpaperMode.Door:
                        {
                            #region handle door
                            _snapperInfo = _snapper.Snap(ApaperLevel.FloorPlan.Walls.Select(o => o.SingleWall2D), e.Location);
                            HandleOpeningClick(currentPt, OpeningType.Door);

                            #endregion
                            break;
                        }
                    case EpaperMode.Roof:
                        {
                            #region handle Roof
                            if (_roofSliceUnderMouse != null)
                            {
                                if (_applyToRoof)
                                {
                                    _roofSliceUnderMouse.BearingHeight = _appliedRoofDesginRules.BearingHeight;
                                    _roofSliceUnderMouse.HeelHeight = _appliedRoofDesginRules.HeelHeight;
                                    _roofSliceUnderMouse.SlopeDegree = _appliedRoofDesginRules.Pitch;
                                    _roofSliceUnderMouse.Overhang = _appliedRoofDesginRules.Overhang;
                                    StitcherRoofGenerator.UpdateRoof(RoofSegments,_building);
                                }
                                else
                                {
                                    var poly = _building.RoofPolygons.FirstOrDefault(o => o.SimplePolygon2D.ToPolygon2D().Contains(_roofSliceUnderMouse.Segment.GetMidPoint()));
                                    if (poly != null)
                                    {
                                        var dlg = new RoofDesignRulesFinal(this, _roofSliceUnderMouse.SlopeDegree, _roofSliceUnderMouse.Overhang, _roofSliceUnderMouse.HeelHeight, _roofSliceUnderMouse.BearingHeight, poly, _building);
                                        if (dlg.ShowDialog() == DialogResult.OK)
                                        {
                                            if (dlg.ApplyToMode)
                                            {
                                                _applyToRoof = true;
                                                _appliedRoofDesginRules = new RoofDesignRulesOptions(dlg.RoofPitch, dlg.OverHang, dlg.BearingHeight, dlg.HeelHeight);
                                            }
                                            else
                                            {
                                                _roofSliceUnderMouse.BearingHeight = dlg.BearingHeight;
                                                _roofSliceUnderMouse.HeelHeight = dlg.HeelHeight;
                                                _roofSliceUnderMouse.SlopeDegree = dlg.RoofPitch;
                                                _roofSliceUnderMouse.Overhang = dlg.OverHang;
                                                StitcherRoofGenerator.UpdateRoof(RoofSegments, _building);
                                            }
                                        }
                                    }
                                }

                            }
                            #endregion
                            break;
                        }
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                if (_drawSlice != null)
                    _drawSlice = null;
                else if (Dragging.IsDragging && !Dragging.IsClick() && _deSelectionSlice != null)
                {
                    var bounds = _deSelectionSlice.GetBounds();
                    _dimensions.RemoveAll(o => bounds.IntersectsWith(o.Slice.GetBounds()));
                    if (RemoveWallsByDeselectedSlice())
                    {
                        OnFloorChanged?.Invoke(null, e);
                        //invoke Undo/redo here
                    }
                    UpdateDimensions();
                    _deSelectionSlice = null;
                }
                //else if (!_ChangingMode)
                //{
                //    if (_applyToRoof)
                //        _applyToRoof = false;
                //    else
                //    {
                //        _ChangingMode = true;
                //        PauseViewer = true;
                //    }
                //}
                //else if (_ChangingMode)
                //{
                //    _ChangingMode = false;
                //    PauseViewer = false;
                //}
                _deSelectionSlice = null;
            }

            this.Dragging.Stop();
            return base.HandleMouseUp(e);
        }

        #endregion

        #region Event Handlers

        private void button_SwapHorizontal_Click(object sender, EventArgs e)
        {
            if (_currentSwappingOpening.SingleLine2D.IsHorizontal())
                _currentSwappingOpening.SingleLine2D.ReverseStartEnd();
            else
                _currentSwappingOpening.ExteriorDirection = -_currentSwappingOpening.ExteriorDirection;
            _currentSwappingOpening.UpdateShownLines();
            Invalidate();
        }

        private void button_SwapVertical_Click(object sender, EventArgs e)
        {
            if (_currentSwappingOpening.SingleLine2D.IsVertical())
                _currentSwappingOpening.SingleLine2D.ReverseStartEnd();
            else
                _currentSwappingOpening.ExteriorDirection = -_currentSwappingOpening.ExteriorDirection;
            _currentSwappingOpening.UpdateShownLines();
            Invalidate();
        }
        private void button_GableRoof_Click(object sender, EventArgs e)
        {
            UpdateRoofButtonImage();
            _roofSliceUnderMouse.IsGable = !_roofSliceUnderMouse.IsGable;
            StitcherRoofGenerator.UpdateRoof(RoofSegments, _building);
            Invalidate();
            Focus();
        }
        private void button_SwapDoorsWindows_Click(object sender, EventArgs e)
        {
            if (_currentSwappingOpening.OpeningType == OpeningType.Door)
            {
                _currentSwappingOpening.OpeningType = OpeningType.Window;
                _buttonSwapDoorHorizontally.Visible = false;
                _buttonSwapDoorVertically.Visible = false;
            }
            else if (_currentSwappingOpening.OpeningType == OpeningType.Window)
            {
                _currentSwappingOpening.OpeningType = OpeningType.Door;
                _buttonSwapDoorHorizontally.Visible = true;
                _buttonSwapDoorVertically.Visible = true;
            }
            _currentSwappingOpening.RevitType = _currentSwappingOpening.OpeningType == OpeningType.Window ? _building.Template.WindowSymbols.OrderBy(o => Math.Abs(o.Width - _currentSwappingOpening.SingleLine2D.Length)).ElementAt(0).SymbolName : _building.Template.DoorSymbols.OrderBy(o => Math.Abs(o.Width - _currentSwappingOpening.SingleLine2D.Length)).ElementAt(0).SymbolName;
            _currentSwappingOpening.UpdateHeightByTemplate(_building.Template);
            if (_currentSwappingOpening.OpeningType == OpeningType.Window)
                _currentSwappingOpening.SillHeight = _currentSwappingOpening.Height + 20;

            _currentSwappingOpening.UpdateShownLines();
            Invalidate();
        }

        #endregion

    }
}
