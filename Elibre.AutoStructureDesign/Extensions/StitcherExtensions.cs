﻿
using Elibre.CAD.StitcherApi.Models.Stitching;
using Elibre.Geometry2D;
using Elibre.Stitcher.Models.Stitching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.Extensions
{
    public static class StitcherExtensions
    {
        public static void FillWallsParts(this StitcherBuilding building)
        {
            var shearSlices = building.Levels.SelectMany(o => o.FloorPlan.Walls.Where(e => e.WallParts != null).SelectMany(e => e.WallParts)).Where(o => o.IsShear).Select(o => o.CenterLine).ToList();
            foreach (var level in building.Levels)
            {
                foreach (var wall in level.FloorPlan.Walls)
                {
                    var slices = wall.Openings.Select(o => o.SingleLine2D.CreateCopy()).ToList();
                    slices.Add(wall.SingleWall2D.CreateCopy());
                    slices.Slice();
                    slices = slices.OrderBy(o => o.GetMidPoint().DistanceTo(wall.SingleWall2D.Start)).ToList();
                    wall.WallParts = slices.Select(o => new StitcherWallPart(o)).ToList();
                    foreach (var part in wall.WallParts)
                    {
                        if (wall.Openings.Any(e => e.SingleLine2D.IsApproxStartEnd(part.CenterLine)))
                            part.CanBeShear = false;
                        else
                            part.CanBeShear = true;

                        if (shearSlices.Any(o => o.IsApproxStartEnd(part.CenterLine)))
                            part.IsShear = true;
                    }
                }
            }
        }

    }
}
