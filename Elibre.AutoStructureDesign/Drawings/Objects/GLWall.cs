﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.Drawings.Floor
{
    public class GLWall
    {
        private List<GLWallOpening> openings = new List<GLWallOpening>();
        private List<GLWallSegment> segments = new List<GLWallSegment>();
        private bool opening_pointer;

        public bool Opening_pointer
        {
            get { return opening_pointer; }
            set { opening_pointer = value; }
        }


        public GLWall(StraightWall eframerWall,double pointerlength, bool op=false)
        {
            opening_pointer = op;
            foreach (var segment in eframerWall.SegmentList)
            {
                if (segment is StraightWallOpening)
                {
                    GLWallOpening opening = new GLWallOpening((StraightWallOpening)segment,pointerlength, opening_pointer);
                    openings.Add(opening);
                }
                else if (segment is StraightWallSegment)
                {
                    GLWallSegment wallSegment = new GLWallSegment((StraightWallSegment)segment,pointerlength);
                    segments.Add(wallSegment);
                }
            }
        }
        public void Render(int programID)
        {
            foreach (var segment in segments)
            {
                segment.Render(programID);
            }
            foreach (var segment in openings)
            {
                segment.Render(programID);
            }
        }
        public void RenderGraphics(int programID)
        {
            foreach (var opening in openings)
            {
                opening.RenderGraphics(programID);
            }
            foreach (var wall in segments)
            {
                wall.RenderGraphics(programID);
            }
        }
        public void Fill(int programID)
        {
            foreach (var wall in segments)
            {
                wall.Fill(programID);
            }
            foreach (var opening in openings)
            {
                opening.FillArrow(programID);
            }
        }
        public List<Slice2D> getSlices()
        {
             List<GLWallSegment> ext =  segments.Where(o => !o.Interior).ToList();
            return ext.SelectMany(o =>o.getSlices()).ToList();
        }

        public List<GLWallOpening> GetWallOpenings()
        {
            return openings;
        }
        public List<GLWallSegment> GetWallSegments()
        {
            return segments;
        }
        public void Dispose()
        {
            foreach (var segment in segments)
            {
                segment.Dispose();
            }
            foreach (var segment in openings)
            {
                segment.Dispose();
            }
        }
    }
}
