﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Drawings.DrawingClasses;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.Geometry2D;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Elibre.AutoStructureDesign.Drawings.Floor
{

    public class GLFloor
    {
        public enum FloorZOrders
        {
            HATCH, WALL, BEAM, POINTER, TEXT
        }
        private List<GLWall> walls;
        private List<GLPost> posts;
        private List<GLBeam> beams;
        private List<GLJoistArea> joistAreas;
        private List<GLBeamHanger> hangers;
        private double pointerlength;
        List<GLGraphicsObject> texts;
        public GLFloor(EFraming.Objects.Floor floor)
        {
            pointerlength = GetFloorWidthOrHeight(floor);
            walls = new List<GLWall>();
            posts = new List<GLPost>();
            beams = new List<GLBeam>();
            joistAreas = new List<GLJoistArea>();
            hangers = new List<GLBeamHanger>();
            texts = new List<GLGraphicsObject>();
            foreach (var beam in floor.ObjectList.OfType<Beam>())
            {
                GLBeam b = new GLBeam(beam);
                beams.Add(b);
                texts.Add(b.GetGraphicObject());
                foreach (var hanger in beam.HangerList)
                {
                    hangers.Add(new GLBeamHanger(hanger));
                }
            }
            foreach (var joistarea in floor.ObjectList.OfType<JoistArea>())
            {
                GLJoistArea j = new GLJoistArea(joistarea);
                while (j.GetGraphicObject().IsIntersects(texts))
                {
                    TranslateGraphicObject(j.GetGraphicObject(), j);
                }
                texts.Add(j.GetGraphicObject());
                joistAreas.Add(j);
            }
            foreach (var post in floor.ObjectList.OfType<Post>())
            {

                GLPost p = new GLPost(post,pointerlength/30, true);
                while (p.GetPointingPattern().IsPointerIntersects(texts))
                {
                    p.GetPointingPattern().FixCollision(PointingPattern.CollisionFixMethod.ROTATE, Math.PI / 2);
                }
                posts.Add(p);
                texts.Add(p.GetPointingPattern().GetGraphicObject());
            }
            foreach (var eframerWall in floor.ObjectList.OfType<StraightWall>())
            {
                GLWall wall = new GLWall(eframerWall, pointerlength / 30, true);
                List<GLWallOpening> ops = wall.GetWallOpenings();
                List<GLWallSegment> segs = wall.GetWallSegments();

                for (int i = 0; i < ops.Count; i++)
                {
                    while (ops[i].GetPointingPattern().IsPointerIntersects(texts))
                    {
                        ops[i].GetPointingPattern().FixCollision(PointingPattern.CollisionFixMethod.MOVE, pointerlength / 85);
                    }
                    texts.Add(ops[i].GetPointingPattern().GetGraphicObject());
                }
                for (int i = 0; i < segs.Count; i++)
                {
                    List<PointingPattern> pps = segs[i].GetPointingPattern();
                    if (pps == null)
                    {
                        continue;
                    }
                    for (int j = 0; j < pps.Count; j++)
                    {
                        while (pps[j].IsPointerIntersects(texts))
                        {
                            pps[j].FixCollision(PointingPattern.CollisionFixMethod.MOVE, pointerlength / 85);
                        }
                        texts.Add(pps[j].GetGraphicObject());
                    }
                }

                walls.Add(wall);
            }
        }

        public void Render(int programID)
        {
            foreach (var wall in walls)
            {
                wall.Render(programID);
            }
            foreach (var post in posts)
            {
                post.Render(programID);
            }
            foreach (var beam in beams)
            {
                beam.Render(programID);
            }
            foreach (var joist in joistAreas)
            {
                joist.Render(programID);
            }
            foreach (var hanger in hangers)
            {
                hanger.Render(programID);
            }
        }

        public void RenderGraphics(int programID)
        {
            foreach (var post in posts)
            {
                post.RenderGraphics(programID);
            }
            foreach (var joist in joistAreas)
            {
                joist.RenderGraphics(programID);
            }
            foreach (var wall in walls)
            {
                wall.RenderGraphics(programID);
            }
            foreach (var beam in beams)
            {
                beam.RenderGraphics(programID);
            }
        }
        public void Fill(int progarmID)
        {
            foreach (var wall in walls)
            {
                wall.Fill(progarmID);
            }
            foreach (var post in posts)
            {
                post.FillArrow(progarmID);
            }
        }
        public Geometry2D.Rectangle2D getBounds()
        {
            var slices = walls.SelectMany(o => o.getSlices()).ToList();
            return slices.GetBounds();
        }
        private double GetFloorWidthOrHeight(EFraming.Objects.Floor floor)
        {
            List<WallSegment> segs = floor.ObjectList.OfType<StraightWall>().SelectMany(o => o.SegmentList).ToList();

            if (segs.Count == 0)
            {
                return 0;
            }
            double min_X = segs.Min(o => o.StartPoint.x);
            double max_X = segs.Max(o => o.EndPoint.x);
            double min_Y = segs.Max(o => o.StartPoint.y);
            double max_Y = segs.Min(o => o.EndPoint.y);

            double width = max_X - min_X;
            double height = Math.Abs(max_Y - min_Y);

            if (width > height)
            {
                return width;
            }
            else
            {
                return height;
            }
        }
        public void Dispose()
        {
            foreach (var wall in walls)
            {
                wall.Dispose();
            }
            foreach (var post in posts)
            {
                post.Dispose();
            }
        }
        private void TranslateGraphicObject(GLGraphicsObject text, GLJoistArea joistarea)
        {
            List<Vector2> copy = new List<Vector2>(text.Positions);
            for (int i = 0; i < copy.Count; i++)
            {
                var v = copy[i];
                Vector3 v3 = new Vector3(v.X, v.Y, 0);
                v3 = Vector3.Transform(v3, text.ModelMatrix);
                copy[i] = new Vector2(v3.X, v3.Y);
            }
            Vector2 translate = new Vector2(copy[2].X - copy[0].X, copy[2].Y - copy[0].Y);
            translate.Normalize();

            text.Translate(translate * (float)(joistarea.Main_slice.Length / 85));

        }

    }
}
