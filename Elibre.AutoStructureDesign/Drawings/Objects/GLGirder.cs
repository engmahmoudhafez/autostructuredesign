﻿using Elibre.AutoStructureDesign.Drawings.BaseClass;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.CAD.StitcherApi.Algorithms.RoofAlgorithms;
using Elibre.Geometry2D;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Elibre.AutoStructureDesign.Drawings
{
    public class GLGirder:GLBaseObject
    {
        private GLGraphicsObject girderText;
        private GLFillableObject girdersFill;

        public GLGirder(Truss girder)
        {
            slices = girder.outer.Slices;
            SlicePolygon2D polygon = girder.outer;
            Slice2D girder_center = girder.SingleLine;

            drawableSegment = new GLDrawableObject(slices);
            girdersFill = new GLFillableObject(new List<SlicePolygon2D> { polygon });
            girdersFill.FillColor = new Vector3(Color.Gray.R / 255.0f, Color.Gray.G / 255.0f, Color.Gray.B / 255.0f);
            drawableSegment.Thickness = 2;

            girderText = new GLGraphicsObject("GIRDER TRUSS", new Font("Arial", 12, FontStyle.Regular), Color.Black);
            girderText.Scale(0.2f);
            GLUtils.AlignTextToSlice(girderText, girder_center,0.3f, GLUtils.GLGraphicsObjectVPosition.UP,GLUtils.GLGraphicsObjectHPosition.MIDDLE);
        }
        public void RenderFill(int programID)
        {
            girdersFill.Render(programID);
        }
        public void RenderGraphics(int programID)
        {
            girderText.Render(programID);
        }

        public GLGraphicsObject GetGraphicObject()
        {
            return girderText;
        }

        public override void Dispose()
        {
            base.Dispose();
            girdersFill.Dispose();
            girderText.Dispose();
        }
    }
}
