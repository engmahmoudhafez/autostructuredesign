﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Drawings.BaseClass;
using Elibre.AutoStructureDesign.Drawings.Floor;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.Geometry2D;
using System.Collections.Generic;

namespace Elibre.AutoStructureDesign.Drawings.DrawingClasses
{
    public class GLPost:GLBaseObject
    {
        string Width;
        string Depth;

        private float wallSlope = 0;

        private PointingPattern pointer;
      

        private bool _pointer;
        public bool _Pointer
        {
            get { return _pointer; }
            set { _pointer = value; }
        }

        public GLPost(Post post,double pointerlength,bool p = false)
        {
            _pointer = p;
            slices = new List<Slice2D>();
            var points = post.Geom2D.VertexList;

            slices.Add(new Slice2D(new Geometry2D.Point2D(points[0].x * 12, points[0].y * 12), new Geometry2D.Point2D(points[1].x * 12, points[1].y * 12)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[1].x * 12, points[1].y * 12), new Geometry2D.Point2D(points[2].x * 12, points[2].y * 12)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[2].x * 12, points[2].y * 12), new Geometry2D.Point2D(points[3].x * 12, points[3].y * 12)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[3].x * 12, points[3].y * 12), new Geometry2D.Point2D(points[0].x * 12, points[0].y * 12)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[0].x * 12, points[0].y * 12), new Geometry2D.Point2D(points[2].x * 12, points[2].y * 12)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[1].x * 12, points[1].y * 12), new Geometry2D.Point2D(points[3].x * 12, points[3].y * 12)));

            Width = post.Section.WidthString;
            Depth = post.Section.DepthString;
            Slice2D postWall = null;
            if (post.ParentWall != null)
            {
                var points_ = post.ParentWall.Geom2D.VertexList;

                postWall = new Slice2D(new Geometry2D.Point2D(points[0].x * 12, points[0].y * 12), new Geometry2D.Point2D(points[1].x * 12, points[1].y * 12));
                if (postWall.IsVertical())
                {
                    if (postWall.Start.Y > postWall.End.Y)
                    {
                        postWall.FlipDirection();
                    }
                }
                else
                {
                    if (postWall.Start.X > postWall.End.X)
                    {
                        postWall.FlipDirection();
                    }
                }
                wallSlope = (float)(postWall.End - postWall.Start).GetAngle();
            }

            drawableSegment = new GLDrawableObject(slices);

            if (_pointer)
            {
                Geometry2D.Point2D center = slices[5].Intersects(slices[4])[0];
                pointer = new PointingPattern(center, pointerlength*12, (float)MathUtil.ToRadian(45), Width + " x " + Depth, wallSlope, 0.07f);
            }
        }
        public override void Render(int programID)
        {
            base.Render(programID);
            pointer?.Render(programID);
        }
        public void FillArrow(int programID)
        {
            pointer?.Fill(programID);
        }
        public void RenderGraphics(int programID)
        {
            pointer?.RenderGraphics(programID);
        }
        public override void Dispose()
        {
            base.Dispose();
            pointer?.Dispose();
        }
        
        public PointingPattern GetPointingPattern()
        {
            return pointer;
        }
    }
}
