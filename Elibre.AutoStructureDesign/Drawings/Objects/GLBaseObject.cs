﻿using Elibre.AutoStructureDesign.Drawings.Floor;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.Drawings.BaseClass
{
    public abstract class GLBaseObject
    {
        protected GLDrawableObject drawableSegment;

        protected List<Slice2D> slices;
        public virtual void Render(int programID)
        {
            drawableSegment.Z_Order = (float)((int)GLFloor.FloorZOrders.WALL);
            drawableSegment.Render(programID);
        }
        public List<Slice2D> getSlices()
        {
            return slices;
        }
        public virtual void Dispose()
        {
            drawableSegment.Dispose();

        }
    }
}
