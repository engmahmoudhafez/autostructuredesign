﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Drawings.BaseClass;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.Geometry2D;
using Elibre.Net.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Elibre.AutoStructureDesign.Drawings.Floor
{
    public class GLJoistArea : GLBaseObject
    {
        SegmentedPolygon Polygon2D;
        string width;
        string depth;
        double spacing;
        Angle angle;
        GLGraphicsObject text;
        private Slice2D main_slice;

        public Slice2D Main_slice
        {
            get { return main_slice; }
            set { main_slice = value; }
        }


        public GLJoistArea(JoistArea joistArea)
        {
            spacing = joistArea.JoistsSpacing;
            width = joistArea.JoistList[0].Section.WidthString;
            depth = joistArea.JoistList[0].Section.DepthString;

            Polygon2D = joistArea.Polygon;
            angle = joistArea.JoistAngle;

            slices = JoistAreaSlices();

            text = new GLGraphicsObject(width + "x" + depth + "@" + spacing.ToString() + " " + "O.C", new Font("Arial", 12, FontStyle.Regular), Color.Black);
            text.Scale(0.25f);
            GLUtils.AlignTextToSlice(text, slices[0], 0.2f, GLUtils.GLGraphicsObjectVPosition.UP, GLUtils.GLGraphicsObjectHPosition.MIDDLE);

            drawableSegment = new GLDrawableObject(slices);
            text.Tag = this;
            main_slice = slices[0];

        }
        public void RenderGraphics(int programID)
        {
            text.Render(programID);
        }
        private List<Slice2D> JoistAreaSlices()
        {
            List<Slice2D> slices = new List<Slice2D>();

            double angle_ = 180 - (90 + angle.Value);

            angle_ = MathUtil.ToRadian(angle_);
            double trans = MathUtil.ToRadian(angle.Value);

            var points = Polygon2D.GetVertexList();

            for (int i = 0; i < points.Count - 1; i++)
            {

                slices.Add(new Slice2D(new Geometry2D.Point2D(points[i].x * 12, points[i].y * 12), new Geometry2D.Point2D(points[i + 1].x * 12, points[i + 1].y * 12)));
            }
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[0].x * 12, points[0].y * 12), new Geometry2D.Point2D(points[points.Count - 1].x * 12, points[points.Count - 1].y * 12)));
            Slice2D slice = slices.FirstOrDefault(o => Math.Atan(o.GetSlope()).IsApproxEqualTo(angle_, 0.1));
            double trans_amount = slices.FirstOrDefault(o => Math.Atan(o.GetSlope()).IsApproxEqualTo(trans, 0.1)).Length;

            double length = slice.Length / 1.15;

            double midpoin_x = Polygon2D.Centroid.x * 12;
            double midpoin_y = Polygon2D.Centroid.y * 12;

            double help = length * Math.Cos(angle_);
            double help2 = length * Math.Sin(angle_);

            double first_x = midpoin_x - (help / 2);
            double second_x = midpoin_x + (help / 2);

            double first_y = midpoin_y - (help2 / 2);
            double second_y = midpoin_y + (help2 / 2);

            Slice2D pattern = new Slice2D(new Geometry2D.Point2D(first_x, first_y), new Geometry2D.Point2D(second_x, second_y));
            double small_length = pattern.Length * 0.05;

            double X_1 = pattern.Start.X + small_length * Math.Cos(Math.PI / 4);
            double X_2 = pattern.End.X - small_length * Math.Cos(Math.PI / 4);
            double Y_1 = pattern.Start.Y + small_length * Math.Sin(Math.PI / 4);
            double Y_2 = pattern.End.Y - small_length * Math.Sin(Math.PI / 4);

            Slice2D one = new Slice2D(pattern.Start, new Geometry2D.Point2D(X_1, Y_1));
            Slice2D two = new Slice2D(pattern.End, new Geometry2D.Point2D(X_2, Y_2));
            List<Slice2D> result = new List<Slice2D>();

            result.Add(pattern);
            result.Add(one);
            result.Add(two);

            return result;
        }
        public void Update()
        {
            GLUtils.AlignTextToSlice(text, slices[0], 0.2f, GLUtils.GLGraphicsObjectVPosition.UP, GLUtils.GLGraphicsObjectHPosition.MIDDLE);

            drawableSegment = new GLDrawableObject(slices);
        }

        public GLGraphicsObject GetGraphicObject()
        {
            return text;
        }

    }


}
