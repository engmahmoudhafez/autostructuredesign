﻿using Elibre.AutoStructureDesign.Drawings.DrawingFrame;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.Geometry2D;
using OpenTK;
using System.Drawing;

namespace Elibre.AutoStructureDesign.Drawings.DrawingClasses
{
    public class GLBitmapDrawing
    {
        private GLGraphicsObject graphics;
        private GLFrame frame;
        private string sheet_name;

        public string Sheet_name
        {
            get { return sheet_name; }
            set { sheet_name = value; }
        }

        private Matrix4 matrix = Matrix4.Identity;

        public GLBitmapDrawing(Bitmap image,string name)
        {
            sheet_name = name;
            graphics = new GLGraphicsObject(image);

            SizeF size = graphics.getSize();
            Rectangle bounds = new Rectangle(0, 0, (int)size.Width, (int)size.Height);
            Bitmap logoBitmap = new Bitmap(Properties.Resources.Elibre_Logo);
            GLGraphicsObject logo = new GLGraphicsObject(logoBitmap);
            frame = new GLFrame(bounds, logo, "XXXX XX XXX", "2/5/2019", sheet_name);
            logoBitmap.Dispose();
        }

        public void Translate(Vector2 v)
        {
            graphics.Translate(v);
            frame.Translate(v);
        }
        public Rectangle2D getOuterRectangle()
        {
            return frame.getOuterRectangle();
        }
        public void Dispose()
        {
            graphics.Dispose();
            frame.Dispose();
        }

        public  void RenderGraphics(int graphicsID)
        {
            graphics.Render(graphicsID);
            frame.RenderGraphics(graphicsID);
        }

        public void RenderDrawable(int drawId)
        {
            frame.Render(drawId);
        }
    }
}
