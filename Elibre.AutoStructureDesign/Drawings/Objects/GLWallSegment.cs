﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Drawings.BaseClass;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.Models;
using Elibre.Geometry2D;
using Elibre.Stitcher.Models.Stitching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.Drawings.Floor
{
    public class GLWallSegment:GLBaseObject
    {
         
        bool IsShear;
        private bool interior;

        public bool Interior
        {
            get { return interior; }

        }

        double length;

        private ShearPattern shearPattern;

        public GLWallSegment(StraightWallSegment segment,double pointerlength)
        {
            slices = new List<Slice2D>();
            var points = segment.Geom2D.VertexList;
            Slice2D core = new Slice2D(new Geometry2D.Point2D((segment.CenterLine.P1.x*12),(segment.CenterLine.P1.y*12))
                , new Geometry2D.Point2D((segment.CenterLine.P2.x*12),(segment.CenterLine.P2.y*12)));

            slices.Add(new Slice2D(new Geometry2D.Point2D(points[0].x*12,points[0].y*12), new Geometry2D.Point2D(points[1].x*12, points[1].y*12)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[1].x*12, points[1].y*12), new Geometry2D.Point2D(points[2].x*12, points[2].y*12)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[2].x*12, points[2].y*12), new Geometry2D.Point2D(points[3].x*12, points[3].y*12)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[3].x*12, points[3].y*12), new Geometry2D.Point2D(points[0].x*12, points[0].y*12)));
           
            IsShear = segment.Shear;
            if (IsShear)
            {
                //float pointerLength = core.IsVertical() ? (float)slices[1].Length * 9.0f : (float)slices[1].Length * 7.0f;
                shearPattern = new ShearPattern(core, "A", (float)slices[1].Length, (float)pointerlength*12);
            }
            if (segment.ParentFramingWall.Use == WallUse.Interior)
            {
                interior = true;
            }
            else
            {
                interior = false;
            }
            if (interior && IsShear)
            {
                List<Slice2D> ss = CrossHatches();
                for (int i = 0; i < ss.Count; i++)
                {
                    slices.Add(ss[i]);
                }
            }
           
            drawableSegment = new GLDrawableObject(slices);
            length = segment.Length;
            drawableSegment.Thickness = 1.5f;
        }
        public void IsPointerIntersects(List<GLGraphicsObject> other)
        {
            
        }
        public override void Render(int programID)
        {
            base.Render(programID);
            if (shearPattern != null)
            {
                shearPattern.Draw(programID);
            }
        }
        public void Fill(int programID)
        {
            if (shearPattern != null)
            {
                shearPattern.Fill(programID);
            }
        }
        public void RenderGraphics(int programID)
        {
            if (shearPattern != null)
            {
                shearPattern.RenderGraphics(programID);
            }
        }
        private List<Slice2D> CrossHatches()
        {
            List<Slice2D> copy = slices.CreateCopy();

            copy = copy.OrderByDescending(o => o.Length).ToList();

            List<Slice2D> result = new List<Slice2D>();

            Slice2D left_slice = copy[0];
            Slice2D rightslice = copy[1];
            Slice2D step_slice = copy[2];

            Vector2D unitvector = new Vector2D(left_slice.End - left_slice.Start);

            unitvector.Normalize();

            int count = (int)(left_slice.Length / step_slice.Length);

            List<Geometry2D.Point2D> left_points = new List<Geometry2D.Point2D>();
            List<Geometry2D.Point2D> right_points = new List<Geometry2D.Point2D>();


            Geometry2D.Point2D temp = left_slice.Start;
            Geometry2D.Point2D temp2 = rightslice.End;


            for (int i = 0; i < count; i++)
            {
                left_points.Add(temp + unitvector * step_slice.Length);
                right_points.Add(temp2 + unitvector * step_slice.Length);

                temp = left_points[i];
                temp2 = right_points[i];
            }
            for (int i = 0; i < left_points.Count - 1; i++)
            {

                Slice2D slice = new Slice2D(left_points[i], right_points[i + 1]);
                Slice2D slice2 = new Slice2D(left_points[i + 1], right_points[i]);

                result.Add(slice);
                result.Add(slice2);

            }

            return result;

        }

        public List<PointingPattern> GetPointingPattern()
        {
            return shearPattern?.GetPointingPatterns();
        }

    }
}
