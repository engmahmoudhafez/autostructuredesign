﻿using Elibre.AutoStructureDesign.Drawings.BaseClass;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.CAD.StitcherApi.Algorithms.RoofAlgorithms;
using Elibre.Geometry2D;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.Drawings
{
    public class GLHip:GLBaseObject
    {
         
        private List<GLGraphicsObject> hipsTexts;

        public GLHip(RoofTrussContainer TrussContainer)
        {
            hipsTexts = new List<GLGraphicsObject>();
            slices = new List<Slice2D>();

            for (int i = 0; i < TrussContainer.HipTrussAreas.Count; i++)
            {
                for (int j = 0; j < TrussContainer.HipTrussAreas[i].Hip2Direc.Count; j++)
                {
                    var hipSlice = TrussContainer.HipTrussAreas[i].Hip2Direc[j].SlopedTruss.SingleLine;
                    slices.Add(hipSlice);
                    var hipText = new GLGraphicsObject("HIP", new Font("Arial", 12, FontStyle.Regular), Color.Black);
                    hipText.Scale(0.06f);
                    GLUtils.AlignTextToSlice(hipText, hipSlice,0.2f, GLUtils.GLGraphicsObjectVPosition.UP,GLUtils.GLGraphicsObjectHPosition.MIDDLE);
                    hipsTexts.Add(hipText);
                }
            }

            drawableSegment = new GLDrawableObject(slices);
            drawableSegment.Thickness = 3;
        }


        public List<GLGraphicsObject> GetGraphicObjects()
        {
            return hipsTexts;
        }
        public void RenderGraphics(int programID)
        {
            foreach (var text in hipsTexts)
            {
                text.Render(programID);
            }
        }
    }
}
