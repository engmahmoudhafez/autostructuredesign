﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Drawings.BaseClass;
using System.Collections.Generic;

namespace Elibre.AutoStructureDesign.Drawings.Floor
{
    class GLBeamHanger :GLBaseObject
    {
        public GLBeamHanger(Hanger hanger)
        {

            slices = new List<Geometry2D.Slice2D>();

            slices = hanger.Gemo2D.ToSlices(12);

            drawableSegment = new Libraries.GLUtils.GLDrawableObject(slices);

        }
        public override void Render(int programID)
        {
            base.Render(programID);
        }
    }
}
