﻿using Elibre.AutoStructureDesign.Drawings.BaseClass;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.CAD.StitcherApi.Algorithms.RoofAlgorithms;
using Elibre.CAD.StitcherApi.Models.Stitching;
using Elibre.Geometry2D;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Elibre.AutoStructureDesign.Drawings
{
    public class GLTruss:GLBaseObject
    {
       

        private List<GLGraphicsObject> trussTexts;
        public GLTruss(RoofTrussContainer TrussContainer, StitcherBuilding Building)
        {
            trussTexts = new List<GLGraphicsObject>();
            slices = new List<Slice2D>();
            if (TrussContainer==null||!TrussContainer.TrussAreas.Any())
            {
                return;
            }
            for (int i = 0; i < TrussContainer.TrussAreas.Count; i++)
            {
                for (int j = 0; j < TrussContainer.TrussAreas[i].Trusses.Count; j++)
                {
                    slices.Add(TrussContainer.TrussAreas[i].Trusses[j].SingleLine);
                }
            }
            for (int i = 0; i < TrussContainer.HipTrussAreas.Count; i++)
            {
                for (int j = 0; j < TrussContainer.HipTrussAreas[i].Hip1Direc.Count; j++)
                {
                    for (int k = 0; k < TrussContainer.HipTrussAreas[i].Hip1Direc[j].Trusses.Count; k++)
                    {
                        slices.Add(TrussContainer.HipTrussAreas[i].Hip1Direc[j].Trusses[k].SingleLine);
                    }
                }
                for (int j = 0; j < TrussContainer.HipTrussAreas[i].Hip2Direc.Count; j++)
                {
                    slices.Add(TrussContainer.HipTrussAreas[i].Hip2Direc[j].SideTruss.SingleLine);
                    for (int k = 0; k < TrussContainer.HipTrussAreas[i].Hip2Direc[j].Trusses1.Count; k++)
                    {
                        slices.Add(TrussContainer.HipTrussAreas[i].Hip2Direc[j].Trusses1[k].SingleLine);

                    }
                    for (int k = 0; k < TrussContainer.HipTrussAreas[i].Hip2Direc[j].Trusses2.Count; k++)
                    {
                        slices.Add(TrussContainer.HipTrussAreas[i].Hip2Direc[j].Trusses2[k].SingleLine);
                    }

                }
                for (int j = 0; j < TrussContainer.HipTrussAreas[i].PerbindicularTruss.Count; j++)
                {
                    for (int k = 0; k < TrussContainer.HipTrussAreas[i].PerbindicularTruss[j].Trusses.Count; k++)
                    {

                        slices.Add(TrussContainer.HipTrussAreas[i].PerbindicularTruss[j].Trusses[k].SingleLine);
                    }
                }
            }

            List<SlicePolygon2D> _slices = Building.RoofPolygons.Select(o => o.SimplePolygon2D).ToList();

            slices = slices.Union(_slices.SelectMany(o => o.Slices).ToList()).ToList();

            drawableSegment = new GLDrawableObject(slices);

            var text = new GLGraphicsObject("Trusses @ 24 o.c", new Font("Arial", 12, FontStyle.Regular), Color.Black,Color.Black,3.0f);
            text.Scale(0.2f);
            GLUtils.AlignTextToSlice(text, slices[5],0.4f, GLUtils.GLGraphicsObjectVPosition.UP,GLUtils.GLGraphicsObjectHPosition.MIDDLE);

            trussTexts.Add(text);
        }
        public void RenderGraphics(int programID)
        {
            foreach (var text in trussTexts)
            {
                text.Render(programID);
            }
        }

        public List<GLGraphicsObject> GetGLGraphicsObjects()
        {
            return trussTexts;
        }

        public override void Dispose()
        {
            base.Dispose();
            for (int i = 0; i < trussTexts.Count; i++)
            {
                trussTexts[i].Dispose();
            }
        }
    }
}
