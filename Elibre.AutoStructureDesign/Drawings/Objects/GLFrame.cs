﻿using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.Geometry2D;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.Drawings.DrawingFrame
{
    public class GLFrame
    {

        protected GLDrawableObject Frame_lines;
        private List<GLGraphicsObject> graphicObjects;

        private Rectangle2D Main_Rectangle;
        private Rectangle2D first_R;
        private Rectangle2D second_R;
        private Rectangle2D third_R;
        private Rectangle2D fourth_R;
        private Rectangle2D outerRectangle;

        private double offsetfromimage = 10;

        private const Double Aspect = 42 / 29.7;

        public double OffsetFromImage
        {
            get { return offsetfromimage; }
            set { offsetfromimage = value;
                Update();
            }
        }

        private double textarea_width = 20;

        public double TextArea_Width 
        {
            get { return textarea_width; }
            set { textarea_width = value;
                Update();
            }
        }

        private double doubleline_spacing = 0.5;

        public double DoubleLine_Spacing
        {
            get { return doubleline_spacing; }
            set { doubleline_spacing = value;
                Update();
            }
        }
        private double textarea_offset = 0.5;

        public float Thickness { get; set; } = 1.5f;

        public GLGraphicsObject Logo
        {
            get
            {
                return logo;
            }
            set
            {
                logo = value;
                Update();
            }
        }
        public string Projectname
        {
            get
            {
                return projectName;
            }
            set
            {
                projectName = value;
                Update();
            }
        }
        public string Date
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
                Update();
            }
        }

        public string SheetName
        {
            get
            {
                return sheetName;
            }
            set
            {
                sheetName = value;
                Update();
            }
        }

        private string projectName;
        private string date;
        private string sheetName;
        GLGraphicsObject logo;
        public GLFrame(Rectangle2D frame, GLGraphicsObject _logo, string _projectName, string _date, string _sheetName)
        {
            logo = _logo;
            projectName = _projectName;
            date = _date;
            sheetName = _sheetName;

            Main_Rectangle = frame;
            Update();
        }

        public void Translate(Vector2 t)
        {
            Frame_lines.Translate(t);
            graphicObjects.ForEach(o => o.Translate(t));
        }

        public void Update()
        {
            if (graphicObjects != null)
            {
                foreach (var g in graphicObjects)
                {
                    g.Dispose();
                }
            }
            double width = 0;
            double height = 0;
            Rectangle2D inner = new Rectangle2D(Main_Rectangle.X, Main_Rectangle.Y, Main_Rectangle.Width, Main_Rectangle.Height);
            inner.Inflate(offsetfromimage * 12, offsetfromimage * 12);
            Point2D centeroid = Main_Rectangle.GetCentroid();

            inner.Inflate(offsetfromimage * 12, offsetfromimage * 12);
            if (Main_Rectangle.Width >= Main_Rectangle.Height)
            {
                width = inner.Width;
                height = width / Aspect;
            }
            else
            {
                height = inner.Height;
                width = height * Aspect;
            }
            Point2D left_new = new Point2D(centeroid.X - width / 2, centeroid.Y - height / 2);
            inner = new Rectangle2D(left_new.X, left_new.Y, width, height);

            double all_width = (42.0 / 30.0) * width;
            textarea_width = (12.0/ 100) * all_width;
            
            
            graphicObjects = new List<GLGraphicsObject>();
            List<Slice2D> slices = new List<Slice2D>();

            slices = inner.ToSlices();
            Slice2D slice = new Slice2D(slices.FirstOrDefault(o => o.IsVertical() && o.Start.X > centeroid.X));
            Slice2D slice2 = new Slice2D(slice);

            slice2.Translate(doubleline_spacing * 12, 0);
            slice.Translate(textarea_width , 0);
            Slice2D bottomleft = new Slice2D(slice2.Start, slice.Start);
            Slice2D topleft = new Slice2D(slice2.End, slice.End);

            slices.Add(slice);
            slices.Add(slice2);
            slices.Add(topleft);
            slices.Add(bottomleft);

            Rectangle2D rightup = new Rectangle2D(topleft.Start, new Size2D(topleft.Length, slice2.Length / 7));
            rightup.Inflate(-textarea_offset * 12, -textarea_offset * 12);
            slices = slices.Union(rightup.ToSlices()).ToList();
            var logoScaleRation = (float)(rightup.Width*.8f / logo.getSize().Width);
            logo.Scale(logoScaleRation);
            logo.Translate(new Point2D(rightup.GetCentroid().X - logo.getSize().Width / 2, -(rightup.GetCentroid().Y - logo.getSize().Height / 2)));
            graphicObjects.Add(logo);

            Rectangle2D rightdown = new Rectangle2D(rightup.X, rightup.Y, rightup.Width, (rightup.Height / 2));
            rightdown.Offset(0, (slice2.Length - (((textarea_offset * 2) * 12) + rightdown.Height)));
            slices = slices.Union(rightdown.ToSlices()).ToList();
            GLGraphicsObject sheet = new GLGraphicsObject("Sheet Name: " + sheetName, new Font("Arial", 12, FontStyle.Regular), Color.Gray);
            logoScaleRation = (float)(rightup.Width * .8f / sheet.getSize().Width);
            sheet.Scale(logoScaleRation);
            sheet.Translate(new Point2D(rightdown.GetCentroid().X - sheet.getSize().Width / 2, -(rightdown.GetCentroid().Y - sheet.getSize().Height / 2)));
            graphicObjects.Add(sheet);

            Rectangle2D rightdowntwo = new Rectangle2D(rightup.X, rightup.Y, rightup.Width, (rightdown.Height * 3));
            rightdowntwo.Offset(0, (slice2.Length - (((textarea_offset * 3) * 12) + rightdown.Height + rightdowntwo.Height)));
            slices = slices.Union(rightdowntwo.ToSlices()).ToList();
            GLGraphicsObject project = new GLGraphicsObject("Project Name", new Font("Arial", 12, FontStyle.Regular), Color.Gray);
            logoScaleRation = (float)(rightup.Width * .8f / project.getSize().Width);
            project.Scale(logoScaleRation);
            project.Translate(new Point2D(rightdowntwo.X, -(rightdowntwo.Y + 2)));
            graphicObjects.Add(project);

            GLGraphicsObject projectN = new GLGraphicsObject(projectName, new Font("Arial", 12, FontStyle.Regular), Color.Gray);
            logoScaleRation = (float)(rightup.Width * .8f / projectN.getSize().Width);
            projectN.Scale(logoScaleRation);
            projectN.Translate(new Point2D(rightdowntwo.GetCentroid().X - projectN.getSize().Width / 2, -(rightdowntwo.Y + project.getSize().Height + 5)));
            graphicObjects.Add(projectN);

            GLGraphicsObject dateO = new GLGraphicsObject("Date: " + date, new Font("Arial", 12, FontStyle.Regular), Color.Gray);
            logoScaleRation = (float)(rightup.Width * .8f / dateO.getSize().Width);
            dateO.Scale(logoScaleRation);
            dateO.Translate(new Point2D(rightdowntwo.X, -(rightdowntwo.Y + rightdowntwo.Height - dateO.getSize().Height - 10)));
            graphicObjects.Add(dateO);

            Rectangle2D rightdownthree = new Rectangle2D(rightup.X, rightup.Y, rightup.Width, (rightdown.Height * 4));
            rightdownthree.Offset(0, (slice2.Length - (((textarea_offset * 4) * 12) + rightdown.Height + rightdowntwo.Height + rightdownthree.Height)));
            slices = slices.Union(rightdownthree.ToSlices()).ToList();

            first_R = rightup;
            second_R = rightdownthree;
            third_R = rightdowntwo;
            fourth_R = rightdown;
            
            List<Slice2D> horizontals = slices.Where(o => o.IsHorizontal()).ToList();
            horizontals[0] = new Slice2D(horizontals[0].Start, slice.Start);
            horizontals[1] = new Slice2D(horizontals[1].End, slice.End);

            all_width = width + textarea_width;
            Rectangle2D rectangle2D = new Rectangle2D(inner.X, inner.Y, all_width, inner.Height);
            rectangle2D.Inflate(doubleline_spacing * 12, doubleline_spacing * 12);
            outerRectangle = rectangle2D;
            slices = slices.Union(rectangle2D.ToSlices()).ToList();

            Frame_lines?.Dispose();
            Frame_lines = new GLDrawableObject(slices,Color.Gray,Thickness);
        }
        public void Render(int programID)
        {
            Frame_lines.Render(programID);
        }
        
        public void RenderGraphics(int programID)
        {
            foreach (var g in graphicObjects)
            {
                g.Render(programID);
            }
        }
        public void Dispose()
        {
            Frame_lines.Dispose();
            foreach (var g in graphicObjects)
            {
                g.Dispose();
            }
        }

        public double TextArea_Offset
        {
            get { return textarea_offset; }
            set
            {
                textarea_offset = value;
                Update();
            }
        }


        public Rectangle2D getOuterRectangle() { return outerRectangle; }
    }
}
