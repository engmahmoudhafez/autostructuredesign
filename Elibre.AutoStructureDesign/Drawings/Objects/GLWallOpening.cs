﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Drawings.BaseClass;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.CAD.StitcherApi.Helpers;
using Elibre.Geometry2D;
using System;
using System.Collections.Generic;

namespace Elibre.AutoStructureDesign.Drawings.Floor
{
    public class GLWallOpening:GLBaseObject
    {

        Slice2D opening_centerline;
        protected GLDrawableObject Opening_centerline;

        string HDR_width;
        string HDR_depth;
        string HDR_materialstring;

        private PointingPattern pointer;

        private bool _pointer;
        public bool _Pointer
        {
            get { return _pointer; }
            set { _pointer = value; }
        }

        public GLWallOpening(StraightWallOpening opening,double pointerlength,bool p=false)
        {
            _pointer = p;
            slices = new List<Slice2D>();
            opening_centerline = new Slice2D(new Geometry2D.Point2D(opening.CenterLine.P1.x*12, opening.CenterLine.P1.y*12),
                new Geometry2D.Point2D(opening.CenterLine.P2.x*12, opening.CenterLine.P2.y*12));
            var points = opening.Geom2D.VertexList;
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[0].x*12, points[0].y*12), new Geometry2D.Point2D(points[1].x*12, points[1].y*12)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[1].x*12, points[1].y*12), new Geometry2D.Point2D(points[2].x*12, points[2].y*12)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[2].x*12, points[2].y*12), new Geometry2D.Point2D(points[3].x*12, points[3].y*12)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[3].x*12, points[3].y*12), new Geometry2D.Point2D(points[0].x*12, points[0].y*12)));
            
            HDR_width = opening.HeaderSection.WidthString;
            HDR_depth = opening.HeaderSection.DepthString;
            HDR_materialstring = opening.HeaderSection.Species;

            drawableSegment = new GLDrawableObject(slices);

            List<Slice2D> centers = new List<Slice2D>();
            centers.Add(opening_centerline);

            Opening_centerline = new GLDrawableObject(centers);

            Opening_centerline.Thickness = 3;
            drawableSegment.Thickness = 3;

            Geometry2D.Point2D center = opening_centerline.GetMidPoint();

            //float pointerLength = 3.5f * (float)slices[1].Length;
            //if (opening_centerline != null)
            //{
            //    pointerLength = opening_centerline.IsVertical() ? (float)slices[1].Length * 3.7f : (float)slices[1].Length * 5.2f;
            //}
            string HDR_width_u;
            double iv;
            UnitsHelper.IsImperialLength(HDR_width, out iv, out HDR_width_u);
            string HDR_depth_u;
            double x;
            UnitsHelper.IsImperialLength(HDR_depth, out x, out HDR_depth_u);

            if (_pointer)
            {
                pointer = new PointingPattern(center, pointerlength*12, (float)MathUtil.ToRadian(30.0), "HDR \n" + HDR_width_u + " x " + HDR_depth_u + " " + "GL",
                (float)Math.Atan(opening_centerline.GetSlope()), 0.2f);
            }
            if (pointer != null)
            {
                pointer.GetGraphicObject().Tag = this;
            }

        }
        public override void Render(int programID)
        {
            base.Render(programID);
            GLUtils.StartDottedLinesMode(3, 0x00FF);
            Opening_centerline.Render(programID);
            GLUtils.StopDottedLinesMode();

            pointer?.Render(programID);
        }
        public void RenderGraphics(int programID)
        {
            pointer?.RenderGraphics(programID);
        }
        public void FillArrow(int programID)
        {
            pointer?.Fill(programID);
        }
        public override void Dispose()
        {
            base.Dispose();
            Opening_centerline.Dispose();
            
        }

        public PointingPattern GetPointingPattern()
        {
            return pointer;
        }

    }
}
