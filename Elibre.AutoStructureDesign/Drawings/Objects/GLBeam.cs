﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Drawings.BaseClass;
using Elibre.AutoStructureDesign.Drawings.Floor;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.Geometry2D;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Elibre.AutoStructureDesign.Drawings.DrawingClasses
{
    public class GLBeam:GLBaseObject
    {
        BeamType type;
        GLDrawableObject Beam_core;
        public Slice2D core_slice;

        private GLGraphicsObject BeamText;
        string Beam_width;
        string Beam_depth;
        string Beam_material;

        double width;
        public GLBeam(Beam beam)
        {
            var points = beam.Geom2D.VertexList;
            slices = new List<Slice2D>();

            core_slice = new Slice2D(new Geometry2D.Point2D(beam.CenterLine.P1.x * 12, beam.CenterLine.P1.y * 12), new Geometry2D.Point2D(beam.CenterLine.P2.x * 12, beam.CenterLine.P2.y * 12));

            slices.Add(new Slice2D(new Geometry2D.Point2D(points[0].x * 12, points[0].y * 12), new Geometry2D.Point2D(points[1].x * 12, points[1].y * 12)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[1].x * 12, points[1].y * 12), new Geometry2D.Point2D(points[2].x * 12, points[2].y * 12)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[2].x * 12, points[2].y * 12), new Geometry2D.Point2D(points[3].x * 12, points[3].y * 12)));
            slices.Add(new Slice2D(new Geometry2D.Point2D(points[3].x * 12, points[3].y * 12), new Geometry2D.Point2D(points[0].x * 12, points[0].y * 12)));

            type = beam.Type;
            Beam_width = beam.Section.WidthString;
            Beam_depth = beam.Section.DepthString;
            Beam_material = beam.Section.Species;
            width = beam.Section.Width;

            if (type == BeamType.Dropped)
            {
                List<Slice2D> slices2 = new List<Slice2D>();
                slices2.Add(new Slice2D(new Geometry2D.Point2D(beam.CenterLine.P1.x * 12, beam.CenterLine.P1.y * 12), new Geometry2D.Point2D(beam.CenterLine.P2.x * 12, beam.CenterLine.P2.y * 12)));
                Beam_core = new GLDrawableObject(slices2);
            }
            else
            {
                slices = slices.Union(getBeamHatches()).ToList();
            }


            drawableSegment = new GLDrawableObject(slices);
            BeamText = new GLGraphicsObject(Beam_width + "X" + Beam_depth + " " + Beam_material, new Font("Arial", 12, FontStyle.Regular), Color.Black);
            BeamText.Scale(0.2f);
            GLUtils.AlignTextToSlice(BeamText, core_slice, (float)width / 14, GLUtils.GLGraphicsObjectVPosition.UP, GLUtils.GLGraphicsObjectHPosition.MIDDLE);
        }
        public override void Render(int programID)
        {
            if (type == BeamType.Dropped)
            {
                GLUtils.StartDottedLinesMode(1, 0xAAA);
                drawableSegment.Z_Order = (float)((int)GLFloor.FloorZOrders.BEAM) * 0.1f;
                drawableSegment.Render(programID);
                GLUtils.StopDottedLinesMode();
                GLUtils.StartDottedLinesMode(1, 0x00FF);
                Beam_core.Z_Order = (float)((int)GLFloor.FloorZOrders.BEAM) * 0.1f;
                Beam_core.Render(programID);
                GLUtils.StopDottedLinesMode();
            }
            else
            {
                base.Render(programID);
            }

        }
        public void RenderGraphics(int programID)
        {
            BeamText.Render(programID);
        }
        public GLGraphicsObject GetGraphicObject()
        {
            return BeamText;
        }
        private List<Slice2D> getBeamHatches()
        {
            slices = slices.OrderByDescending(o => o.Length).ToList();

            List<Slice2D> result = new List<Slice2D>();

            Slice2D left_slice = slices[0];
            Slice2D rightslice = slices[1];
            Slice2D step_slice = slices[2];

            Vector2D unitvector = new Vector2D(left_slice.End - left_slice.Start);

            unitvector.Normalize();

            int count = (int)(left_slice.Length / step_slice.Length);

            List<Geometry2D.Point2D> left_points = new List<Geometry2D.Point2D>();
            List<Geometry2D.Point2D> right_points = new List<Geometry2D.Point2D>();


            Geometry2D.Point2D temp = left_slice.Start;
            Geometry2D.Point2D temp2 = rightslice.End;


            for (int i = 0; i < count; i++)
            {
                left_points.Add(temp + unitvector * step_slice.Length);
                right_points.Add(temp2 + unitvector * step_slice.Length);

                temp = left_points[i];
                temp2 = right_points[i];
            }
            for (int i = 0; i < left_points.Count - 1; i++)
            {

                Slice2D slice = new Slice2D(left_points[i], right_points[i + 1]);
                result.Add(slice);
            }

            return result;

        }
    }
}
