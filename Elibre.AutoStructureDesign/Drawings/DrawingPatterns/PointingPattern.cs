﻿using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Elibre.AutoStructureDesign.Drawings.Floor
{
    public class PointingPattern
    {
        public enum CollisionFixMethod
        {
            MOVE, ROTATE, MIRROR
        }
        GLDrawableObject drawable;
        GLFillableObject arrow;

        private GLGraphicsObject text;

        Point2D point; double length; double theta; string str; double rotation; double textScale; bool isMirrored = false; bool isArrow = true;
        public PointingPattern(Point2D point, double length, double theta, string str, double rotation, double textScale, bool isMirrored = false, bool isArrow = true)
        {
            this.point = point;
            this.length = length;
            this.theta = theta;
            this.str = str;
            this.rotation = rotation;
            this.textScale = textScale;
            this.isMirrored = isMirrored;
            this.isArrow = isArrow;
            init();
        }

        private void init()
        {
            drawable?.Dispose();
            arrow?.Dispose();
            text?.Dispose();

            text = new GLGraphicsObject(str, new Font("Arial", 12, FontStyle.Regular), Color.Black);
            text.Scale((float)textScale);

            var d = length * Math.Tan(theta);
            Slice2D slice1 = new Slice2D(point, new Point2D(point.X + d, point.Y - length));
            Slice2D slice2 = new Slice2D(slice1.End, new Point2D(slice1.End.X + 2 * 2, slice1.End.Y));

            Slice2D arrowSlice1 = null;
            Slice2D arrowSlice2 = null;
            Slice2D arrowSlice3 = null;
            if (isArrow)
            {
                arrowSlice1 = new Slice2D(point, new Point2D(point.X - 2f, point.Y - 8));
                arrowSlice2 = new Slice2D(point, new Point2D(point.X + 2f, point.Y - 8));
                arrowSlice3 = new Slice2D(arrowSlice1.End, arrowSlice2.End);

                var a = Math.PI / 2 + (slice1.End - slice1.Start).GetAngle();
                GLUtils.RotateSlice(arrowSlice1, point, a);
                GLUtils.RotateSlice(arrowSlice2, point, a);
                GLUtils.RotateSlice(arrowSlice3, point, a);
            }

            if (isMirrored)
            {
                slice1 = new Slice2D(point, new Point2D(point.X - d, point.Y - length));
                slice2 = new Slice2D(slice1.End, new Point2D(slice1.End.X - 2 * 2, slice1.End.Y));
            }
            var v = slice2.End - slice2.Start;
            v.Normalize();
            v *= text.getSize().Width;
            slice2.End = slice2.Start + v;

            GLUtils.RotateSlice(slice1, point, rotation);
            GLUtils.RotateSlice(slice2, point, rotation);

            if (isArrow)
            {
                GLUtils.RotateSlice(arrowSlice1, point, rotation);
                GLUtils.RotateSlice(arrowSlice2, point, rotation);
                GLUtils.RotateSlice(arrowSlice3, point, rotation);
            }

            GLUtils.AlignTextToSlice(text, slice2, 0, GLUtils.GLGraphicsObjectVPosition.UP, GLUtils.GLGraphicsObjectHPosition.MIDDLE);

            drawable = new GLDrawableObject(new List<Slice2D>() { slice1, slice2 });
            List<Slice2D> pointingSlices = new List<Slice2D>();
            if (isArrow)
            {
                arrowSlice2.FlipDirection();
                pointingSlices = new List<Slice2D>() { arrowSlice1, arrowSlice3, arrowSlice2 };
            }
            else
            {
                pointingSlices = GLUtils.CreateCircleSlices(point, 1.5f, 20);
            }
            SlicePolygon2D polygon = new SlicePolygon2D(pointingSlices);
            polygon.UpdateTriangles();
            arrow = new GLFillableObject(polygon);
        }
        public void Render(int programID)
        {
            drawable.Z_Order = (float)((int)GLFloor.FloorZOrders.POINTER) * 0.1f;
            drawable.Render(programID);
        }
        public void Fill(int programID)
        {
            arrow.Z_Order = (float)((int)GLFloor.FloorZOrders.POINTER) * 0.1f;
            arrow.Render(programID);
        }
        public void RenderGraphics(int programID)
        {
            text.Z_Order = (float)((int)GLFloor.FloorZOrders.TEXT) * 0.1f;
            text.Render(programID);
        }
        public bool IsPointerIntersects(List<GLGraphicsObject> others)
        {
            foreach (var item in others)
            {
                if (text.IsIntersects(item))
                    return true;
            }
            return false;
        }

        public GLGraphicsObject GetGraphicObject()
        {
            return text;
        }
        public void FixCollision(CollisionFixMethod method, double value)
        {
            if (method == CollisionFixMethod.MOVE)
            {
                if (this.length + value > 2)
                    this.length += value;
            }
            else if (method == CollisionFixMethod.ROTATE)
            {
                    this.rotation += value;
            }
            else if (method == CollisionFixMethod.MIRROR)
            {
                isMirrored = !isMirrored;
            }
            init();
        }
        public void Dispose()
        {
            arrow.Dispose();
            drawable.Dispose();
            text.Dispose();
        }
    }
}
