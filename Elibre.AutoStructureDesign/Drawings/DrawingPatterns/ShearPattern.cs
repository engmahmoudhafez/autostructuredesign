﻿using Elibre.AutoStructureDesign.Drawings.Floor;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.CAD.StitcherApi.Helpers;
using Elibre.Geometry2D;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Elibre.AutoStructureDesign.Drawings
{
    public class ShearPattern
    {
        private GLDrawableObject drawable;
        private GLGraphicsObject typeText;
        private GLGraphicsObject lengthText;

        private PointingPattern leftPointer;
        private PointingPattern rightPointer;

        public ShearPattern(Slice2D exteriorSlice, string _typeText, float thickness, float poiterLength)
        {
            var shearSlice = exteriorSlice.CreateCopy();

            typeText = new GLGraphicsObject("A", new Font("Arial", 12, FontStyle.Regular), Color.Black);
            typeText.Scale(0.025f);
            string s;
            double iv;
            UnitsHelper.IsImperialLength(shearSlice.Length.ToString(), out iv, out s);
            lengthText = new GLGraphicsObject(s, new Font("Arial", 12, FontStyle.Regular), Color.Black);
            lengthText.Scale(0.06f);

            var triangleSide = thickness * 1.8;
            var midPoint = shearSlice.GetMidPoint();
            var vec = shearSlice.End - shearSlice.Start;
            var rotAngle = vec.GetAngle();
            GLUtils.RotateSlice(shearSlice, midPoint, -rotAngle);

            var coreSlice = shearSlice.CreateCopy();
            coreSlice.Start = new Point2D(coreSlice.Start.X + thickness * 1.5, coreSlice.Start.Y);
            coreSlice.End = new Point2D(coreSlice.End.X - thickness * 1.5, coreSlice.End.Y);

            shearSlice.Start = new Point2D(shearSlice.Start.X, shearSlice.Start.Y - thickness * 1.5);
            shearSlice.End = new Point2D(shearSlice.End.X, shearSlice.End.Y - thickness * 1.5);

            var midPointAfter = shearSlice.GetMidPoint();
            var triSlice1 = new Slice2D(midPointAfter, new Point2D(midPointAfter.X - triangleSide, midPointAfter.Y - triangleSide));
            var triSlice2 = new Slice2D(midPointAfter, new Point2D(midPointAfter.X + triangleSide, midPointAfter.Y - triangleSide));
            var triSlice3 = new Slice2D(new Point2D(midPointAfter.X - triangleSide, midPointAfter.Y - triangleSide), new Point2D(midPointAfter.X + triangleSide, midPointAfter.Y - triangleSide));
            var typeSlice = new Slice2D(new Point2D(midPointAfter.X - triangleSide, midPointAfter.Y - 0.5 * triangleSide), new Point2D(midPointAfter.X + triangleSide, midPointAfter.Y - 0.5 * triangleSide));
            var lenSlice = new Slice2D(new Point2D(midPointAfter.X - triangleSide, midPointAfter.Y - (triangleSide + lengthText.getSize().Height / 2)), new Point2D(midPointAfter.X + triangleSide, midPointAfter.Y - (triangleSide + lengthText.getSize().Height / 2)));


            GLUtils.RotateSlice(shearSlice, midPoint, rotAngle);
            GLUtils.RotateSlice(triSlice1, midPoint, rotAngle);
            GLUtils.RotateSlice(triSlice2, midPoint, rotAngle);
            GLUtils.RotateSlice(triSlice3, midPoint, rotAngle);
            GLUtils.RotateSlice(typeSlice, midPoint, rotAngle);
            GLUtils.RotateSlice(lenSlice, midPoint, rotAngle);

            GLUtils.RotateSlice(coreSlice, midPoint, rotAngle);

            GLUtils.AlignTextToSlice(typeText, typeSlice, 0, GLUtils.GLGraphicsObjectVPosition.CENTER, GLUtils.GLGraphicsObjectHPosition.MIDDLE);
            GLUtils.AlignTextToSlice(lengthText, lenSlice, 0, GLUtils.GLGraphicsObjectVPosition.CENTER, GLUtils.GLGraphicsObjectHPosition.MIDDLE);

            drawable = new GLDrawableObject(new List<Slice2D>() { shearSlice, triSlice1, triSlice2, triSlice3 }, Color.Black);

            leftPointer = new PointingPattern(coreSlice.Start, poiterLength, (float)MathUtil.ToRadian(30), "MST84", (float)rotAngle, 0.1f, false, false);
            rightPointer = new PointingPattern(coreSlice.End, poiterLength, (float)MathUtil.ToRadian(30), "MST84", (float)rotAngle, 0.1f, false, false);
        }

        public void Draw(int programID)
        {
            drawable.Render(programID);
            leftPointer.Render(programID);
            rightPointer.Render(programID);
        }
        public void Fill(int programID)
        {
            leftPointer.Fill(programID);
            rightPointer.Fill(programID);
        }
        public void RenderGraphics(int programID)
        {
            typeText.Render(programID);
            lengthText.Render(programID);
            leftPointer.RenderGraphics(programID);
            rightPointer.RenderGraphics(programID);
        }
        public void Dispose()
        {
            drawable.Dispose();
            typeText.Dispose();
            lengthText.Dispose();
            leftPointer.Dispose();
            rightPointer.Dispose();
        }

        public List<PointingPattern> GetPointingPatterns()
        {
            return new List<PointingPattern> { leftPointer, rightPointer };
        }

        public List<GLGraphicsObject> GetGraphicObjects()
        {
            return new List<GLGraphicsObject>() { typeText, lengthText };
        }
    }
}
