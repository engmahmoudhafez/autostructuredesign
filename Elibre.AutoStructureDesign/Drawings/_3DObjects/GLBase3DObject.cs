﻿using Elibre.AutoStructureDesign.Libraries.GLUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.Drawings._3DObjects
{
    public abstract class GLBase3DObject
    {
        protected GLObject3D faces;

        public abstract void Render(int ProgramID);
        public virtual void Dispose()
        {
            faces.Dispose();
        }

    }
}
