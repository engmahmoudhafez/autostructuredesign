﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.Drawings._3DObjects
{
    class GL3DBeam : GLBase3DObject
    {

        List<Vector3> beam_points;
        List<int> beam_indices;

        public GL3DBeam(Beam  beam)
        {
            beam_points = new List<Vector3>();
            beam_indices = new List<int>();
            for (int i = 0; i < beam.Geom3D.VertexList.Length; i++)
            {
                beam_points.Add(new Vector3((float)beam.Geom3D.VertexList[i].x, (float)beam.Geom3D.VertexList[i].z, (float)beam.Geom3D.VertexList[i].y));
            }

            for (int i = 0; i < beam.Geom3D.QuadList?.Length; i++)
            {
                beam_indices.Add(beam.Geom3D.QuadList[i].ID1);
                beam_indices.Add(beam.Geom3D.QuadList[i].ID2);
                beam_indices.Add(beam.Geom3D.QuadList[i].ID3);
                beam_indices.Add(beam.Geom3D.QuadList[i].ID3);
                beam_indices.Add(beam.Geom3D.QuadList[i].ID4);
                beam_indices.Add(beam.Geom3D.QuadList[i].ID1);
            }

            for (int i = 0; i < beam.Geom3D.TrianglesList?.Length; i++)
            {
                beam_indices.Add(beam.Geom3D.TrianglesList[i].ID1);
                beam_indices.Add(beam.Geom3D.TrianglesList[i].ID2);
                beam_indices.Add(beam.Geom3D.TrianglesList[i].ID3);
            }
            faces = new GLObject3D(beam_points, beam_indices);

        }

        public override void Render(int ProgramID)
        {
            faces.Render(ProgramID);
        }
    }
}
