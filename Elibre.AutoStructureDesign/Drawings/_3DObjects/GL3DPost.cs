﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.Drawings._3DObjects
{
    class GL3DPost : GLBase3DObject
    {

        List<Vector3> post_points;
        List<int> post_indices;

        public GL3DPost(Post post)
        {

            post_points = new List<Vector3>();
            post_indices = new List<int>();
            for (int i = 0; i < post.Geom3D.VertexList.Length; i++)
            {
                post_points.Add(new Vector3((float)post.Geom3D.VertexList[i].x, (float)post.Geom3D.VertexList[i].z, (float)post.Geom3D.VertexList[i].y));
            }

            for (int i = 0; i < post.Geom3D.QuadList?.Length; i++)
            {
                post_indices.Add(post.Geom3D.QuadList[i].ID1);
                post_indices.Add(post.Geom3D.QuadList[i].ID2);
                post_indices.Add(post.Geom3D.QuadList[i].ID3);
                post_indices.Add(post.Geom3D.QuadList[i].ID3);
                post_indices.Add(post.Geom3D.QuadList[i].ID4);
                post_indices.Add(post.Geom3D.QuadList[i].ID1);
            }

            for (int i = 0; i < post.Geom3D.TrianglesList?.Length; i++)
            {
                post_indices.Add(post.Geom3D.TrianglesList[i].ID1);
                post_indices.Add(post.Geom3D.TrianglesList[i].ID2);
                post_indices.Add(post.Geom3D.TrianglesList[i].ID3);
            }
            faces = new GLObject3D(post_points, post_indices);
        }

        public override void Render(int ProgramID)
        {
            faces.Render(ProgramID);
        }
    }
}
