﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.Drawings._3DObjects
{
    class GL3DHeader : GLBase3DObject
    {

        List<Vector3> header_points;
        List<int> header_indices;
        public GL3DHeader(Header header )
        {
            header_points = new List<Vector3>();
            header_indices = new List<int>();
            for (int i = 0; i < header.Geom3D.VertexList.Length; i++)
            {
                header_points.Add(new Vector3((float)header.Geom3D.VertexList[i].x, (float)header.Geom3D.VertexList[i].z, (float)header.Geom3D.VertexList[i].y));
            }

            for (int i = 0; i < header.Geom3D.QuadList?.Length; i++)
            {
                header_indices.Add(header.Geom3D.QuadList[i].ID1);
                header_indices.Add(header.Geom3D.QuadList[i].ID2);
                header_indices.Add(header.Geom3D.QuadList[i].ID3);
                header_indices.Add(header.Geom3D.QuadList[i].ID3);
                header_indices.Add(header.Geom3D.QuadList[i].ID4);
                header_indices.Add(header.Geom3D.QuadList[i].ID1);
            }

            for (int i = 0; i < header.Geom3D.TrianglesList?.Length; i++)
            {
                header_indices.Add(header.Geom3D.TrianglesList[i].ID1);
                header_indices.Add(header.Geom3D.TrianglesList[i].ID2);
                header_indices.Add(header.Geom3D.TrianglesList[i].ID3);
            }
            faces = new GLObject3D(header_points, header_indices);
        }

        public override void Render(int ProgramID)
        {
            faces.Render(ProgramID);
        }
    }
}
