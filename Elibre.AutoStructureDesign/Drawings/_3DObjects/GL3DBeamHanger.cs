﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.Drawings._3DObjects
{
    class GL3DBeamHanger : GLBase3DObject
    {

        List<Vector3> hanger_points;
        List<int> hanger_indices;

        public GL3DBeamHanger(Hanger hanger)
        {
            hanger_points = new List<Vector3>();
            hanger_indices = new List<int>();
           
            for (int i = 0; i < hanger.Gemo3D.VertexList.Length; i++)
            {
                hanger_points.Add(new Vector3((float)hanger.Gemo3D.VertexList[i].x, (float)hanger.Gemo3D.VertexList[i].z, (float)hanger.Gemo3D.VertexList[i].y));
            }

            for (int i = 0; i < hanger.Gemo3D.QuadList?.Length; i++)
            {
                hanger_indices.Add(hanger.Gemo3D.QuadList[i].ID1);
                hanger_indices.Add(hanger.Gemo3D.QuadList[i].ID2);
                hanger_indices.Add(hanger.Gemo3D.QuadList[i].ID3);
                hanger_indices.Add(hanger.Gemo3D.QuadList[i].ID3);
                hanger_indices.Add(hanger.Gemo3D.QuadList[i].ID4);
                hanger_indices.Add(hanger.Gemo3D.QuadList[i].ID1);
            }

            for (int i = 0; i < hanger.Gemo3D.TrianglesList?.Length; i++)
            {
                hanger_indices.Add(hanger.Gemo3D.TrianglesList[i].ID1);
                hanger_indices.Add(hanger.Gemo3D.TrianglesList[i].ID2);
                hanger_indices.Add(hanger.Gemo3D.TrianglesList[i].ID3);
            }
            faces = new GLObject3D(hanger_points, hanger_indices);



        }
        public override void Render(int ProgramID)
        {
            faces.Render(ProgramID);
        }
    }
}
