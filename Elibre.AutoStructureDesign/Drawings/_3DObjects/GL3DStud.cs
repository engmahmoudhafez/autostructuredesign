﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.Geometry2D;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.AutoStructureDesign.Drawings._3DObjects
{
    public class GL3DStud:GLBase3DObject
    {


        private List<Vector3> stud_points;

        public List<Vector3> Stud_points
        {
            get { return stud_points; }
        }
        List<int> stud_indices;
        public GL3DStud(Stud  stud)
        {
            stud_points = new List<Vector3>();
            stud_indices = new List<int>();
            for (int i = 0; i < stud.Geom3D.VertexList.Length; i++)
            {
                stud_points.Add(new Vector3((float)stud.Geom3D.VertexList[i].x, (float)stud.Geom3D.VertexList[i].z, (float)stud.Geom3D.VertexList[i].y));
            }

            for (int i = 0; i < stud.Geom3D.QuadList?.Length; i++)
            {
                stud_indices.Add(stud.Geom3D.QuadList[i].ID1);
                stud_indices.Add(stud.Geom3D.QuadList[i].ID2);
                stud_indices.Add(stud.Geom3D.QuadList[i].ID3);
                stud_indices.Add(stud.Geom3D.QuadList[i].ID3);
                stud_indices.Add(stud.Geom3D.QuadList[i].ID4);
                stud_indices.Add(stud.Geom3D.QuadList[i].ID1);
            }

            for (int i = 0; i < stud.Geom3D.TrianglesList?.Length; i++)
            {
                stud_indices.Add(stud.Geom3D.TrianglesList[i].ID1);
                stud_indices.Add(stud.Geom3D.TrianglesList[i].ID2);
                stud_indices.Add(stud.Geom3D.TrianglesList[i].ID3);
            }
            faces = new GLObject3D(stud_points, stud_indices);
        }
        public override void Render(int ProgramID)
        {
            faces.Render(ProgramID);
        }
    }
}
