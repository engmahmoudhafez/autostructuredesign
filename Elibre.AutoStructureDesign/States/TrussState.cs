﻿
using EFraming.Objects;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.Models;
using Elibre.CAD.StitcherApi.Algorithms.RoofAlgorithms;
using Elibre.CAD.StitcherApi.Models.Stitching;
using Elibre.Geometry2D;
using Elibre.Net.Core;
using Elibre.Net.Geometry;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign.States
{
    public class TrussState : GLState
    {
        protected List<TrussJoint2D> _jointList = new List<TrussJoint2D>();
        protected List<TrussMember2D> _memberList = new List<TrussMember2D>();
        private StitcherBuilding _stitcherBuildingObj;
        private CAD.StitcherApi.Algorithms.RoofAlgorithms.Truss _truss;
        private List<Slice2D> _trussLines;
        private List<GLGraphicsObject> _reactionText;
        private GLDrawableObject _trussObjectGL;
        public TrussState(CAD.StitcherApi.Algorithms.RoofAlgorithms.Truss truss)
        {
            _truss = truss;
            Update();
            CenterDrawings();
        }
        public override void CenterDrawings()
        {
            if (_stitcherBuildingObj == null || _trussLines == null)
                return;

            CenterBounds(_trussLines.GetBounds());
        }

        public override void Dispose()
        {
            _trussObjectGL.Dispose();
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (HandlePanningMouseDown(e))
                return;
            return;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (HandlePanningMouseMove(e))
                return;
            return;
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (base.HandlePanningMouseUp(e))
                return;
            return;
        }

        public void Update()
        {
            _trussObjectGL?.Dispose();
            _stitcherBuildingObj = EStitcherSingleton.GetInstance.Building;
            _trussLines = GetTrussSlices();
            _trussObjectGL = new GLDrawableObject(_trussLines, System.Drawing.Color.Black, 1);

        }
        private List<Slice2D> GetTrussSlices()
        {
            List<Slice2D> result = null;

            if (_truss == null)
                return result;

            _jointList = new List<TrussJoint2D>();
            _memberList = new List<TrussMember2D>();

            double bottom_y_coord = double.MaxValue;


            Point3d P0 = _truss.TrussEnvelopeSlices.FirstOrDefault().Item1.Start.ToPoint3d();

            Point2d P1 = new Point2d(_truss.TrussEnvelopeSlices.FirstOrDefault().Item1.Start.ToPoint3d());
            Point2d P2 = new Point2d(_truss.TrussEnvelopeSlices.FirstOrDefault().Item1.End.ToPoint3d());

            Point2d delta = P2 - P1;
            double theta = MathKernel.Atanxy(delta.y, delta.x);

            int n = _truss.Joints.Count;
            for (int i = 0; i < n; i++)
            {
                Point3d P = _truss.Joints[i].Position.ToPoint3d() - P0;

                if (!MathKernel.IsZero(theta))
                {
                    GeometryEx.RotateAboutZ(-1 * theta, ref P.x, ref P.y, ref P.z);
                }
                _jointList.Add(new TrussJoint2D(P.x, P.z));
            }

            foreach (CAD.StitcherApi.Algorithms.RoofAlgorithms.TrussMember member in _truss.Members)
            {
                int ID1 = _truss.Joints.IndexOf(member.Start);
                int ID2 = _truss.Joints.IndexOf(member.End);

                if ((ID1 >= 0) && (ID2 >= 0))
                {
                    Point3d[] v = member.BaseProfile.Select(p => p.ToPoint3d()).ToArray();
                    List<Point2d> v2 = new List<Point2d>();

                    if (v != null)
                    {
                        foreach (Point3d p in v)
                        {
                            Point3d p2 = p - P0;

                            if (!MathKernel.IsZero(theta))
                            {
                                GeometryEx.RotateAboutZ(-1 * theta, ref p2.x, ref p2.y, ref p2.z);
                            }

                            v2.Add(new Point2d(p2.x, p2.z));
                        }
                    }

                    TrussMember2D mem2D = new TrussMember2D(_jointList[ID1], _jointList[ID2]);
                    mem2D.Geom2D = new Geom2D(v2.ToArray());
                    if (member.Type == StitcherRoofTrussApp.Models.TrussMemberType.Bottom)
                    {
                        var min_y = v2.Min(p => p.y);
                        if (min_y < bottom_y_coord)
                        {
                            bottom_y_coord = min_y;
                        }
                    }
                    _memberList.Add(mem2D);
                }
            }

            result = _memberList.SelectMany(mem => mem.Geom2D.ToSlices(1)).ToList();
            return result;
        }
      
        public override void Render()
        {
            var drawId = StartDraw();
            _trussObjectGL?.Render(drawId);
        }
    }
}
