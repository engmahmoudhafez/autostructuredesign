﻿using Elibre.AutoStructureDesign.Drawings.DrawingClasses;
using Elibre.AutoStructureDesign.Drawings.DrawingFrame;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.Properties;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign.States
{
    class DetailsState : GLState
    {

        private List<GLBitmapDrawing> bitmapDrawings;

        public DetailsState()
        {
            bitmapDrawings = new List<GLBitmapDrawing>();
            GLBitmapDrawing detail_1 = new GLBitmapDrawing(Resources.Details_all,"Details");
            bitmapDrawings.Add(detail_1);
            GLBitmapDrawing detail_2 = new GLBitmapDrawing(Resources.Details_all_2, "Details");
            detail_2.Translate(new Vector2(0,- ((float)detail_1.getOuterRectangle().Height + 100)));
            bitmapDrawings.Add(detail_2);
            //GLBitmapDrawing detail_3 = new GLBitmapDrawing(Resources.DT_3, "Detail-3");
            //detail_3.Translate(new Vector2(0, -((float)detail_2.getOuterRectangle().Height*2 + 200)));
            //bitmapDrawings.Add(detail_3);
            //GLBitmapDrawing detail_4 = new GLBitmapDrawing(Resources.DT_4, "Detail-4");
            //detail_4.Translate(new Vector2(0, -((float)detail_3.getOuterRectangle().Height*3 + 300)));
            //bitmapDrawings.Add(detail_4);
            //GLBitmapDrawing detail_5 = new GLBitmapDrawing(Resources.DT_5, "Detail-5");
            //detail_5.Translate(new Vector2(0, -((float)detail_4.getOuterRectangle().Height*4 + 400)));
            //bitmapDrawings.Add(detail_5);


        }
        public override void UpdateStateItems()
        {

        }
        public override void CenterDrawings()
        {
            var rect = bitmapDrawings.First().getOuterRectangle();
            CenterBounds(rect);
        }

        public override void Dispose()
        {
            bitmapDrawings.ForEach(o => o.Dispose());
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (base.HandlePanningMouseDown(e))
                return;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (HandlePanningMouseMove(e))
                return;
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (base.HandlePanningMouseUp(e))
                return;
        }

        public override void Render()
        {
            int graphicsID = StartGraphics();
            bitmapDrawings.ForEach(o => o.RenderGraphics(graphicsID));

            int drawId = StartDraw();
            bitmapDrawings.ForEach(o => o.RenderDrawable(drawId));
        }
    }
}
