﻿using Elibre.AutoStructureDesign.Dialogs;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.Models;
using Elibre.CAD.StitcherApi.Algorithms.RoofAlgorithms;
using Elibre.CAD.StitcherApi.Models;
using Elibre.CAD.StitcherApi.Models.Stitching;
using Elibre.Geometry2D;
using Elibre.Utilities;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign.Drawings
{
    public class RoofTrussViewer : GLState
    {
        private StitcherBuilding _stitcherBuildingObj;
        private RoofTrussContainer _container;
        private GLFillableObject _roofPolygonsGL;
        private GLDrawableObject _roofLinesGL;
        private GLDrawableObject _exteriorWalls;
        private List<GLGraphicsObject> _trussesNamesGL;

        private List<Truss> _allTrusses;
        private GLDrawableObject _TrussesCenterLinesGL;

        private GLDrawableObject _testSlices;
        private GLDrawableObject _testSlices2;
        private List<GLFillableObject> _testPolys;
        private GLFillableObject _testPolys2;

        public List<Slice2D> HipPointsCircles;
        public List<Slice2D> TestSlices;
        public List<SlicePolygon2D> TestPolys;
        public List<SlicePolygon2D> TestPolys2;

        private RoofSlice _roofLineUnderMouse;
        private GLDrawableObject _roofLineUnderMouseGL;
        private Truss _trussUnderMouse;
        private GLDrawableObject _roofTrussUnderMouseGL;
        public RoofTrussViewer()
        {
            UpdateShownRoof();

        }
        private void FillLists()
        {
            //HipPointsCircles = RoofTrussDetector.TestSlices;
            //TestPolys = RoofTrussDetector.TestPolys;
            //TestPolys2 = RoofTrussDetector.TestPolys2;
            //return;

            ////test hip lines per points dict
            //hipLists = new List<GLDrawableObject>();
            //var rand = new Random();
            //foreach (var row in RoofTrussDetector.HipPointsDict)
            //{
            //    var list = new List<Slice2D>() { row.Key, row.Value };
            //    hipLists.Add(new GLDrawableObject(list, System.Drawing.Color.FromArgb(rand.Next(255), rand.Next(255), rand.Next(255)),7));
            //}

            TestSlices = new List<Slice2D>();
            ////test algorithm rectangles ==> test 1
            //HipPointsCircles = RoofTrussDetector.TestSlices;

            //Draw truss plan ==> test 2
            //TestPolys = _container.TrussAreas.Select(o => o.Outer).ToList();
            //TestPolys2 = _container.HipTrussAreas.Select(o => o.OuterPolygon).ToList();
            //HipPointsCircles = _container.HipTrussAreas.SelectMany(o => o.OuterPolygon.Slices).ToList();
            //HipPointsCircles.AddRange(_container.TrussAreas.SelectMany(o => o.Outer.Slices));

            //accyula truss plan
            HipPointsCircles = _container.HipTrussAreas.SelectMany(o => o.GirderTruss.outer.Slices).ToList();
            HipPointsCircles.AddRange(_container.HipTrussAreas.SelectMany(o => o.PerbindicularTruss.SelectMany(e => e.Girders.SelectMany(m => m.outer.Slices))));
            HipPointsCircles.AddRange(_container.TrussAreas.SelectMany(o => o.Girders.SelectMany(e => e.outer.Slices)));
            TestSlices = _container.HipTrussAreas.SelectMany(o => o.Hip1Direc.SelectMany(e => e.Trusses.SelectMany(m => m.outer.Slices))).ToList();
            TestSlices.AddRange(_container.HipTrussAreas.SelectMany(o => o.PerbindicularTruss.SelectMany(e => e.Trusses.SelectMany(m => m.outer.Slices))));
            TestSlices.AddRange(_container.HipTrussAreas.SelectMany(o => o.Hip2Direc.SelectMany(e => e.Trusses1.SelectMany(m => m.outer.Slices))));
            TestSlices.AddRange(_container.HipTrussAreas.SelectMany(o => o.Hip2Direc.SelectMany(e => e.Trusses2.SelectMany(m => m.outer.Slices))));
            TestSlices.AddRange(_container.HipTrussAreas.SelectMany(o => o.Hip2Direc.SelectMany(e => e.SideTruss.outer.Slices)));
            TestSlices.AddRange(_container.HipTrussAreas.SelectMany(o => o.Hip2Direc.SelectMany(e => e.SlopedTruss.outer.Slices)));
            TestSlices.AddRange(_container.TrussAreas.SelectMany(o => o.Trusses.SelectMany(e => e.outer.Slices)));
            _allTrusses = new List<Truss>();
            _allTrusses.AddRange(_container.HipTrussAreas.Select(o => o.GirderTruss));
            _allTrusses.AddRange(_container.HipTrussAreas.SelectMany(o => o.PerbindicularTruss.SelectMany(e => e.Girders.Select(m => m))));
            _allTrusses.AddRange(_container.TrussAreas.SelectMany(o => o.Girders.Select(e => e)));
            _allTrusses.AddRange(_container.HipTrussAreas.SelectMany(o => o.Hip1Direc.SelectMany(e => e.Trusses.Select(m => m))));
            _allTrusses.AddRange(_container.HipTrussAreas.SelectMany(o => o.PerbindicularTruss.SelectMany(e => e.Trusses.Select(m => m))));
            _allTrusses.AddRange(_container.HipTrussAreas.SelectMany(o => o.Hip2Direc.SelectMany(e => e.Trusses1.Select(m => m))));
            _allTrusses.AddRange(_container.HipTrussAreas.SelectMany(o => o.Hip2Direc.SelectMany(e => e.Trusses2.Select(m => m))));
            _allTrusses.AddRange(_container.HipTrussAreas.SelectMany(o => o.Hip2Direc.Select(e => e.SideTruss)));
            _allTrusses.AddRange(_container.HipTrussAreas.SelectMany(o => o.Hip2Direc.Select(e => e.SlopedTruss)));
            _allTrusses.AddRange(_container.TrussAreas.SelectMany(o => o.Trusses.Select(e => e)));

            //texts
            _trussesNamesGL = new List<GLGraphicsObject>();
            foreach (var truss in _allTrusses)
            {
                var GLGraphicsObject = new GLGraphicsObject(truss.Name, new System.Drawing.Font("Arial", 12f), System.Drawing.Color.Black);
                GLGraphicsObject.Scale(0.03f);
                var midPt = truss.SingleLine.GetMidPoint();
                GLGraphicsObject.Translate(new OpenTK.Vector3((float)midPt.X, -(float)midPt.Y, 0));
                _trussesNamesGL.Add(GLGraphicsObject);
            }
        }

        internal void UpdateShownRoof()
        {
            _stitcherBuildingObj = EStitcherSingleton.GetInstance.Building;
            _container = EStitcherSingleton.GetInstance.TrussContainer;
            if (_container == null)
                return;

            var exterioWallsLines = _stitcherBuildingObj.Levels.Last(o => o.FloorPlan != null && o.FloorPlan.Walls.Count > 0).FloorPlan.Walls.Where(o => !o.IsInteriorWall).SelectMany(o => o.GetCores(_stitcherBuildingObj.Template).SelectMany(ee => ee.Slices)).ToList();
            _exteriorWalls = new GLDrawableObject(exterioWallsLines, System.Drawing.Color.Gray, 1);

            FillLists();
            var roofPolygons = _stitcherBuildingObj.RoofPolygons.Select(o => o.SimplePolygon2D).ToList();
            _roofPolygonsGL?.Dispose();
            _roofPolygonsGL = new GLFillableObject(roofPolygons, System.Drawing.Color.LightBlue);
            _roofLinesGL?.Dispose();
            _roofLinesGL = new GLDrawableObject(roofPolygons.SelectMany(o => o.Slices).ToList(), System.Drawing.Color.Black, 3);
            if (_allTrusses != null)
            {
                _TrussesCenterLinesGL = new GLDrawableObject(_allTrusses.Select(o => o.SingleLine).ToList(), System.Drawing.Color.Black, 1);
                _TrussesCenterLinesGL.LineType = GLDrawableObject.LineTypes.DASH_DOTTED;
            }
            if (TestSlices != null)
                _testSlices = new GLDrawableObject(TestSlices, System.Drawing.Color.Blue, 2);
            if (HipPointsCircles != null)
                _testSlices2 = new GLDrawableObject(HipPointsCircles, System.Drawing.Color.Red, 4);
            var rand = new Random();
            _testPolys = new List<GLFillableObject>();
            if (TestPolys != null)
            {
                foreach (var poly in TestPolys)
                {
                    _testPolys.Add(new GLFillableObject(poly, System.Drawing.Color.FromArgb(rand.Next(255), rand.Next(255), rand.Next(255))) { Alpha = 150f / 255f });
                }
            }
            if (TestPolys2 != null)
                _testPolys2 = new GLFillableObject(TestPolys2, System.Drawing.Color.Blue) { Alpha = 150f / 255f };
        }
        public override void Render()
        {
            var drawId = StartDraw();
            _roofLinesGL?.Render(drawId);
            _exteriorWalls?.Render(drawId);
            _testSlices?.Render(drawId);
            _testSlices2?.Render(drawId);
            _TrussesCenterLinesGL?.Render(drawId);
            _roofLineUnderMouseGL?.Dispose();
            if (_roofLineUnderMouse != null)
            {
                _roofLineUnderMouseGL = new GLDrawableObject(new List<Slice2D>() { _roofLineUnderMouse.Segment }, System.Drawing.Color.Orange, 4);
                _roofLineUnderMouseGL.Render(drawId);
            }
            _roofTrussUnderMouseGL?.Dispose();
            if (_trussUnderMouse!=null)
            {
                _roofTrussUnderMouseGL = new GLDrawableObject(new List<Slice2D>() { _trussUnderMouse.SingleLine }, System.Drawing.Color.Green, 6);
                _roofTrussUnderMouseGL.Render(drawId);
            }

            int fillID = StartFill();
            _roofPolygonsGL?.Render(fillID);
            _testPolys?.ForEach(o => o.Render(fillID));
            _testPolys2?.Render(fillID);

            var textId = StartGraphics();
            _trussesNamesGL?.ForEach(o => o.Render(textId));
        }
        public override void CenterDrawings()
        {
            if (_stitcherBuildingObj == null)
                return;
            var roofLines = _stitcherBuildingObj.RoofPolygons.SelectMany(o => o.SimplePolygon2D.Slices).ToList();
            CenterBounds(roofLines.GetBounds());
        }
        public override void Dispose()
        {
            _roofPolygonsGL?.Dispose();
            _roofLinesGL?.Dispose();
            _exteriorWalls?.Dispose();
        }
        public override void OnMouseMove(MouseEventArgs e)
        {
            if (HandlePanningMouseMove(e))
                return;
            var currentPt = ToWorld(e.X, e.Y);
            var roofLines = _stitcherBuildingObj.RoofLines.SelectMany(o => o.Select(ee => ee)).ToList();
            _roofLineUnderMouse = roofLines.FirstOrDefault(o => o.Segment.Contains(currentPt, ZoomToWorld(7)));
            if (_roofLineUnderMouse == null)
                _trussUnderMouse = _allTrusses.FirstOrDefault(o => o.SingleLine.Contains(currentPt, ZoomToWorld(7)));
        }
        public override void OnMouseDown(MouseEventArgs e)
        {
            if (HandlePanningMouseDown(e))
                return;
            var currentPt = ToWorld(e.X, e.Y);

        }
        public override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (_roofLineUnderMouse != null)
                {
                    _roofLineUnderMouse.IsGable = !_roofLineUnderMouse.IsGable;
                    StitcherRoofGenerator.UpdateRoof(_stitcherBuildingObj.RoofLines, _stitcherBuildingObj, _container);
                    UpdateShownRoof();
                }
                else if (_trussUnderMouse != null)
                {
                    //To Madona 
                    new SingleTrussViewer(_trussUnderMouse).ShowDialog();
                   // new Form().ShowDialog();
                }
            }
            this.Dragging.Stop();
        }

    }
}
