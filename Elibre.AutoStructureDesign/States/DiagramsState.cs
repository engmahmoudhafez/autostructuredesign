﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.Models;

using Elibre.CAD.StitcherApi.Models.Stitching;
using Elibre.Geometry2D;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System;
using System.Drawing;
using Elibre.AutoStructureDesign.Drawings.Floor;
using Elibre.AutoStructureDesign.StructureParser.GLObjects;

namespace Elibre.AutoStructureDesign.States
{
    public class DiagramsState : GLState
    {
        private StitcherBuilding _stitcherBuildingObj;
        private List<StructureParser.GLObjects.GLWall> _walls;
        private List<Slice2D> _boundrySlices;
        private List<Slice2D> _circlesSlices;
        private GLDrawableObject _wallObjectGL;
        private GLDrawableObject _hatchLinesGL;
        private GLDrawableObject _borderGL;
        private GLDrawableObject _shearLinesGL;
        private GLDrawableObject _circlesObject;
        GLGraphicsObject _text;
        private List<GLGraphicsObject> _reactionText;
        DiagramSlices _digramSlices = null;
        public ActiveResults ActiveResults { get; set; }
        public DiagramType DiagramType { get; set; }
        public ActiveWindResults ActiveWindResults { get; set; }
        public DiagramsState()
        {
            // this function will be called when initializing combo box
            UpdateShownLevel(0);
        }
        public override void CenterDrawings()
        {
            if (_stitcherBuildingObj == null || _boundrySlices == null || !_boundrySlices.Any())
                return;

            CenterBounds(_boundrySlices.GetBounds());
        }

        public override void Dispose()
        {
            _wallObjectGL.Dispose();
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (HandlePanningMouseDown(e))
                return;
            return;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (HandlePanningMouseMove(e))
                return;
            return;
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (base.HandlePanningMouseUp(e))
                return;
            return;
        }
        public void UpdateShownLevel(int lvlIndex)
        {
            _wallObjectGL?.Dispose();
            _stitcherBuildingObj = EStitcherSingleton.GetInstance.Building;
            _walls = GetGlWalls(lvlIndex);

            if (DiagramType == DiagramType.Unknown)
                return;

            switch (ActiveResults)
            {
                case ActiveResults.Seismic:
                    _digramSlices = DiagramsManager.GetSeismicDiagram(lvlIndex, DiagramType);
                    break;
                case ActiveResults.Wind:
                    _digramSlices = DiagramsManager.GetWindDiagram(lvlIndex, ActiveWindResults, DiagramType);
                    break;

            }

            if (_digramSlices == null)
                return;
            Color color = GetColor();

            _hatchLinesGL = new GLDrawableObject(_digramSlices.HatchLines, color, 1.5f);
            _borderGL = new GLDrawableObject(_digramSlices.BorderLines, color, 3);
            _circlesSlices = _digramSlices.ShearLines.SelectMany(e => GLUtils.CreateCircleSlices(e.CirclePosition, ShearLineSlice.CircleRadius * 12, 70)).ToList();
            _circlesObject = new GLDrawableObject(_circlesSlices, Color.Blue, 2);
            _shearLinesGL = new GLDrawableObject(_digramSlices.ShearLines.Select(e => e.Line).ToList(), Color.Blue, 2);
            _reactionText = GetReactionTexts(_digramSlices.ShearLines);
            _text = GetTextObject(_digramSlices);
            SetBoundrySlices();
            CenterDrawings();

        }

        private List<StructureParser.GLObjects.GLWall> GetGlWalls(int lvlIndex)
        {
            var floor = StructuralExporter.Document?.Building?.FloorList[lvlIndex + 1];
            if (floor == null)
                return null;
            var walls = floor.ObjectList.OfType<StraightWall>();
            if (!walls.Any())
                return null;

            var result = new List<StructureParser.GLObjects.GLWall>();
            foreach (var w in walls)
            {
                result.Add(new StructureParser.GLObjects.GLWall(w));
            }

            return result;
        }

        private void SetBoundrySlices()
        {
            var result = new List<Slice2D>();
            if (_walls != null || !_walls.Any())
            {
                result.AddRange(_walls.SelectMany(e => e.getSlices()));
            }
            if (_digramSlices != null)
            {
                result.AddRange(_digramSlices.GetSlices());
            }
            if (_circlesSlices != null && _circlesSlices.Any())
            {
                result.AddRange(_circlesSlices);
            }
            _boundrySlices = result;
        }

        private List<GLGraphicsObject> GetReactionTexts(List<ShearLineSlice> shearLines)
        {
            var result = new List<GLGraphicsObject>();
            if (!shearLines.Any())
                return null;

            foreach (var t in shearLines)
            {


                double xShift = 0;
                double yShift = 0;
                var txtHeight = 40;
                var center = t.CirclePosition;

                if (t.Angle >= 90)
                {
                    yShift += txtHeight;

                }
                else
                {
                    xShift += txtHeight;
                }

                if (!string.IsNullOrEmpty(t.ReactionText))
                {
                    GLGraphicsObject text = new GLGraphicsObject(t.ReactionText, new Font("Arial", 12, FontStyle.Regular), Color.Black);
                    text.Scale(.2f);
                    var slice = new Slice2D(new Geometry2D.Point2D(center.X + xShift, center.Y + yShift), new Geometry2D.Point2D(center.X + xShift * 2, center.Y + yShift * 2));
                    GLUtils.AlignTextToSlice(text, slice, 10, GLUtils.GLGraphicsObjectVPosition.CENTER, GLUtils.GLGraphicsObjectHPosition.MIDDLE);
                    result.Add(text);
                }

                var nSlice = new Slice2D(new Geometry2D.Point2D(center.X - xShift, center.Y - yShift), new Geometry2D.Point2D(center.X + xShift, center.Y + yShift));
                var name = new GLGraphicsObject(t.Name, new Font("Arial", 12, FontStyle.Regular), Color.Black);
                name.Scale(.05f);
                GLUtils.AlignTextToSlice(name, nSlice, 0, GLUtils.GLGraphicsObjectVPosition.CENTER, GLUtils.GLGraphicsObjectHPosition.MIDDLE);

                result.Add(name);
            }

            return result;
        }

        private GLGraphicsObject CreateText(string textValue, float scale, float angle, Geometry2D.Point2D position)
        {
            GLGraphicsObject text = new GLGraphicsObject(textValue, new Font("Arial", 12, FontStyle.Regular), Color.Black);
            text.Rotate(angle);
            text.Scale(scale);
            text.Translate(new Geometry2D.Point2D(position.X, -position.Y));
            return text;
        }

        private GLGraphicsObject GetTextObject(DiagramSlices ds)
        {
            GLGraphicsObject text;

            text = new GLGraphicsObject(ds.Text, new Font("Arial", 12, FontStyle.Regular), Color.Black);
            text.Scale(0.2f);
            var txtOffset = 10;
            var cen = ds.TextPosition;
            var slice = new Slice2D(new Geometry2D.Point2D((cen.X - txtOffset), cen.Y), new Geometry2D.Point2D((cen.X), cen.Y));

            GLUtils.AlignTextToSlice(text, slice, 0, GLUtils.GLGraphicsObjectVPosition.CENTER, GLUtils.GLGraphicsObjectHPosition.MIDDLE);
            return text;
        }

        private Color GetColor()
        {
            //switch (DiagramType)
            //{

            //    case DiagramType.DistributedLoad:
            //        return System.Drawing.Color.FromArgb(128, 128, 255); //violet
            //    case DiagramType.ShearForce:
            //        return System.Drawing.Color.Red; //red
            //    case DiagramType.BendingMoment:
            //        return System.Drawing.Color.FromArgb(255, 128, 0); //orange
            //    default:
            //        return System.Drawing.Color.Red;

            //}
            return Color.Red;
        }



        public override void Render()
        {
            var drawId = StartDraw();
            _wallObjectGL?.Render(drawId);
            _hatchLinesGL?.Render(drawId);
            _shearLinesGL?.Render(drawId);
            _borderGL?.Render(drawId);
            _circlesObject?.Render(drawId);
            _walls?.ForEach(e => e.Render(drawId));
            /////////////
            int textID = StartGraphics();
            _text?.Render(textID);
            _reactionText?.ForEach(e => e.Render(textID));
            _walls?.ForEach(e => e.RenderGraphics(textID));
        }

    }
}
