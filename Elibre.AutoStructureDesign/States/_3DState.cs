﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Drawings._3DObjects;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.Models;
using Elibre.Geometry2D;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign.States
{
    public class _3DState : GLState
    {

        private float lastX, lastY;
        private bool IsRotate = false;
        private bool IsPan = false;
        private Vector3 min, max, center;

        List<GL3DStud> building_studs = new List<GL3DStud>();
        List<GL3DHeader> building_headers = new List<GL3DHeader>();
        List<GL3DPost> building_posts = new List<GL3DPost>();
        List<GL3DBeam> building_beams = new List<GL3DBeam>();
        List<GL3DBeamHanger> building_beamhangers = new List<GL3DBeamHanger>();



        public _3DState()
        {
            GetBuildingStuds();
            SetMinMaxAndCenter();
            GetBuildingHeaders();
            GetBuildingPosts();
            GetBuildingBeamsAndHangers();
        }
        public override void CenterDrawings()
        {
            CenterBounds(new Geometry2D.Rectangle2D());
        }

        public override void CenterBounds(Geometry2D.Rectangle2D rect)
        {
            var radius = (max - min).Length;
            var eye = new Vector3(0, 1, 0) * radius;
            camera3D = new Camera3D(eye, center, glControl.Width, glControl.Height);
        }
        public override void Dispose()
        {
            foreach (var item in building_studs)
            {
                item.Dispose();
            }
            foreach (var item in building_headers)
            {
                item.Dispose();
            }
            foreach (var item in building_posts)
            {
                item.Dispose();
            }
            foreach (var item in building_beams)
            {
                item.Dispose();
            }
            foreach (var item in building_beamhangers)
            {
                item.Dispose();
            }
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (HandlePanningMouseDown(e))
                return;
            if (e.Button == MouseButtons.Left)
                IsRotate = true;
            else if (e.Button == MouseButtons.Middle)
                IsPan = true;
            lastX = e.X;
            lastY = e.Y;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (HandlePanningMouseMove(e))
                return;
            float xoffset = MathHelper.DegreesToRadians(e.X - lastX);
            float yoffset = MathHelper.DegreesToRadians(e.Y - lastY);
            if (IsRotate)
                camera3D.Rotate(xoffset, yoffset);
            if (IsPan)
                camera3D.Pan(new Vector2(xoffset, yoffset));

            lastX = e.X;
            lastY = e.Y;
        }
        public override void OnMouseWheel(MouseEventArgs e)
        {
            camera3D.Radius -= e.Delta / 30;
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (base.HandlePanningMouseUp(e))
                return;
            IsRotate = false;
            IsPan = false;
        }

        public override void Render()
        {
            camera3D?.UpdateProjection(glControl.Width, glControl.Height);
            _3DProgramId = Start3D();
            foreach (var item in building_studs)
            {
                item.Render(_3DProgramId);
            }
            foreach (var item in building_headers)
            {
                item.Render(_3DProgramId);
            }
            foreach (var item in building_posts)
            {
                item.Render(_3DProgramId);
            }
            foreach (var item in building_beams)
            {
                item.Render(_3DProgramId);
            }
            foreach (var item in building_beamhangers)
            {
                item.Render(_3DProgramId);
            }
        }

        private void GetBuildingStuds()
        {
            for (int i = 0; i < EStitcherSingleton.GetInstance.StructureBuildling.FloorList.Count; i++)
            {
                foreach (var item in EStitcherSingleton.GetInstance.StructureBuildling.FloorList[i].ObjectList.OfType<StraightWall>())
                {
                    List<Stud> studs = item.GetAllStuds();
                    for (int j = 0; j < studs.Count; j++)
                    {
                        GL3DStud glstud = new GL3DStud(studs[j]);
                        building_studs.Add(glstud);
                    }
                }
            }


        }
        private void GetBuildingHeaders()
        {
            for (int i = 0; i < EStitcherSingleton.GetInstance.StructureBuildling.FloorList.Count; i++)
            {
                foreach (var item in EStitcherSingleton.GetInstance.StructureBuildling.FloorList[i].ObjectList.OfType<StraightWall>())
                {
                    List<Header> headers = item.GetHeaders().ToList();
                    for (int j = 0; j < headers.Count; j++)
                    {
                        GL3DHeader header = new GL3DHeader(headers[j]);
                        building_headers.Add(header);
                    }
                }
            }

        }
        private void GetBuildingPosts()
        {
            for (int i = 0; i < EStitcherSingleton.GetInstance.StructureBuildling.FloorList.Count; i++)
            {
                foreach (var item in EStitcherSingleton.GetInstance.StructureBuildling.FloorList[i].ObjectList.OfType<Post>())
                {
                    GL3DPost post = new GL3DPost(item);
                    building_posts.Add(post);
                }
            }
        }
        private void GetBuildingBeamsAndHangers()
        {
            for (int i = 0; i < EStitcherSingleton.GetInstance.StructureBuildling.FloorList.Count; i++)
            {
                foreach (var item in EStitcherSingleton.GetInstance.StructureBuildling.FloorList[i].ObjectList.OfType<Beam>())
                {
                    GL3DBeam beam = new GL3DBeam(item);
                    building_beams.Add(beam);
                    for (int j = 0; j < item.HangerList.Count; j++)
                    {
                        GL3DBeamHanger hanger = new GL3DBeamHanger(item.HangerList[j]);
                        building_beamhangers.Add(hanger);
                    }
                }
            }
        }
        private void SetMinMaxAndCenter()
        {
            List<Vector3> all_studs = building_studs.SelectMany(o => o.Stud_points).ToList();
            min = max = all_studs[0];
            foreach (var item in all_studs)
            {
                min.X = min.X <= item.X ? min.X : item.X;
                min.Y = min.Y <= item.Y ? min.Y : item.Y;
                min.Z = min.Z <= item.Z ? min.Z : item.Z;

                max.X = max.X >= item.X ? max.X : item.X;
                max.Y = max.Y >= item.Y ? max.Y : item.Y;
                max.Z = max.Z >= item.Z ? max.Z : item.Z;
            }
            center = new Vector3(min.X + (max.X - min.X) / 2, min.Y + (max.Y - min.Y) / 2, min.Z + (max.Z - min.Z) / 2);

        }
    }
}
