﻿using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.Models;
using Elibre.CAD.StitcherApi.Models.Stitching;
using Elibre.Geometry2D;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign.Drawings
{
    public class WallState : GLState
    {
        private StitcherBuilding _stitcherBuildingObj;
        private List<Slice2D> _wallLines;
        private GLDrawableObject _wallObjectGL;
        private GLDrawableObject _studsObjectGL;
        private Slice2D _wallLineUnderMouse;
        private GLDrawableObject _postsObjectGL;

        public WallState()
        {
           // this function will be called when initializing combo box
            UpdateShownLevel(0);
        }
        public override void CenterDrawings()
        {
            if (_stitcherBuildingObj == null || _wallLines == null)
                return;

            CenterBounds(_wallLines.GetBounds());
        }

        public override void Dispose()
        {
            _wallObjectGL.Dispose();
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (HandlePanningMouseDown(e))
                return;
            return;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (HandlePanningMouseMove(e))
                return;
            return;
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (base.HandlePanningMouseUp(e))
                return;
            return;
        }
        public void UpdateShownLevel(int lvlIndex)
        {      
            _wallObjectGL?.Dispose();
            _stitcherBuildingObj = EStitcherSingleton.GetInstance.Building;
            _wallLines = GetWalls(lvlIndex);
            _wallObjectGL = new GLDrawableObject(_wallLines, System.Drawing.Color.Black, 3);
            _studsObjectGL = new GLDrawableObject(StructuralExporter.GetStudsSlices(lvlIndex), Color.Red, 2);
            _postsObjectGL = new GLDrawableObject(StructuralExporter.GetPostsSlices(lvlIndex), Color.Green, 2);
            
        }

        private List<Slice2D> GetWalls(int lvlIndex)
        {
            List<Slice2D> result=null;
            var lvl = _stitcherBuildingObj.Levels[lvlIndex];
            if (lvl!=null&& lvl.FloorPlan.Walls.Any())
            {
                result = lvl.FloorPlan.Walls.SelectMany(o => o.GetPerimeterCoreSlices()).ToList();
            }

            return result;
        }

        public override void Render()
        {
            var drawId = StartDraw();
            _wallObjectGL?.Render(drawId);
            _studsObjectGL?.Render(drawId);
            _postsObjectGL?.Render(drawId);
        }
    }
}
