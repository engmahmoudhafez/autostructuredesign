﻿using EFraming.Objects;
using Elibre.AutoStructureDesign.Drawings.DrawingClasses;
using Elibre.AutoStructureDesign.Drawings.DrawingFrame;
using Elibre.AutoStructureDesign.Drawings.Floor;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.Models;
using Elibre.CAD.StitcherApi.Algorithms.RoofAlgorithms;
using Elibre.CAD.StitcherApi.Models.Stitching;
using Elibre.Geometry2D;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign.Drawings
{
    class RoofState : GLState
    {
        private GLTruss drTrusses;
        private GLHip drHips;
        private List<GLGirder> drGirders;
        private List<GLWall> rfWalls = new List<GLWall>();
        private List<GLBeam> rfBeams = new List<GLBeam>();
        private List<GLPost> rfPosts = new List<GLPost>();
        private List<GLBeamHanger> rfBeamsHangers = new List<GLBeamHanger>();
        private List<GLGraphicsObject> texts = new List<GLGraphicsObject>();
        private GLFrame rfFrame;
        private double pointerlength;

        public RoofState()
        {
            UpdateStateItems();
        }
        public override void UpdateStateItems()
        {
            pointerlength = GetRoofWidthOrHeight(EStitcherSingleton.GetInstance.StructureBuildling);
            drTrusses = new Drawings.GLTruss(EStitcherSingleton.GetInstance.TrussContainer, EStitcherSingleton.GetInstance.Building);
            drHips = new Drawings.GLHip(EStitcherSingleton.GetInstance.TrussContainer);
            GetRoofGirders(EStitcherSingleton.GetInstance.TrussContainer);
            GetRoofBeams();
            GetRoofPosts(EStitcherSingleton.GetInstance.Building);
            GetRoofWallsAndOpenings(EStitcherSingleton.GetInstance.StructureBuildling);
        }
        public override void CenterDrawings()
        {
            var slices = rfWalls.SelectMany(o => o.getSlices()).ToList();
            Geometry2D.Rectangle2D bounds = slices.GetBounds();
            Bitmap logoBitmap = new Bitmap(Properties.Resources.Elibre_Logo);
            GLGraphicsObject logo = new GLGraphicsObject(logoBitmap);
            rfFrame = new GLFrame(bounds, logo, "XXXX XX XXX", "2/5/2019", "Roof");
            logoBitmap.Dispose();
            CenterBounds(rfFrame.getOuterRectangle());
        }

        public override void Dispose()
        {
            drTrusses.Dispose();
            rfFrame.Dispose();
            drHips.Dispose();
            for (int i = 0; i < drGirders.Count; i++)
            {
                drGirders[i].Dispose();
            }
            for (int i = 0; i < rfWalls.Count; i++)
            {
                rfWalls[i].Dispose();
            }
            for (int i = 0; i < rfWalls.Count; i++)
            {
                rfWalls[i].Dispose();
            }
            for (int i = 0; i < rfBeamsHangers.Count; i++)
            {
                rfBeamsHangers[i].Dispose();
            }
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (base.HandlePanningMouseDown(e))
                return;

        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (HandlePanningMouseMove(e))
                return;
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (base.HandlePanningMouseUp(e))
                return;

        }

        public override void Render()
        {
            int drawID = StartDraw();
            drTrusses.Render(drawID);
            if (rfFrame != null)
            {
                rfFrame.Render(drawID);
            }
            drHips.Render(drawID);
            for (int i = 0; i < drGirders.Count; i++)
            {
                drGirders[i].Render(drawID);
            }
            for (int i = 0; i < rfWalls.Count; i++)
            {
                rfWalls[i].Render(drawID);
            }
            for (int i = 0; i < rfBeams.Count; i++)
            {
                rfBeams[i].Render(drawID);
            }
            for (int i = 0; i < rfPosts.Count; i++)
            {
                rfPosts[i].Render(drawID);
            }
            for (int i = 0; i < rfBeamsHangers.Count; i++)
            {
                rfBeamsHangers[i].Render(drawID);
            }

            int fillID = StartFill();
            for (int i = 0; i < drGirders.Count; i++)
            {
                drGirders[i].RenderFill(fillID);
            }
            for (int i = 0; i < rfWalls.Count; i++)
            {
                rfWalls[i].Fill(fillID);
            }

            int textID = StartGraphics();
            drHips.RenderGraphics(textID);
            drTrusses.RenderGraphics(textID);
            for (int i = 0; i < rfWalls.Count; i++)
            {
                rfWalls[i].RenderGraphics(textID);
            }
            for (int i = 0; i < rfBeams.Count; i++)
            {
                rfBeams[i].RenderGraphics(textID);
            }
            for (int i = 0; i < drGirders.Count; i++)
            {
                drGirders[i].RenderGraphics(textID);
            }
            rfFrame.RenderGraphics(textID);

        }

        private void GetRoofWallsAndOpenings(Building Building)
        {
            int count = EStitcherSingleton.GetInstance.StructureBuildling.FloorList.Count - 1;

            List<StraightWall> walls = Building.FloorList[count].ObjectList.OfType<StraightWall>().ToList();


            for (int i = 0; i < walls.Count; i++)
            {
                GLWall wall = new GLWall(walls[i],pointerlength/30);
                List<GLWallSegment> segs = wall.GetWallSegments();

                for (int j = 0; j < segs.Count; j++)
                {
                    List<PointingPattern> pps = segs[j].GetPointingPattern();
                    if (pps == null)
                    {
                        continue;
                    }
                    for (int k = 0; k < pps.Count; k++)
                    {
                        if (pps[k].IsPointerIntersects(texts))
                        {

                        }
                        texts.Add(pps[k].GetGraphicObject());
                    }
                }

                rfWalls.Add(wall);

            }
        }
        private void GetRoofGirders(RoofTrussContainer TrussContainer)
        {
            drGirders = new List<GLGirder>();
            for (int i = 0; i < TrussContainer.TrussAreas.Count; i++)
            {
                for (int j = 0; j < TrussContainer.TrussAreas[i].Girders.Count; j++)
                {
                    GLGirder girder = new GLGirder(TrussContainer.TrussAreas[i].Girders[j]);
                    texts.Add(girder.GetGraphicObject());
                    drGirders.Add(girder);
                }
            }
            for (int i = 0; i < TrussContainer.HipTrussAreas.Count; i++)
            {
                GLGirder girder = new GLGirder(TrussContainer.HipTrussAreas[i].GirderTruss);
                texts.Add(girder.GetGraphicObject());
                drGirders.Add(girder);

                for (int j = 0; j < TrussContainer.HipTrussAreas[i].PerbindicularTruss.Count; j++)
                {
                    for (int k = 0; k < TrussContainer.HipTrussAreas[i].PerbindicularTruss[j].Girders.Count; k++)
                    {
                        GLGirder girder_t = new GLGirder(TrussContainer.HipTrussAreas[i].PerbindicularTruss[j].Girders[k]);
                        texts.Add(girder.GetGraphicObject());
                        drGirders.Add(girder_t);
                    }
                }
            }

            List<GLGraphicsObject> hipstexts = drHips?.GetGraphicObjects();

            for (int i = 0; i < hipstexts.Count; i++)
            {
                texts.Add(hipstexts[i]);
            }

        }
        private void GetRoofBeams()
        {
            rfBeams.Clear();

            int count = EStitcherSingleton.GetInstance.StructureBuildling.FloorList.Count - 1;
            List<Beam> beams = EStitcherSingleton.GetInstance.StructureBuildling.FloorList[count].ObjectList.OfType<Beam>().ToList();
            for (int j = 0; j < beams.Count; j++)
            {
                GLBeam beam_ = new GLBeam(beams[j]);
                while (beam_.GetGraphicObject().IsIntersects(texts))
                {
                    TranslateGraphicObject(beam_.GetGraphicObject(),beam_);
                }
                texts.Add(beam_.GetGraphicObject());
                rfBeams.Add(beam_);
                for (int k = 0; k < beams[j].HangerList.Count; k++)
                {
                    rfBeamsHangers.Add(new GLBeamHanger(beams[j].HangerList[k]));
                }
            }
        }
        private void GetRoofPosts(StitcherBuilding Building)
        {
            rfPosts.Clear();
            int count = EStitcherSingleton.GetInstance.StructureBuildling.FloorList.Count - 1;
            List<Post> posts = EStitcherSingleton.GetInstance.StructureBuildling.FloorList[count].ObjectList.OfType<Post>().ToList();

            for (int j = 0; j < posts.Count; j++)
            {
                GLPost post = new GLPost(posts[j],pointerlength/30);
                rfPosts.Add(post);
            }
        }
        private double GetRoofWidthOrHeight(Building Building_)
        {
            List<StraightWall> walls = Building_.FloorList[Building_.FloorList.Count - 1].ObjectList.OfType<StraightWall>().ToList();
            var segments = walls.SelectMany(o => o.SegmentList).ToList();
            double min_X = segments.Min(o => o.StartPoint.x);
            double max_X = segments.Max(o => o.EndPoint.x);
            double min_Y = segments.Max(o => o.StartPoint.y);
            double max_Y = segments.Min(o => o.EndPoint.y);

            double width = max_X - min_X;
            double height = Math.Abs(max_Y - min_Y);

            if (width > height)
            {
                return width;
            }
            else
            {
                return height;
            }

        }

        private void TranslateGraphicObject(GLGraphicsObject text, GLBeam beam)
        {
            List<Vector2> copy = new List<Vector2>(text.Positions);
            for (int i = 0; i <copy.Count; i++)
            {
                var v = copy[i];
                Vector3 v3 = new Vector3(v.X, v.Y, 0);
                v3 = Vector3.Transform(v3, text.ModelMatrix);
               copy[i] = new Vector2(v3.X, v3.Y);
            }
            Vector2 translate = new Vector2(copy[2].X - copy[0].X, copy[2].Y - copy[0].Y);
            translate.Normalize();

            text.Translate(translate*(float)(beam.core_slice.Length/85));

        }

    }
}
