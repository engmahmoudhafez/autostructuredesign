﻿using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.Models;
using Elibre.CAD.StitcherApi.Helpers;
using Elibre.CAD.StitcherApi.Models.Stitching;
using Elibre.Geometry2D;
using Elibre.Stitcher.Models.Stitching;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign.States
{
    public class AnalysisState : GLState
    {
        #region Fields

        public Mode CurrentMode;
        private StitcherBuilding _stitcherBuildingObj;
        private int _levelIndex;
        //walls/shear walls
        private GLDrawableObject _wallObjectGL;
        private GLFillableObject _shearWallsObjectGL;
        //drawn beams
        private GLDrawableObject _beamsGL;
        private GLFillableObject _beamsFillGL;
        //windows/doors
        private GLDrawableObject _windows_DoorsLinesGL;
        private GLFillableObject _windows_DoorsPolysGL;

        //drawn columns
        private GLDrawableObject _columnsGL;
        private GLFillableObject _columnsFillGL;
        //joist area
        private GLDrawableObject _joistAreaLinesGL;
        private GLFillableObject _joistAreaPolysGL;
        //studs
        private GLDrawableObject _studsGL;

        private Slice2D _drawSlice;
        private GLDrawableObject _drawSliceGL;
        //selection/deselection
        private Slice2D _selectionRect;
        private GLRectangle _selectionGLRect;
        private Slice2D _deSelectionRect;
        private GLRectangle _deSelectionGLRect;

        //snapper
        private Point2DSnapper _snapper { get; set; }
        private Point2DSnappingInfo _snapperInfo { get; set; }
        private GLRectangle _snipperGL;
        #endregion

        #region enums


        private enum DrawLayer
        {
            ShearWalls,
            WallLines,
            Window_DoorsPolys,
            Window_DoorsLines,
            JoistAreaPolys,
            JoistAreaLines,
            ColumnPolys,
            ColumnLines,
            BeamLines,
            BeamPolys,
            DrawSlice,
            WallsStuds,
            SelectionRect,
            DeSelectionRect,
            SnapperRect,
        }
        public enum Mode
        {
            JoistArea, Beam, Post, ShearWall
        }

        #endregion

        #region Constractor

        public AnalysisState()
        {
            var options = new SnappingOptions(Point2DSnappingType.Vertical | Point2DSnappingType.Horizontal | Point2DSnappingType.MiddlePoint | Point2DSnappingType.EndPoint | Point2DSnappingType.StartPoint | Point2DSnappingType.Perpindicular | Point2DSnappingType.Nearest | Point2DSnappingType.Perpindicular);
            _snapper = new Point2DSnapper(this, options);
            UpdateBuilding();
            _levelIndex = 0;
        }
        #endregion

        #region Methods

        public void UpdateBuilding()
        {
            _stitcherBuildingObj = EStitcherSingleton.GetInstance.Building;

        }

        private List<Slice2D> GetSnapLines()
        {
            var lines = _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Walls.SelectMany(o => o.GetPerimeterCoreSlices()).ToList();
            lines.AddRange(_stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Beams.SelectMany(o => o.Get2DSlices()));
            lines.AddRange(_stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Columns.SelectMany(o => o.Get2DSlices()));
            lines.AddRange(_stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Columns.SelectMany(o => o.GetX()));
            lines.AddRange(_stitcherBuildingObj.Levels[_levelIndex].FloorPlan.JoistAreas.SelectMany(o => o.OuterPoly.Slices));
            return lines;
        }

        internal void UpdateShownDrawing(int selectedIndex)
        {
            _levelIndex = selectedIndex;
            _wallObjectGL?.Dispose();
            var walls = _stitcherBuildingObj.Levels[selectedIndex].FloorPlan.Walls.SelectMany(o => o.GetPerimeterCoreSlices()).ToList();
            _wallObjectGL = new GLDrawableObject(walls, System.Drawing.Color.Black);
            _wallObjectGL.Z_Order = (int)DrawLayer.WallLines * 0.01f;
            UpdateWindows_DoorsGL();
            UpdateShearWallsGl();
            UpdateBeamsGL();
            UpdateColumnsGL();
            UpdateShownJoistArea();
            UpdateStudsGL();
            CenterDrawings();
        }

        private void UpdateWindows_DoorsGL()
        {
            //update _windows_DoorsLinesGL
            _windows_DoorsLinesGL?.Dispose();
            var windows_DoorsLines = _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Walls.SelectMany(o => o.Openings).SelectMany(o => o.ShownLines).ToList();
            var windows_doorsOuters = _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Walls.SelectMany(o => o.Openings.Select(e => e.SingleLine2D.GetRectanglePoly(o.Thickness))).ToList();
            windows_DoorsLines.AddRange(windows_doorsOuters.SelectMany(o => o.Slices));

            _windows_DoorsLinesGL = new GLDrawableObject(windows_DoorsLines, Color.Black, 1f);
            _windows_DoorsLinesGL.Z_Order = (int)DrawLayer.Window_DoorsLines * 0.01f;

            //update _windows_DoorsPolysGL
            _windows_DoorsPolysGL?.Dispose();
            _windows_DoorsPolysGL = new GLFillableObject(windows_doorsOuters, Color.White);
            _windows_DoorsPolysGL.Z_Order = (int)DrawLayer.Window_DoorsPolys * 0.01f;

        }
        private void UpdateStudsGL()
        {
            _studsGL?.Dispose();
            _studsGL = new GLDrawableObject(StructuralExporter.GetStudsSlices(_levelIndex), Color.Blue, 2);
            _studsGL.Z_Order = (int)DrawLayer.WallsStuds * 0.01f;
        }
        private void UpdateColumnsGL()
        {
            _columnsGL?.Dispose();
            var columnLines = _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Columns.SelectMany(o => o.Get2DSlices()).ToList();
            columnLines.AddRange(_stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Columns.SelectMany(o => o.GetX()));

            _columnsGL = new GLDrawableObject(columnLines, Color.Black, 2f);
            _columnsGL.Z_Order = (int)DrawLayer.ColumnLines * 0.01f;

            _columnsFillGL?.Dispose();
            var columnPolys = _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Columns.Select(o => o.ColumnOuterPolygon.ToSlicePolygon()).ToList();
            _columnsFillGL = new GLFillableObject(columnPolys, Color.LightGray);
            _columnsFillGL.Z_Order = (int)DrawLayer.ColumnPolys * 0.01f;
        }
        private void UpdateBeamsGL()
        {
            _beamsGL?.Dispose();
            var beamLines = _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Beams.SelectMany(o => o.Get2DSlices()).ToList();
            _beamsGL = new GLDrawableObject(beamLines, Color.Black, 2f);
            _beamsGL.Z_Order = (int)DrawLayer.BeamLines * 0.01f;

            _beamsFillGL?.Dispose();
            var beamsPolys = _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Beams.Select(o => o.Rectangle.ToSlicePolygon()).ToList();
            _beamsFillGL = new GLFillableObject(beamsPolys, Color.LightSkyBlue);
            _beamsFillGL.Alpha = 0.9f;
            _beamsFillGL.Z_Order = (int)DrawLayer.BeamPolys * 0.01f;

        }
        private void UpdateShearWallsGl()
        {
            var shearWalls = _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Walls.SelectMany(e => e.WallParts.Where(o => o.IsShear).Select(ee => ee.CenterLine.GetRectanglePoly(e.Thickness))).ToList();
            _shearWallsObjectGL = new GLFillableObject(shearWalls, System.Drawing.Color.Red);
            _shearWallsObjectGL.Z_Order = (int)DrawLayer.ShearWalls * 0.01f;
        }
        private void UpdateShownJoistArea()
        {
            _joistAreaLinesGL?.Dispose();
            var joistOuters = _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.JoistAreas.SelectMany(o => o.OuterPoly.Slices).ToList();
            _joistAreaLinesGL = new GLDrawableObject(joistOuters, Color.Orange, 2f);
            _joistAreaLinesGL.Z_Order = (int)DrawLayer.JoistAreaLines * 0.01f;

            _joistAreaPolysGL?.Dispose();
            var joistPolys = _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.JoistAreas.Select(o => o.OuterPoly).ToList();
            _joistAreaPolysGL = new GLFillableObject(joistPolys, Color.LightGray);
            _joistAreaPolysGL.Alpha = 0.5f;
            _joistAreaPolysGL.Z_Order = (int)DrawLayer.JoistAreaPolys * 0.01f;
        }
        #endregion

        #region Overrides

        public override void Render()
        {
            _snipperGL?.Dispose();
            if ((CurrentMode == Mode.Beam || CurrentMode == Mode.Post || CurrentMode == Mode.JoistArea) && _snapperInfo != null && _snapperInfo.SnappingType != Point2DSnappingType.None && _snapperInfo.SnappingType != Point2DSnappingType.Horizontal && _snapperInfo.SnappingType != Point2DSnappingType.Vertical)
            {
                var SnappingWidth = 10;
                var location = _snapperInfo.TargetPoint;
                var loc = (new Vector3((float)location.X, (float)location.Y, 0));
                var rectangle = new Rectangle2D(loc.X - SnappingWidth / (2.0 * ZoomFactor), loc.Y - SnappingWidth / (2.0 * ZoomFactor), SnappingWidth / ZoomFactor, SnappingWidth / ZoomFactor);
                _snipperGL = new GLRectangle(rectangle, Color.Green, Color.Yellow, 2, 1, 1);
                _snipperGL.Z_Order = (int)DrawLayer.SnapperRect * 0.01f;
            }

            int fillID = StartFill();
            _shearWallsObjectGL?.Render(fillID);
            _beamsFillGL?.Render(fillID);
            _columnsFillGL?.Render(fillID);
            _joistAreaPolysGL?.Render(fillID);
            _windows_DoorsPolysGL?.Render(fillID);

            var drawID = StartDraw();
            _joistAreaLinesGL?.Render(drawID);
            _wallObjectGL?.Render(drawID);
            _selectionGLRect?.Dispose();
            if (_selectionRect != null)
            {
                _selectionGLRect = new GLRectangle(_selectionRect, Color.Orange, Color.Orange, 1, 1, 100.0f / 255.0f);
                _selectionGLRect.Z_Order = (int)DrawLayer.SelectionRect * 0.01f;
            }
            _deSelectionGLRect?.Dispose();
            if (_deSelectionRect != null)
            {
                _deSelectionGLRect = new GLRectangle(_deSelectionRect, Color.Gray, Color.Gray, 1, 1, 100.0f / 255.0f);
                _deSelectionGLRect.Z_Order = (int)DrawLayer.DeSelectionRect * 0.01f;
            }
            _drawSliceGL?.Dispose();
            if (_drawSlice != null)
            {
                _drawSliceGL = new GLDrawableObject(new List<Slice2D>() { _drawSlice }, Color.Gray);
                _drawSliceGL.Z_Order = (int)DrawLayer.DrawSlice * 0.01f;
                _drawSliceGL.Render(drawID);
            }
            _selectionGLRect?.Draw(drawID);
            _deSelectionGLRect?.Draw(drawID);
            _beamsGL?.Render(drawID);
            _columnsGL?.Render(drawID);
            _snipperGL?.Draw(drawID);
            _windows_DoorsLinesGL?.Render(drawID);
            _studsGL?.Render(drawID);

        }
        public override void CenterDrawings()
        {
            var lines = _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Walls.SelectMany(o => o.Rectangle2D).GetBounds();
            CenterBounds(lines);
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (HandlePanningMouseDown(e))
                return;
            var currentPt = ToWorld(e.Location.X, e.Location.Y);
            if (e.Button == MouseButtons.Left)
            {
                switch (CurrentMode)
                {
                    case Mode.JoistArea:
                        var lines = GetSnapLines();
                        _snapperInfo = _snapper.Snap(lines, e.Location);
                        _selectionRect = new Slice2D(_snapperInfo.TargetPoint, _snapperInfo.TargetPoint);

                        break;
                    case Mode.Beam:
                        break;
                    case Mode.Post:
                        break;
                    case Mode.ShearWall:
                        if (e.Button == MouseButtons.Left)
                            _selectionRect = new Slice2D(currentPt, currentPt);
                        break;
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                _deSelectionRect = new Slice2D(currentPt, currentPt);

            }
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (HandlePanningMouseMove(e))
                return;
            var currentPt = ToWorld(e.Location.X, e.Location.Y);
            if (_deSelectionRect != null)
                _deSelectionRect.End = currentPt;
            switch (CurrentMode)
            {
                case Mode.JoistArea:
                    var snapLines = GetSnapLines();
                    _snapperInfo = _snapper.Snap(snapLines, e.Location);
                    if (_selectionRect != null)
                        _selectionRect.End = _snapperInfo.TargetPoint;

                    break;
                case Mode.Beam:
                    var lines = GetSnapLines();
                    if (_drawSlice != null)
                    {
                        _snapperInfo = _snapper.Snap(lines, e.Location, _drawSlice.Start);
                        _drawSlice.End = _snapperInfo.TargetPoint;
                    }
                    else
                    {
                        _snapperInfo = _snapper.Snap(lines, e.Location);
                    }
                    break;
                case Mode.Post:
                    var singleLines = _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Walls.Select(o => o.SingleWall2D).ToList();
                    _snapperInfo = _snapper.Snap(singleLines, e.Location);

                    break;
                case Mode.ShearWall:
                    {
                        if (_selectionRect != null)
                            _selectionRect.End = currentPt;
                        break;
                    }
            }
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (base.HandlePanningMouseUp(e))
                return;
            var currentPt = ToWorld(e.Location.X, e.Location.Y);
            StructuralExporter.ResetStructureBuilding();
            if (e.Button == MouseButtons.Left)
            {
                switch (CurrentMode)
                {
                    case Mode.JoistArea:
                        #region joist area
                        if (_selectionRect != null)
                        {
                            if (_stitcherBuildingObj.Levels[_levelIndex].FloorPlan.JoistAreas.Any(o => o.OuterPoly.Intersect(_selectionRect.GetBounds())))
                                return;
                            _selectionRect.End = _snapperInfo.TargetPoint;
                            var poly = _selectionRect.GetBounds().ToSlicePolygon2D();
                            if (!poly.ToPolygon2D().PolygonArea().IsApproxZero())
                            {
                                var joistArea = new JoistArea() { OuterPoly = poly };
                                _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.JoistAreas.Add(joistArea);

                                UpdateShownJoistArea();
                            }
                            _selectionRect = null;
                            _selectionGLRect = null;
                        }
                        #endregion
                        break;
                    case Mode.Beam:
                        #region Beam

                        if (_drawSlice == null)
                            _drawSlice = new Slice2D(_snapperInfo.TargetPoint, _snapperInfo.TargetPoint);
                        else if (!_drawSlice.Length.IsApproxZero())
                        {
                            _drawSlice.End = _snapperInfo.TargetPoint;
                            //get beam type
                            var wallThickness = _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Walls[0].Thickness;
                            var type = _stitcherBuildingObj.Template.BeamsSymbols.FirstOrDefault(o => o.Width.IsApproxEqualTo(wallThickness));
                            if (type == null)
                            {
                                var thicknessString = UnitsHelper.ShowLengthText(_stitcherBuildingObj.Template, wallThickness);
                                type = new Stitcher.Revit.RevitNewSymbol() { Width = wallThickness, SymbolName = $"Elibre New Beam Type {wallThickness}" };
                                _stitcherBuildingObj.Template.BeamsSymbols.Add(type);
                            }
                            var poly = _drawSlice.GetRectanglePoly(type.Width).ToPolygon2D();
                            var beam = new StitcherBeam(poly)
                            {
                                BeamLine2D = _drawSlice,
                                LevelId = _stitcherBuildingObj.Levels[_levelIndex].Id,
                                RevitTypeName = type.SymbolName
                            };
                            _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Beams.Add(beam);
                            _drawSlice = null;
                            UpdateBeamsGL();
                        }
                        #endregion
                        break;
                    case Mode.Post:
                        #region Post
                        if (_stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Columns.Any(o => o.ColumnOuterPolygon.Contains(currentPt)))
                            return;
                        var wallUnderMouse = _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Walls.FirstOrDefault(o => o.SingleWall2D.Contains(_snapperInfo.TargetPoint));
                        Polygon2D postPoly = null;
                        bool isInWall;
                        string revitType = "";
                        if (wallUnderMouse != null)
                        {
                            isInWall = true;
                            //get post type
                            var thickness = wallUnderMouse.GetCoreThickness();
                            var depth = thickness;
                            var type = _stitcherBuildingObj.Template.ColumnSymbols.FirstOrDefault(o => o.Width.IsApproxEqualTo(thickness));
                            if (type != null)
                            {
                                revitType = type.SymbolName;
                                depth = type.Height;
                            }
                            else
                            {
                                var thicknessString = UnitsHelper.ShowLengthText(_stitcherBuildingObj.Template, thickness);
                                var newType = new Stitcher.Revit.RevitNewSymbol()
                                {
                                    SymbolName = $"Elibre New Post Type {thicknessString}x{thicknessString}",
                                    Width = thickness,
                                    Height = thickness,
                                };
                                _stitcherBuildingObj.Template.ColumnSymbols.Add(newType);
                                revitType = newType.SymbolName;
                            }
                            var vec = wallUnderMouse.SingleWall2D.GetUnitVector();
                            var normVec = vec.GetNormalVec();
                            var ptOnWall = _snapperInfo.TargetPoint;
                            var pt1 = ptOnWall + normVec * (thickness / 2) - vec * (depth / 2);
                            var pt2 = pt1 + vec * depth;
                            var pt3 = pt2 - normVec * thickness;
                            var pt4 = pt3 - vec * depth;
                            postPoly = new Polygon2D(new List<Point2D>() { pt1, pt2, pt3, pt4 });
                        }
                        else
                        {
                            isInWall = false;
                            revitType = _stitcherBuildingObj.Template.ColumnSymbols[0].SymbolName;
                            var rect = new Rectangle2D(currentPt, new Size2D());
                            rect.Inflate(_stitcherBuildingObj.Template.ColumnSymbols[0].Width / 2, _stitcherBuildingObj.Template.ColumnSymbols[0].Height / 2);
                            postPoly = rect.ToPolygon2D();
                        }
                        var post = new StitcherColumn(postPoly)
                        {
                            BaseLevelId = _stitcherBuildingObj.Levels[_levelIndex].Id,
                            IsInWall = isInWall,
                            TopLevelId = _stitcherBuildingObj.TopWallLevels[_levelIndex].Id,
                            RevitTypeName = revitType,
                        };
                        _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Columns.Add(post);
                        UpdateColumnsGL();
                        #endregion
                        break;
                    case Mode.ShearWall:
                        #region Shear wall

                        if (_selectionRect != null)
                        {
                            var bounds = _selectionRect.GetBounds();
                            var boundsLines = bounds.ToSlices();
                            var intersectingParts = _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Walls.SelectMany(o => o.WallParts.Where(x => x.CanBeShear)).Where(o => boundsLines.Any(m => o.CenterLine.IsIntersects(m)) || bounds.Contains(o.CenterLine)).ToList();
                            if (intersectingParts.Any(o => o.IsShear))
                                intersectingParts.ForEach(o => o.IsShear = false);
                            else
                                intersectingParts.ForEach(o => o.IsShear = true);

                            UpdateShearWallsGl();
                            _selectionRect = null;
                            _selectionGLRect = null;
                        }
                        #endregion
                        break;
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                if (_deSelectionRect != null)
                {
                    var bounds = _deSelectionRect.GetBounds();
                    if (!bounds.GetArea().IsApproxZero())
                    {
                        var boundsSlices = bounds.ToSlices();
                        var boundsPoly = bounds.ToPolygon2D();
                        _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Beams.RemoveAll(o => o.Rectangle.IsIntersect(boundsPoly));
                        _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.Columns.RemoveAll(o => o.ColumnOuterPolygon.IsIntersect(boundsPoly));
                        _stitcherBuildingObj.Levels[_levelIndex].FloorPlan.JoistAreas.RemoveAll(o => o.OuterPoly.ToPolygon2D().IsIntersect(boundsPoly));
                        UpdateBeamsGL();
                        UpdateColumnsGL();
                        UpdateShownJoistArea();
                    }
                }
                _selectionGLRect = null;
                _selectionRect = null;
                _deSelectionGLRect = null;
                _deSelectionRect = null;
                _drawSlice = null;
            }
        }

        public override void Dispose()
        {
            _wallObjectGL?.Dispose();
            _shearWallsObjectGL?.Dispose();
            _beamsGL?.Dispose();
            _beamsFillGL?.Dispose();
            _drawSliceGL?.Dispose();
            _selectionGLRect?.Dispose();
            _snipperGL?.Dispose();

        }
        #endregion
    }
}
