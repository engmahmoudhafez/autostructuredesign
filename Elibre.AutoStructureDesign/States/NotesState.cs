﻿using Elibre.AutoStructureDesign.Drawings.DrawingClasses;
using Elibre.AutoStructureDesign.Drawings.DrawingFrame;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.Properties;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign.States
{
    class NotesState : GLState
    {
        private List<GLBitmapDrawing> bitmapDrawings;

        public NotesState()
        {
            bitmapDrawings = new List<GLBitmapDrawing>();
            GLBitmapDrawing note1 = new GLBitmapDrawing(Resources.Notes_1_2,"Notes");
            bitmapDrawings.Add(note1);
            GLBitmapDrawing note2 = new GLBitmapDrawing(Resources.Notes_2,"Notes");
            note2.Translate(new Vector2(0, -((float)note1.getOuterRectangle().Height + 100)));
            bitmapDrawings.Add(note2);
        }
        public override void CenterDrawings()
        {
            var rect = bitmapDrawings.First().getOuterRectangle();
            CenterBounds(rect);
        }

        public override void Dispose()
        {
            bitmapDrawings.ForEach(o => o.Dispose());
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (base.HandlePanningMouseDown(e))
                return;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (HandlePanningMouseMove(e))
                return;
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (base.HandlePanningMouseUp(e))
                return;
        }

        public override void Render()
        {
            int graphicsID = StartGraphics();
            bitmapDrawings.ForEach(o=>o.RenderGraphics(graphicsID));

            int drawId = StartDraw();
            bitmapDrawings.ForEach(o => o.RenderDrawable(drawId));
        }
    }
}
