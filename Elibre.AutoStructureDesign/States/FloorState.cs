﻿using Elibre.AutoStructureDesign.Drawings.DrawingFrame;
using Elibre.AutoStructureDesign.Drawings.Floor;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.AutoStructureDesign.Models;
using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign.States
{
    public class FloorState : GLState
    {
        List<GLFloor> floors;
        GLFloor activeFloor;
        private GLFrame flFrame;

        public FloorState()
        {
            UpdateStateItems();
        }

        public override void UpdateStateItems()
        {
            var eframingLevels = EStitcherSingleton.GetInstance.StructureBuildling.FloorList;
            floors = new List<GLFloor>();
            foreach (var item in eframingLevels)
            {
                GLFloor floor = new GLFloor(item);
                floors.Add(floor);
            }
        }

        public void setActiveLevel(int index)
        {
            activeFloor = floors[index];
        }
        public override void CenterDrawings()
        {
            Rectangle2D bounds = activeFloor.getBounds();
            Bitmap logoBitmap = new Bitmap(Properties.Resources.Elibre_Logo);
            GLGraphicsObject logo = new GLGraphicsObject(logoBitmap);
            flFrame = new GLFrame(bounds, logo, "XXXX XX XXX", "2/5/2019", "Floor 1");
            logoBitmap.Dispose();

            CenterBounds(flFrame.getOuterRectangle());
        }

        public override void Dispose()
        {
            flFrame.Dispose();
            foreach (var item in floors)
            {
                item.Dispose();
            }
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (base.HandlePanningMouseDown(e))
                return;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (HandlePanningMouseMove(e))
                return;
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (base.HandlePanningMouseUp(e))
                return;
        }

        public override void Render()
        {
            int drawID = StartDraw();
            activeFloor.Render(drawID);
            if (flFrame != null)
            {
                flFrame.Render(drawID);
            }
            int textID = StartGraphics();
            activeFloor.RenderGraphics(textID);
            flFrame.RenderGraphics(textID);

            int FillID = StartFill();
            activeFloor.Fill(FillID);
        }
    }
}
