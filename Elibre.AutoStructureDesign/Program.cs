﻿using Elibre.AutoStructureDesign.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elibre.AutoStructureDesign
{
    static class Program
    {
        enum _Process_DPI_Awareness
        {
            Process_DPI_Unaware = 0,
            Process_System_DPI_Aware = 1,
            Process_Per_Monitor_DPI_Aware = 2
        }

        [DllImport("user32.dll")]
        private static extern bool SetProcessDPIAware();

        [DllImport("shcore.dll")]
        private static extern int SetProcessDpiAwareness(_Process_DPI_Awareness value);
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AllowHDPI();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
        private static void AllowHDPI()
        {
            #region HDPI Support - M.Hassan Dec. 11, 2018

            int winVer = Environment.OSVersion.Version.Major;

            if (winVer >= 6 /* Windows Vista (6), 7, 8, 8.1, 10 */)
            {
                if (winVer >= 6.3 /* 8.1 */) // Set dpi for each monitor - [good; starts from windows 8.1]
                {
                    try
                    {
                        SetProcessDpiAwareness(_Process_DPI_Awareness.Process_Per_Monitor_DPI_Aware);
                    }
                    catch { try { SetProcessDPIAware(); } catch { } } // In case of failure of the new function
                }
                else
                {
                    try { SetProcessDPIAware(); } catch { } // OLD, starts from Vista
                }
            }

            #endregion
        }


    }
}
