﻿using EFraming;
using EFraming.Objects;
using Elibre.AutoStructureDesign.Libraries.GLUtils;
using Elibre.CAD.StitcherApi.Algorithms.RoofAlgorithms;
using Elibre.CAD.StitcherApi.Models.Stitching;

namespace Elibre.AutoStructureDesign.Models
{
    public sealed class EStitcherSingleton
    {
        public bool IsStitcherModel { get; set; }
        public StitcherBuilding Building { get; set; }
        public Building StructureBuildling { get; set; }
        public RoofTrussContainer TrussContainer { get; set; }
        public GLStateRenderer GlobalGLRenderer { get; private set; }
        public GLStatesManager StatesManager { get; private set; }

        private static EStitcherSingleton _stitcherSingleton = null;
        private static readonly object padlock = new object();
        EStitcherSingleton()
        {
            GlobalGLRenderer = new GLStateRenderer();
            StatesManager = new GLStatesManager();
            GlobalGLRenderer.setStatesManager(StatesManager);
        }

        public static EStitcherSingleton GetInstance
        {
            get
            {
                lock (padlock)
                {
                    if (_stitcherSingleton == null)
                    {
                        _stitcherSingleton = new EStitcherSingleton();
                    }
                    return _stitcherSingleton;
                }
            }
        }

    }
}
