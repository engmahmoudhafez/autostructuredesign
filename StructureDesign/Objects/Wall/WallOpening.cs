﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StructureDesign
{
    public class WallOpening : StraightWallSegment
    {
        public OpeningType OpeningType { get; set; }
        public StraightWallSegment BottomSegment { get; set; }
        public StraightWallSegment TopSegment { get; set; }
        public double SillHeight { get; set; }
        public double OpeningHeight { get; set; }
        public double HeadHeight { get; set; }
        public Header Header { get; set; }
        public Header Sill { get; set; }
        public Stud StartHeaderStud { get; set; }
        public Stud EndHeaderStud { get; set; }


        public WallOpening(Point2D startPoint, Point2D endPoint, Section section, OpeningType type, StraightWall parent, double headerHeight, double openingHeight)
            : base(startPoint, endPoint, section, parent)
        {
            OpeningType = type;
            HeadHeight = headerHeight;
            OpeningHeight = openingHeight;
        }
        public void Create()
        {

            CalculateSillHeight();
            CreateHeader();
            CreateStartEndStuds();
            CreateTopSegment();

            if (OpeningType == OpeningType.Window)
            {
                CreateSill();
                CreateBottomSegment();
            }

        }

        private void CreateBottomSegment()
        {
            var v = this.ParentFramingWall.Vector;
            var sp = StartPoint + StartHeaderStud.Section.Width * v;
            var ep = EndPoint - EndHeaderStud.Section.Width * v;

            BottomSegment = new StraightWallSegment(sp, ep, this.Section, this.ParentFramingWall);
            double z1 = ParentFramingWall.BottomZLevel + SillHeight - Sill.Section.Width;
            double z2 = ParentFramingWall.BottomZLevel + BottomWallPlates.Count * BottomWallPlates[0].Depth;
            BottomSegment.FillWithStuds(z1, z2);
        }

        private void CreateSill()
        {
            var v = this.ParentFramingWall.Vector;
            var sp = StartPoint + StartHeaderStud.Section.Width * v;
            var ep = EndPoint - EndHeaderStud.Section.Width * v;
            var z = SillHeight - this.Section.Width + ParentFramingWall.BottomZLevel;
            Sill = new Header(sp, ep, z, this);
            Sill.CreateGeometry3D();
        }

        private void CreateTopSegment()
        {
            TopSegment = new StraightWallSegment(StartPoint, EndPoint, this.Section, this.ParentFramingWall);
            double z1 = ParentFramingWall.TopZLevel - TopWallPlates.Count * TopWallPlates[0].Depth;
            double z2 = ParentFramingWall.BottomZLevel + HeadHeight + Header.Section.Width;
            TopSegment.FillWithStuds(z1, z2);
        }

        private void CreateStartEndStuds()
        {
            var z1 = ParentFramingWall.BottomZLevel + HeadHeight;
            var z2 = ParentFramingWall.BottomZLevel + BottomWallPlates.Count * BottomWallPlates[0].Depth;
            var v = ParentFramingWall.Vector;
            var p1 = StartPoint + ((this.Section.Width / 2) * v);
            var p2 = EndPoint - ((this.Section.Width / 2) * v);
            StartHeaderStud = new Stud(p1, this.Section.Width, this.Section.Depth, v, z1, z2);
            EndHeaderStud = new Stud(p2, this.Section.Width, this.Section.Depth, v, z1, z2);
        }

        private void CalculateSillHeight()
        {
            if (OpeningType == OpeningType.Door)
            {
                SillHeight = 0;
            }
            else if (OpeningType == OpeningType.Window)
            {
                SillHeight = HeadHeight - OpeningHeight;
            }
        }

        private void CreateHeader()
        {
            Header = new Header(HeadHeight + ParentFramingWall.BottomZLevel, this);
            Header.CreateGeometry3D();

        }

        public List<Stud> GetStudList()
        {
            var result = new List<Stud>();
            result.Add(StartHeaderStud);
            result.Add(EndHeaderStud);
            result.AddRange(TopSegment?.StudList);
            if (OpeningType == OpeningType.Window)
            {
                result.AddRange(BottomSegment?.StudList);
            }
            return result;
        }
    }
}
