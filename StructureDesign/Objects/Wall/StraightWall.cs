﻿using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Text;

namespace StructureDesign
{
    public class StraightWall : FramingObject
    {
        public List<StraightWallSegment> SegmentList { get; set; }

        private List<StraightWall> _connectedWallsAtStart;
        private List<StraightWall> _connectedWallsAtEnd;

        public List<StraightWall> ConnectedWallsAtEnd
        {
            get
            {
                if (_connectedWallsAtEnd == null)
                {
                    if (ConnectedWallsAtEndIds == null || !ConnectedWallsAtEndIds.Any())
                    {
                        return new List<StraightWall>();
                    }
                    _connectedWallsAtEnd = new List<StraightWall>();
                    foreach (var id in ConnectedWallsAtEndIds)
                    {
                        _connectedWallsAtEnd.Add(Parent.walls.Find(e => e.Id == id));
                    }
                }

                return _connectedWallsAtEnd;
            }
            //set { _connectedWallsAtStart = value; }
        }
        public List<StraightWall> ConnectedWallsAtStart
        {
            get
            {
                if (_connectedWallsAtStart == null)
                {
                    if (ConnectedWallsAtStartIds == null || !ConnectedWallsAtStartIds.Any())
                    {
                        return new List<StraightWall>();
                    }
                    _connectedWallsAtStart = new List<StraightWall>();
                    foreach (var id in ConnectedWallsAtStartIds)
                    {
                        _connectedWallsAtStart.Add(Parent.walls.Find(e => e.Id == id));
                    }
                }

                return _connectedWallsAtStart;
            }
            //set { _connectedWallsAtStart = value; }   
        }
        public bool IsInteriorWall { get; set; }
        public int NumTopPlates { get; set; }
        public int NumBottomPlates { get; set; }
        public double StartStudSpacing { get; set; }
        public double EndStudSpacing { get; set; }
        public double StudSpacing { get; set; }
        public Section Section { get; set; }
        public double Thickness { get; set; }
        public List<WallOpening> WallOpenings { get; set; }
        public List<Point2D> PerimeterCorePoints { get; set; }
        public Line2D CenterLine { get; set; }
        public double TopZLevel { get; set; }
        public double BottomZLevel { get; set; }
        public bool IsShear { get; set; }
        private List<Guid> ConnectedWallsAtStartIds { get; set; }
        private List<Guid> ConnectedWallsAtEndIds { get; set; }
        public RoofTrussContainer Parent { get; set; }
        public Vector2D Vector
        {
            get { return new Vector2D(CenterLine.End - CenterLine.Start).GetUnit(); }
        }

        public StraightWall(StitcherWall wall, StitcherBuilding building, RoofTrussContainer container)
        {
            #region Initilization

            Id = wall.Id;
            Parent = container;
            ConnectedWallsAtStartIds = wall.NeighborWallsIdsFromStart;
            ConnectedWallsAtEndIds = wall.NeighborWallsIdsFromEnd;
            Thickness = wall.Thickness;
            IsInteriorWall = wall.IsInteriorWall;
            PerimeterCorePoints = wall.GetPerimeterCorePoints();
            Section = new Section(Thickness, 1.5);
            IsShear = wall.IsBearing;
            NumTopPlates = 2;
            NumBottomPlates = 3;
            StartStudSpacing = 0;
            EndStudSpacing = 0;
            StudSpacing = 20;
            TopZLevel = building.Levels[wall.LevelIndex].Elevation - wall.TopLevelOffset;
            BottomZLevel = building.Levels[wall.LevelIndex].Elevation - wall.UnConnectedHeight + wall.BaseLevelOffset;
            #endregion
            SetCenterLine(wall);

            #region SegmentsMapping
            SegmentList = new List<StraightWallSegment>();

            if (wall.Openings.Any())
            {
                var openings = wall.Openings.ToList();
                openings = openings.OrderBy(o => o.SingleLine2D.Start.DistanceTo(CenterLine.Start)).ToList();
                var projectedStartPoints = openings.Select(o => MathGeoUtility.PointToLineProjection(o.SingleLine2D.Start, CenterLine.Start, CenterLine.End)).ToList();
                var projectedEndPoints = openings.Select(o => MathGeoUtility.PointToLineProjection(o.SingleLine2D.End, CenterLine.Start, CenterLine.End)).ToList();
                CreateDefaultSegment(CenterLine.Start, CenterLine.Start.DistanceTo(projectedStartPoints[0]) < CenterLine.Start.DistanceTo(projectedEndPoints[0]) ? projectedStartPoints[0] : projectedEndPoints[0], true, false);
                for (var i = 0; i < openings.Count - 1; i++)
                {
                    Point2D p1, p2;
                    if (projectedStartPoints[i].DistanceTo(CenterLine.Start) < projectedEndPoints[i].DistanceTo(CenterLine.Start))
                    {
                        p1 = projectedStartPoints[i];
                        p2 = projectedEndPoints[i];
                    }
                    else
                    {
                        p2 = projectedStartPoints[i];
                        p1 = projectedEndPoints[i];
                    }
                    CreateOpening(p1, p2, openings[i].OpeningType, openings[i].HeaderHeight, openings[i].Height);
                    CreateDefaultSegment(p2, p2.DistanceTo(projectedStartPoints[i + 1]) < p2.DistanceTo(projectedEndPoints[i + 1]) ? projectedStartPoints[i + 1] : projectedEndPoints[i + 1], false, false);
                }

                #region Create End Segment

                var ii = openings.Count - 1;
                Point2D sp, ep;
                if (projectedStartPoints[ii].DistanceTo(CenterLine.Start) < projectedEndPoints[ii].DistanceTo(CenterLine.Start))
                {
                    sp = projectedStartPoints[ii];
                    ep = projectedEndPoints[ii];
                }
                else
                {
                    ep = projectedStartPoints[ii];
                    sp = projectedEndPoints[ii];
                }
                CreateOpening(sp, ep, openings[ii].OpeningType, openings[ii].HeaderHeight, openings[ii].Height);
                CreateDefaultSegment(ep, CenterLine.End, false, true);
                // CreateDefaultSegment(p2, p2.DistanceTo(projectedStartPoints[ii + 1]) < p2.DistanceTo(projectedEndPoints[i + 1]) ? projectedStartPoints[i + 1] : projectedEndPoints[i + 1], true, true);
                #endregion

            }
            else
            {
                CreateDefaultSegment(CenterLine.Start, CenterLine.End, true, true);

            }



            #endregion
        }

        internal Post GetNearstPost(Point2D p)
        {
            var posts = new List<Post>();
            foreach (var s in SegmentList)
            {
                if (s.StartPost != null)
                {
                    posts.Add(s.StartPost);
                }
                if (s.EndPost != null)
                {
                    posts.Add(s.EndPost);
                }
            }

            return posts.OrderBy(e => e.Position.DistanceTo(p)).FirstOrDefault();
        }

        internal Post GetStartPost()
        {
            return SegmentList.First().StartPost;
        }

        internal void UpdatePostAtCorner(Post post)
        {
            //var sp = GetStartPost();
            //var ep = GetEndPost();
            //if (sp==null||ep==null)
            //{
            //    return;
            //}
            var targetPost = GetNearstPost(post.Position);
            var p = targetPost.Parent;
            p.UpdatePost(targetPost, post);

            //temp=null;

        }

        internal Post GetEndPost()
        {
            return SegmentList.Last().EndPost;
        }

        public void SetCenterLine(StitcherWall wall)
        {
            Line2D shortLine;
            if (PerimeterCorePoints[0].DistanceTo(PerimeterCorePoints[1]) < PerimeterCorePoints[2].DistanceTo(PerimeterCorePoints[3]))
            {
                shortLine = new Line2D(PerimeterCorePoints[0], PerimeterCorePoints[1]);
            }
            else
            {
                shortLine = new Line2D(PerimeterCorePoints[3], PerimeterCorePoints[2]);
            }
            Point2D c1 = new Point2D((PerimeterCorePoints[0].X + PerimeterCorePoints[3].X) * 0.5, (PerimeterCorePoints[0].Y + PerimeterCorePoints[3].Y) * 0.5);
            Point2D c2 = new Point2D((PerimeterCorePoints[1].X + PerimeterCorePoints[2].X) * 0.5, (PerimeterCorePoints[1].Y + PerimeterCorePoints[2].Y) * 0.5);

            var cl1 = MathGeoUtility.PointToLineProjection(shortLine.Start, c1, c2);
            var cl2 = MathGeoUtility.PointToLineProjection(shortLine.End, c1, c2);

            if (cl1.DistanceTo(wall.SingleWall2D.Start) < cl2.DistanceTo(wall.SingleWall2D.Start))
            {
                CenterLine = new Line2D(cl1, cl2);
            }
            else
            {
                CenterLine = new Line2D(cl2, cl1);
            }

        }
        protected void CreateDefaultSegment(Point2D startPoint, Point2D endPoint, bool addStartPost, bool addEndPost)
        {
            StraightWallSegment segment = new StraightWallSegment(startPoint, endPoint, Section, this);

            segment.CreateWallPlates();
            if (IsShear)
            {
                segment.CreateWallPosts(true, true);
            }
            segment.FillWithStuds();
            SegmentList.Add(segment);
        }
        protected void CreateOpening(Point2D startPoint, Point2D endPoint, OpeningType type, double headerHeight, double openingHeight)
        {
            var segment = new WallOpening(startPoint, endPoint, Section, type, this, headerHeight, openingHeight);
            segment.CreateWallPlates();
            segment.Create();
            SegmentList.Add(segment);

        }

        public override double GetLoadDistanceFromPosition(Point2D pos)
        {
            throw new NotImplementedException();
        }
    }
}
