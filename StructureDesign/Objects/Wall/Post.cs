﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StructureDesign
{
    public class Post : Stud
    {
        public Post(Point2D pos, double studWidth, double studDepth, Vector2D wallDirection, double z1, double z2, StraightWallSegment parent) : base(pos, studWidth, studDepth, wallDirection, z1, z2)
        {
            Parent = parent;
        }
    }
}
