﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StructureDesign
{
    public abstract class ElementSolver : IDisposable
    {
        public int NLoadCases = 0;
        public FramingObject Element = null; // External reference
        public bool OwnWeightIncluded = false;
        public Dictionary<int, List<Load>> LoadList = new Dictionary<int, List<Load>>();
        public List<PointSupport> PointSupportList = new List<PointSupport>();
        public List<LineSupport> LineSupportList = new List<LineSupport>();
        public bool Disposed = false;



        #region Constructor(s)
        public ElementSolver()
        {

        }

        protected ElementSolver(FramingObject element)
        {

            Element = element;
            BuildLoadList();

        }

        #endregion

        protected void BuildLoadList()
        {

            LoadList = new Dictionary<int, List<Load>>();

            if (Element == null)
                return;

            LoadList.Add(0, new List<Load>()); // Dead

            LoadList.Add(1, new List<Load>()); // Seismic North
            LoadList.Add(2, new List<Load>()); // Seismic South
            LoadList.Add(3, new List<Load>()); // Seismic East
            LoadList.Add(4, new List<Load>()); // Seismic West

            LoadList.Add(5, new List<Load>()); // Wind North
            LoadList.Add(6, new List<Load>()); // Wind South
            LoadList.Add(7, new List<Load>()); // Wind East
            LoadList.Add(8, new List<Load>()); // Wind West

            if (Element.UpperUpliftPointLoadList != null)
            {
                foreach (UpliftPointLoad load in Element.UpperUpliftPointLoadList)
                {
                    if (load == null)
                        continue;

                    double d = Element.GetLoadDistanceFromPosition(load.Position);

                    LoadList[0].Add(new PointLoad(d, load.DeadComulative));

                    LoadList[1].Add(new PointLoad(d, load.SeismicComulative_North));
                    LoadList[2].Add(new PointLoad(d, load.SeismicComulative_South));
                    LoadList[3].Add(new PointLoad(d, load.SeismicComulative_East));
                    LoadList[4].Add(new PointLoad(d, load.SeismicComulative_West));

                    LoadList[5].Add(new PointLoad(d, load.WindComulative_North));
                    LoadList[6].Add(new PointLoad(d, load.WindComulative_South));
                    LoadList[7].Add(new PointLoad(d, load.WindComulative_East));
                    LoadList[8].Add(new PointLoad(d, load.WindComulative_West));
                }
            }

            NLoadCases = LoadList.Count; // 9

        }

        public abstract bool Run();
        public abstract bool TransferReactionsToSupports();
        public abstract bool ApplyPostProcessing();

        public void Dispose()
        {
            if (Disposed)
                return;

            Dispose(true);
            GC.SuppressFinalize(this);

            Disposed = true;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Element = null;

                if (LoadList != null)
                {
                    foreach (KeyValuePair<int, List<Load>> loadInfo in LoadList)
                    {
                        if (loadInfo.Value == null)
                            return;

                        foreach (var obj in loadInfo.Value)
                        {
                            if (obj == null)
                                continue;

                            if (obj is IDisposable)
                                (obj as IDisposable).Dispose();
                        }

                        loadInfo.Value.Clear();
                    }

                    LoadList.Clear();
                    LoadList = null;
                }
                PointSupportList?.Clear(); // External references, don't dispose
                PointSupportList = null;
                LineSupportList?.Clear(); // External references, don't dispose
                LineSupportList = null;
            }

        }
    }
}
