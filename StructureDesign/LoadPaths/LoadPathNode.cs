﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StructureDesign
{
    public class LoadPathNode : IDisposable
    {
        public bool Solved = false;
        public FramingObject PivotObject = null;

        public LoadPathNode(FramingObject pivot)
        {
            PivotObject = pivot;
        }

        public void Dispose()
        {
            PivotObject = null;
        }
        public bool Run()
        {

            if (PivotObject == null)
                return false;

            if (Solved)
                return true;

            bool bRetVal = true;

            ElementSolver solver = ElementSolverFactory.Create(PivotObject);

            if (solver != null)
            {
                if (solver.Run() == false) // Can't be solved
                {
                    bRetVal = false;
                    return bRetVal;
                }

                /////////////////////////////////
                // Post processing
                solver.ApplyPostProcessing();
                ////////////////////////////////

                solver.Dispose();
                solver = null;

                Solved = true;
            }

            return bRetVal;

        }
    }
}
