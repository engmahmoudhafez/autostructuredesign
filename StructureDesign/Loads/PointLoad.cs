﻿using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Text;

namespace StructureDesign
{
    public class PointLoad : Load
    {
        public Point3D Position = new Point3D();
        public double Value = 0.0;
        public double Distance = 0.0;

        public PointLoad(double dist, double val)
    : base()
        {
            Distance = dist;
            Value = val;
        }
    }
}
