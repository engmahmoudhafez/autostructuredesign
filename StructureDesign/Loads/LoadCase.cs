﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StructureDesign
{
    public enum LoadCase
    {
        Dead,
        Live,
        RoofLive,
        BalancedSnow,
        UnbalancedSnow,
        Seismic,
        Wind
    }
}
