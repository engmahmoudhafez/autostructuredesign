﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StructureDesign
{
    public class Load
    {
        public object Source = null;
        public LoadCase LoadCase = LoadCase.Dead;
        public bool Disposed = false;
    }
}
