﻿using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Text;

namespace StructureDesign
{
    public class UpliftPointLoad : UpliftLoad
    {

        public object Source = null;
        public double Distance = 0.0;
        public Point2D Position = new Point2D();

        #region Constructor(s)
        public UpliftPointLoad()
            : base()
        {

        }

        #endregion

    }
}
