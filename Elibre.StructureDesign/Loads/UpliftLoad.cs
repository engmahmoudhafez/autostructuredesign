﻿namespace Elibre.StructureDesign
{
    public class UpliftLoad
    {
        #region Fields

        #region Dead
        public double Dead = 0.0;           // Local to floor
        public double DeadComulative = 0.0; // Carry upper floors
        #endregion

        #region Local Loads

        #region Seismic

        #region Vertical
        public double SeismicLocal_North = 0.0;
        public double SeismicLocal_South = 0.0;
        #endregion

        #region Horizontal
        public double SeismicLocal_East = 0.0;
        public double SeismicLocal_West = 0.0;
        #endregion

        #endregion

        #region Wind

        #region Vertical
        public double WindLocal_North = 0.0;
        public double WindLocal_South = 0.0;
        #endregion

        #region Horizontal
        public double WindLocal_East = 0.0;
        public double WindLocal_West = 0.0;
        #endregion

        #endregion

        #endregion

        #region Comulative Loads
        #region Seismic

        #region Vertical
        public double SeismicComulative_North = 0.0;
        public double SeismicComulative_South = 0.0;
        #endregion

        #region Horizontal
        public double SeismicComulative_East = 0.0;
        public double SeismicComulative_West = 0.0;
        #endregion

        #endregion

        #region Wind

        #region Vertical
        public double WindComulative_North = 0.0;
        public double WindComulative_South = 0.0;
        #endregion

        #region Horizontal
        public double WindComulative_East = 0.0;
        public double WindComulative_West = 0.0;
        #endregion

        #endregion
        #endregion

        public object Owner = null;
        public bool Disposed = false;
        #endregion


        #region Constructor(s)

        public UpliftLoad()
        {

        }

        public UpliftLoad(UpliftLoad data, double scaleFactor = 1.0)
        {
            if (data == null)
                return;

            Dead = data.Dead * scaleFactor;

            #region Local Loads

            #region Seismic

            #region Vertical
            SeismicLocal_North = data.SeismicLocal_North * scaleFactor;
            SeismicLocal_South = data.SeismicLocal_South * scaleFactor;
            #endregion

            #region Horizontal
            SeismicLocal_East = data.SeismicLocal_East * scaleFactor;
            SeismicLocal_West = data.SeismicLocal_West * scaleFactor;
            #endregion

            #endregion

            #region Wind

            #region Vertical
            WindLocal_North = data.WindLocal_North * scaleFactor;
            WindLocal_South = data.WindLocal_South * scaleFactor;
            #endregion

            #region Horizontal
            WindLocal_East = data.WindLocal_East * scaleFactor;
            WindLocal_West = data.WindLocal_West * scaleFactor;
            #endregion

            #endregion

            #endregion

            #region Comulative Loads
            #region Seismic

            #region Vertical
            SeismicComulative_North = data.SeismicComulative_North * scaleFactor;
            SeismicComulative_South = data.SeismicComulative_South * scaleFactor;
            #endregion

            #region Horizontal
            SeismicComulative_East = data.SeismicComulative_East * scaleFactor;
            SeismicComulative_West = data.SeismicComulative_West * scaleFactor;
            #endregion

            #endregion

            #region Wind

            #region Vertical
            WindComulative_North = data.WindComulative_North * scaleFactor;
            WindComulative_South = data.WindComulative_South * scaleFactor;
            #endregion

            #region Horizontal
            WindComulative_East = data.WindComulative_East * scaleFactor;
            WindComulative_West = data.WindComulative_West * scaleFactor;
            #endregion

            #endregion
            #endregion
        }

        #endregion

        /// <summary>
        /// Add to comulative Loads only
        /// </summary>
        /// <param name="data"></param>
        public void Add(UpliftLoad data)
        {
            if (data == null)
                return;

            DeadComulative += data.DeadComulative;

            #region Comulative Loads

            #region Seismic

            #region Vertical
            SeismicComulative_North += data.SeismicComulative_North;
            SeismicComulative_South += data.SeismicComulative_South;
            #endregion

            #region Horizontal
            SeismicComulative_East += data.SeismicComulative_East;
            SeismicComulative_West += data.SeismicComulative_West;
            #endregion

            #endregion

            #region Wind

            #region Vertical
            WindComulative_North += data.WindComulative_North;
            WindComulative_South += data.WindComulative_South;
            #endregion

            #region Horizontal
            WindComulative_East += data.WindComulative_East;
            WindComulative_West += data.WindComulative_West;
            #endregion

            #endregion

            #endregion
        }

        public void Clear()
        {
            #region Dead
            Dead = 0.0;           // Local to floor
            DeadComulative = 0.0; // Carry upper floors
            #endregion

            #region Local Loads

            #region Seismic

            #region Vertical
            SeismicLocal_North = 0.0;
            SeismicLocal_South = 0.0;
            #endregion

            #region Horizontal
            SeismicLocal_East = 0.0;
            SeismicLocal_West = 0.0;
            #endregion

            #endregion

            #region Wind

            #region Vertical
            WindLocal_North = 0.0;
            WindLocal_South = 0.0;
            #endregion

            #region Horizontal
            WindLocal_East = 0.0;
            WindLocal_West = 0.0;
            #endregion

            #endregion

            #endregion

            #region Comulative Loads
            #region Seismic

            #region Vertical
            SeismicComulative_North = 0.0;
            SeismicComulative_South = 0.0;
            #endregion

            #region Horizontal
            SeismicComulative_East = 0.0;
            SeismicComulative_West = 0.0;
            #endregion

            #endregion

            #region Wind

            #region Vertical
            WindComulative_North = 0.0;
            WindComulative_South = 0.0;
            #endregion

            #region Horizontal
            WindComulative_East = 0.0;
            WindComulative_West = 0.0;
            #endregion

            #endregion
            #endregion
        }

    }
}
