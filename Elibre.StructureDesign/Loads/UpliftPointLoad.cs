﻿using Elibre.Geometry2D;

namespace Elibre.StructureDesign
{
    public class UpliftPointLoad : UpliftLoad
    {

        public object Source = null;
        public double Distance = 0.0;
        public Point2D Position = new Point2D();

        #region Constructor(s)
        public UpliftPointLoad()
            : base()
        {

        }

        #endregion

    }
}
