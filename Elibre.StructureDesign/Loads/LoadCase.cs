﻿namespace Elibre.StructureDesign
{
    public enum LoadCase
    {
        Dead,
        Live,
        RoofLive,
        BalancedSnow,
        UnbalancedSnow,
        Seismic,
        Wind
    }
}
