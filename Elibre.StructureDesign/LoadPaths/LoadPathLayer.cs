﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Elibre.StructureDesign
{
    public class LoadPathLayer : IDisposable
    {
        public bool Solved = false;
        public List<LoadPathNode> NodeList = new List<LoadPathNode>(); // Just pointers, clear list but don't dispose objects.

        public bool IsEmpty()
        {
            if (NodeList == null)
                return true;

            if (!NodeList.Any())
                return true;

            return false;
        }

        public void Dispose()
        {
            if (NodeList == null)
            {
                NodeList.Clear();
                NodeList = null;
            }
        }
        public bool Run()
        {

            Solved = false;
            bool bAllNodesSolved = true;

            if (NodeList == null)
                return false;

            if (NodeList.Count <= 0)
                return false;

            foreach (LoadPathNode node in NodeList)
            {
                if (node == null)
                    continue;

                if (node.Run() == false)
                {
                    bAllNodesSolved = false;
                    // Can we continue if node failed to be solved ???
                    // yes, and mark it as returned error.

                    //Globals.DisplaySolverMessage("Error: Analysis Failed [" + node.PivotObject.Name + "]", messagetype.error);
                }
            }

            Solved = bAllNodesSolved;

            return Solved;

        }
        public void AddNode(LoadPathNode node)
        {
            if (node == null)
                return;

            if (node.PivotObject == null)
                return;

            if (NodeList == null)
                NodeList = new List<LoadPathNode>();

            if (NodeList.IndexOf(node) < 0)
                NodeList.Add(node);
        }
    }
}
