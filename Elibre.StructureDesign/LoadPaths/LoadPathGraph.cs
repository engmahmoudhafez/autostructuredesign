﻿using System;
using System.Collections.Generic;

namespace Elibre.StructureDesign
{
    public class LoadPathGraph
    {
        public List<FramingObject> ObjectList = new List<FramingObject>();
        protected List<FramingObject> UnSupportedObjects = new List<FramingObject>();
        protected List<FramingObject> FailedObjects = new List<FramingObject>();
        protected List<LoadPathLayer> LayerList = new List<LoadPathLayer>();
        protected List<LoadPathNode> NodeList = new List<LoadPathNode>();
        protected List<FramingObject> NodeObjects = new List<FramingObject>();

        public LoadPathGraph()
        {

        }

        public bool BuildLoadGraph()
        {

            FilterObjects();

            ClearLayers();
            ClearNodeList();
            UnSupportedObjects?.Clear();
            FailedObjects?.Clear();

            if (LayerList == null)
                LayerList = new List<LoadPathLayer>();

            //TODO: implment the rest of the functions
            LoadPathLayer foundationLayer = new LoadPathLayer();/*= CreateFoundationLayer()*/;

            if ((foundationLayer == null) || foundationLayer.IsEmpty()) // Can't get foundation objects
                return false;

            AddToNodeList(foundationLayer.NodeList);
            LayerList.Add(foundationLayer);

            int nObjects = ObjectList.Count;
            if (nObjects <= 0)
                return true;

            #region Build solution path

            int MaxIterations = nObjects; // Maximum allowed layers shouldn't exceed the numer of objects
            int iteration = 0;

            LoadPathLayer topLayer = foundationLayer;

            while (iteration < MaxIterations)
            {
                iteration++;

                if (ObjectList.Count <= 0) // No more objects to be added
                    break;

                LoadPathLayer layer = GetNextLayer();

                if (layer == null)
                    break;

                if (layer.IsEmpty())
                    break;

                AddToNodeList(layer.NodeList);
                LayerList.Add(layer);

                topLayer = layer;
            }
            #endregion

            #region Objects that failed to be added
            if (ObjectList.Count > 0)
            {
                FailedObjects = new List<FramingObject>(ObjectList);
                ObjectList.Clear();
            }
            #endregion

            return true;
        }
        protected void FilterObjects()
        {
            if (ObjectList == null)
                return;

            List<FramingObject> filterredList = new List<FramingObject>();

            foreach (FramingObject obj in ObjectList)
            {
                if (obj == null)
                    continue;

                FramingObject newObject = CastToMainValidObject(obj);

                if (newObject == null)
                    continue;

                if (filterredList.IndexOf(newObject) < 0)
                    filterredList.Add(newObject);
            }

            if (filterredList.Count > 0)
            {
                ObjectList.Clear();
                ObjectList = filterredList;
            }

        }
        protected void AddToNodeList(List<LoadPathNode> node_list)
        {
            if (node_list == null)
                return;

            if (node_list.Count <= 0)
                return;

            if (NodeList == null)
                NodeList = new List<LoadPathNode>();

            if (NodeObjects == null)
                NodeObjects = new List<FramingObject>();

            NodeList.AddRange(node_list);

            foreach (LoadPathNode node in node_list)
            {
                if (node == null)
                    continue;

                if (node.PivotObject == null)
                    continue;

                if (NodeObjects.IndexOf(node.PivotObject) < 0)
                    NodeObjects.Add(node.PivotObject);
            }
        }

        public bool Run()
        {
            if (LayerList == null)
                return false;

            bool bRetVal = true;
            List<LoadPathLayer> reversedLayers = new List<LoadPathLayer>(LayerList);
            reversedLayers.Reverse();
            foreach (LoadPathLayer layer in reversedLayers) // Start from top to down layers - upper objects first
            {
                if (layer == null)
                    continue;

                if (layer.IsEmpty())
                    continue;

                if (layer.Run() == false)
                {
                    bRetVal = false;
                    // Can we continue if layer failed to be solved ???
                    // yes, and mark it as returned error.
                }
            }

            if (bRetVal)
            {
                //TODO: in eframer it is commented!!
                //UpdateTotalReactionInfo();

                //////////////////////////////////////////////////////////////
                // ??? is this a debug ???
                //double sumLoads = Globals.Building.CalcTotalWeight();
                //////////////////////////////////////////////////////////////
            }

            return bRetVal;

        }
        protected LoadPathLayer GetNextLayer()
        {
            int k = -1;
            List<int> idList = new List<int>(); // Indices of objects to be removed from parent list (foundation pads/straps)
            LoadPathLayer layer = new LoadPathLayer();

            #region Add object which has all supports added before
            foreach (FramingObject obj in ObjectList)
            {
                k++;

                if (obj == null)
                {
                    idList.Add(k);
                    continue;
                }

                List<FramingObject> sup_list = GetObjectSupports(obj);

                if (sup_list == null)
                    continue;

                if (sup_list.Count <= 0)
                    continue;

                bool bAllSupportsFound = true;

                foreach (FramingObject sup in sup_list)
                {
                    if (NodeObjects.IndexOf(sup) < 0)
                    {
                        bAllSupportsFound = false;
                        break;
                    }
                }

                if (bAllSupportsFound)
                {
                    idList.Add(k);

                    LoadPathNode node = new LoadPathNode(obj);
                    layer.AddNode(node);
                }
            }
            #endregion

            #region Remove added nodes from object list
            if (idList.Count > 0)
            {
                idList.Reverse();

                foreach (int index in idList)
                {
                    ObjectList.RemoveAt(index);
                }
            }
            #endregion

            //TODO: For Debug Only
            ///////////////////////////////////////////////////////////
            //// Debug only
            //List<string> name_list = GetObjectsNames(layer.NodeList);
            ///////////////////////////////////////////////////////////

            return layer;
        }
        protected List<FramingObject> GetObjectSupports(FramingObject obj)
        {
            if (obj == null)
                return null;

            if (obj.SupportList == null)
                return null;

            List<FramingObject> sup_list = new List<FramingObject>();

            foreach (Support sup in obj.SupportList)
            {
                if (sup == null)
                    continue;

                FramingObject validSupport = CastToMainValidObject(sup.FramingObject);

                if (validSupport != null)
                {
                    if (sup_list.IndexOf(validSupport) < 0)
                        sup_list.Add(validSupport);
                }
            }

            return sup_list;
        }
        protected FramingObject CastToMainValidObject(FramingObject obj)
        {

            if (obj == null)
                return null;

            Type objType = obj.GetType();

            if (objType == typeof(Header))
            {
                Header header = obj as Header;
                WallOpening opening = header.Parent;

                if (opening != null)
                {
                    var wall = opening.ParentFramingWall;

                    if (wall != null)
                        return wall;
                }
            }

            return obj;

        }

        public void Dispose()
        {
            Clear();

            ObjectList = null;
            UnSupportedObjects = null;
            FailedObjects = null;
            LayerList = null;
            NodeList = null;
            NodeObjects = null;
        }
        protected void Clear()
        {
            ClearLayers();
            ClearNodeList();

            ObjectList?.Clear();
            UnSupportedObjects?.Clear();
            FailedObjects?.Clear();

        }
        protected void ClearLayers()
        {
            if (LayerList != null)
            {
                foreach (LoadPathLayer layer in LayerList)
                {
                    if (layer != null)
                        layer.Dispose();

                }

                LayerList.Clear();
            }
        }
        protected void ClearNodeList()
        {
            if (NodeList != null)
            {
                foreach (LoadPathNode node in NodeList)
                {
                    if (node != null)
                        node.Dispose();
                }

                NodeList.Clear();
            }
            NodeObjects?.Clear();
        }
    }
}
