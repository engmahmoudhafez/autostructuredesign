﻿using Elibre.Geometry2D;

namespace Elibre.StructureDesign
{
    public class PointSupport : Support
    {
        protected double Distance = 0.0;
        protected Point2D Position = new Point2D();
    }
}
