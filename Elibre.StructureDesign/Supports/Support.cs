﻿using System;

namespace Elibre.StructureDesign
{
    public abstract class Support
    {
        public Guid FramingObjID = Guid.Empty;
        public FramingObject FramingObject = null;
        public SupportReaction SupportReaction = null;

        #region Constructor(s)

        public Support()
        {

        }

        public Support(Support sup)
        {
            if (sup == null)
                return;

            FramingObjID = sup.FramingObjID;
            FramingObject = sup.FramingObject;
        }

        #endregion
    }
}
