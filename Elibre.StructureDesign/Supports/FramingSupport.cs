﻿using System;

namespace Elibre.StructureDesign
{
    public abstract class FramingSupport
    {
        protected readonly Guid ID = Guid.NewGuid();
        protected FramingObject Support = null;
    }
}
