﻿using System.Collections.Generic;

namespace Elibre.StructureDesign
{
    public class StructuralBuilding
    {
        public List<StraightWall> Walls = new List<StraightWall>();
    }
}
