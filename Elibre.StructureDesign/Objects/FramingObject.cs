﻿using Elibre.Geometry2D;
using System;
using System.Collections.Generic;

namespace Elibre.StructureDesign
{
    public abstract class FramingObject : /*IGeometry2D,*/ IGeometry3D
    {

        public Guid Id { get; set; }
        public string Name { get; set; }
        //public Geom2D Geom2D { get ; set ; }
        public Geom3D Geom3D { get; set; }


        public List<UpliftPointLoad> UpperUpliftPointLoadList = new List<UpliftPointLoad>();
        public List<Support> SupportList = new List<Support>();
        public FramingObject() { }

        //TODO: must be implmented for every object
        public abstract double GetLoadDistanceFromPosition(Point2D pos);
    }
}
