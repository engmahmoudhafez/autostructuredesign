﻿namespace Elibre.StructureDesign
{
    public class Section
    {
        public double Depth { get; set; }
        public double Width { get; set; }

        public Section(double depth, double width)
        {
            Depth = depth;
            Width = width;
        }
    }
}
