﻿using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elibre.StructureDesign
{
    public class StraightWallSegment : FramingObject
    {
        public Point2D StartPoint { get; set; }
        public Point2D EndPoint { get; set; }
        public List<Stud> StudList { get; set; }
        public Section Section { get; set; }
        public Post StartPost { get; set; }
        public Post EndPost { get; set; }
        public StraightWall ParentFramingWall { get; set; }
        //public List<Point3D> TopProfile { get; set; }
        //public List<Point3D> BottomProfile { get; set; }

        public List<WallPlate> TopWallPlates { get; set; }
        public List<WallPlate> BottomWallPlates { get; set; }


        #region Constructor(s)

        #endregion
        public StraightWallSegment(Point2D startPoint, Point2D endPoint, Section section, StraightWall straightWall)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
            Section = section;
            ParentFramingWall = straightWall;
        }

        public void CreateWallPlates()
        {
            CreateTopPlates();
            CreateBottomPlates();
        }

        private void CreateBottomPlates()
        {
            BottomWallPlates = new List<WallPlate>();
            BottomWallPlates.Add(new WallPlate(new List<Point3D> { new Point3D(StartPoint, ParentFramingWall.BottomZLevel), new Point3D(EndPoint, ParentFramingWall.BottomZLevel) }, ParentFramingWall.Thickness, ParentFramingWall.Thickness * 0.5, false, ParentFramingWall));
            if (ParentFramingWall.NumBottomPlates > 0)
            {
                double dz = BottomWallPlates[0].SegmentList[0].ParentWallPlate.Depth;

                for (int i = 1; i < ParentFramingWall.NumBottomPlates; i++)
                {
                    var bottomPlate = new WallPlate(new List<Point3D> { new Point3D(StartPoint, ParentFramingWall.BottomZLevel), new Point3D(EndPoint, ParentFramingWall.BottomZLevel) }, ParentFramingWall.Thickness, ParentFramingWall.Thickness * 0.5, false, ParentFramingWall);
                    bottomPlate.Translate(0, 0, i * dz);

                    BottomWallPlates.Add(bottomPlate);
                }
            }
        }

        private void CreateTopPlates()
        {
            TopWallPlates = new List<WallPlate>();
            TopWallPlates.Add(new WallPlate(new List<Point3D> { new Point3D(StartPoint, ParentFramingWall.TopZLevel), new Point3D(EndPoint, ParentFramingWall.TopZLevel) }, ParentFramingWall.Thickness, ParentFramingWall.Thickness * 0.5, true, ParentFramingWall));
            if (ParentFramingWall.NumTopPlates > 0)
            {
                for (int i = 1; i < ParentFramingWall.NumTopPlates; i++)
                {
                    var topPlate = new WallPlate(new List<Point3D> { new Point3D(StartPoint, ParentFramingWall.TopZLevel), new Point3D(EndPoint, ParentFramingWall.TopZLevel) }, ParentFramingWall.Thickness, ParentFramingWall.Thickness * 0.5, true, ParentFramingWall);

                    for (var j = 0; j < TopWallPlates[0].SegmentList.Count; j++)
                    {
                        double dz = TopWallPlates[0].SegmentList[j].ParentWallPlate.Depth;

                        topPlate.TranslateSegment(0, 0, -i * dz, j);

                        // To Align top Plane of the last wall plate with the base plane of the previous wall plate
                        topPlate.SegmentList[j].ChangeTopPlane(TopWallPlates.Last().SegmentList[j].VertexList.Take(4).ToArray());

                    }

                    //// Trim the last wall plate segments together
                    //topPlate.TrimSegmentsTogther();

                    TopWallPlates.Add(topPlate);
                }
            }
        }

        public void FillWithStuds()
        {
            double z1 = ParentFramingWall.TopZLevel - TopWallPlates.Count * TopWallPlates[0].Depth;
            double z2 = ParentFramingWall.BottomZLevel + BottomWallPlates.Count * BottomWallPlates[0].Depth;
            FillWithStuds(z1, z2);
        }
        public void CreateWallPosts(bool addStartPost, bool addEndPost)
        {

            var u = this.ParentFramingWall.Vector;
            double z1 = ParentFramingWall.TopZLevel - TopWallPlates.Count * TopWallPlates[0].Depth;
            double z2 = ParentFramingWall.BottomZLevel + BottomWallPlates.Count * BottomWallPlates[0].Depth;

            var postWidth = Section.Depth;
            var postDepth = Section.Depth;

            Point2D cen1 = GetNearstStartPoint();
            Point2D cen2 = GetNearstEndPoint();
            double L = (cen2 - cen1).GetLength();




            if (addStartPost)
            {
                double startDistance = 0.5 * postWidth;
                var startPos = cen1 + u * startDistance;
                StartPost = new Post(startPos, postWidth, postDepth, u, z1, z2, this);

            }

            if (addEndPost)
            {
                double endDistance = L - 0.5 * postWidth;
                var endPos = cen1 + u * endDistance;
                EndPost = new Post(endPos, postWidth, postDepth, u, z1, z2, this);

            }
        }
        public void FillWithStuds(double z1, double z2)
        {

            StudList = new List<Stud>();
            var addStartStud = StartPost == null;
            var addEndStud = EndPost == null;
            FillWithStuds(Section.Width, Section.Depth, ParentFramingWall.StudSpacing,
                ParentFramingWall.StartStudSpacing, ParentFramingWall.EndStudSpacing, z1, z2, addStartStud, addEndStud);
        }

        public void FillWithStuds(double studWidth, double studDepth, double studSpacing, double startStudSpacing,
            double endStudSpacing, double z1, double z2, bool addStartStud = true,
            bool addEndStud = true)
        {
            //get the nearest edge point after trimming

            Point2D cen1 = GetNearstStartPoint();
            Point2D cen2 = GetNearstEndPoint();
            //////////
            double L = (cen2 - cen1).GetLength();
            double startDistance = 0.5 * studWidth;
            double endDistance = L - 0.5 * studWidth;
            //double L = (cen2 - cen1).GetLength();
            //double startDistance = 0.5 * studWidth;
            //double endDistance = L - 0.5 * studWidth;


            startDistance += startStudSpacing;
            endDistance -= endStudSpacing;
            var u = this.ParentFramingWall.Vector;


            List<Point2D> positionList = new List<Point2D>();



            if (addStartStud)
                positionList.Add(cen1 + u * startDistance);

            double d = startDistance + studSpacing;

            while ((endDistance - d) > studWidth)
            {
                positionList.Add(cen1 + u * d);
                d += studSpacing;
            }

            if (addEndStud)
                positionList.Add(cen1 + u * endDistance);



            Stud s;
            for (int k = 0; k < positionList.Count; k++)
            {
                Point2D pos = positionList[k];
                s = new Stud(pos, studWidth, studDepth, u, z1, z2);
                StudList.Add(s);
            }


            //var sp = GetNearstStartPoint();
            //var ep = GetNearstEndPoint();
            //var l = ep .DistanceTo( sp);
            //var accuDist = studWidth/2;
            //var pos = new List<double>();
            //var vec = new Vector2D(ep - sp).GetUnit();
            //while (l< accuDist +studWidth/2)
            //{
            //    pos.Add(accuDist);
            //    accuDist += studSpacing;
            //}
            //foreach (var p in pos)
            //{
            //    var position = sp + vec * accuDist;
            //    StudList.Add(new Stud(position,studWidth,studDepth,vec,ParentFramingWall.TopZLevel,ParentFramingWall.BottomZLevel));

            //}
        }

        internal void UpdatePost(Post targetPost, Post post)
        {
            if (StartPost == targetPost)
            {
                var temp = StartPost;
                StartPost = post;
                temp = null;
            }
            else
            {
                var temp = EndPost;
                EndPost = post;
                temp = null;
            }
        }

        private Point2D GetNearstEndPoint()
        {
            return EndPoint;
        }

        private Point2D GetNearstStartPoint()
        {
            return StartPoint;
        }

        public override double GetLoadDistanceFromPosition(Point2D pos)
        {
            throw new NotImplementedException();
        }
    }
}
