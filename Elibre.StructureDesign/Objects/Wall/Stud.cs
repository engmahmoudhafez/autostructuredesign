﻿using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elibre.StructureDesign
{
    public class Stud : FramingObject
    {
        public Point2D Position { get; set; }
        public Point2D StartPoint { get; set; }
        public Point2D EndPoint { get; set; }
        public Geom3D Geo3D { get; private set; }
        public Section Section { get; set; }
        public StraightWallSegment Parent { get; set; }
        public Stud() { }

        public Stud(Point2D pos, double studWidth, double studDepth, Vector2D wallDirection, double z1, double z2)
        {

            Position = pos;
            Section = new Section(studDepth, studWidth);
            StartPoint = pos - 0.5 * wallDirection * studWidth;
            EndPoint = pos + 0.5 * wallDirection * studWidth;

            Point2D p1 = new Point2D();
            Point2D p2 = new Point2D();
            Point2D p3 = new Point2D();
            Point2D p4 = new Point2D();

            MathGeoUtility.OffsetRight(StartPoint, EndPoint, 0.5 * studDepth, out p1, out p2);
            MathGeoUtility.OffsetLeft(StartPoint, EndPoint, 0.5 * studDepth, out p3, out p4);

            Vector2D[] v1 = { p1.ToVector2D(), p2.ToVector2D(), p4.ToVector2D(), p3.ToVector2D() };
            Vector3D[] v = CreateVertexList(v1, z1, z2);
            Quad3d[] quadList = CreateFaceList(v);

            CreateFaceList(quadList);
            CreateEdgeList();
        }

        public Vector3D[] CreateVertexList(Vector2D[] v, double z1, double z2)
        {
            Vector3D[] v2 = new Vector3D[8];

            v2[0] = new Vector3D(v[0].X, v[0].Y, z1);
            v2[1] = new Vector3D(v[1].X, v[1].Y, z1);
            v2[2] = new Vector3D(v[2].X, v[2].Y, z1);
            v2[3] = new Vector3D(v[3].X, v[3].Y, z1);

            v2[4] = new Vector3D(v[0].X, v[0].Y, z2);
            v2[5] = new Vector3D(v[1].X, v[1].Y, z2);
            v2[6] = new Vector3D(v[2].X, v[2].Y, z2);
            v2[7] = new Vector3D(v[3].X, v[3].Y, z2);

            return v2;
        }
        public Quad3d[] CreateFaceList(Vector3D[] v)
        {
            Quad3d[] quadList = new Quad3d[6];

            quadList[0] = new Quad3d(v[0], v[1], v[5], v[4]);
            quadList[1] = new Quad3d(v[1], v[2], v[6], v[5]);
            quadList[2] = new Quad3d(v[2], v[3], v[7], v[6]);
            quadList[3] = new Quad3d(v[3], v[0], v[4], v[7]);
            quadList[4] = new Quad3d(v[0], v[3], v[2], v[1]);
            quadList[5] = new Quad3d(v[4], v[5], v[6], v[7]);

            return quadList;
        }
        protected void CreateFaceList(Quad3d[] quadList)
        {

            if (quadList == null)
                return;

            Geo3D = new Geom3D();

            List<Vector3D> vertexList = new List<Vector3D>();
            List<Vector3D> normalList = new List<Vector3D>();
            List<QuadInfo> quadInfoList = new List<QuadInfo>();

            int vertexCount = 0;

            foreach (Quad3d quad in quadList)
            {
                vertexList.Add(quad.P1);
                vertexList.Add(quad.P2);
                vertexList.Add(quad.P3);
                vertexList.Add(quad.P4);

                QuadInfo qi = new QuadInfo(vertexCount,
                                           vertexCount + 1,
                                           vertexCount + 2,
                                           vertexCount + 3);

                quadInfoList.Add(qi);
                vertexCount += 4;
            }

            Geo3D.QuadList = quadInfoList.ToArray();
            Geo3D.VertexList = vertexList.ToArray();
            Geo3D.NormalList = new Vector3D[vertexList.Count];

            foreach (QuadInfo qi in Geo3D.QuadList)
            {
                Vector3D normal;
                Geo3D.CalcFaceNormal(qi.ID1, qi.ID2, qi.ID3, qi.ID4, Geo3D.VertexList, out normal);

                Geo3D.NormalList[qi.ID1] = normal;
                Geo3D.NormalList[qi.ID2] = normal;
                Geo3D.NormalList[qi.ID3] = normal;
                Geo3D.NormalList[qi.ID4] = normal;
            }
        }
        protected void CreateEdgeList()
        {
            if (Geo3D == null)
                return;

            if (Geo3D.QuadList == null)
                return;

            List<EdgeInfo> edgeInfoList = new List<EdgeInfo>();

            foreach (QuadInfo qi in Geo3D.QuadList)
            {
                edgeInfoList.Add(new EdgeInfo(qi.ID1, qi.ID2));
                edgeInfoList.Add(new EdgeInfo(qi.ID2, qi.ID3));
                edgeInfoList.Add(new EdgeInfo(qi.ID3, qi.ID4));
                edgeInfoList.Add(new EdgeInfo(qi.ID4, qi.ID1));
            }
            Geo3D.EdgeList = edgeInfoList.ToArray();
        }

        public override double GetLoadDistanceFromPosition(Point2D pos)
        {
            throw new NotImplementedException();
        }
    }
}
