﻿using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Elibre.StructureDesign
{
    public class WallPlateSegment : FramingObject
    {
        public WallPlate ParentWallPlate { get; set; }
        public Point3D[] VertexListSegment { get; set; }
        public Plane BasePlane { get; set; }
        public Plane TopPlane { get; set; }
        public Vector3D[] VertexList { get; set; }
        public Geom3D Geo3D { get; set; }

        public WallPlateSegment(Point3D[] vertexList, bool top, WallPlate _parent)

        {

            VertexListSegment = vertexList;
            ParentWallPlate = _parent;
            CreateGeometry3D();

        }

        private void CreateGeometry3D()
        {
            var v1 = VertexListSegment.Select(v => new Vector3D(v.X, v.Y, v.Z)).ToArray();

            Plane plane1 = new Plane(v1);

            Vector3D normal = new Vector3D(plane1.A, plane1.B, plane1.C);

            if (plane1.C < 0)
                normal = -1 * normal;

            normal.Normalize();

            var depth = ParentWallPlate.Depth;
            if (ParentWallPlate.IsTopPlate) // Adjust base polygon
            {
                var v2 = new Vector3D[4];

                v2[0] = v1[0] - normal * depth;
                v2[1] = v1[1] - normal * depth;
                v2[2] = v1[2] - normal * depth;
                v2[3] = v1[3] - normal * depth;

                Plane plane2 = new Plane(v2);

                BasePlane = plane2;
                TopPlane = plane1;
            }
            else
            {
                var v2 = new Vector3D[4];

                v2[0] = v1[0] + normal * depth;
                v2[1] = v1[1] + normal * depth;
                v2[2] = v1[2] + normal * depth;
                v2[3] = v1[3] + normal * depth;

                Plane plane2 = new Plane(v2);

                BasePlane = plane1;
                TopPlane = plane2;
            }

            VertexList = CreateVertexList(v1);
            Quad3d[] quadList = CreateFaceList(VertexList);

            CreateFaceList(quadList);
            CreateEdgeList();
        }

        protected Vector3D[] CreateVertexList(Vector3D[] v)
        {
            Vector3D[] v2 = new Vector3D[8];

            v2[0] = new Vector3D(v[0].X, v[0].Y, BasePlane.GetZ(v[0].X, v[0].Y));
            v2[1] = new Vector3D(v[1].X, v[1].Y, BasePlane.GetZ(v[1].X, v[1].Y));
            v2[2] = new Vector3D(v[2].X, v[2].Y, BasePlane.GetZ(v[2].X, v[2].Y));
            v2[3] = new Vector3D(v[3].X, v[3].Y, BasePlane.GetZ(v[3].X, v[3].Y));

            v2[4] = new Vector3D(v[0].X, v[0].Y, TopPlane.GetZ(v[0].X, v[0].Y));
            v2[5] = new Vector3D(v[1].X, v[1].Y, TopPlane.GetZ(v[1].X, v[1].Y));
            v2[6] = new Vector3D(v[2].X, v[2].Y, TopPlane.GetZ(v[2].X, v[2].Y));
            v2[7] = new Vector3D(v[3].X, v[3].Y, TopPlane.GetZ(v[3].X, v[3].Y));

            return v2;
        }

        protected Quad3d[] CreateFaceList(Vector3D[] v)
        {
            Quad3d[] quadList = new Quad3d[6];

            quadList[0] = new Quad3d(v[0], v[1], v[5], v[4]);
            quadList[1] = new Quad3d(v[1], v[2], v[6], v[5]);
            quadList[2] = new Quad3d(v[2], v[3], v[7], v[6]);
            quadList[3] = new Quad3d(v[3], v[0], v[4], v[7]);
            quadList[4] = new Quad3d(v[0], v[3], v[2], v[1]);
            quadList[5] = new Quad3d(v[4], v[5], v[6], v[7]);

            return quadList;
        }
        protected void CreateFaceList(Quad3d[] quadList)
        {

            if (quadList == null)
                return;

            Geo3D = new Geom3D();

            List<Vector3D> vertexList = new List<Vector3D>();
            List<Vector3D> normalList = new List<Vector3D>();
            List<QuadInfo> quadInfoList = new List<QuadInfo>();

            int vertexCount = 0;

            foreach (Quad3d quad in quadList)
            {
                vertexList.Add(quad.P1);
                vertexList.Add(quad.P2);
                vertexList.Add(quad.P3);
                vertexList.Add(quad.P4);

                QuadInfo qi = new QuadInfo(vertexCount,
                                           vertexCount + 1,
                                           vertexCount + 2,
                                           vertexCount + 3);

                quadInfoList.Add(qi);
                vertexCount += 4;
            }

            Geo3D.QuadList = quadInfoList.ToArray();
            Geo3D.VertexList = vertexList.ToArray();
            Geo3D.NormalList = new Vector3D[vertexList.Count];

            foreach (QuadInfo qi in Geo3D.QuadList)
            {
                Vector3D normal;
                Geo3D.CalcFaceNormal(qi.ID1, qi.ID2, qi.ID3, qi.ID4, Geo3D.VertexList, out normal);

                Geo3D.NormalList[qi.ID1] = normal;
                Geo3D.NormalList[qi.ID2] = normal;
                Geo3D.NormalList[qi.ID3] = normal;
                Geo3D.NormalList[qi.ID4] = normal;
            }
        }
        private void CreateEdgeList()
        {
            if (Geo3D == null)
                return;

            if (Geo3D.QuadList == null)
                return;

            List<EdgeInfo> edgeInfoList = new List<EdgeInfo>();

            foreach (QuadInfo qi in Geo3D.QuadList)
            {
                edgeInfoList.Add(new EdgeInfo(qi.ID1, qi.ID2));
                edgeInfoList.Add(new EdgeInfo(qi.ID2, qi.ID3));
                edgeInfoList.Add(new EdgeInfo(qi.ID3, qi.ID4));
                edgeInfoList.Add(new EdgeInfo(qi.ID4, qi.ID1));
            }

            Geo3D.EdgeList = edgeInfoList.ToArray();
        }
        public void Translate(double dx, double dy, double dz)
        {
            if (BasePlane != null)
                BasePlane.Translate(dx, dy, dz);

            if (TopPlane != null)
                TopPlane.Translate(dx, dy, dz);

            if (Geo3D != null)
                Geo3D.Translate(dx, dy, dz);

            for (var i = 0; i < VertexList.Length; i++)
            {
                VertexList[i] = VertexList[i] + new Vector3D(dx, dy, dz);
            }
        }

        public void ChangeTopPlane(Vector3D[] topPlaneVertices)
        {
            var v1 = VertexListSegment;

            Plane plane = new Plane(topPlaneVertices);

            TopPlane = plane;

            VertexList[4] = topPlaneVertices[0];
            VertexList[5] = topPlaneVertices[1];
            VertexList[6] = topPlaneVertices[2];
            VertexList[7] = topPlaneVertices[3];

            RefreshGeomtry3D();
        }
        public void RefreshGeomtry3D()
        {
            Quad3d[] quadList = CreateFaceList(VertexList);

            CreateFaceList(quadList);
            CreateEdgeList();
        }

        public override double GetLoadDistanceFromPosition(Point2D pos)
        {
            throw new NotImplementedException();
        }
    }
}
