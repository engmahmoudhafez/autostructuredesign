﻿using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elibre.StructureDesign
{
    public class WallPlate : FramingObject
    {
        public double Width { get; set; }
        public double Depth { get; set; }
        public bool IsTopPlate { get; set; }
        public StraightWall ParentWall { get; set; }
        public List<Point3D> Profile { get; set; }
        public List<WallPlateSegment> SegmentList { get; set; }

        public WallPlate(List<Point3D> profile, double width, double depth, bool top, StraightWall parentwall)
        {

            if (profile == null)
                return;

            if (profile != null)
                Profile = new List<Point3D>(profile);

            Width = width;
            Depth = depth;
            IsTopPlate = top;
            ParentWall = parentwall;

            CreateGeometry3D();

        }
        private Point3D[] CreateSegmentVertexList(Point3D startPoint, Point3D endPoint)
        {

            double w = 0.5 * Width;
            var u = MathGeoUtility.GetLeftNormal(new Vector2D(startPoint.X, startPoint.Y), new Vector2D(endPoint.X, endPoint.Y));

            var p1 = new Point3D(startPoint.X + u.X * w,
                                     startPoint.Y + u.Y * w,
                                     startPoint.Z);

            var p2 = new Point3D(endPoint.X + u.X * w,
                                     endPoint.Y + u.Y * w,
                                     endPoint.Z);

            var p3 = new Point3D(startPoint.X - u.X * w,
                                     startPoint.Y - u.Y * w,
                                     startPoint.Z);

            var p4 = new Point3D(endPoint.X - u.X * w,
                                     endPoint.Y - u.Y * w,
                                     endPoint.Z);

            Point3D[] v1 = { p1, p3, p4, p2 };
            return v1;

        }
        private void CreateGeometry3D()
        {
            int n = Profile.Count;

            for (int i = 0; i < n - 1; i++)
            {

                WallPlateSegment segment = new WallPlateSegment(CreateSegmentVertexList(Profile[i], Profile[i + 1]), IsTopPlate, this);

                if (SegmentList == null)
                    SegmentList = new List<WallPlateSegment>();

                SegmentList.Add(segment);
            }

            //if (SegmentList == null)
            //    return;

            ////
            //if (!_trim)
            //    return;
            //TrimSegments();
        }
        public void TranslateSegment(double dx, double dy, double dz, int index)
        {
            if (SegmentList != null && index < SegmentList.Count)
            {
                if (SegmentList[index] != null)
                {
                    SegmentList[index].Translate(dx, dy, dz);
                }
            }
        }
        public void Translate(double dx, double dy, double dz)
        {
            if (Profile != null)
            {
                int n = Profile.Count;

                for (int i = 0; i < n; i++)
                {
                    Profile[i] = Profile[i] + new Vector3D(dx, dy, dz);
                }
            }

            if (SegmentList != null)
            {
                foreach (WallPlateSegment segment in SegmentList)
                {
                    if (segment != null)
                        segment.Translate(dx, dy, dz);
                }
            }
        }

        public override double GetLoadDistanceFromPosition(Point2D pos)
        {
            throw new NotImplementedException();
        }
    }
}
