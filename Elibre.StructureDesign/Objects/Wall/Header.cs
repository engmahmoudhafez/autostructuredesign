﻿using Elibre.Geometry2D;
using System;
using System.Collections.Generic;

namespace Elibre.StructureDesign
{
    public class Header : FramingObject
    {
        public Point3D StartPoint { get; set; }
        public Point3D EndPoint { get; set; }

        public Section Section { get; set; }
        public WallOpening Parent { get; set; }
        public Geom3D Geo3D { get; set; }


        public Header(double z, WallOpening parent) : this(parent.StartPoint, parent.EndPoint, z, parent)
        {

        }
        public Header(Point2D startPoint, Point2D endPoint, double z, WallOpening parent)
        {
            StartPoint = new Point3D(startPoint, z);
            EndPoint = new Point3D(endPoint, z);
            Section = parent.Section;
        }



        public void CreateGeometry3D()
        {
            double w = 0.5 * Section.Depth;
            var d = Section.Width;

            Vector2D u = MathGeoUtility.GetLeftNormal(new Vector2D(StartPoint.X, StartPoint.Y), new Vector2D(EndPoint.X, EndPoint.Y));

            Vector3D p1 = new Vector3D(StartPoint.X + u.X * w,
                StartPoint.Y + u.Y * w,
                StartPoint.Z);

            Vector3D p2 = new Vector3D(EndPoint.X + u.X * w,
                EndPoint.Y + u.Y * w,
                EndPoint.Z);

            Vector3D p3 = new Vector3D(StartPoint.X - u.X * w,
                StartPoint.Y - u.Y * w,
                StartPoint.Z);

            Vector3D p4 = new Vector3D(EndPoint.X - u.X * w,
                EndPoint.Y - u.Y * w,
                EndPoint.Z);

            Vector3D[] v1 = { p1, p3, p4, p2 };

            Plane basePlane = new Plane(v1);

            Vector3D normal = new Vector3D(basePlane.A, basePlane.B, basePlane.C);

            if (basePlane.C < 0)
                normal = -1 * normal;

            normal.Normalize();

            Vector3D[] v2 = new Vector3D[4];

            v2[0] = v1[0] + normal * d;
            v2[1] = v1[1] + normal * d;
            v2[2] = v1[2] + normal * d;
            v2[3] = v1[3] + normal * d;

            Plane topPlane = new Plane(v2);

            Vector3D[] v = CreateVertexList(v1, basePlane, topPlane);
            Quad3d[] quadList = CreateFaceList(v);

            CreateFaceList(quadList);
        }

        protected Vector3D[] CreateVertexList(Vector3D[] v, Plane basePlane, Plane topPlane)
        {
            Vector3D[] v2 = new Vector3D[8];

            v2[0] = new Vector3D(v[0].X, v[0].Y, basePlane.GetZ(v[0].X, v[0].Y));
            v2[1] = new Vector3D(v[1].X, v[1].Y, basePlane.GetZ(v[1].X, v[1].Y));
            v2[2] = new Vector3D(v[2].X, v[2].Y, basePlane.GetZ(v[2].X, v[2].Y));
            v2[3] = new Vector3D(v[3].X, v[3].Y, basePlane.GetZ(v[3].X, v[3].Y));
            v2[4] = new Vector3D(v[0].X, v[0].Y, topPlane.GetZ(v[0].X, v[0].Y));
            v2[5] = new Vector3D(v[1].X, v[1].Y, topPlane.GetZ(v[1].X, v[1].Y));
            v2[6] = new Vector3D(v[2].X, v[2].Y, topPlane.GetZ(v[2].X, v[2].Y));
            v2[7] = new Vector3D(v[3].X, v[3].Y, topPlane.GetZ(v[3].X, v[3].Y));

            //_depthInZ = Math.Abs(v2[4].z - v2[0].z);

            return v2;
        }

        public Quad3d[] CreateFaceList(Vector3D[] v)
        {
            Quad3d[] quadList = new Quad3d[6];

            quadList[0] = new Quad3d(v[0], v[1], v[5], v[4]);
            quadList[1] = new Quad3d(v[1], v[2], v[6], v[5]);
            quadList[2] = new Quad3d(v[2], v[3], v[7], v[6]);
            quadList[3] = new Quad3d(v[3], v[0], v[4], v[7]);
            quadList[4] = new Quad3d(v[0], v[3], v[2], v[1]);
            quadList[5] = new Quad3d(v[4], v[5], v[6], v[7]);

            return quadList;
        }
        protected void CreateFaceList(Quad3d[] quadList)
        {

            if (quadList == null)
                return;

            Geo3D = new Geom3D();

            List<Vector3D> vertexList = new List<Vector3D>();
            List<Vector3D> normalList = new List<Vector3D>();
            List<QuadInfo> quadInfoList = new List<QuadInfo>();

            int vertexCount = 0;

            foreach (Quad3d quad in quadList)
            {
                vertexList.Add(quad.P1);
                vertexList.Add(quad.P2);
                vertexList.Add(quad.P3);
                vertexList.Add(quad.P4);

                QuadInfo qi = new QuadInfo(vertexCount,
                                           vertexCount + 1,
                                           vertexCount + 2,
                                           vertexCount + 3);

                quadInfoList.Add(qi);
                vertexCount += 4;
            }

            Geo3D.QuadList = quadInfoList.ToArray();
            Geo3D.VertexList = vertexList.ToArray();
            Geo3D.NormalList = new Vector3D[vertexList.Count];

            foreach (QuadInfo qi in Geo3D.QuadList)
            {
                Vector3D normal;
                Geo3D.CalcFaceNormal(qi.ID1, qi.ID2, qi.ID3, qi.ID4, Geo3D.VertexList, out normal);

                Geo3D.NormalList[qi.ID1] = normal;
                Geo3D.NormalList[qi.ID2] = normal;
                Geo3D.NormalList[qi.ID3] = normal;
                Geo3D.NormalList[qi.ID4] = normal;
            }
        }

        public override double GetLoadDistanceFromPosition(Point2D pos)
        {
            throw new NotImplementedException();
        }
    }
}
