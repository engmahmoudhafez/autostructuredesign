﻿using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.StructureDesign
{
    public class UpLiftPostManager
    {
        internal static void ManagePostsAtIntersections(List<StraightWall> walls)
        {
            var shearWalls = walls.Where(e => e.IsShear);
            foreach (var w in shearWalls)
            {
                //var id2 = "12d0a097-5c48-46a0-b56e-3e2ba0f5e83c";
                ////8559505e-0ed1-4b08-bfca-997f630452a9
                //var id1 = "f48843a2-1c23-4f4b-b0c1-17643673ef96";

                //if (w.Id.ToString()== id1)
                //{

                //}
                //else if(w.Id.ToString()==id2)
                //{

                //}
                var sShear = w.ConnectedWallsAtStart.Where(e => e.IsShear);
                var sCount = sShear.Count();
                var eShear = w.ConnectedWallsAtEnd.Where(e => e.IsShear);
                var eCount = eShear.Count();

                #region Manage Connection At Start


                if (sCount == 1)
                {
                    MangeCornerWalls(w, sShear.First());
                }
                //TODO: manage posts at three walls connection not implemented
                //else if (sCount == 2)
                //{
                //    MangeTripleWalls(w);
                //}
                //else if (sCount > 2)
                //{

                //}
                #endregion

                #region ManageConnectionAtEnd
                if (eCount == 1)
                {
                    MangeCornerWalls(w, eShear.First());
                }
                //else if (eCount == 2)
                //{
                //    MangeTripleWalls(w);
                //}
                //else if (sCount > 2)
                //{

                //}
                #endregion

            }
        }

        private static void MangeTripleWalls(StraightWall w)
        {
            //  throw new NotImplementedException();
        }

        private static void MangeCornerWalls(StraightWall w, StraightWall connectedWall /*connectedWall*/)
        {
            StraightWall majorWall = w;
            StraightWall minorWall = connectedWall;                        //assume
            bool isStart = false;
            bool isEnd = false;

            var v1 = w.PerimeterCorePoints;
            var v2 = connectedWall.PerimeterCorePoints;
            //MathGeoUtility.LineIntersect2D(v1[0], v1[1], v2[0], v2[1], out p0, out t1, out t2);
            //MathGeoUtility.LineIntersect2D(v1[0], v1[1], v2[2], v2[3], out p1, out t2, out t3);
            #region Check Angle Between Walls

            var angle = Vector2D.DotProduct(w.CenterLine.Direction, connectedWall.CenterLine.Direction);
            if (Math.Abs(angle) > .8)
                return;
            #endregion
            #region FirstWall

            // test start Edge of the connected wall with current Wall.
            if (MathGeoUtility.IsPointOnLineSegment(v2[0], v1[0], v1[1]) && MathGeoUtility.IsPointOnLine(v2[3], v1[0], v1[1]))
            {
                majorWall = w;
                minorWall = connectedWall;
                isStart = true;
            }
            else if (MathGeoUtility.IsPointOnLineSegment(v2[0], v1[2], v1[3]) && MathGeoUtility.IsPointOnLine(v2[3], v1[2], v1[3]))

            {
                majorWall = w;
                minorWall = connectedWall;
                isStart = true;
            }
            // test start Edge of the connected wall with current Wall.
            else if (MathGeoUtility.IsPointOnLineSegment(v2[1], v1[0], v1[1]) && MathGeoUtility.IsPointOnLine(v2[2], v1[0], v1[1]))
            {
                majorWall = w;
                minorWall = connectedWall;
                isEnd = true;
            }
            else if (MathGeoUtility.IsPointOnLineSegment(v2[1], v1[2], v1[3]) && MathGeoUtility.IsPointOnLine(v2[2], v1[2], v1[3]))

            {
                majorWall = w;
                minorWall = connectedWall;
                isEnd = true;
            }

            #endregion
            #region SecondWall
            else if (MathGeoUtility.IsPointOnLineSegment(v1[0], v2[0], v2[1]) && MathGeoUtility.IsPointOnLine(v1[3], v2[0], v2[1]))
            {
                majorWall = connectedWall;
                minorWall = w;
                isStart = true;
            }
            else if (MathGeoUtility.IsPointOnLineSegment(v1[0], v2[2], v2[3]) && MathGeoUtility.IsPointOnLine(v1[3], v2[2], v2[3]))

            {
                majorWall = connectedWall;
                minorWall = w;
                isStart = true;
            }
            // test start Edge of the connected wall with current Wall.
            else if (MathGeoUtility.IsPointOnLineSegment(v1[1], v2[0], v2[1]) && MathGeoUtility.IsPointOnLine(v1[2], v2[0], v2[1]))
            {
                majorWall = connectedWall;
                minorWall = w;
                isEnd = true;
            }
            else if (MathGeoUtility.IsPointOnLineSegment(v1[1], v2[2], v2[3]) && MathGeoUtility.IsPointOnLine(v1[2], v2[2], v2[3]))

            {
                majorWall = connectedWall;
                minorWall = w;
                isEnd = true;
            }
            else
            {
                return;
            }
            #endregion

            // get major Post
            var vv = minorWall.PerimeterCorePoints;
            //var sp = majorWall.GetStartPost();
            //var ep = majorWall.GetEndPost();
            //if (sp==null||ep ==null)
            //{
            //    return;
            //}

            if (isStart)
            {

                Post post = majorWall.GetNearstPost(vv[0]);
                minorWall.UpdatePostAtCorner(post);
            }
            else if (isEnd)
            {
                Post post = majorWall.GetNearstPost(vv[1]);

                minorWall.UpdatePostAtCorner(post);
            }


        }
    }
}
