﻿namespace Elibre.StructureDesign
{
    public interface IGeometry3D
    {
        Geom3D Geom3D { get; set; }
    }
}
