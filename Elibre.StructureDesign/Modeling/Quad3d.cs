﻿using Elibre.Geometry2D;

namespace Elibre.StructureDesign
{
    public struct Quad3d
    {
        #region Fields
        public double x1;
        public double y1;
        public double z1;
        public double x2;
        public double y2;
        public double z2;
        public double x3;
        public double y3;
        public double z3;
        public double x4;
        public double y4;
        public double z4;


        #endregion

        #region Properties
        public Vector3D P1
        {
            set
            {
                x1 = value.X;
                y1 = value.Y;
                z1 = value.Z;
            }

            get { return new Vector3D(x1, y1, z1); }
        }

        public Vector3D P2
        {
            set
            {
                x2 = value.X;
                y2 = value.Y;
                z2 = value.Z;
            }

            get { return new Vector3D(x2, y2, z2); }
        }

        public Vector3D P3
        {
            set
            {
                x3 = value.X;
                y3 = value.Y;
                z3 = value.Z;
            }

            get { return new Vector3D(x3, y3, z3); }
        }

        public Vector3D P4
        {
            set
            {
                x4 = value.X;
                y4 = value.Y;
                z4 = value.Z;
            }

            get { return new Vector3D(x4, y4, z4); }
        }

        #endregion

        #region Constructor(s)
        public Quad3d(Vector3D p1, Vector3D p2, Vector3D p3, Vector3D p4)
        {
            x1 = p1.X;
            y1 = p1.Y;
            z1 = p1.Z;

            x2 = p2.X;
            y2 = p2.Y;
            z2 = p2.Z;

            x3 = p3.X;
            y3 = p3.Y;
            z3 = p3.Z;

            x4 = p4.X;
            y4 = p4.Y;
            z4 = p4.Z;
        }

        #endregion

        public Vector3D GetNormal()
        {
            return (Vector3D.CrossProduct((P2 - P1), (P4 - P1)).GetUnit());
        }

        public Quad3d GetReversedQuad()
        {
            Quad3d q = this;
            q.P2 = P4;
            q.P3 = P3;
            q.P4 = P1;
            return q;
        }

        public void Reverse()
        {
            Vector3D tmp = P4;
            P4 = P2;
            P2 = tmp;
        }
    }
}
