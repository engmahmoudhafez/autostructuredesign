﻿using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Elibre.StructureDesign
{
    public class Plane
    {

        #region Fields

        protected double m_a = 0.0;

        protected double m_b = 0.0;

        protected double m_c = 0.0;

        protected double m_d = 0.0;

        protected List<Vector3D> _vertexList = null;

        protected bool _disposed = false;
        #endregion

        #region Properties

        public double A
        {
            set { m_a = value; }
            get { return m_a; }
        }

        public double B
        {
            set { m_b = value; }
            get { return m_b; }
        }

        public double C
        {
            set { m_c = value; }
            get { return m_c; }
        }

        public double D
        {
            set { m_d = value; }
            get { return m_d; }
        }

        public List<Vector3D> VertexList
        {
            set { _vertexList = value; }
            get { return _vertexList; }
        }

        #endregion

        #region Constructor(s)
        public Plane()
        {

        }

        public Plane(double a, double b, double c, double d)
        {
            m_a = a;
            m_b = b;
            m_c = c;
            m_d = d;
        }

        public Vector3D Normal
        {
            get
            {
                return new Vector3D(m_a, m_b, m_c);
            }
        }


        public Plane(Vector3D p1, Vector3D p2, Vector3D p3)
        {

            Vector3D v1 = p2 - p1;
            Vector3D v2 = p3 - p1;
            Vector3D n = Vector3D.CrossProduct(v1, v2).GetUnit();

            m_a = n.X;
            m_b = n.Y;
            m_c = n.Z;
            m_d = -(m_a * p1.X + m_b * p1.Y + m_c * p1.Z);

            _vertexList = new List<Vector3D>();

            _vertexList.Add(p1);
            _vertexList.Add(p2);
            _vertexList.Add(p3);

        }

        public Plane(List<Vector3D> points)
        {

            double[] plane = GetPolygonNormal(points.ToArray());

            m_a = plane[0];
            m_b = plane[1];
            m_c = plane[2];
            m_d = plane[3];

            _vertexList = new List<Vector3D>(points);

        }

        public List<Vector3D> OrderClockWise()
        {

            if (_vertexList == null)
                return new List<Vector3D>();

            var sorted = new List<Vector3D>();
            var c = new Vector3D(-100000, -100000, -100000);
            sorted.Add(_vertexList[0]);
            var inserted = false;
            int index = 0;
            foreach (var point in _vertexList.Skip(1))
            {
                inserted = false;
                foreach (var s in sorted)
                {

                    if (!inserted)
                    {

                        var a_c = s - c;
                        var b_c = point - c;
                        double x, y, z;
                        if (Vector3D.DotProduct(Normal, Vector3D.CrossProduct(a_c, b_c)) < 0)
                        {
                            //point(B) is clockwise from point(A)
                            index = sorted.IndexOf(s);
                            inserted = true;

                        }
                    }

                }
                if (!inserted)
                {
                    sorted.Add(point);

                }
                else
                {
                    sorted.Insert(index, point);

                }
            }

            return sorted;

        }

        public Plane(Vector3D[] points)
        {
            Create(points);
        }

        public Plane(Plane plane)
        {
            if (plane == null)
                return;

            m_a = plane.m_a;
            m_b = plane.m_b;
            m_c = plane.m_c;
            m_d = plane.m_d;

            if (plane._vertexList != null)
            {
                _vertexList = new List<Vector3D>(plane._vertexList);
            }

        }
        #endregion

        public virtual void Create(Vector3D[] points)
        {

            double[] plane = GetPolygonNormal(points);

            m_a = plane[0];
            m_b = plane[1];
            m_c = plane[2];
            m_d = plane[3];

            _vertexList = new List<Vector3D>(points);


        }

        public Vector3D GetZPoint(Vector2D point)
        {
            return new Vector3D(point.X, point.Y, GetZ(new Vector3D(point.X, point.Y, 0)));
        }

        public double GetZ(Vector3D point)
        {
            return GetZ(point.X, point.Y);
        }

        public double GetZ(double x, double y)
        {
            // A.X + B.Y + C.Z + D = 0

            if (m_c != 0.0)
            {
                return -(m_a * x + m_b * y + m_d) / m_c;
            }
            else
            {
                // Revise, vertical plane and z may vary (infinite number of points)
                return 0.0;
            }
        }

        public static double[] GetPolygonNormal(Vector3D[] verts)
        {
            int nVerts = verts.Length;
            double[] plane = { 0.0, 0.0, 0.0, 0.0 }; // 4 Elements
            Vector3D normal = new Vector3D(0.0, 0.0, 0.0);

            for (int i = 0; i < nVerts; i++)
            {
                Vector3D u = verts[i];
                Vector3D v = verts[(i + 1) % nVerts];

                normal.X += (u.Y - v.Y) * (u.Z + v.Z);
                normal.Y += (u.Z - v.Z) * (u.X + v.X);
                normal.Z += (u.X - v.X) * (u.Y + v.Y);
            }

            // normalize the polygon normal to obtain the first
            // three coefficients of the plane equation

            var vec = normal;
            vec.Normalize();
            plane[0] = vec.X;
            plane[1] = vec.Y;
            plane[2] = vec.Z;


            // compute the last coefficient of the plane equation
            plane[3] = -Vector3D.DotProduct(verts[0], vec);

            return plane;
        }


        public void Translate(double dx, double dy, double dz)
        {
            if (_vertexList != null)
            {
                List<Vector3D> v = new List<Vector3D>();

                foreach (Vector3D p in _vertexList)
                {
                    v.Add(new Vector3D(p.X + dx, p.Y + dy, p.Z + dz));
                }

                Create(v.ToArray());
            }
        }

        public bool IsHorizontal()
        {
            double E = MathGeoUtility.Epsilon;
            return ((Math.Abs(m_a) <= E) && (Math.Abs(m_b) <= E) && (Math.Abs(m_c) > E));
        }

        public bool IsVertical()
        {
            double E = MathGeoUtility.Epsilon;
            return (((Math.Abs(m_a) > E) || (Math.Abs(m_b) > E)) && (Math.Abs(m_c) <= E));
        }

        public bool Intersect(Vector3D p1, Vector3D p2, out Vector3D p, out double t)
        {
            double E = MathGeoUtility.Epsilon;

            double AA = -(m_a * p1.X + m_b * p1.Y + m_c * p1.Z + m_d);
            double BB = m_a * (p2.X - p1.X) + m_b * (p2.Y - p1.Y) + m_c * (p2.Z - p1.Z);

            // Initialize return values
            t = 0.0;
            p = new Vector3D(0.0, 0.0, 0.0);

            //////////////////////////////////////////
            // Divide by zero, so no intersection.
            if (Math.Abs(BB) <= E)
                return false;

            //if (BB == 0.0)
            //    return false;
            //////////////////////////////////////////

            double tt = (AA / BB);

            t = tt;
            p.X = p1.X + tt * (p2.X - p1.X);
            p.Y = p1.Y + tt * (p2.Y - p1.Y);
            p.Z = p1.Z + tt * (p2.Z - p1.Z);

            return true;
        }

    }
}
