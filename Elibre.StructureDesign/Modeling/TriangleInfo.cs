﻿namespace Elibre.StructureDesign
{
    public struct TriangleInfo
    {
        public int ID1;
        public int ID2;
        public int ID3;

        public TriangleInfo(int ID1, int ID2, int ID3)
        {
            this.ID1 = ID1;
            this.ID2 = ID2;
            this.ID3 = ID3;
        }
    }
}
