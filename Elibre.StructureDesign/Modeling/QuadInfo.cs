﻿namespace Elibre.StructureDesign
{
    public struct QuadInfo
    {
        public int ID1;
        public int ID2;
        public int ID3;
        public int ID4;

        public QuadInfo(int ID1, int ID2, int ID3, int ID4)
        {
            this.ID1 = ID1;
            this.ID2 = ID2;
            this.ID3 = ID3;
            this.ID4 = ID4;
        }
    }
}
