﻿namespace Elibre.StructureDesign
{
    public struct EdgeInfo
    {
        public int ID1;
        public int ID2;

        public EdgeInfo(int ID1, int ID2)
        {
            this.ID1 = ID1;
            this.ID2 = ID2;
        }

        public static bool isEqual(EdgeInfo e1, EdgeInfo e2)
        {
            if ((e1.ID1 == e2.ID1) && (e1.ID2 == e2.ID2))
                return true;

            if ((e1.ID1 == e2.ID2) && (e1.ID2 == e2.ID1))
                return true;

            return false;
        }
    }
}
