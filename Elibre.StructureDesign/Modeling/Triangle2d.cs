﻿using Elibre.Geometry2D;

namespace Elibre.StructureDesign
{
    public struct Triangle2d
    {
        #region Fields
        public double x1;
        public double y1;

        public double x2;
        public double y2;

        public double x3;
        public double y3;
        #endregion

        #region Properties
        public Point2D P1
        {
            set
            {
                x1 = value.X;
                y1 = value.Y;
            }

            get { return new Point2D(x1, y1); }
        }

        public Point2D P2
        {
            set
            {
                x2 = value.X;
                y2 = value.Y;
            }

            get { return new Point2D(x2, y2); }
        }

        public Point2D P3
        {
            set
            {
                x3 = value.X;
                y3 = value.Y;
            }

            get { return new Point2D(x3, y3); }
        }

        #endregion

        #region Constructor(s)

        public Triangle2d(Point2D p1, Point2D p2, Point2D p3)
        {
            x1 = p1.X;
            y1 = p1.Y;

            x2 = p2.X;
            y2 = p2.Y;

            x3 = p3.X;
            y3 = p3.Y;
        }

        public Triangle2d(Point3D p1, Point3D p2, Point3D p3)
        {
            x1 = p1.X;
            y1 = p1.Y;

            x2 = p2.X;
            y2 = p2.Y;

            x3 = p3.X;
            y3 = p3.Y;
        }

        public Triangle2d(Triangle3d t)
        {
            x1 = t.x1;
            y1 = t.y1;

            x2 = t.x2;
            y2 = t.y2;

            x3 = t.x3;
            y3 = t.y3;
        }

        public Triangle2d(Triangle2d t)
        {
            x1 = t.x1;
            y1 = t.y1;

            x2 = t.x2;
            y2 = t.y2;

            x3 = t.x3;
            y3 = t.y3;
        }

        #endregion

        public Triangle2d Translated(double dx, double dy)
        {
            Point2D p1 = new Point2D(x1 + dx, y1 + dy);
            Point2D p2 = new Point2D(x2 + dx, y2 + dy);
            Point2D p3 = new Point2D(x3 + dx, y3 + dy);

            return new Triangle2d(p1, p2, p3);
        }

    }
}
