﻿using Elibre.Geometry2D;

namespace Elibre.StructureDesign
{
    public struct Triangle3d
    {
        #region Fields
        public double x1;
        public double y1;
        public double z1;
        public double x2;
        public double y2;
        public double z2;
        public double x3;
        public double y3;
        public double z3;
        #endregion

        #region Properties
        public Vector3D P1
        {
            set
            {
                x1 = value.X;
                y1 = value.Y;
                z1 = value.Z;
            }

            get { return new Vector3D(x1, y1, z1); }
        }

        public Vector3D P2
        {
            set
            {
                x2 = value.X;
                y2 = value.Y;
                z2 = value.Z;
            }

            get { return new Vector3D(x2, y2, z2); }
        }

        public Vector3D P3
        {
            set
            {
                x3 = value.X;
                y3 = value.Y;
                z3 = value.Z;
            }

            get { return new Vector3D(x3, y3, z3); }
        }

        #endregion

        #region Constructor(s)

        public Triangle3d(Vector3D p1, Vector3D p2, Vector3D p3)
        {
            x1 = p1.X;
            y1 = p1.Y;
            z1 = p1.Z;

            x2 = p2.X;
            y2 = p2.Y;
            z2 = p2.Z;

            x3 = p3.X;
            y3 = p3.Y;
            z3 = p3.Z;
        }

        #endregion

        public Triangle3d GetReversedTriangle()
        {
            return new Triangle3d(P3, P2, P1);
        }
    }
}
