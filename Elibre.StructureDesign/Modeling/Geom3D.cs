﻿using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Elibre.StructureDesign
{
    public class Geom3D
    {
        #region Fields
        protected Vector3D[] _vertexList = null;
        protected Vector3D[] _normalList = null;
        protected EdgeInfo[] _edgeList = null;
        protected QuadInfo[] _quadList = null;
        protected TriangleInfo[] _trianglesList = null;
        #endregion

        #region Properties
        public Vector3D[] VertexList
        {
            set { _vertexList = value; }
            get { return _vertexList; }
        }

        public Vector3D[] NormalList
        {
            set { _normalList = value; }
            get { return _normalList; }
        }

        public EdgeInfo[] EdgeList
        {
            set { _edgeList = value; }
            get { return _edgeList; }
        }

        public QuadInfo[] QuadList
        {
            set { _quadList = value; }
            get { return _quadList; }
        }

        public TriangleInfo[] TrianglesList
        {
            set { _trianglesList = value; }
            get { return _trianglesList; }
        }

        #endregion

        #region Constructor(s)

        public Geom3D()
        {



        }

        #endregion

        //by hegazy
        public static Quad3d Extrude(Vector3D p1, Vector3D p2, Vector3D normal, double length)
        {
            var q = new Quad3d(p1, p2, p2 + normal * length, p1 + normal * length);
            //no need to adjust windings
            //var n = ((line.P2 - line.P1) ^ normal).GetUnitVector();
            //PolygonUtility.AdjustQuadWinding(ref q, n);
            return q;
        }
        public static Quad3d Extrude(Line3D line, Vector3D normal, double length)
        {
            var q = new Quad3d(new Vector3D(line.Start), new Vector3D(line.End), new Vector3D(line.End) + normal * length, new Vector3D(line.Start) + normal * length);
            //no need to adjust windings
            //var n = ((line.P2 - line.P1) ^ normal).GetUnitVector();
            //PolygonUtility.AdjustQuadWinding(ref q, n);
            return q;
        }
        public static void AdjustQuadWinding(ref Quad3d q, Vector3D normal)
        {
            if (Vector3D.DotProduct(q.GetNormal(), normal) < 0) q.Reverse();
        }

        //by hegazy
        public static List<Quad3d> Extrude(Quad3d q, Vector3D normal, double length)
        {
            var extrusion = new List<Quad3d>();

            //bottom face
            AdjustQuadWinding(ref q, normal * -1);
            extrusion.Add(q);

            //sides
            //q winding -ve normal
            extrusion.Add(Extrude(q.P2, q.P1, normal, length));
            extrusion.Add(Extrude(q.P3, q.P2, normal, length));
            extrusion.Add(Extrude(q.P4, q.P3, normal, length));
            extrusion.Add(Extrude(q.P1, q.P4, normal, length));

            //top face
            q.P1 += normal * length;
            q.P2 += normal * length;
            q.P3 += normal * length;
            q.P4 += normal * length;
            AdjustQuadWinding(ref q, normal);
            extrusion.Add(q);


            return extrusion;
        }
        public void CalcFaceNormals()
        {

            if (_vertexList == null)
                return;

            int n = _vertexList.Length;

            if (n <= 0)
                return;

            _normalList = new Vector3D[n];

            if (_quadList != null)
            {
                foreach (QuadInfo qi in _quadList)
                {
                    Vector3D normal;
                    CalcFaceNormal(qi.ID1, qi.ID2, qi.ID3, qi.ID4, _vertexList, out normal);

                    _normalList[qi.ID1] = normal;
                    _normalList[qi.ID2] = normal;
                    _normalList[qi.ID3] = normal;
                    _normalList[qi.ID4] = normal;

                }
            }
        }
        public void CalcFaceNormal(int ID1, int ID2, int ID3, int ID4, Vector3D[] vertexList, out Vector3D normal)
        {
            // Calculation of polygon normal using the Martin Newell method.
            int i, j;
            int[] ID = { ID1, ID2, ID3, ID4 };
            int VID, VID2;

            normal = new Vector3D();

            for (i = 0; i < 4; i++)
            {
                j = (i + 1) % 4;
                VID = ID[i];
                VID2 = ID[j];

                normal.X += (vertexList[VID].Y - vertexList[VID2].Y) * (vertexList[VID].Z + vertexList[VID2].Z);
                normal.Y += (vertexList[VID].Z - vertexList[VID2].Z) * (vertexList[VID].X + vertexList[VID2].X);
                normal.Z += (vertexList[VID].X - vertexList[VID2].X) * (vertexList[VID].Y + vertexList[VID2].Y);
            }

            Normalize(ref normal);
        }
        public void CalcTriangleNormal(int ID1, int ID2, int ID3, Vector3D[] vertexList, out Vector3D normal)
        {
            // Calculation of polygon normal using the Martin Newell method.
            int i, j;
            int[] ID = { ID1, ID2, ID3 };
            int VID, VID2;

            normal = new Vector3D();

            for (i = 0; i < 3; i++)
            {
                j = (i + 1) % 3;
                VID = ID[i];
                VID2 = ID[j];

                normal.X += (vertexList[VID].Y - vertexList[VID2].Y) * (vertexList[VID].Z + vertexList[VID2].Z);
                normal.Y += (vertexList[VID].Z - vertexList[VID2].Z) * (vertexList[VID].X + vertexList[VID2].X);
                normal.Z += (vertexList[VID].X - vertexList[VID2].X) * (vertexList[VID].Y + vertexList[VID2].Y);

            }

            Normalize(ref normal);
        }
        public Vector3D[] CalcTrianglesAndFacesNormals(int nVertex, int nTriangles, int nFaces, Vector3D[] vertexList, TriangleInfo[] trianglesList, QuadInfo[] faceList, bool bSmoothNormals)
        {
            try
            {
                Vector3D[] normalList = null;
                Vector3D normal = new Vector3D();

                if (bSmoothNormals)
                {
                    normalList = new Vector3D[nVertex];
                    int[] n = new int[nVertex];
                    n.Initialize();

                    for (int i = 0; i < nTriangles; i++)
                    {
                        int[] ID = { trianglesList[i].ID1, trianglesList[i].ID2, trianglesList[i].ID3 };
                        CalcTriangleNormal(trianglesList[i].ID1, trianglesList[i].ID2, trianglesList[i].ID3, vertexList, out normal);

                        for (int j = 0; j < 3; j++)
                        {
                            int VID = ID[j];

                            normalList[VID].X += normal.X;
                            normalList[VID].Y += normal.Y;
                            normalList[VID].Z += normal.Z;

                            n[VID]++;
                        }
                    }

                    for (int i = 0; i < nFaces; i++)
                    {
                        int[] ID = { faceList[i].ID1, faceList[i].ID2, faceList[i].ID3, faceList[i].ID4 };
                        CalcFaceNormal(faceList[i].ID1, faceList[i].ID2, faceList[i].ID3, faceList[i].ID4, vertexList, out normal);

                        for (int j = 0; j < 4; j++)
                        {
                            int VID = ID[j];

                            normalList[VID].X += normal.X;
                            normalList[VID].Y += normal.Y;
                            normalList[VID].Z += normal.Z;

                            n[VID]++;
                        }
                    }

                    for (int i = 0; i < nVertex; i++)
                    {
                        if (n[i] > 1)
                        {
                            normalList[i].X /= n[i];
                            normalList[i].Y /= n[i];
                            normalList[i].Z /= n[i];

                            Normalize(ref normalList[i]);
                        }
                    }
                }
                else
                {
                    normalList = new Vector3D[nTriangles + nFaces];

                    int k = 0;
                    for (int i = 0; i < nTriangles; i++)
                    {
                        CalcTriangleNormal(trianglesList[i].ID1, trianglesList[i].ID2, trianglesList[i].ID3, vertexList, out normal);
                        normalList[k] = normal;

                        k++;
                    }

                    for (int i = 0; i < nFaces; i++)
                    {
                        CalcFaceNormal(faceList[i].ID1, faceList[i].ID2, faceList[i].ID3, faceList[i].ID4, vertexList, out normal);
                        normalList[k] = normal;

                        k++;
                    }
                }

                return normalList;
            }
            catch
            {
                return null;
            }
        }
        public void Normalize(ref Vector3D p)
        {
            double L = Math.Sqrt(p.X * p.X + p.Y * p.Y + p.Z * p.Z);

            if (L > MathGeoUtility.Epsilon)
            {
                p.X /= L;
                p.Y /= L;
                p.Z /= L;
            }
        }
        public void CreateFaceList(List<Triangle3d> triList, List<Quad3d> quadList)
        {

            List<Vector3D> vertexList = new List<Vector3D>();
            List<TriangleInfo> triInfoList = new List<TriangleInfo>();
            List<QuadInfo> quadInfoList = new List<QuadInfo>();

            #region Create TriangleInfo List

            if (triList != null)
            {
                int c = 0;
                foreach (Triangle3d tri in triList)
                {
                    vertexList.Add(tri.P1);
                    vertexList.Add(tri.P2);
                    vertexList.Add(tri.P3);

                    TriangleInfo t = new TriangleInfo(c, c + 1, c + 2);
                    triInfoList.Add(t);

                    c += 3;
                }
            }
            #endregion

            #region Create QuadInfo List
            if (quadList != null)
            {
                int c = 0;
                int n0 = vertexList.Count;

                foreach (Quad3d quad in quadList)
                {
                    vertexList.Add(quad.P1);
                    vertexList.Add(quad.P2);
                    vertexList.Add(quad.P3);
                    vertexList.Add(quad.P4);

                    QuadInfo q = new QuadInfo(c + n0, (c + 1) + n0, (c + 2) + n0, (c + 3) + n0);
                    quadInfoList.Add(q);
                    c += 4;
                }
            }
            #endregion

            #region Calc Normals

            int nv = 0;
            int nt = 0;
            int nq = 0;
            int nf = 0;

            if (vertexList != null)
                nv = vertexList.Count;

            if (triList != null)
                nt = triList.Count;

            if (quadList != null)
                nq = quadList.Count;

            nf = nt + nq;


            VertexList = vertexList.ToArray();
            TrianglesList = triInfoList.ToArray();
            QuadList = quadInfoList.ToArray();

            Vector3D[] normals = CalcTrianglesAndFacesNormals(nv, nt, nq, VertexList, TrianglesList, QuadList, false);
            NormalList = new Vector3D[nv];

            for (int i = 0; i < nt; i++)
            {
                int id1 = TrianglesList[i].ID1;
                int id2 = TrianglesList[i].ID2;
                int id3 = TrianglesList[i].ID3;

                NormalList[id1] = normals[i];
                NormalList[id2] = normals[i];
                NormalList[id3] = normals[i];
            }

            for (int i = 0; i < nq; i++)
            {
                int id1 = QuadList[i].ID1;
                int id2 = QuadList[i].ID2;
                int id3 = QuadList[i].ID3;
                int id4 = QuadList[i].ID4;

                NormalList[id1] = normals[i + nt];
                NormalList[id2] = normals[i + nt];
                NormalList[id3] = normals[i + nt];
                NormalList[id4] = normals[i + nt];
            }

            #endregion
        }

        public void createQuadEdgeList()
        {

            if (_quadList != null && _quadList.Any())
            {
                var edgeList = new List<EdgeInfo>();
                foreach (var quad in _quadList)
                {
                    edgeList.Add(new EdgeInfo(quad.ID1, quad.ID2));
                    edgeList.Add(new EdgeInfo(quad.ID2, quad.ID3));
                    edgeList.Add(new EdgeInfo(quad.ID3, quad.ID4));
                    edgeList.Add(new EdgeInfo(quad.ID4, quad.ID1));
                }
                _edgeList = edgeList.ToArray();
            }
        }

        public void CreateEdgeList(IEnumerable<Vector3D> pts)
        {
            int count = pts.Count();
            int i0 = VertexList.Length;
            List<Vector3D> vertexList = new List<Vector3D>(count);
            List<EdgeInfo> edgeList = new List<EdgeInfo>();
            for (int i = 0; i < count; ++i)
            {
                Vector3D pti = pts.ElementAt(i);
                Vector3D ptj = pts.ElementAt((i + 1) % count);
                vertexList.Add(pti);

                var einfo = new EdgeInfo(i0 + i, i0 + (i + 1) % count);
                edgeList.Add(einfo);
            }

            if (VertexList == null)
                VertexList = vertexList.ToArray();
            else
                VertexList = VertexList.Concat(vertexList).ToArray();

            if (EdgeList == null)
                EdgeList = edgeList.ToArray();
            else
                EdgeList = EdgeList.Concat(edgeList).ToArray();
        }

        public void Translate(double dx, double dy, double dz)
        {
            if (_vertexList != null)
            {
                int n = _vertexList.Length;

                for (int i = 0; i < n; i++)
                {
                    _vertexList[i] = _vertexList[i] + new Vector3D(dx, dy, dz);
                }
            }
        }

    }
}
