﻿using Elibre.Geometry2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elibre.StructureDesign
{
    public static class MathGeoUtility
    {
        public const double Epsilon = 1E-4;
        public static Vector2D GetLeftNormal(Vector2D P1, Vector2D P2)
        {
            var d = P2 - P1;
            Vector3D V = new Vector3D(d.X, d.Y, 0);
            double L = d.GetLength();

            if (Math.Abs(L) <= Epsilon) // zero length line.
                return new Vector2D(-1.0, 0.0);

            Vector3D ZAxis = Vector3D.ZAxis;
            Vector3D n = Vector3D.CrossProduct(ZAxis, V).GetUnit();

            return new Vector2D(n.X, n.Y);
        }

        public static Vector2D GetLeftNormal(Vector2D u)
        {
            return GetLeftNormal(new Vector2D(), u);
        }

        public static void OffsetRight(Point2D P1, Point2D P2, double delta, out Point2D P3, out Point2D P4)
        {
            P3 = P1;
            P4 = P2;

            Vector3D V = new Vector3D(P2.X - P1.X, P2.Y - P1.Y, 0);
            double L = V.GetLength();

            if (Math.Abs(L) <= Epsilon) // Vertical or zero length line.
                return;

            var ZAxis = Vector3D.ZAxis;
            var n = Vector3D.CrossProduct(V, ZAxis).GetUnit();

            P3 = new Point2D(P1.X + delta * n.X, P1.Y + delta * n.Y);
            P4 = new Point2D(P2.X + delta * n.X, P2.Y + delta * n.Y);
        }

        public static void OffsetLeft(Point2D P1, Point2D P2, double delta, out Point2D P3, out Point2D P4)
        {
            P3 = P1;
            P4 = P2;

            Vector3D V = new Vector3D(P2.X - P1.X, P2.Y - P1.Y, 0);
            double L = V.GetLength();

            if (Math.Abs(L) <= Epsilon) // Vertical or zero length line.
                return;

            var ZAxis = Vector3D.ZAxis;
            var n = Vector3D.CrossProduct(ZAxis, V).GetUnit();

            P3 = new Point2D(P1.X + delta * n.X, P1.Y + delta * n.Y);
            P4 = new Point2D(P2.X + delta * n.X, P2.Y + delta * n.Y);
        }

        public static bool IsPointOnLine(Point2D P0, Point2D P1, Point2D P2)
        {
            double E = MathGeoUtility.Epsilon;
            double d = PointLineDist(P0, P1, P2);
            return d < E;
        }

        public static double PointLineDist(Point2D P0, Point2D P1, Point2D P2)
        {
            //Cross product V1 x V2 = (V1)(V2)(sin(theta))
            //D = |V1|sin(theta) = (V1 . V2) / |V2|

            var V1 = new Vector3D(P0.X - P1.X, P0.Y - P1.Y, 0);
            var V2 = new Vector3D(P2.X - P1.X, P2.Y - P1.Y, 0);
            var V3 = Vector3D.CrossProduct(V1, V2);              //overloading the 

            double L1 = V1.GetLength();
            double L2 = V2.GetLength();

            if (Math.Abs(L2) <= MathGeoUtility.Epsilon)
                return L1;

            double cp = V3.GetLength();
            return cp / L2;
        }
        public static bool IsPointOnLineSegment(Point2D P0, Point2D P1, Point2D P2, double tolerance = Epsilon)
        {
            double d = PointLineDist(P0, P1, P2);

            if (d > tolerance) // Not collinear
                return false;

            Point2D projectedPoint = PointToLineProjection(P0, P1, P2);

            var V = P2 - P1;
            var V1 = P1 - projectedPoint;
            var V2 = P2 - projectedPoint;

            double L = V.GetLength();
            double L1 = V1.GetLength();
            double L2 = V2.GetLength();

            return (Math.Abs(L - (L1 + L2)) <= tolerance);
        }
        public static Point2D PointToLineProjection(Point2D P0, Point2D P1, Point2D P2)
        {
            var V1 = P0 - P1;
            var V2 = P2 - P1;

            double L2 = V2.GetLength();

            if (L2 <= MathGeoUtility.Epsilon)
                return P1;

            double dp = Vector2D.DotProduct(V1, V2);
            double d = dp / L2;
            double t = d / L2;

            return (P1 + t * V2);
        }
        public static bool LineIntersect3D(Point3D p1, Point3D p2, Point3D p3, Point3D p4, out Point3D p0)
        {
            double x0, y0, z0, t1, t2;

            bool retVal = LineIntersect3D(p1.X, p1.Y, p1.Z, p2.X, p2.Y, p2.Z,
                                          p3.X, p3.Y, p3.Z, p4.X, p4.Y, p4.Z,
                                          out x0, out y0, out z0, out t1, out t2);

            p0 = new Point3D(x0, y0, z0);
            return retVal;
        }
        public static bool LineIntersect3D(Point3D p1, Point3D p2, Point3D p3, Point3D p4, out Point3D p0, out double t1, out double t2)
        {
            double x0, y0, z0;

            bool retVal = LineIntersect3D(p1.X, p1.Y, p1.Z, p2.X, p2.Y, p2.Z,
                                          p3.X, p3.Y, p3.Z, p4.X, p4.Y, p4.Z,
                                          out x0, out y0, out z0, out t1, out t2);

            p0 = new Point3D(x0, y0, z0);
            return retVal;
        }
        public static bool LineIntersect3D(double x1, double y1, double z1, double x2, double y2, double z2,
                                          double x3, double y3, double z3, double x4, double y4, double z4,
                                          out double x0, out double y0, out double z0, out double t1, out double t2)
        {
            #region Initialize output data
            x0 = 0.0;
            y0 = 0.0;
            z0 = 0.0;
            t1 = 0.0;
            t2 = 0.0;
            #endregion


            if (!IsCoplanar(x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, Epsilon))
                return false;

            if (IsParallel(x2 - x1, y2 - y1, z2 - z1, x4 - x3, y4 - y3, z4 - z3, Epsilon))
            {
                return false; // Need to be implemented, may be parallel but intersect at end points 
            }

            if (!IsParallel(x2 - x1, y2 - y1, 0.0, x4 - x3, y4 - y3, 0.0, Epsilon))
            {
                if (LineIntersect2D(x1, y1, x2, y2, x3, y3, x4, y4, out x0, out y0, out t1, out t2))
                {
                    z0 = z1 + t1 * (z2 - z1);
                    return true;
                }
            }

            if (!IsParallel(0.0, y2 - y1, z2 - z1, 0.0, y4 - y3, z4 - z3, Epsilon))
            {
                if (LineIntersect2D(y1, z1, y2, z2, y3, z3, y4, z4, out y0, out z0, out t1, out t2))
                {
                    x0 = x1 + t1 * (x2 - x1);
                    return true;
                }
            }

            if (!IsParallel(x2 - x1, 0.0, z2 - z1, x4 - x3, 0.0, z4 - z3, Epsilon))
            {
                if (LineIntersect2D(z1, x1, z2, x2, z3, x3, z4, x4, out z0, out x0, out t1, out t2))
                {
                    y0 = y1 + t1 * (y2 - y1);
                    return true;
                }
            }

            return false;
        }
        public static bool IsCoplanar(double xs1, double ys1, double zs1, double xe1, double ye1, double ze1, double xs2, double ys2, double zs2, double xe2, double ye2, double ze2, double tolerance)
        {
            double d;
            double[,] A = new double[3, 3] {{xe1 - xs1, ye1 - ys1, ze1 - zs1},
                                            {xs2 - xs1, ys2 - ys1, zs2 - zs1},
                                            {xe2 - xs1, ye2 - ys1, ze2 - zs1}};

            // Normalize just to keep coordinates consistent (in the same region)
            Normalize(ref A[0, 0], ref A[0, 1], ref A[0, 2]);
            Normalize(ref A[1, 0], ref A[1, 1], ref A[1, 2]);
            Normalize(ref A[2, 0], ref A[2, 1], ref A[2, 2]);

            d = Det3(A);
            return (Math.Abs(d) <= tolerance);
        }
        public static bool IsParallel(double dx1, double dy1, double dz1, double dx2, double dy2, double dz2, double tolerance)
        {
            double nx, ny, nz, area;

            Normalize(ref dx1, ref dy1, ref dz1);
            Normalize(ref dx2, ref dy2, ref dz2);

            CrossProd(dx1, dy1, dz1, dx2, dy2, dz2, out nx, out ny, out nz); // Normalize to keep consistent coordinates

            area = VectLength(nx, ny, nz); // Area between two vectors

            return (area <= tolerance);
        }
        /// <summary>
        /// Cross product of two vectors
        /// V1 = (x1, y1, z1)
        /// V2 = (x2, y2, z2)
        /// The cross product resulting vector is V3 = (x3, y3, z3)
        /// </summary>
        public static void CrossProd(double x1, double y1, double z1, double x2, double y2, double z2, out double x3, out double y3, out double z3)
        {
            x3 = y1 * z2 - z1 * y2;
            y3 = -(x1 * z2 - z1 * x2);
            z3 = x1 * y2 - y1 * x2;
        }
        /// <summary>
        /// 3D Vector length
        /// V = (x, y, z)
        /// </summary>
        public static double VectLength(double x, double y, double z)
        {
            return Math.Sqrt(x * x + y * y + z * z);
        }
        public static void Normalize(ref double vx, ref double vy, ref double vz)
        {
            double L = Math.Sqrt(vx * vx + vy * vy + vz * vz);

            if (L > Epsilon)
            {
                vx /= L;
                vy /= L;
                vz /= L;
            }
        }
        public static double Det3(double[,] A)
        {
            double det = 0.0;

            det += A[0, 0] * (A[1, 1] * A[2, 2] - A[1, 2] * A[2, 1]);
            det -= A[0, 1] * (A[1, 0] * A[2, 2] - A[1, 2] * A[2, 0]);
            det += A[0, 2] * (A[1, 0] * A[2, 1] - A[1, 1] * A[2, 0]);

            return det;
        }
        public static bool LineIntersect2D(Point2D p1, Point2D p2,
                                   Point2D p3, Point2D p4,
                                   out Point2D p0, out double t1, out double t2)
        {
            p0 = new Point2D();
            var p0X = p0.X;
            var p0Y = p0.Y;
            var r = LineIntersect2D(p1.X, p1.Y, p2.X, p2.Y,
                                   p3.X, p3.Y, p4.X, p4.Y,
                                   out p0X, out p0Y, out t1, out t2);
            p0 = new Point2D(p0X, p0Y);
            return r;
        }
        public static bool LineIntersect2D(double x1, double y1, double x2, double y2,
                                   double x3, double y3, double x4, double y4,
                                   out double x0, out double y0, out double t1, out double t2)
        {
            double Epsilon = MathGeoUtility.Epsilon;

            double A, B, C, D, E, F;
            double Delta, Delta1, Delta2;
            double x01, y01;
            double x02, y02;

            x0 = y0 = t1 = t2 = 0.0;

            A = x2 - x1;
            B = -(x4 - x3);
            C = y2 - y1;
            D = -(y4 - y3);
            E = x3 - x1;
            F = y3 - y1;

            Delta = A * D - B * C;

            if (Math.Abs(Delta) <= Epsilon)
            {
                double dx, dy;

                dx = x1 - x3;
                dy = y1 - y3;
                if (Math.Sqrt(dx * dx + dy * dy) <= Epsilon)
                {
                    x0 = x1;
                    y0 = y1;
                    t1 = 0.0;
                    t2 = 0.0;
                    return true;
                }

                dx = x1 - x4;
                dy = y1 - y4;
                if (Math.Sqrt(dx * dx + dy * dy) <= Epsilon)
                {
                    x0 = x1;
                    y0 = y1;
                    t1 = 0.0;
                    t2 = 1.0;
                    return true;
                }


                dx = x2 - x3;
                dy = y2 - y3;
                if (Math.Sqrt(dx * dx + dy * dy) <= Epsilon)
                {
                    x0 = x2;
                    y0 = y2;
                    t1 = 1.0;
                    t2 = 0.0;
                    return true;
                }

                dx = x2 - x4;
                dy = y2 - y4;
                if (Math.Sqrt(dx * dx + dy * dy) <= Epsilon)
                {
                    x0 = x2;
                    y0 = y2;
                    t1 = 1.0;
                    t2 = 1.0;
                    return true;
                }

                return false; // No solution (parallel lines).
            }


            Delta1 = E * D - B * F;
            Delta2 = A * F - E * C;

            t1 = (Delta1 / Delta);
            t2 = (Delta2 / Delta);

            x01 = x1 + t1 * (x2 - x1);
            y01 = y1 + t1 * (y2 - y1);

            x02 = x3 + t2 * (x4 - x3);
            y02 = y3 + t2 * (y4 - y3);

            x0 = 0.5 * (x01 + x02);
            y0 = 0.5 * (y01 + y02);

            if (Math.Abs(t1) <= Epsilon) t1 = 0.0;
            if (Math.Abs(1.0 - t1) <= Epsilon) t1 = 1.0;

            if (Math.Abs(t2) <= Epsilon) t2 = 0.0;
            if (Math.Abs(1.0 - t2) <= Epsilon) t2 = 1.0;

            return true;
        }
        public static bool IsPointOnLineSegment(Point3D P0, Point3D P1, Point3D P2, double tolerance)
        {
            double d = PointLineDist(P0, P1, P2);

            if (d > tolerance) // Not collinear
                return false;

            Point3D projectedPoint = PointToLineProjection(P0, P1, P2);

            Vector3D V = P2 - P1;
            Vector3D V1 = P1 - projectedPoint;
            Vector3D V2 = P2 - projectedPoint;

            double L = V.GetLength();
            double L1 = V1.GetLength();
            double L2 = V2.GetLength();

            return (Math.Abs(L - (L1 + L2)) <= tolerance);
        }
        public static double PointLineDist(Point3D P0, Point3D P1, Point3D P2)
        {
            // Cross product V1 x V2 = (V1)(V2)(sin(theta))
            // D = |V1|sin(theta) = (V1 . V2) / |V2|

            Vector3D V1 = P0 - P1;
            Vector3D V2 = P2 - P1;
            Vector3D V3 = Vector3D.CrossProduct(V1, V2);

            double L1 = V1.GetLength();
            double L2 = V2.GetLength();

            if (Math.Abs(L2) <= MathGeoUtility.Epsilon)
                return L1;

            double cp = V3.GetLength();
            return cp / L2;
        }
        public static Point3D PointToLineProjection(Point3D P0, Point3D P1, Point3D P2)
        {
            Vector3D V1 = P0 - P1;
            Vector3D V2 = P2 - P1;

            double L2 = V2.GetLength();

            if (Math.Abs(L2) <= MathGeoUtility.Epsilon)
                return P1;

            double dp = Vector3D.DotProduct(V1, V2);
            double d = dp / L2;
            double t = d / L2;

            return (P1 + t * V2);

        }
        public static Point3D GetPointRatio(Point3D p1, Point3D p2, double ratio, double segmentsCount)
        {
            return new Point3D((segmentsCount - ratio) * p1.X / segmentsCount + ratio * p2.X / segmentsCount,
                (segmentsCount - ratio) * p1.Y / segmentsCount + ratio * p2.Y / segmentsCount,
                (segmentsCount - ratio) * p1.Z / segmentsCount + ratio * p2.Z / segmentsCount);
        }
        public static Vector3D GetNormal(List<Vector3D> pts)
        {
            if (pts.Count == 2)
            {
                var normal = new Vector3D();
                var vec = (pts[1] - pts[0]).GetUnit();
                normal = Vector3D.CrossProduct(Vector3D.ZAxis, vec);
                return normal.GetUnit();
            }

            else
                return poly_normal(pts);
        }

        public static Vector3D poly_normal(IEnumerable<Vector3D> poly)
        {
            int i, count = poly.Count();
            Vector3D n = new Vector3D(0.0, 0.0, 0.0);
            for (i = 0; i < count; ++i)
            {
                var r = Vector3D.CrossProduct(poly.ElementAt(i), poly.ElementAt((i + 1) % count));
                n = new Vector3D(n.X + r.X, n.Y + r.Y, n.Z + r.Z);
            }
            n.Normalize();
            return n;
        }
        public static Point3D ConvertVector3DToPoint3D(Vector3D p)
        {
            return new Point3D(p.X, p.Z, p.Y);
        }
        public static bool IsPointLeftToLine(double x0, double y0, double x1, double y1, double x2, double y2)
        {
            double dx1, dy1, dz1;
            double dx2, dy2, dz2;
            double nx, ny, nz;

            dx1 = x2 - x1;
            dy1 = y2 - y1;
            dz1 = 0.0;

            dx2 = x0 - x1;
            dy2 = y0 - y1;
            dz2 = 0.0;

            CrossProd(dx1, dy1, dz1, dx2, dy2, dz2, out nx, out ny, out nz);
            return (nz > 0.0);
        }
        public static List<Point2D> GetConvexHull(List<Point2D> points)
        {
            if (points == null)
                return null;

            if (points.Count() <= 1)
                return points.ToList();

            int n = points.Count(), k = 0;
            List<Point2D> H = new List<Point2D>(new Point2D[2 * n]);

            points.Sort((a, b) =>
                 a.X == b.X ? a.Y.CompareTo(b.Y) : a.X.CompareTo(b.X));

            // Build lower hull
            for (int i = 0; i < n; ++i)
            {
                while (k >= 2 && Cross(H[k - 2], H[k - 1], points[i]) <= 0)
                    k--;
                H[k++] = points[i];
            }

            // Build upper hull
            for (int i = n - 2, t = k + 1; i >= 0; i--)
            {
                while (k >= t && Cross(H[k - 2], H[k - 1], points[i]) <= 0)
                    k--;
                H[k++] = points[i];
            }
            return H.Take(k - 1).ToList();
        }
        public static double Cross(Point2D O, Point2D A, Point2D B)
        {
            return (A.X - O.X) * (B.Y - O.Y) - (A.Y - O.Y) * (B.X - O.X);
        }
    }
}
