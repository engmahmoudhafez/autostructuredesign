﻿namespace Elibre.Geometry2D
{
    public static class Extensions
    {
        public static Vector2D ToVector2D(this Point2D p)
        {
            return new Vector2D(p.X, p.Y);
        }
        public static Vector3D ToVector3D(this Point2D p)
        {
            return new Vector3D(p.X, p.Y, 0);
        }
        public static Vector3D ToVector3D(this Point3D p)
        {
            return new Vector3D(p.X, p.Y, p.Z);
        }
        public static Vector2D ToVector2D(this Point3D p)
        {
            return new Vector2D(p.X, p.Y);
        }
        public static Point3D ToPoint3D(this Vector3D p)
        {
            return new Point3D(p.X, p.Y, p.Z);
        }
        public static Point2D ToPoint2D(this Vector3D p)
        {
            return new Point2D(p.X, p.Y);
        }
        public static Point2D ToPoint2D(this Vector2D p)
        {
            return new Point2D(p.X, p.Y);
        }
        public static Point3D ToPoint3D(this Vector2D p)
        {
            return new Point3D(p.X, p.Y, 0);
        }
    }

}
